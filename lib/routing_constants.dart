const String HomeRoute = '/';
const String LoginRoute = '/login';
const String RegisterRoute = '/register';

// User routes
const String SettingsRoute = 'user/settings';
const String LocationMapRoute = 'user/location-picker';
const String UserProfileRoute = 'user/profile';

// Category routes
const String CategoryListRoute = '/category/list';
const String CategoryDetailRoute = 'category/detail';

// Trader routes
const String CreateTraderRoute = '/trader/create';
const String TraderSettingsRoute = '/trader/settings';
const String TraderProfileRoute = '/trader/profile';
const String TraderImagesRoute = 'trader/images';
const String ManageMembersRoute = 'trader/members';
const String BusinessTypesRoute = 'trader/business-types';
const String TraderLocationRoute = 'trader/location';
const String OrderNotesRoute = 'trader/order-notes';

// Product routes
const String ProductListRoute = 'product/list';
const String ProductListChildRoute = 'product/child-list';

// Draft order routes
const String DraftOrderRoute = 'draft-order';

// Order routes
const String OrderDetailRoute = 'order/detail';
