import 'package:another_flushbar/flushbar.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_settings_screens/flutter_settings_screens.dart';
import 'package:get_it/get_it.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:linnet_flutter_app/routes.dart';
import 'package:linnet_flutter_app/src/authentication/authentication.dart';
import 'package:linnet_flutter_app/src/login/login.dart';
import 'package:linnet_flutter_app/src/providers/socket_provider.dart';
import 'package:linnet_flutter_app/src/screens/home_screen.dart';
import 'package:linnet_flutter_app/src/screens/splash_screen.dart';
import 'package:linnet_flutter_app/src/services/auth_api_service.dart';
import 'package:linnet_flutter_app/src/services/google_maps_services.dart';
import 'package:linnet_flutter_app/src/services/trader_api_service.dart';
import 'package:linnet_flutter_app/src/services/user_api_service.dart';
import 'package:linnet_flutter_app/src/snackbar/snackbar.dart';
import 'package:linnet_flutter_app/src/user/user.dart';
import 'package:linnet_flutter_app/src/utils/utils.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

class SimpleBlocObserver extends BlocObserver {
  @override
  void onChange(BlocBase<dynamic> cubit, Change<dynamic> change) {
    print('${cubit.runtimeType} $change');
    super.onChange(cubit, change);
  }

  @override
  void onTransition(
      Bloc<dynamic, dynamic> bloc, Transition<dynamic, dynamic> transition) {
    debugPrint('${bloc.runtimeType} $transition');
    super.onTransition(bloc, transition);
  }

  @override
  void onError(BlocBase<dynamic> cubit, Object error, StackTrace stackTrace) {
    print('${cubit.runtimeType} $error $stackTrace');
    super.onError(cubit, error, stackTrace);
  }
}

void setUpLogging() {
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((LogRecord rec) {
    print('${rec.level.name}: ${rec.time}: ${rec.message}');
  });
}

Future<dynamic> main() async {
  Bloc.observer = SimpleBlocObserver();
  setUpLogging();
  await Settings.init();

  WidgetsFlutterBinding.ensureInitialized();
  await dotenv.load();

  final getIt = GetIt.instance;
  final AuthApiService authApiService = AuthApiService.create();

  getIt.registerSingleton<AuthApiService>(authApiService);

  final SnackbarCubit snackbarCubit = SnackbarCubit();
  final UserApiService userApiService = UserApiService.create();
  final UserBloc userBloc =
      UserBloc(userApiService: userApiService, snackbarCubit: snackbarCubit);

  final AuthenticationBloc authBloc =
      AuthenticationBloc(authApiService: authApiService, userBloc: userBloc);
  getIt.registerSingleton<AuthenticationBloc>(authBloc);

  runApp(
    MultiProvider(
      providers: [
        Provider<UserApiService>(
          create: (_) => userApiService,
          dispose: (_, UserApiService service) => service.client.dispose(),
        ),
        Provider<AuthApiService>(
          create: (_) => authApiService,
          dispose: (_, AuthApiService service) => service.client.dispose(),
        ),
        BlocProvider<UserBloc>(
          create: (BuildContext context) => userBloc,
        ),
        BlocProvider<AuthenticationBloc>(
          create: (BuildContext context) => authBloc,
        ),
        BlocProvider<SnackbarCubit>(
            create: (BuildContext context) => snackbarCubit),
      ],
      child: LinnetApp(),
    ),
  );
}

class LinnetApp extends StatefulWidget {
  const LinnetApp();

  @override
  _LinnetAppState createState() => _LinnetAppState();
}

class _LinnetAppState extends State<LinnetApp> {
  Flushbar<dynamic>? _flushbar;
  late AuthenticationBloc authenticationBloc;
  late UserBloc userBloc;
  late SnackbarCubit snackbarCubit;
  late SocketProvider socketProvider;

  @override
  void initState() {
    authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
    userBloc = BlocProvider.of<UserBloc>(context);
    snackbarCubit = BlocProvider.of<SnackbarCubit>(context);
    socketProvider = SocketProvider();
    authenticationBloc.add(const AppStarted());
    userBloc.add(const FetchCurrentUser());
    super.initState();
  }

  @override
  void dispose() {
    authenticationBloc.close();
    userBloc.close();
    _flushbar?.dismiss();
    snackbarCubit.close();
    socketProvider.dispose();
    super.dispose();
  }

  // void setGoogleChopperClient() {
  //   googleClient = GoogleMapsServices.create();
  // }

  void _onFlushbarChanged(FlushbarStatus? status) {
    final SnackbarCubit snackCubit = BlocProvider.of<SnackbarCubit>(context);
    switch (status) {
      case FlushbarStatus.DISMISSED:
        snackCubit.reset();
        break;
      default:
        break;
    }
  }

  Flushbar<dynamic> _buildFlushbar(
      String? title, String? message, IconData icon, Color iconColor,
      {Color backgroundColor = const Color(0xFF303030),
      bool iconPulse = false}) {
    return Flushbar<dynamic>(
      message: message,
      title: title,
      margin: const EdgeInsets.all(8.0),
      shouldIconPulse: iconPulse,
      borderRadius: BorderRadius.circular(8.0),
      icon: Icon(icon, size: 28, color: iconColor),
      onStatusChanged: (FlushbarStatus? status) => _onFlushbarChanged(status),
      backgroundColor: backgroundColor,
    );
  }

  void _showFlushbar(BuildContext context, Flushbar<dynamic> flushbar) {
    _flushbar?.dismiss();
    _flushbar = flushbar;
    _flushbar?.show(context);

    /* If we don't dismiss it for the user, user might not dismiss it 
      and miss further errors. */
    Future<dynamic>.delayed(const Duration(milliseconds: 6000)).then((_) {
      _flushbar?.dismiss();
    });
  }

  @override
  Widget build(BuildContext context) {
    final TextTheme textTheme = Theme.of(context).textTheme;

    return MultiProvider(
      providers: <SingleChildWidget>[
        ChangeNotifierProvider<CurrentLocationProvider>(
            create: (_) => CurrentLocationProvider()),
        ChangeNotifierProvider<SocketProvider>(create: (_) => socketProvider),
        Provider<GoogleMapsServices>(
          create: (_) => GoogleMapsServices.create(),
          dispose: (_, GoogleMapsServices service) => service.client.dispose(),
        ),
        Provider<TraderApiService>(
          create: (_) => TraderApiService.create(),
          dispose: (_, TraderApiService service) => service.client.dispose(),
        ),
      ],
      child: MaterialApp(
          theme: ThemeData(
            // primaryColor: Color(0xff424242),
            // accentColor: Color(0xffffa000),
            primaryColor: const Color(0xff1f2833),
            accentColor: const Color(0xff66fcf1),
            appBarTheme: AppBarTheme(
              iconTheme: IconThemeData(color: Colors.grey[600]),
              textTheme: GoogleFonts.montserratTextTheme(textTheme).copyWith(
                headline6: GoogleFonts.openSans(
                    textStyle: textTheme.headline6,
                    color: Colors.white,
                    letterSpacing: 0.5),
                subtitle2: GoogleFonts.openSans(
                    textStyle: textTheme.subtitle2,
                    color: Colors.white,
                    fontSize: 18.0),
              ),
            ),
            // fontFamily: 'Monteserrat',
            // textTheme: TextTheme(
            //   headline:TextStyle(fontSize: 42.0,
            // fontWeight: FontWeight.bold),
            //   title: TextStyle(fontSize: 26.0,
            //  fontWeight: FontWeight.bold),
            //   body1: TextStyle(fontSize: 14.0),
            // ),

            textTheme: GoogleFonts.montserratTextTheme(
              Theme.of(context).textTheme,
            ),
            // headline6: GoogleFonts.openSans(
            //     textStyle: textTheme.headline6, letterSpacing: 0.5),
            // bodyText1: textTheme.bodyText1.copyWith(
            //   letterSpacing: 0.15,
            // ),
            // bodyText2: textTheme.bodyText2.copyWith(
            //   letterSpacing: 0.15,
            // ),
          ),
          onGenerateRoute: (RouteSettings settings) =>
              LinnetRouter.generateRoute(settings),
          home: BlocListener<SnackbarCubit, SnackbarState>(
            listener: (BuildContext context, SnackbarState state) {
              state.when(
                  initial: () => _flushbar?.dismiss(),
                  success: (String? title, String message) {
                    final Flushbar<dynamic> flushbar = _buildFlushbar(
                        title, message, Icons.check_circle, Colors.green);
                    _showFlushbar(context, flushbar);
                  },
                  error: (String? title, String message) {
                    final Flushbar<dynamic> flushbar = _buildFlushbar(
                        title, message, Icons.error_outline, Colors.white,
                        backgroundColor: const Color(0xffff5f52),
                        iconPulse: true);
                    _showFlushbar(context, flushbar);
                  });
            },
            child: const HomeScreen(),
          )),
    ); // onGenerateRoute: Router.generateRoute,
  }
}
