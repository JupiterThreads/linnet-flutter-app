import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

const bool debugMode = !kReleaseMode;

// for Android must use this for testing
// const String BACKEND_BASE_URL = 'http://10.0.2.2:8000';

enum AccountType { TRADE_ACCOUNT, PERSONAL_ACCOUNT }

enum ActionType {
  CREATE,
  EDIT,
  DELETE,
}

const Map<String, Color> OrderStatusColor = <String, Color>{
  'COMPLETED': Colors.green,
  'CANCELLED': Colors.red,
  'PLACED': Colors.blue,
  'REFUNDED': Colors.brown,
  'DECLINED': Colors.deepPurple,
  'DISPUTED': Colors.blueGrey,
};

enum TradeAccountType {
  OWNER,
  ADMIN,
  WRITE,
  READ,
}

const Map<String, Color> AccountBadgeColor = <String, Color>{
  'owner': Colors.blueGrey,
  'admin': Colors.green,
  'write': Colors.lightBlue,
  'read': Colors.amber,
};

enum SocketAction {
  UPDATE_ORDER,
}

const List<String> filterOptions = <String>['All', 'Published', 'Draft'];

enum HTTP_TYPE {
  GET,
  POST,
  PUT,
  DELETE,
  PATCH,
}

const Map<HTTP_TYPE, String> HTTP_TYPES = <HTTP_TYPE, String>{
  HTTP_TYPE.GET: 'get',
  HTTP_TYPE.POST: 'post',
  HTTP_TYPE.PUT: 'put',
  HTTP_TYPE.DELETE: 'delete',
  HTTP_TYPE.PATCH: 'patch',
};

enum FilterType {
  ALL,
  PUBLISHED,
  DRAFT,
}

const List<Map<String, dynamic>> filterOptionsMap = <Map<String, dynamic>>[
  <String, dynamic>{'name': 'All', 'value': FilterType.ALL},
  <String, dynamic>{'name': 'Published', 'value': FilterType.PUBLISHED},
  <String, dynamic>{'name': 'Draft', 'value': FilterType.DRAFT},
];

enum NotificationType {
  SUCCESS,
  ERROR,
}

int GOOGLE_MAPS_SEARCH_RADIUS = 80467;

const int MAX_API_RETRIES = 3;
int apiRetryCount = 0;
