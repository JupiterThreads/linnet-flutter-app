import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:linnet_flutter_app/src/common/widgets/loading_indicator.dart';
import 'package:linnet_flutter_app/src/search/search.dart';
import 'package:linnet_flutter_app/src/trader/trader.dart';

class TraderSearchDelegate extends SearchDelegate {
  TraderSearchDelegate(this.searchCubit)
      : super(searchFieldLabel: 'Search name');
  final SearchCubit searchCubit;

  @override
  List<Widget> buildActions(BuildContext context) {
    return <Widget>[
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      ),
    ];
  }

  @override
  ThemeData appBarTheme(BuildContext context) {
    final ThemeData theme = ThemeData();
    return theme.copyWith(
      primaryColor: Colors.white,
      primaryIconTheme: theme.primaryIconTheme.copyWith(color: Colors.grey),
      textTheme: theme.textTheme.copyWith(
        title: TextStyle(fontWeight: FontWeight.normal, fontSize: 16.0),
      ),
    );
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    query = query.trim();
    if (query.isEmpty) {
      return Container();
    }

    searchCubit.performSearch(query);
    return BlocBuilder<SearchCubit, SearchState>(
        bloc: searchCubit,
        builder: (BuildContext context, SearchState state) {
          return state.when(
              initial: () => Container(),
              loading: () => LoadingIndicator(),
              populated: (List<dynamic> items) => ListView.builder(
                itemCount: items.length,
                itemBuilder: (BuildContext context, int index) =>
                    _buildItemRow(context, items[index] as TradeAccount),
                ),
              empty: () => _buildFeedback('No traders found'),
              error: () => _buildFeedback('Error occured'));
        });
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return Container();
  }

  Widget _buildFeedback(String message) {
    return Center(
      child: Text(message),
    );
  }

  Widget _buildItemRow(BuildContext context, TradeAccount tradeAccount) {
    return ListTile(
        leading: const Icon(Icons.store),
        title: Text(tradeAccount.name),
        onTap: () {
          close(context, tradeAccount);
        });
  }
}
