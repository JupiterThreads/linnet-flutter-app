import 'package:flutter/material.dart';
import 'package:linnet_flutter_app/src/common/widgets/dummy_search_input.dart';
import 'package:linnet_flutter_app/src/trader/ui/trader_item_shimmer.dart';
import 'package:shimmer/shimmer.dart';

class TraderListLoading extends StatelessWidget {
  final ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      controller: _scrollController,
      slivers: <Widget>[
        SliverAppBar(
          expandedHeight: MediaQuery.of(context).size.height / 2,
          flexibleSpace: FlexibleSpaceBar(
            centerTitle: true,
            title: Container(
                width: MediaQuery.of(context).size.width,
                margin: const EdgeInsets.only(left: 8, right: 8),
                child: Shimmer.fromColors(
                  baseColor: Colors.grey[300]!,
                  highlightColor: Colors.grey[100]!,
                  child: const DummySearchInput(),
                )),
            background: SizedBox(
                child: Shimmer.fromColors(
                    baseColor: Colors.grey[200]!,
                    highlightColor: Colors.grey[100]!,
                    child: Container(color: Colors.white))),
          ),
        ),
        _buildShimmerTraderList(),
      ],
    );
  }

  Widget _buildShimmerTraderList() {
    return SliverPadding(
      padding: const EdgeInsets.all(8.0),
      sliver: SliverList(delegate: SliverChildBuilderDelegate(
        (_, __) {
          return TraderCardViewShimmer();
        },
        childCount: 10,
      )),
    );
  }
}

class TraderCardViewShimmer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
          side: const BorderSide(color: Colors.white70),
      ),
      child: TraderItemShimmer(),
      )
    );
  }
}
