import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:linnet_flutter_app/routing_constants.dart';
import 'package:linnet_flutter_app/src/common/models/models.dart'
    show ApiStateLoading, Paginate;
import 'package:linnet_flutter_app/src/common/widgets/widgets.dart'
    show Loading, Error, CircleAccountAvatar, DummySearchInput;
import 'package:linnet_flutter_app/src/search/cubit/search_cubit.dart';
import 'package:linnet_flutter_app/src/services/distance_matrix_api_service.dart';
import 'package:linnet_flutter_app/src/services/google_maps_services.dart';
import 'package:linnet_flutter_app/src/services/trader_api_service.dart';
import 'package:linnet_flutter_app/src/snackbar/cubit/snackbar_cubit.dart';
import 'package:linnet_flutter_app/src/trader/trader.dart';
import 'package:linnet_flutter_app/src/trader/ui/trader_list_loading.dart';
import 'package:linnet_flutter_app/src/trader/ui/trader_locations_map.dart';
import 'package:linnet_flutter_app/src/user/user.dart';
import 'package:linnet_flutter_app/src/utils/utils.dart';
import 'package:provider/provider.dart';

class TraderListScreen extends StatefulWidget {
  const TraderListScreen({Key? key}) : super(key: key);

  @override
  _TraderListScreenState createState() => _TraderListScreenState();
}

class _TraderListScreenState extends State<TraderListScreen> {
  late TraderBloc bloc;

  @override
  void dispose() {
    bloc.close();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    final TraderApiService traderService =
        Provider.of<TraderApiService>(context, listen: false);
    final GoogleMapsServices googleMapsServices =
        Provider.of<GoogleMapsServices>(context, listen: false);
    final DistanceMatrixApiService distanceMatrixApiService =
        googleMapsServices.client.getService<DistanceMatrixApiService>();

    bloc = TraderBloc(
        traderApiService: traderService,
        snackbarCubit: BlocProvider.of<SnackbarCubit>(context),
        distanceMatrixApiService: distanceMatrixApiService);
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<CurrentLocationProvider>(builder:
        (BuildContext context, CurrentLocationProvider locationProvider, _) {
      return BlocProvider<TraderBloc>(
        create: (BuildContext context) => bloc,
        child: TraderListContainer(location: locationProvider.location),
      );
    });
  }
}

class TraderListContainer extends StatefulWidget {
  const TraderListContainer({Key? key, this.location}) : super(key: key);
  final LatLng? location;

  @override
  _TraderListContainerState createState() => _TraderListContainerState();
}

class _TraderListContainerState extends State<TraderListContainer> {
  late TraderBloc _bloc;

  @override
  void initState() {
    super.initState();
    _bloc = BlocProvider.of<TraderBloc>(context);
    // TODO what if no location??
    if (widget.location != null) {
      _bloc.add(TraderList(location: widget.location!));
    }
  }

  @override
  void didUpdateWidget(TraderListContainer oldWidget) {
    super.didUpdateWidget(oldWidget);
    final LatLng? oldLocation = oldWidget.location;
    final LatLng? newLocation = widget.location;
    if (oldLocation != newLocation) {
      _bloc.add(TraderList(location: newLocation));
    }
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () => _fetchTraderList(context),
      child: BlocBuilder<TraderBloc, TraderState>(
        builder: (BuildContext context, TraderState state) {
          return state.traderListState.when(
              done: (_) => TraderListView(traders: state.traders),
              loading: (_) => TraderListLoading(),
              error: (String? message) => Center(
                    child: message != null ? Error(
                      errorMessage: message,
                      onRetryPressed: () => _fetchTraderList(context),
                    ) : null,
                  ));
        },
      ),
    );
  }

  Future<void> _fetchTraderList(BuildContext context) {
    final TraderBloc bloc = BlocProvider.of<TraderBloc>(context);
    bloc.add(TraderList(location: widget.location));
    return Future<void>.value();
  }
}

class TraderListView extends StatefulWidget {
  const TraderListView({Key? key, required this.traders}) : super(key: key);

  final List<TradeAccount> traders;

  @override
  _TraderListViewState createState() => _TraderListViewState();
}

class _TraderListViewState extends State<TraderListView> {
  late TraderBloc _bloc;
  // DraftOrderBloc _draftOrderBloc;
  final ScrollController _scrollController = ScrollController();
  List<TradeAccount> get _traderList => widget.traders;

  @override
  void initState() {
    super.initState();
    _bloc = BlocProvider.of<TraderBloc>(context);
    // _draftOrderBloc = BlocProvider.of<DraftOrderBloc>(context);
    _scrollController.addListener(_scrollListener);
  }

  @override
  void dispose() {
    _scrollController.removeListener(_scrollListener);
    super.dispose();
  }

  void _scrollListener() {
    if (_scrollController.offset >=
        _scrollController.position.maxScrollExtent) {
      if (_bloc.state.nextQuery != null &&
          _bloc.state.traderNextListState is ApiStateLoading) {
        _bloc.add(const TraderNextList());
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      controller: _scrollController,
      slivers: <Widget>[
        SliverAppBar(
          expandedHeight: MediaQuery.of(context).size.height / 2,
          flexibleSpace: FlexibleSpaceBar(
            centerTitle: true,
            title: Container(
              width: MediaQuery.of(context).size.width,
              margin: const EdgeInsets.only(left: 8, right: 8),
              child: DummySearchInput(onTap: () => _showSearchPage(context)),
            ),
            background: TraderLocationsMap(),
          ),
        ),
        _buildTraderList(),
        if (_bloc.state.traderNextListState is ApiStateLoading)
          const SliverToBoxAdapter(
            child: Loading(),
          )
      ],
    );
  }

  Widget _buildTraderList() {
    if (_traderList.isEmpty) {
      return SliverToBoxAdapter(
          child: Container(
        padding: const EdgeInsets.all(20.0),
        height: MediaQuery.of(context).size.height / 3,
        child: Center(
          child: _buildNotFoundPlaceholder(),
        ),
      ));
    }
    return SliverPadding(
      padding: const EdgeInsets.all(8.0),
      sliver: SliverList(
          delegate: SliverChildBuilderDelegate(
        (BuildContext context, int index) {
          return _buildTraderRow(context, _traderList[index]);
        },
        childCount: _bloc.state.traders.length,
      )),
    );
  }

  Widget? _getTraderDistance(TradeAccount account) {
    if (account.distanceDisplay.isNotEmpty) {
      return Text(
        account.distanceDisplay,
        style: TextStyle(
          color: Colors.black.withOpacity(0.6),
          fontSize: 12,
        ),
      );
    }
    return null;
  }

  Widget _buildAvatar(TradeAccount account) {
    if (account.avatar != null) {
      return CachedNetworkImage(
          imageUrl: account.avatar!,
          imageBuilder: (BuildContext context, ImageProvider imageProvider) {
            return CircleAccountAvatar(imageProvider: imageProvider);
          },
          placeholder: (BuildContext context, String url) =>
              const CircularProgressIndicator(),
          errorWidget: (BuildContext context, String url, dynamic error) =>
              const Icon(Icons.store));
    }
    return const Icon(Icons.store);
  }

  Widget _buildTraderRow(BuildContext context, TradeAccount account) {
    return TraderCardView(
      leading: _buildAvatar(account),
      title: Text(account.name),
      trailing: _getTraderDistance(account),
      subtitle: Text(account.getBusinessTypesDisplay()),
      onTap: () =>
          Navigator.pushNamed(context, CategoryListRoute, arguments: account),
    );
  }

  Widget _buildNotFoundPlaceholder() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text('No traders found near your location.',
            style: Theme.of(context).textTheme.subtitle1),
        const SizedBox(height: 8),
        Text('''Some traders may not have registered their location.''',
            style: Theme.of(context).textTheme.caption,
            textAlign: TextAlign.center),
        Text(
            '''You can search for traders by entering their name in the above search bar.''',
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.caption),
      ],
    );
  }

  Future<void> _showSearchPage(BuildContext context) async {
    final SearchCubit searchCubit =
        SearchCubit(callback: (String query) => _searchTraders(query, context));
    final dynamic account = await showSearch(
      context: context,
      delegate: TraderSearchDelegate(searchCubit),
    );

    if (account != null) {
      Navigator.pushNamed(context, CategoryListRoute, arguments: account);
    }
  }

  Future<List<TradeAccount>> _searchTraders(
      String query, BuildContext context) async {
    final Map<String, String> queryMap = <String, String>{
      'name': query,
    };
    final TraderApiService traderApiService =
        Provider.of<TraderApiService>(context, listen: false);
    Paginate<TradeAccount> paginate;
    List<TradeAccount> results = <TradeAccount>[];
    try {
      final Response<Paginate<TradeAccount>> response =
          await traderApiService.getTraders(query: queryMap);
      paginate = response.body!;
      results = paginate.results;
    } catch (error) {
      debugPrint(error.toString());
    }
    return results;
  }
}
