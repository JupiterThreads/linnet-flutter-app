import 'package:flutter/material.dart';

class TraderCardView extends StatelessWidget {

  const TraderCardView({
    this.title,
    this.trailing,
    this.subtitle,
    this.onTap,
    this.isHighlighted = false,
    this.leading,
  });

  final Widget? title;
  final Widget? trailing;
  final Widget? subtitle;
  final VoidCallback? onTap;
  final bool isHighlighted;
  final Widget? leading;

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
          side: _buildBorderSide(),
        ),
      child: ListTile(
        dense: true,
        leading: leading,
        trailing: trailing,
        title: title,
        subtitle: subtitle,
        onTap: onTap,
      )
    );
  }

  BorderSide _buildBorderSide() {
    if (isHighlighted) {
      return const BorderSide(color: Colors.green, width: 1);
    }
    return const BorderSide(color: Colors.white70, width: 1);
  }
}
