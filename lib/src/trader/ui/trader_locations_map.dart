import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:linnet_flutter_app/src/common/widgets/location_preview.dart';
import 'package:linnet_flutter_app/src/trader/trader.dart';

class TraderLocationsMap extends StatefulWidget {
  @override
  _TraderLocationsMapState createState() => _TraderLocationsMapState();
}

class _TraderLocationsMapState extends State<TraderLocationsMap> {
  List<LatLng> locationMarkers = <LatLng>[];
  late TraderBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<TraderBloc>(context);
    _setLocationMarkers();
  }

  void _setLocationMarkers() {
    if (mounted) {
      setState(() {
        for (final TradeAccount trader in bloc.state.traders) {
          if (trader.location != null) {
            locationMarkers.add(trader.location!.latLng);
          }
        }
      });
    }
  }

  TradeAccount _getMarkerTrader(int index) {
    return bloc.state.traders[index];
  }

  @override
  Widget build(BuildContext context) {
    return LocationPreview(
        selectedPosition: bloc.state.userLocation,
        markerPositions: locationMarkers,
        getMarkerTrader: _getMarkerTrader,
        enableUserLocation: true,
        zoom: 13,
        enableScroll: true);
  }
}
