import 'package:chopper/chopper.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_settings_screens/flutter_settings_screens.dart';
import 'package:linnet_flutter_app/src/common/models/models.dart'
    show Paginate, ApiState;
import 'package:linnet_flutter_app/src/services/distance_matrix_api_service.dart';
import 'package:linnet_flutter_app/src/services/trader_api_service.dart';
import 'package:linnet_flutter_app/src/snackbar/cubit/snackbar_cubit.dart';
import 'package:linnet_flutter_app/src/trader/bloc/bloc.dart';
import 'package:linnet_flutter_app/src/trader/models/distance_matrix.dart';
import 'package:linnet_flutter_app/src/trader/models/trade_account.dart';
import 'package:linnet_flutter_app/src/utils/utils.dart';

class TraderBloc extends Bloc<TraderEvent, TraderState> {
  TraderBloc({
    required this.distanceMatrixApiService,
    required this.traderApiService,
    required this.snackbarCubit,
  }) : super(TraderState());

  TraderApiService traderApiService;
  SnackbarCubit snackbarCubit;
  DistanceMatrixApiService distanceMatrixApiService;

  @override
  Stream<TraderState> mapEventToState(TraderEvent event) async* {
    if (event is TraderList) {
      yield state.copyWith(
        traderListState: const ApiState.loading('Fetching traders...'),
        nextQuery: null,
        userLocation: event.location,
      );

      final Map<String, dynamic> queryMap = <String, dynamic>{};
      if (event.location != null) {
        queryMap['longitude'] = '${event.location!.longitude}';
        queryMap['latitude'] = '${event.location!.latitude}';
      }

      try {
        final Response<Paginate<TradeAccount>> response =
            await traderApiService.getTraders(query: queryMap);
        final Paginate<TradeAccount> paginate = response.body!;
        
        final List<TradeAccount> traders =
            await _getTradersWithDistances(paginate.results);

        yield state.copyWith(
          traders: traders,
          traderListState: const ApiState.done(),
          nextQuery:
              paginate.next != null ? Utils.getParams(paginate.next!) : null,
        );
      } catch (error) {
        debugPrint(error.toString());
        yield state.copyWith(
          traderListState: const ApiState.error(),
        );
      }
    }

    if (event is TraderNextList) {
      yield state.copyWith(
        traderNextListState: ApiState.loading(),
      );

      try {
        final Response<Paginate<TradeAccount>> response =
            await traderApiService.getTraders(query: state.nextQuery ?? {});

        final Paginate<TradeAccount> paginate = response.body!;

        final List<TradeAccount> traders =
            await _getTradersWithDistances(paginate.results);

        yield state.copyWith(
          traders: List<TradeAccount>.from(state.traders)..addAll(traders),
          traderNextListState: const ApiState.done(),
          nextQuery:
              paginate.next != null ? Utils.getParams(paginate.next!) : null,
        );
      } catch (error) {
        debugPrint(error.toString());
        yield state.copyWith(
          traderNextListState: ApiState.error(),
        );
      }
    }
  }

  Future<List<TradeAccount>> _getTradersWithDistances(
      List<TradeAccount> traders) async {
    List<TradeAccount> updatedTraders = <TradeAccount>[];

    try {
      final List<DistanceMatrix> distances =
          await _getDistances(traders).timeout(const Duration(seconds: 2));

      if (distances.isEmpty) {
        return traders;
      }

      traders.asMap().forEach((int index, TradeAccount _trader) {
        final TradeAccount traderWDistance =
            _trader.copyWith(distanceMatrix: distances[index]);
        updatedTraders.add(traderWDistance);
      });
      updatedTraders = TradeAccount.getSortedTradersByDistance(updatedTraders);
    } catch (error, stacktrace) {
      print(stacktrace);
      debugPrint(error.toString());
    }
    return updatedTraders;
  }

  Future<List<DistanceMatrix>> _getDistances(List<TradeAccount> traders) async {
    final String destinations = _getFlatDestinationList(traders);
    final String origins = Utils.locationQueryStr(state.userLocation);
    final String units = Settings.getValue<String>('dm_units', 'imperial');
    final String mode = Settings.getValue<String>('dm_travel_mode', 'driving');

    List<DistanceMatrix> results = <DistanceMatrix>[];

    try {
      final Response<List<DistanceMatrix>> response =
          await distanceMatrixApiService.getDistances(origins, destinations,
              units: units, mode: mode);
      results = response.body!;
    } catch (error) {
      debugPrint(error.toString());
      snackbarCubit.showError('Unable to fetch distances.');
    }
    return results;
  }

  String _getFlatDestinationList(List<TradeAccount> traders) {
    return traders
        .map((TradeAccount trader) =>
            trader.placeId ?? Utils.locationQueryStr(trader.location?.latLng))
        .toList()
        .join('|');
  }

  List<TradeAccount> filterTradersFromSearchQuery(String query) {
    final RegExp regExp = RegExp(query, caseSensitive: false);
    return query.isNotEmpty
        ? state.traders
            .where(
                (TradeAccount item) => regExp.matchAsPrefix(item.name) != null)
            .toList()
        : state.traders;
  }
}
