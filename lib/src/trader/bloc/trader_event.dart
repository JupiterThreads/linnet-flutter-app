import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

part 'trader_event.freezed.dart';

@freezed
class TraderEvent with _$TraderEvent {
  const factory TraderEvent.fetchTraders({
    @Default(null) LatLng? location,
  }) = TraderList;
  const factory TraderEvent.fetchNextTraders() = TraderNextList;
}
