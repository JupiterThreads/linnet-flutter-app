import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:linnet_flutter_app/src/common/models/api_state.dart';
import 'package:linnet_flutter_app/src/trader/models/trade_account.dart';

part 'trader_state.freezed.dart';

@freezed
class TraderState with _$TraderState {
  factory TraderState({
    @Default(<TradeAccount>[]) List<TradeAccount> traders,
    @Default(ApiState.loading()) ApiState traderListState,
    @Default(null) ApiState? traderNextListState,
    @Default(ApiState.loading('Accessing your location...')) 
      ApiState locationStatus,
    @Default(null) Map<String, dynamic>? nextQuery,
    @Default(null) LatLng? userLocation,
  }) = _TraderState;
}



