import 'package:freezed_annotation/freezed_annotation.dart';

part 'map_duration.g.dart';
part 'map_duration.freezed.dart';

@freezed
class MapDuration with _$MapDuration {
  const factory MapDuration(String text, int value) = _MapDuration;

  factory MapDuration.fromJson(Map<String, dynamic> json) =>
      _$MapDurationFromJson(json);
}
