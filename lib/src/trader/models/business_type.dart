import 'package:freezed_annotation/freezed_annotation.dart';

part 'business_type.freezed.dart';
part 'business_type.g.dart';

@freezed
abstract class BusinessType implements _$BusinessType {

const factory BusinessType(
    String name,
    {@Default(null) String? id,
  }) = _BusinessType;

  const BusinessType._();
  
  factory BusinessType.fromJson(Map<String, dynamic> json) =>
      _$BusinessTypeFromJson(json);

  static List<dynamic> businessTypesToJson(List<BusinessType> types) =>
      types.map((BusinessType type) => type.toJson()).toList();

  static List<BusinessType> businessTypesFromJson(List<dynamic> jsonList) =>
      jsonList
          .map((dynamic json) =>
              BusinessType.fromJson(json as Map<String, dynamic>))
          .toList();

  static List<String> getBusinessTypeNames(List<BusinessType> types) =>
      types.map((BusinessType type) => type.name).toList();

  static List<BusinessType> createTypesFromList(List<String> types) =>
      types.map((String name) => BusinessType(name)).toList();
}
