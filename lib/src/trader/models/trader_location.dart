import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'trader_location.freezed.dart';
part 'trader_location.g.dart';

@freezed
abstract class TraderLocation implements _$TraderLocation {
  const factory TraderLocation({
    required String type,
    required List<double> coordinates}) = _TraderLocation;
  const TraderLocation._();

  factory TraderLocation.fromJson(Map<String, dynamic> json) =>
    _$TraderLocationFromJson(json);

  double get longitude => coordinates[0];

  double get latitude => coordinates[1];

  LatLng get latLng {
    return LatLng(latitude, longitude);
  }
}