import 'package:linnet_flutter_app/src/trader/trader.dart';
import 'package:linnet_flutter_app/src/user/user.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:linnet_flutter_app/src/utils/utils.dart';
import 'business_type.dart';

part 'trade_account.g.dart';
part 'trade_account.freezed.dart';

@freezed
abstract class TradeAccount implements _$TradeAccount {
  const factory TradeAccount({
    @Default(null) int? id,
    required String name,
    required User owner,
    @JsonKey(name: 'business_types') 
      @Default(<BusinessType>[]) List<BusinessType>? businessTypes,
    @JsonKey(name: 'date_created') String? dateCreated,
    @Default(null) String? number,
    @Default(null) TraderLocation? location,
    @Default(null) String? currency,
    @JsonKey(name: 'is_visible') @Default(false) bool isVisible,
    @JsonKey(name: 'order_notes_label') @Default(null) String? orderNotesLabel,
    @JsonKey(name: 'order_notes_required') @Default(false)
        bool orderNotesRequired,
    @Default(null) String? avatar,
    @JsonKey(name: 'cover_image') @Default(null) String? coverImage,
    @JsonKey(ignore: true) DistanceMatrix? distanceMatrix,
    @JsonKey(name: 'accepted_terms')
    List<AcceptedTerms>? acceptedTerms,
    @JsonKey(name: 'terms_and_conditions')
    TermsAndConditions? termsAndConditions,
    @Default(null) String? placeId,
  }) = _TradeAccount;
  const TradeAccount._();

  @override
  String toString() {
    return 'TradeAccount(id: $id, name: $name)';
  }

  factory TradeAccount.fromJson(Map<String, dynamic> json) =>  _$TradeAccountFromJson(json);

  static TradeAccount convertFromJson(Map<String, dynamic> json) {
    return _$TradeAccountFromJson(json);
  }

  static List<TradeAccount> tradeAccountsFromJson(List<dynamic> jsonList) {
    final List<TradeAccount> traderList = jsonList
        .map((dynamic json) =>
            TradeAccount.fromJson(json as Map<String, dynamic>))
        .toList()
          ..reversed;
    return List<TradeAccount>.from(traderList.reversed);
  }

  static List<TradeAccount> sortAlphabetically(List<TradeAccount> accounts) {
    accounts.sort((TradeAccount a, TradeAccount b) => a.name.compareTo(b.name));
    return accounts;
  }

  List<String> getBusinessTypesNameList() {
    List<String> names = <String>[];
    if (businessTypes == null) return names;
    names = businessTypes!.map((BusinessType type) => type.name).toList();
    return Utils.sortAlphabetically(names);
  }

  String getBusinessTypesDisplay() {
    return getBusinessTypesNameList().join(',').replaceAll(RegExp(','), ', ');
  }
  // traders without a distance won't be shown
  static List<TradeAccount> getSortedTradersByDistance(
      List<TradeAccount> traders) {
    traders.sort((TradeAccount a, TradeAccount b) => a
        .distanceMatrix!.distance.value
        .compareTo(b.distanceMatrix!.distance.value));
    return traders;
  }

  String get distanceDisplay {
    return distanceMatrix?.distance.text ?? '';
  }
}
