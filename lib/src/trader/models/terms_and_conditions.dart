import 'package:json_annotation/json_annotation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'terms_and_conditions.freezed.dart';
part 'terms_and_conditions.g.dart';

@freezed
abstract class TermsAndConditions implements _$TermsAndConditions {

  const factory TermsAndConditions({
      required int id,
      @JsonKey(name: 'version_number') required String versionNumber,
      required String content,
      @JsonKey(name: 'date_created') 
      required String dateCreated}) = _TermsAndConditions;
  const TermsAndConditions._();

  factory TermsAndConditions.fromJson(Map<String, dynamic> json) =>
      _$TermsAndConditionsFromJson(json);
}
