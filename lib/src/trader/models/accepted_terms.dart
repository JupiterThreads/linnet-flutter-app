import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:linnet_flutter_app/src/trader/models/terms_and_conditions.dart';

part 'accepted_terms.freezed.dart';
part 'accepted_terms.g.dart';

@freezed
class AcceptedTerms with _$AcceptedTerms {
  const factory AcceptedTerms(
        int id,
      @JsonKey(name: 'terms_and_conditions')
          TermsAndConditions termsAndConditions,
      @JsonKey(name: 'date_accepted')
          String dateAccepted) = _AcceptedTerms;

  factory AcceptedTerms.fromJson(Map<String, dynamic> json) =>
      _$AcceptedTermsFromJson(json);
}
