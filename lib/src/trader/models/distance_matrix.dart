import 'package:linnet_flutter_app/src/trader/models/models.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'distance_matrix.g.dart';
part 'distance_matrix.freezed.dart';

@freezed
abstract class DistanceMatrix implements _$DistanceMatrix {
  const factory DistanceMatrix(
    Distance distance,
    @JsonKey(name: 'duration') MapDuration mapDuration,
  ) = _DistanceMatrix;

  const DistanceMatrix._();
  factory DistanceMatrix.fromJson(Map<String, dynamic> json) =>
      _$DistanceMatrixFromJson(json);
}