import 'package:linnet_flutter_app/src/trader/models/trade_account.dart';
import 'package:linnet_flutter_app/src/user/models/user.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'member.g.dart';
part 'member.freezed.dart';

@freezed
abstract class Member implements _$Member {
  const factory Member({
    required int id,
    User? user,
    required String permission,
    @JsonKey(name: 'trade_account') TradeAccount? tradeAccount,
  }) = _Member;
  const Member._();

  factory Member.fromJson(Map<String, dynamic> json) => _$MemberFromJson(json);

  @override
  String toString() {
    return 'Member(id: $id, user: $user, tradeAccount: $tradeAccount)';
  }

  static List<Member> membersFromJson(List<dynamic> jsonList) => jsonList
      .map((dynamic json) => Member.fromJson(json as Map<String, dynamic>))
      .toList();

  static const List<String> Permissions = <String>[
    'read',
    'write',
    'admin',
  ];

  static String getPermissionDescription(String perm) {
    final Map<String, String> descriptions = <String, String>{
      'read': 'Allows a member to view all orders, products and categories.',
      'write':
          'Allows a member to create, amend and delete products and categories, and update orders.',
      'admin': 'Allows a member to amend location, and add and remove members.'
    };

    if (descriptions.containsKey(perm)) {
      return descriptions[perm]!;
    }
    return '';
  }

  String? get avatar {
    return user?.profile?.avatar;
  }

  String? get name {
    return user?.name;
  }
}
