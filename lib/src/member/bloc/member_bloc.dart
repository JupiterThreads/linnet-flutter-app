import 'dart:async';

import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:linnet_flutter_app/src/common/models/models.dart';
import 'package:linnet_flutter_app/src/member/bloc/bloc.dart';
import 'package:linnet_flutter_app/src/member/models/member.dart';
import 'package:linnet_flutter_app/src/services/trader_api_service.dart';
import 'package:linnet_flutter_app/src/snackbar/cubit/snackbar_cubit.dart';
import 'package:linnet_flutter_app/src/trader/models/trade_account.dart';
import 'package:linnet_flutter_app/src/user/models/user.dart';
import 'package:linnet_flutter_app/src/utils/utils.dart';
import 'package:collection/collection.dart';

class MemberBloc extends Bloc<MemberEvent, MemberState> {
  MemberBloc({
    required this.traderApiService,
    required this.snackbarCubit,
    required this.tradeAccount,
  }) : super(MemberState(tradeAccount: tradeAccount));

  TraderApiService traderApiService;
  SnackbarCubit snackbarCubit;
  TradeAccount tradeAccount;

  @override
  Stream<MemberState> mapEventToState(
    MemberEvent event,
  ) async* {
    if (event is MemberList) {
      yield state.copyWith(
          memberListState: const ApiState.loading('Fetching members'));
      try {
        final Response<List<Member>> response =
            await traderApiService.getMembers(tradeAccount.id!);
        final List<Member> members = response.body!;
        yield state.copyWith(
          members: members,
          memberListState: const ApiState.done(),
        );
      } catch (error) {
        debugPrint(error.toString());
        yield state.copyWith(
          memberListState: const ApiState.error('Something went wrong'),
        );
      }
    }

    if (event is CreateMember) {
      yield state.copyWith(
        processMemberState: const ApiState.loading('Adding member'),
      );

      try {
        final Response<Member> response =
            await traderApiService.addMember(tradeAccount.id!,
                getUserData(event.user, event.permissionName));
        final Member member = response.body!;

        add(AddMember(member: member));
        yield state.copyWith(
          processMemberState: const ApiState.done(),
        );
      } catch (error) {
        yield state.copyWith(
          processMemberState: const ApiState.error(),
        );
      }
    }

    if (event is UpdateMember) {
      yield state.copyWith(
        processMemberState: const ApiState.loading('Updating member'),
      );
      // this is adding a member when we want to update????
      try {
        final Response<Member> response =
            await traderApiService.updateMember(tradeAccount.id!,
                getUserData(event.member.user!, event.permissionName));
        final Member member = response.body!;

        add(ReplaceMember(member: member));
        yield state.copyWith(
          processMemberState: const ApiState.done(),
        );
      } catch (error) {
        debugPrint(error.toString());
        yield state.copyWith(
          processMemberState: const ApiState.done(),
        );
      }
    }

    if (event is DeleteMember) {
      yield state.copyWith(
        processMemberState: const ApiState.loading('Removing member'),
      );

      try {
        await traderApiService.removeMember(tradeAccount.id!, event.member.id);
        add(RemoveMember(memberId: event.member.id));
        yield state.copyWith(
          processMemberState: const ApiState.done(),
        );
      } catch (error) {
        debugPrint(error.toString());
        yield state.copyWith(
          processMemberState: const ApiState.error('Failed to remove member'),
        );
      }
    }

    if (event is AddMember) {
      final Member? foundMember = state.members
          .firstWhereOrNull((Member member) => member.id == event.member.id);

      if (foundMember != null) return;

      yield state.copyWith(
        members: List<Member>.of(state.members)..add(event.member),
      );
    }

    if (event is RemoveMember) {
      yield state.copyWith(
          members: List<Member>.from(state.members)
            ..removeWhere((Member member) => member.id == event.memberId));
    }

    if (event is ReplaceMember) {
      print('replacing member');
      yield state.copyWith(
        members: List<Member>.from(state.members)
            .map((Member _member) =>
                _member.id == event.member.id ? event.member : _member)
            .toList(),
      );
    }
  }

  Map<String, dynamic> getUserData(User user, String permissionName) {
    return <String, dynamic>{
      'user_id': user.id,
      'permission': permissionName,
    };
  }

  Future<List<User>> searchNewUsers(String query) async {
    final Map<String, String> queryMap = <String, String>{
      'email': query,
    };

    List<User> users = <User>[];
    try {
      final Response<List<User>> response =
          await traderApiService.searchUsers(state.tradeAccount.id!, queryMap);
      users = response.body!;
    } catch (error) {
      debugPrint(error.toString());
      snackbarCubit.showError('Something went wrong searching for users.');
    }
    return users;
  }

  //TODO should be going to backend for searching members
  Future<List<Member>> searchMembers(String query) {
    List<Member> members = <Member>[];
    final RegExp regExp = RegExp(query, caseSensitive: false);
    members = state.members
        .where(
            (Member member) => regExp.matchAsPrefix(member.user!.name!) != null)
        .toList();
    return Future<List<Member>>.value(members);
  }

  // List<Member> getMemberListForUser(User user) {
  //   if (user.id == tradeAccount.owner.id) {
  //     return state.members;
  //   }
  //   final String permission =
  //       user.getPermissionFromAccount(tradeAccount)!;
  //   return state.members
  //       .where((Member member) => member.permission.level < permission.level)
  //       .toList();
  // }
}
