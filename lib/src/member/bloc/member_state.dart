import 'package:linnet_flutter_app/src/common/models/api_state.dart';
import 'package:linnet_flutter_app/src/member/models/member.dart';
import 'package:linnet_flutter_app/src/trader/models/trade_account.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'member_state.freezed.dart';

@freezed
class MemberState with _$MemberState {
  factory MemberState({
    @Default(<Member>[]) List<Member> members,
    required TradeAccount tradeAccount,
    @Default(ApiState.loading()) ApiState memberListState,
    ApiState? processMemberState,
  }) = _MemberState;
}
