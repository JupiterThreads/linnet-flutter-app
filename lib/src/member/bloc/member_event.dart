import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/material.dart';
import 'package:linnet_flutter_app/src/member/models/member.dart';
import 'package:linnet_flutter_app/src/trader/models/trade_account.dart';
import 'package:linnet_flutter_app/src/user/models/user.dart';

part 'member_event.freezed.dart';

@freezed
class MemberEvent with _$MemberEvent {
  const factory MemberEvent.fetchMembers() = MemberList;

  const factory MemberEvent.createMember({
    required User user,
    required String permissionName,
  }) = CreateMember;

  const factory MemberEvent.updateMember({
    required Member member,
    required String permissionName,
  }) = UpdateMember;

  const factory MemberEvent.deleteMember({
    required Member member,
  }) = DeleteMember;

  const factory MemberEvent.addMember({
    required Member member,
  }) = AddMember;

  const factory MemberEvent.removeMember({
    required int memberId,
  }) = RemoveMember;

  const factory MemberEvent.replaceMember({
    required Member member,
  }) = ReplaceMember;
}
