import 'package:flutter/material.dart';
import 'package:linnet_flutter_app/src/member/models/member.dart';

class RemoveMemberDialog extends StatefulWidget {
  const RemoveMemberDialog({Key? key, required this.member}) : super(key: key);
  final Member member;

  @override
  _RemoveMemberDialogState createState() => _RemoveMemberDialogState();
}

class _RemoveMemberDialogState extends State<RemoveMemberDialog> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text('Remove Member?'),
      content: RichText(
        text: TextSpan(
          style: const TextStyle(
            fontSize: 14.0,
            color: Colors.black,
          ),
          children: <TextSpan>[
            const TextSpan(text: 'Are you sure you want to remove '),
            TextSpan(
              text: '${widget.member.user!.name}?',
              style: const TextStyle(fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
      actions: <Widget>[
        TextButton(
          onPressed: () => Navigator.pop(context, false),
          child: Text('CANCEL',
              style: TextStyle(color: Theme.of(context).primaryColor)),
        ),
        TextButton(
          onPressed: () => Navigator.pop(context, true),
          child: const Text('REMOVE'),
        ),
      ],
    );
  }
}
