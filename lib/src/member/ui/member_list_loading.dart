import 'package:flutter/material.dart';
import 'package:linnet_flutter_app/src/member/ui/member_item_shimmer.dart';

class MemberListLoading extends StatelessWidget {
  const MemberListLoading({this.itemCount = 20});
  final int itemCount;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: EdgeInsets.zero,
      itemBuilder: (BuildContext context, int index) => _buildRow(),
      itemCount: itemCount,
    );
  }

  Widget _buildRow() {
    return MemberItemShimmer();
  }
}
