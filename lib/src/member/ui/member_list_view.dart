import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:linnet_flutter_app/src/common/widgets/circle_account_avatar.dart';
import 'package:linnet_flutter_app/src/common/widgets/user_account_badge.dart';
import 'package:linnet_flutter_app/src/member/bloc/bloc.dart';
import 'package:linnet_flutter_app/src/trader_management/trader_management.dart';
import 'package:linnet_flutter_app/src/member/ui/remove_member_dialog.dart';
import 'package:linnet_flutter_app/src/member/models/member.dart';

class MemberListView extends StatefulWidget {
  const MemberListView({Key? key, required this.members}) : super(key: key);
  final List<Member> members;

  @override
  _MemberListViewState createState() => _MemberListViewState();
}

class _MemberListViewState extends State<MemberListView> {
  final SlidableController slidableController = SlidableController();
  List<Member> get _members => widget.members;
  late MemberBloc _memberBloc;

  @override
  void initState() {
    super.initState();
    _memberBloc = BlocProvider.of<MemberBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: EdgeInsets.zero,
      itemBuilder: (BuildContext context, int index) =>
          _buildMemberRow(_members[index]),
      itemCount: _members.length,
    );
  }

  Widget _buildMemberRow(Member member) {
    return Slidable(
      key: Key(member.id.toString()),
      controller: slidableController,
      actionPane: const SlidableDrawerActionPane(),
      secondaryActions: <Widget>[
        IconSlideAction(
          caption: 'Remove',
          color: Colors.red,
          icon: Icons.delete,
          onTap: () async {
            final bool remove = await _showRemoveMemberDialog(context, member);
            if (remove) {
              _memberBloc.add(DeleteMember(member: member));
            }
          },
        ),
      ],
      child: ListTile(
        dense: true,
        key: Key(member.id.toString()),
        leading: CircleAccountAvatar(account: member),
        title: Text(member.name!,
            style: const TextStyle(fontWeight: FontWeight.bold)),
        subtitle: Text(member.user!.email!),
        trailing: UserAccountBadge(accountType: member.permission),
        onTap: () async {
          final String? permissionName =
              await _showPermissionsDialog(context, member);
          if (permissionName != null) {
            _memberBloc.add(
                UpdateMember(member: member, permissionName: permissionName));
          }
        },
      ),
    );
  }

  Future<bool> _showRemoveMemberDialog(
      BuildContext context, Member member) async {
    final bool? remove = await showDialog<bool>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return RemoveMemberDialog(member: member);
        });
    return remove ?? false;
  }

  Future<String?> _showPermissionsDialog(
      BuildContext context, Member member) async {
    return showDialog<String>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return PermissionsDialog(
              defaultPermission: member.permission,
              titleText: 'Update permission for ${member.user!.name}',
              buttonText: 'UPDATE');
        });
  }
}
