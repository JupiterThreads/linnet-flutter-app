import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:linnet_flutter_app/src/common/widgets/loading_indicator.dart';
import 'package:linnet_flutter_app/src/member/bloc/bloc.dart';
import 'package:linnet_flutter_app/src/member/models/member.dart';
import 'package:linnet_flutter_app/src/member/ui/member_list_view.dart';
import 'package:linnet_flutter_app/src/search/search.dart';
import 'package:collection/collection.dart';

class SearchMembersDelegate extends SearchDelegate<dynamic> {
  SearchMembersDelegate({
    required this.searchCubit,
    required this.memberBloc,
    String hintText = 'Search...',
  }) : super(
          searchFieldLabel: hintText,
          keyboardType: TextInputType.text,
          textInputAction: TextInputAction.search,
        );

  final SearchCubit searchCubit;
  final MemberBloc memberBloc;

  @override
  ThemeData appBarTheme(BuildContext context) {
    final ThemeData theme = ThemeData();
    return theme.copyWith(
      primaryColor: Colors.white,
      primaryIconTheme: theme.primaryIconTheme.copyWith(color: Colors.grey),
      textTheme: theme.textTheme.copyWith(
        headline6: 
          const TextStyle(fontWeight: FontWeight.normal, fontSize: 16.0),
      ),
    );
  }

  @override
  List<Widget> buildActions(BuildContext context) {
    return <Widget>[
      IconButton(
        icon: const Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: const Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    query = query.trim();
    if (query.isEmpty) {
      return Container();
    }

    searchCubit.performSearch(query);
    return BlocProvider<MemberBloc>.value(
        value: memberBloc,
        child: BlocBuilder<SearchCubit, SearchState>(
            bloc: searchCubit,
            builder: (BuildContext context, SearchState state) {
              return state.when(
                initial: () => Container(),
                loading: () => LoadingIndicator(),
                populated: (List<dynamic> items) =>
                    _buildMemberListView(items as List<Member>),
                empty: () => _buildFeedback('No members found'),
                error: () => _buildFeedback('Error occurred'),
              );
            }));
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return Container();
  }

  Widget _buildFeedback(String message) {
    return Center(
      child: Text(message),
    );
  }

  Widget _buildMemberListView(List<Member> members) {
    return BlocBuilder<MemberBloc, MemberState>(
        builder: (BuildContext context, MemberState state) {
      return MemberListView(members: getUpdatedMembers(state.members, members));
    });
  }

  List<Member> getUpdatedMembers(
      List<Member> memberList, List<Member> searchedMemberList) {
    return memberList
        .where((Member member) =>
            searchedMemberList.firstWhereOrNull(
                (Member _member) => _member.id == member.id) !=
            null)
        .toList();
  }
}
