import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

import 'package:linnet_flutter_app/src/member/bloc/bloc.dart';
import 'package:linnet_flutter_app/src/member/models/member.dart';
import 'package:linnet_flutter_app/src/member/ui/member_list_loading.dart';
import 'package:linnet_flutter_app/src/member/ui/search_members_delegate.dart';
import 'package:linnet_flutter_app/src/providers/socket_provider.dart';
import 'package:linnet_flutter_app/src/search/cubit/search_cubit.dart';
import 'package:linnet_flutter_app/src/services/trader_api_service.dart';
import 'package:linnet_flutter_app/src/snackbar/cubit/snackbar_cubit.dart';
import 'package:linnet_flutter_app/src/trader/models/trade_account.dart';
import 'package:linnet_flutter_app/src/trader/trader.dart';
import 'package:linnet_flutter_app/src/trader_management/trader_management.dart';
import 'package:linnet_flutter_app/src/member/ui/member_list_view.dart';
import 'package:linnet_flutter_app/src/user/user.dart';
import 'package:linnet_flutter_app/src/websockets/event_types/membership_event_types.dart';
import 'package:provider/provider.dart';

class MembersScreen extends StatefulWidget {
  @override
  _MembersScreenState createState() => _MembersScreenState();
}

class _MembersScreenState extends State<MembersScreen> {
  late MemberBloc _memberBloc;
  late UserBloc _userBloc;
  late TradeAccount _account;
  late SocketProvider socketProvider;

  @override
  void initState() {
    super.initState();
    _userBloc = BlocProvider.of<UserBloc>(context);
    final TraderApiService traderApiService =
        Provider.of<TraderApiService>(context, listen: false);
    _account = _userBloc.state.selectedAccount as TradeAccount;
    _memberBloc = MemberBloc(
        tradeAccount: _account,
        traderApiService: traderApiService,
        snackbarCubit: BlocProvider.of<SnackbarCubit>(context));
    _memberBloc.add(const MemberList());

    socketProvider = Provider.of<SocketProvider>(context, listen: false);
    socketProvider.addListener(socketListener);
  }

  void socketListener() {
    final dynamic data = socketProvider.data;
    final String type = data['type'] as String;

    switch (type) {
      case ADD_MEMBERSHIP_ACCOUNT:
        _memberBloc.add(AddMember(
            member: Member.fromJson(data['payload'] as Map<String, dynamic>)));
        break;
      case UPDATE_MEMBERSHIP_ACCOUNT:
        _memberBloc.add(ReplaceMember(
            member: Member.fromJson(data['payload'] as Map<String, dynamic>)));
        break;
      case REMOVE_MEMBERSHIP_ACCOUNT:
        _memberBloc.add(RemoveMember(memberId: data['payload'] as int));
        break;
      default:
        return;
    }
  }

  @override
  Widget build(BuildContext context) {
    final ScrollController _scrollController =
        PrimaryScrollController.of(context) ?? ScrollController();
    return Scaffold(
        body: BlocProvider<MemberBloc>(
          create: (BuildContext context) => _memberBloc,
          child: NestedScrollView(
            controller: _scrollController,
            headerSliverBuilder:
                (BuildContext context, bool innerBoxScrolled) => <Widget>[
              SliverAppBar(
                floating: true,
                title: const Text('Members'),
                actions: <Widget>[
                  IconButton(
                    icon: const Icon(Icons.search),
                    onPressed: () => _performMemberSearch(context),
                  ),
                ],
              ),
            ],
            body: MemberListContainer(),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Theme.of(context).accentColor,
          onPressed: () => _perfromNewUserSearch(context),
          child: const Icon(Icons.add),
        ));
  }

  //TODO
  Future<void> _performMemberSearch(BuildContext context) async {
    final SearchCubit searchCubit =
        SearchCubit(callback: _memberBloc.searchMembers);
    await _showMemberSearchPage(context, searchCubit);
  }

  Future<void> _perfromNewUserSearch(BuildContext context) async {
    final SearchCubit searchCubit =
        SearchCubit(callback: _memberBloc.searchNewUsers);
    final dynamic result = await _showUserSearchPage(context, searchCubit);

    if (result !=null) {
      final User user = result as User;
      final String? permissionName = await _showPermissionDialog(context, user);
      if (permissionName != null) {
        _memberBloc
            .add(CreateMember(user: user, permissionName: permissionName));
      }
    }
  }

  //TODO only show permissions for admin or owner
  Future<String?> _showPermissionDialog(BuildContext context, User user) async {
    return showDialog<String>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return PermissionsDialog(
            titleText: 'Select permission for ${user.name}', buttonText: 'ADD');
      },
    );
  }

  Future<dynamic> _showUserSearchPage(
      BuildContext context, SearchCubit searchCubit) async {
    return showSearch(
        context: context,
        delegate: SearchUsersDelegate(
          searchCubit: searchCubit,
          hintText: 'Enter exact email address...',
        ));
  }

  Future<dynamic> _showMemberSearchPage(
      BuildContext context, SearchCubit searchCubit) async {
    return showSearch(
        context: context,
        delegate: SearchMembersDelegate(
          searchCubit: searchCubit,
          memberBloc: _memberBloc,
          hintText: 'Enter name...',
        ));
  }
}

class MemberListContainer extends StatefulWidget {
  @override
  _MemberListContainerState createState() => _MemberListContainerState();
}

class _MemberListContainerState extends State<MemberListContainer> {
  final SlidableController slidableController = SlidableController();
  late UserBloc _userBloc;
  late MemberBloc _memberBloc;

  @override
  void initState() {
    _userBloc = BlocProvider.of<UserBloc>(context);
    _memberBloc = BlocProvider.of<MemberBloc>(context);
    super.initState();
  }

  // TODO error
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MemberBloc, MemberState>(
      builder: (BuildContext context, MemberState state) {
        return state.memberListState.when(
            done: (_) => state.members.isEmpty
                ? _buildNoMembersPlaceholder()
                : MemberListView(
                    members: state.members),
            loading: (_) => const MemberListLoading(),
            error: (_) => _buildNoMembersPlaceholder());
      },
    );
  }

  Widget _buildNoMembersPlaceholder() {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('No members', style: Theme.of(context).textTheme.subtitle1),
          const SizedBox(height: 8),
          Text(
              '''Add a member to your account by clicking on the plus button.''',
              style: Theme.of(context).textTheme.caption,
              textAlign: TextAlign.center),
        ],
      ),
    );
  }
}
