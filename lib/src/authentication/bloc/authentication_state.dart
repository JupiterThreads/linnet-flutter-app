import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:linnet_flutter_app/src/user/models/user.dart';

part 'authentication_state.freezed.dart';

@freezed
class AuthenticationState with _$AuthenticationState {
  const factory AuthenticationState.unauthenticated() =
      Unauthenticated;
  const factory AuthenticationState.authenticated() = Authenticated;
}
