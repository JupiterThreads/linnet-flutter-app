import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:linnet_flutter_app/src/authentication/authentication.dart';
import 'package:linnet_flutter_app/src/services/auth_api_service.dart';
import 'package:linnet_flutter_app/src/user/blocs/user/bloc.dart';
import 'package:linnet_flutter_app/src/utils/utils.dart';
import 'package:linnet_flutter_app/src/websockets/websocket_manager.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  AuthenticationBloc({required this.authApiService, required this.userBloc})
      : super(const Unauthenticated());

  WebSocketManager socketManager = WebSocketManager();
  AuthApiService authApiService;
  FlutterSecureStorage storage = const FlutterSecureStorage();
  UserBloc userBloc;
  static String clientId = dotenv.env['CLIENT_ID']!;
  static String clientSecret = dotenv.env['CLIENT_SECRET']!;

  @override
  Stream<AuthenticationState> mapEventToState(
      AuthenticationEvent event) async* {
    if (event is LoggedOut) {
      await Utils.deleteTokens();
      yield const Unauthenticated();
      userBloc.add(const UserReset());
      socketManager.reset();
    }

    if (event is LoggedIn) {
      await Utils.persistTokens(event.tokenData);
      yield const Authenticated();
    }

    if (event is HasRefreshed) {
      yield const Authenticated();
    }

    if (event is AppStarted) {
      final String? expireDateStr = await storage.read(key: 'expire_date');

      if (expireDateStr != null) {
        final DateTime now = DateTime.now();
        final DateTime expireDate = DateTime.parse(expireDateStr);
        if (expireDate.isAfter(now)) {
          yield const Authenticated();
        } else {
          yield const Unauthenticated();
        }
      }
    }
  }
}
