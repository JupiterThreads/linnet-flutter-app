import 'package:linnet_flutter_app/src/user/models/user.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'authentication_event.freezed.dart';

@freezed
class AuthenticationEvent with _$AuthenticationEvent {
  const factory AuthenticationEvent.loggedOut() = LoggedOut;
  const factory AuthenticationEvent.loggedIn({
    required Map<String, dynamic> tokenData,
  }) = LoggedIn;
  const factory AuthenticationEvent.refresh() = RefreshToken;
  const factory AuthenticationEvent.appStarted() = AppStarted;
  const factory AuthenticationEvent.hasRefreshed() = HasRefreshed;
}
