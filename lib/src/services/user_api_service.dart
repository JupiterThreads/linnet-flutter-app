import 'package:chopper/chopper.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:linnet_flutter_app/src/services/utils/utils.dart';
import 'package:linnet_flutter_app/src/user/models/models.dart';
import 'package:http/http.dart' show MultipartFile;

part 'user_api_service.chopper.dart';

@ChopperApi()
abstract class UserApiService extends ChopperService {
 
  @Post(path: '/users')
  Future<Response<User>> register(@Body() Map<String, dynamic> data);

  @Get(path: '/users/current-user')
  Future<Response<User>> getCurrentUser();

  @Get(path: 'users/{id}')
  Future<Response<User>> getUser(@Path('id') int id);

  @Patch(path: 'users/{id}')
  Future<Response<User>> partialUpdateUser(
      @Path('id') int id, @Body() Map<String, dynamic> data);

  @Patch(path: 'user_profile/{id}')
  Future<Response<UserProfile>> partialUpdateProfile(
      @Path('id') int id, @Body() Map<String, dynamic> data);

  @Patch(path: 'user_profile/{id}')
  @Multipart()
  Future<Response<UserProfile>> uploadFile(
      @Path('id') int id, @PartFile() MultipartFile avatar);

  static UserApiService create() {
    final String backendUrl = dotenv.env['BACKEND_BASE_URL']!;

    final converter = JsonToTypeConverter({
        User: (dynamic jsonData) =>
            User.fromJson(jsonData as Map<String, dynamic>),
        UserProfile: (dynamic jsonData) =>
            UserProfile.fromJson(jsonData as Map<String, dynamic>),
      });

    final ChopperClient client = ChopperClient(
      baseUrl: backendUrl,
      interceptors: [HeaderInterceptor()],
      services: <ChopperService>[
        _$UserApiService(),
      ],
      converter: converter,
      authenticator: MyAuthenticator(),
    );
    return _$UserApiService(client);
  }
}
