import 'package:chopper/chopper.dart';

part 'geocode_api_service.chopper.dart';

@ChopperApi(baseUrl: '/geocode')
abstract class GeocodeApiService extends ChopperService {
  @Get(path: '/json')
  Future<Response<dynamic>> getAddress(@Query() String latlng);

  static GeocodeApiService create([ChopperClient? client]) {
    return _$GeocodeApiService(client);
  }
}
