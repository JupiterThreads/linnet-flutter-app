import 'package:chopper/chopper.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:linnet_flutter_app/src/product_variation/models/product_variation.dart';
import 'package:linnet_flutter_app/src/services/utils/utils.dart';

part 'product_variation_api_service.chopper.dart';

@ChopperApi(baseUrl: '/variations')
abstract class ProductVariationApiService extends ChopperService {
  @Post()
  Future<Response<ProductVariation>> createProductVariation(
      @Body() Map<String, dynamic> data);

  @Put(path: '/{id}')
  Future<Response<ProductVariation>> updateProductVariation(
      @Path('id') int id, @Body() Map<String, dynamic> data);

  @Patch(path: '/{id}')
  Future<Response<ProductVariation>> partialUpdateVariation(
      @Path('id') int id, @Body() Map<String, dynamic> data);

  @Delete(path: '/{id}')
  Future<Response<dynamic>> deleteProductVariation(@Path('id') int id);

  @Post(path: '/update-order')
  Future<Response<dynamic>> updateOrder(@Body() dynamic data);

  static ProductVariationApiService create(
      int traderId, int categoryId, int productId) {
    final String backendUrl = dotenv.env['BACKEND_BASE_URL']!;
    final String baseUrl =
        '$backendUrl/traders/$traderId/categories/$categoryId/products/$productId';

    final ChopperClient client = ChopperClient(
      baseUrl: baseUrl,
      interceptors: [HeaderInterceptor()],
      authenticator: MyAuthenticator(),
      services: <ChopperService>[_$ProductVariationApiService()],
      converter: JsonToTypeConverter({
        ProductVariation: (dynamic jsonData) =>
            ProductVariation.fromJson(jsonData as Map<String, dynamic>)
      }),
    );
    return _$ProductVariationApiService(client);
  }
}
