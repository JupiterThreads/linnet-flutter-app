import 'package:chopper/chopper.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:linnet_flutter_app/src/place/models/models.dart';
import 'package:linnet_flutter_app/src/services/distance_matrix_api_service.dart';
import 'package:linnet_flutter_app/src/services/geocode_api_service.dart';
import 'package:linnet_flutter_app/src/services/place_api_service.dart';
import 'package:linnet_flutter_app/src/services/utils/json_to_type_converter.dart';
import 'package:linnet_flutter_app/src/trader/models/distance_matrix.dart';

class GoogleMapsServices {
  ChopperClient client;

  static final ChopperClient _chopperClient = ChopperClient(
      baseUrl: 'https://maps.googleapis.com/maps/api',
      interceptors: <dynamic> [
        _addQuery
      ],
      services: <ChopperService>[
        PlaceApiService.create(),
        DistanceMatrixApiService.create(),
        GeocodeApiService.create(),
      ],
      converter: JsonToTypeConverter({
        DistanceMatrix: (dynamic jsonData) => DistanceMatrix
          .fromJson(jsonData as Map<String, dynamic>),
        Place: (dynamic jsonData) => Place.fromJson(
          jsonData as Map<String, dynamic>),
        AutoCompleteItem: (dynamic jsonData) => AutoCompleteItem.fromJson(
          jsonData as Map<String, dynamic>),
        NearbyPlace: (dynamic jsonData) => NearbyPlace.fromJson(
          jsonData as Map<String, dynamic>),
      }),
      );

  factory GoogleMapsServices.create() {
    return GoogleMapsServices._internal(_chopperClient);
  }

  GoogleMapsServices._internal(this.client);

  static Request _addQuery(Request req) {
    final Map<String, dynamic> params = 
      Map<String, dynamic>.from(req.parameters);
    params['key'] = dotenv.env['GOOGLE_API_KEY'];
    return req.copyWith(parameters: params);
  }
}

