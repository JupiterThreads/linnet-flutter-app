import 'package:chopper/chopper.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:linnet_flutter_app/src/category/models/models.dart';
import 'package:linnet_flutter_app/src/common/models/paginate.dart';
import 'package:linnet_flutter_app/src/services/utils/utils.dart';
import 'package:http/http.dart' show MultipartFile;

part 'category_api_service.chopper.dart';

@ChopperApi(baseUrl: '/categories')
abstract class CategoryApiService extends ChopperService {
  @Get()
  Future<Response<Paginate<Category>>> getCategories(
      {@QueryMap() Map<String, dynamic> query = const <String, dynamic>{}});

  @Get(path: '/{id}')
  Future<Response<Category>> getCategory(@Path('id') int id);

  @Delete(path: '/{id}')
  Future<Response<dynamic>> deleteCategory(@Path('id') int id);

  @Put(path: '/{id}')
  Future<Response<Category>> updateCategory(
      @Path('id') int id, @Body() Map<String, dynamic> data);

  @Patch(path: '/{id}')
  Future<Response<Category>> partialUpdate(
      @Path('id') int id, @Body() Map<String, dynamic> data);

  @Patch(path: '/{id}')
  @Multipart()
  Future<Response<Category>> uploadImage(
      @Path('id') int id, @PartFile() MultipartFile image);

  @Post(path: '/update-order')
  Future<Response<dynamic>> updateCategoryDisplayOrder(@Body() dynamic data);

  @Post()
  Future<Response<Category>> createCategory(@Body() Map<String, dynamic> data);

  @Post()
  @Multipart()
  Future<Response<Category>> createCategoryWithImage(
    @Part() String name,
    @PartFile() MultipartFile image, {
    @Part() String? description,
    @Part('is_published') bool? isPublished,
  });


  static CategoryApiService create(int traderId) {
    final String backendUrl = dotenv.env['BACKEND_BASE_URL']!;
    final String baseUrl = '$backendUrl/traders/$traderId';

    final ChopperClient client = ChopperClient(
      baseUrl: baseUrl,
      interceptors: [HeaderInterceptor()],
      authenticator: MyAuthenticator(),
      services: <ChopperService>[
        _$CategoryApiService(),
      ],
      converter: JsonToTypeConverter({
        Category: (dynamic jsonData) =>
            Category.fromJson(jsonData as Map<String, dynamic>)
      }),
    );
    return _$CategoryApiService(client);
  }
}
