import 'package:chopper/chopper.dart';
import 'package:linnet_flutter_app/src/place/place.dart';

part 'place_api_service.chopper.dart';

@ChopperApi(baseUrl: '/place')
abstract class PlaceApiService extends ChopperService {
  @Get(path: '/autocomplete/json')
  Future<Response<List<AutoCompleteItem>>> getSuggestedPlaces(
    @Query('sessiontoken') String sessionToken,
    @Query() String input, 
    {@QueryMap() Map<String, dynamic> query = const <String, dynamic>{}}
    );

  @Get(path: '/details/json')
  Future<Response<dynamic>> getLocation(@Query('placeid') String placeId);

  @Get(path: '/nearbysearch/json')
  Future<Response<List<NearbyPlace>>> 
    getNearbyPlaces(@Query() String location, { 
      @Query() String radius='150',
  });

  @Get(path: '/findplacefromtext/json')
  Future<Response<List<Place>>> placeSearch(
    @Query() String input,
    {@Query() String? fields, 
    @Query('inputtype') String inputType = 'textquery', 
    @Query('locationbias') String? locationBias}
  );

  static PlaceApiService create([ChopperClient? client]) {
    return _$PlaceApiService(client);
  }
}
