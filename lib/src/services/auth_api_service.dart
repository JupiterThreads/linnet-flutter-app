import 'package:chopper/chopper.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

part 'auth_api_service.chopper.dart';

@ChopperApi(baseUrl: '/o/token/')
abstract class AuthApiService extends ChopperService {
  @FactoryConverter(
      request: FormUrlEncodedConverter.requestFactory,
      response: convertResponse,
  )
  @Post(optionalBody: true)
  Future<Response<dynamic>> getAccessToken(
    @Field('username') String email,
    @Field() String password, 
    @Field('client_id') String clientId,
    @Field('client_secret') String clientSecret, {
    @Field('grant_type') String grantType = 'password',
  });
  
  @FactoryConverter(
    request: FormUrlEncodedConverter.requestFactory,
    response: convertResponse,
  )
  @Post(optionalBody: true)
  Future<Response<dynamic>> getAccessTokenFromRefresh(
    @Field('refresh_token') String refreshToken,
    @Field('client_id') String clientId,
    @Field('client_secret') String clientSecret, {
    @Field('grant_type') String grantType = 'refresh_token',
  });

  static AuthApiService create() {
    final String backendUrl = dotenv.env['DOMAIN']!;

    final ChopperClient client =
        ChopperClient(baseUrl: backendUrl, 
        converter: const FormUrlEncodedConverter(), 
        services: <ChopperService>[
      _$AuthApiService(),
    ]);
    return _$AuthApiService(client);
  }
}


Response<T> convertResponse<T>(Response res) =>
    JsonConverter().convertResponse(res);