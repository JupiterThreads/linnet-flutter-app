import 'dart:async';
import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_settings_screens/flutter_settings_screens.dart';
import 'package:linnet_flutter_app/src/api/api_exception.dart';


class HeaderInterceptor implements RequestInterceptor, ResponseInterceptor {
  static const String AUTH_HEADER = 'Authorization';
  static const String TRADER_MODE = 'trader-mode';
  FlutterSecureStorage storage = const FlutterSecureStorage();
  static String clientId = dotenv.env['CLIENT_ID']!;
  static String clientSecret = dotenv.env['CLIENT_SECRET']!;

  Future<String?> getAccessToken() async {
    String? accessToken;
    try {
      accessToken = await storage.read(key: 'access_token');
    } on PlatformException catch (e) {
      debugPrint(e.toString());
      accessToken = null;
    }
    return accessToken;
  }

  Future<Map<String, String>> getHeaders() async {
    final Map<String, String> headers = <String, String>{
      'Content-type': 'application/json',
      'Accept': 'application/json',
      TRADER_MODE: Settings.getValue<bool>('trader_mode', false).toString()
    };
    final String? accessToken = await getAccessToken();

    if (accessToken != null) {
      headers[AUTH_HEADER] = 'Bearer $accessToken';
    }
    return headers;
  }

  @override
  FutureOr<Request> onRequest(Request request) async {
    final Map<String, String> headers = await getHeaders();
    return request.copyWith(headers: headers);
  }

  @override
  FutureOr<Response> onResponse(Response response) async {
    switch (response.statusCode) {
      case 204:
      case 201:
      case 200:
        return response;
      case 400:
        throw BadRequestException(response.body.toString());
      case 403:
      case 401:
        return response;
      case 404:
        throw NotFoundException(response.body.toString());
      case 429:
        throw TooManyRequestsException(response.body.toString());
      case 500:
      default:
        throw FetchDataException(
            '''Error occured while Communication with Server 
                with StatusCode : ${response.statusCode}''');
    }
  }
}
