import 'dart:convert';
import 'package:chopper/chopper.dart';
import 'package:flutter/foundation.dart';
import 'package:linnet_flutter_app/src/common/models/paginate.dart';
import 'package:linnet_flutter_app/src/place/models/models.dart';
import 'package:linnet_flutter_app/src/trader/models/distance_matrix.dart';

class JsonToTypeConverter extends JsonConverter {
  const JsonToTypeConverter(this.typeToJsonFactoryMap);
  final Map<Type, Function> typeToJsonFactoryMap;

  @override
  Response<BodyType> convertResponse<BodyType, InnerType>(Response response) {
    return response.copyWith(
      body: fromJsonData<BodyType, InnerType>(
          response, typeToJsonFactoryMap[InnerType]),
    );
  }

  T fromJsonData<T, InnerType>(Response response, Function? jsonParser) {
    dynamic jsonMap = {};

    // for some reason this getting called twice with the desrialized data
    // so we need to return early

    if (response.body.runtimeType != String) {
      return response.body as T;
    }

    if (response.body.isEmpty as bool) {
      return jsonMap as T;
    }

    try {
      jsonMap = json.decode(utf8.decode(response.bodyBytes));
    } catch (error) {
      debugPrint(error.toString());
    }

    final String paginateType = 'Paginate<${InnerType.toString()}>';

    if (T.toString() == paginateType) {
      return Paginate<InnerType>.fromJson(
          jsonMap as Map<String, dynamic>, jsonParser!) as T;
    }

    if (InnerType == AutoCompleteItem) {
      jsonMap = jsonMap['predictions'];
    } else if (InnerType == NearbyPlace) {
      jsonMap = jsonMap['results'];
    } else if (InnerType == Place) {
      jsonMap = jsonMap['candidates'];
    } else if (InnerType == DistanceMatrix) {
      jsonMap = jsonMap['rows'][0]['elements'];
    }

    if (jsonMap is List) {
      return jsonMap
          .map((dynamic item) =>
              jsonParser!(item as Map<String, dynamic>) as InnerType)
          .toList() as T;
    }

    if (jsonParser != null) {
      return jsonParser(jsonMap) as T;
    }

    return jsonMap as T;
  }
}
