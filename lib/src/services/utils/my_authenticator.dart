import 'dart:async';

import 'package:chopper/chopper.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get_it/get_it.dart';
import 'package:linnet_flutter_app/constants.dart';
import 'package:linnet_flutter_app/src/authentication/authentication.dart';
import 'package:linnet_flutter_app/src/services/auth_api_service.dart';
import 'package:linnet_flutter_app/src/utils/utils.dart';

class MyAuthenticator extends Authenticator {
  static const String AUTH_HEADER = 'Authorization';
  static String clientId = dotenv.env['CLIENT_ID']!;
  static String clientSecret = dotenv.env['CLIENT_SECRET']!;
  FlutterSecureStorage storage = const FlutterSecureStorage();

  @override
  FutureOr<Request?> authenticate(
      Request request, Response<dynamic> response) async {
    if (response.statusCode == 401) {
      apiRetryCount += 1;

      if (apiRetryCount == MAX_API_RETRIES) {
        return null;
      }

      String? newToken = await refreshToken();

      final Map<String, String> updatedHeaders =
          Map<String, String>.of(request.headers);

      if (newToken != null) {
        apiRetryCount = 0;
        newToken = 'Bearer $newToken';
        updatedHeaders.update('Authorization', (String _) => newToken!,
            ifAbsent: () => newToken!);
        return request.copyWith(headers: updatedHeaders);
      }
    }
    return null;
  }

  Future<String?> refreshToken() async {
    GetIt getIt = GetIt.instance;

    final AuthApiService authApiService = getIt<AuthApiService>();
    final AuthenticationBloc authBloc = getIt<AuthenticationBloc>();
    final String? refreshToken = await storage.read(key: 'refresh_token');

    if (refreshToken == null) {
      return null;
    }

    String? accessToken;

    try {
      final Response<dynamic> response = await authApiService
          .getAccessTokenFromRefresh(refreshToken, clientId, clientSecret);
      final Map<String, dynamic> data = response.body as Map<String, dynamic>;
      await Utils.persistTokens(data);
      accessToken = data['access_token'] as String;
      authBloc.add(const HasRefreshed());
    } catch (err) {
      authBloc.add(const LoggedOut());
    }

    return accessToken;
  }
}
