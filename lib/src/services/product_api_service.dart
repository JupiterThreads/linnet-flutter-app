import 'package:chopper/chopper.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' show MultipartFile;
import 'package:linnet_flutter_app/src/common/models/paginate.dart';
import 'package:linnet_flutter_app/src/product/models/product.dart';
import 'package:linnet_flutter_app/src/services/utils/utils.dart';

part 'product_api_service.chopper.dart';

@ChopperApi(baseUrl: '/products')
abstract class ProductApiService extends ChopperService {
  @Get()
  Future<Response<Paginate<Product>>> getProducts(
    {@QueryMap() Map<String, dynamic> query = const <String, dynamic>{}});

  @Get(path: '/{id}')
  Future<Response<Product>> getProduct(@Path('id') int id);

  @Delete(path: '/{id}')
  Future<Response<dynamic>> deleteProduct(@Path('id') int id);

  @Put(path: '/{id}')
  Future<Response<Product>> updateProduct(
      @Path('id') int id, @Body() Map<String, dynamic> data);

  @Patch(path: '/{id}')
  Future<Response<Product>> partialUpdate(
      @Path('id') int id, @Body() Map<String, dynamic> data);

  @Patch(path: '/{id}')
  @Multipart()
  Future<Response<Product>> uploadImage(
      @Path('id') int id, @PartFile() MultipartFile image);

  @Post(path: '/update-order')
  Future<Response<dynamic>> updateProductDisplayOrder(@Body() dynamic data);

  @Post()
  Future<Response<Product>> createProduct(@Body() dynamic data);

  @Post(path: '/{id}/create-variations')
  Future<Response<Product>> createVariations(
      @Path('id') int id, @Body() dynamic data);

  static ProductApiService create(int traderId, int categoryId) {
    final String backendUrl = dotenv.env['BACKEND_BASE_URL']!;
    final String baseUrl =
        '$backendUrl/traders/$traderId/categories/$categoryId';

    final ChopperClient client = ChopperClient(
      baseUrl: baseUrl,
      interceptors: [HeaderInterceptor()],
      authenticator: MyAuthenticator(),
      services: <ChopperService>[_$ProductApiService()],
      converter: JsonToTypeConverter({
        Product: (dynamic jsonData) =>
            Product.fromJson(jsonData as Map<String, dynamic>)
      }),
    );
    return _$ProductApiService(client);
  }
}
