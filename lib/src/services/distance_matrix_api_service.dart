import 'package:chopper/chopper.dart';
import 'package:linnet_flutter_app/src/trader/models/distance_matrix.dart';

part 'distance_matrix_api_service.chopper.dart';

@ChopperApi(baseUrl: '/distancematrix/json')
abstract class DistanceMatrixApiService extends ChopperService {
  @Get()
  Future<Response<List<DistanceMatrix>>> getDistances(
    @Query() String origins,
    @Query() String destinations, {
    @Query() String units = 'imperial',
    @Query() String mode = 'driving',
  });

  static DistanceMatrixApiService create([ChopperClient? client]) {
    return _$DistanceMatrixApiService(client);
  }
}
