import 'package:chopper/chopper.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:linnet_flutter_app/src/common/models/paginate.dart';
import 'package:linnet_flutter_app/src/member/models/member.dart';
import 'package:linnet_flutter_app/src/services/utils/utils.dart';
import 'package:linnet_flutter_app/src/trader/models/models.dart';
import 'package:linnet_flutter_app/src/user/models/models.dart';
import 'package:http/http.dart' show MultipartFile;

part 'trader_api_service.chopper.dart';

@ChopperApi(baseUrl: '/traders')
abstract class TraderApiService extends ChopperService {
  @Get()
  Future<Response<Paginate<TradeAccount>>> getTraders(
      {@QueryMap() Map<String, dynamic> query = const <String, dynamic>{}});

  @Get(path: '/{id}')
  Future<Response<TradeAccount>> getTrader(@Path('id') int id);

  @Put(path: '/{id}')
  Future<Response<TradeAccount>> update(
      @Path('id') int id, @Body() dynamic data);

  @Patch(path: '/{id}')
  Future<Response<TradeAccount>> partialUpdate(
      @Path('id') int id, @Body() dynamic data);

  @Post()
  Future<Response<TradeAccount>> createAccount(
    @Body() dynamic data,
  );

  @Patch(path: '/{id}')
  @Multipart()
  Future<Response<TradeAccount>> uploadAvatar(@Path('id') int id,
    @PartFile() MultipartFile avatar);

  @Patch(path: '/{id}')
  @Multipart()
  Future<Response<TradeAccount>> uploadCoverImage(@Path('id') int id, 
  @PartFile('cover_image') MultipartFile coverImage);

  @Get(path: '/{id}/members')
  Future<Response<List<Member>>> getMembers(@Path('id') int id);

  @Post(path: '/{id}/members')
  Future<Response<Member>> addMember(
      @Path('id') int id, @Body() Map<String, dynamic> data);

  @Patch(path: '/{id}/members')
  Future<Response<Member>> updateMember(
    @Path('id') int id, @Body() Map<String, dynamic> data);

  @Delete(path: '/{id}/members')
  Future<Response<dynamic>> removeMember(
      @Path('id') int id, @Query('member_id') int memberId);

  @Get(path: '/{id}/search-users')
  Future<Response<List<User>>> searchUsers(
      @Path('id') int id, @QueryMap() Map<String, dynamic> query);

  @Get(path: '/business-types')
  Future<Response<List<BusinessType>>> getBusinessTypes();

  @Get(path: '/currency-options')
  Future<Response<List<dynamic>>> getCurrencies();

  @Get(path: '/terms-conditions')
  Future<Response<TermsAndConditions>> getTermsAndConditions();

  static TraderApiService create() {
    final String backendUrl = dotenv.env['BACKEND_BASE_URL']!;

    final ChopperClient client = ChopperClient(
      baseUrl: backendUrl,
      interceptors: [HeaderInterceptor(), HttpLoggingInterceptor()],
      authenticator: MyAuthenticator(),
      services: <ChopperService>[
        _$TraderApiService(),
      ],
      converter: JsonToTypeConverter({
        BusinessType: (dynamic jsonData) =>
            BusinessType.fromJson(jsonData as Map<String, dynamic>),
        Member: (dynamic jsonData) =>
            Member.fromJson(jsonData as Map<String, dynamic>),
        TermsAndConditions: (dynamic jsonData) =>
            TermsAndConditions.fromJson(jsonData as Map<String, dynamic>),
        TradeAccount: (dynamic jsonData) =>
            TradeAccount.fromJson(jsonData as Map<String, dynamic>),
        User: (dynamic jsonData) =>
            User.fromJson(jsonData as Map<String, dynamic>),
      }),
    );
    return _$TraderApiService(client);
  }
}
