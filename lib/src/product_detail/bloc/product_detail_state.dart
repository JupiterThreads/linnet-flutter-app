import 'package:flutter/widgets.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:http/http.dart' show MultipartFile;

part 'product_detail_state.freezed.dart';


@freezed 
class ProductDetailState with _$ProductDetailState {
  factory ProductDetailState({
    @Default(null) String? name,
    @Default(null) String? description,
    @Default(null) double? unitPrice,
    @Default(false) bool isPublished,
    @Default(null) MultipartFile? imageFile,
    @Default(null) Image? image,
  }) = _ProductDetailState;
}
