import 'package:flutter/widgets.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:http/http.dart' show MultipartFile;

part 'product_detail_event.freezed.dart';

@freezed
class ProductDetailEvent with _$ProductDetailEvent {
  const factory ProductDetailEvent.nameChanged({
    required String name,
  }) = ProductNameChanged;

  const factory ProductDetailEvent.descriptionChanged({
    required String description,
  }) = ProductDescriptionChanged;

  const factory ProductDetailEvent.priceChanged({
    required double unitPrice,
  }) = ProductPriceChanged;

  const factory ProductDetailEvent.publishedChanged({
    required bool isPublished,
  }) = ProductPublishChanged;

  const factory ProductDetailEvent.imageFileChanged({
    required MultipartFile? imageFile,
    required Image? image,
  }) = ProductImageFileChanged;

  const factory ProductDetailEvent.performSave() = PerformSave;
}
