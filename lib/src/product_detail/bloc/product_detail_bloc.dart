import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:linnet_flutter_app/src/product/bloc/bloc.dart';
import 'package:linnet_flutter_app/src/product/bloc/product_bloc.dart';
import 'package:linnet_flutter_app/src/product/models/product.dart';
import 'package:linnet_flutter_app/src/product_detail/bloc/bloc.dart';
import 'package:linnet_flutter_app/src/product_detail/bloc/product_detail_event.dart';
import 'package:linnet_flutter_app/src/product_variation/bloc/variation_bloc.dart';
import 'package:linnet_flutter_app/src/product_variation/models/product_variation.dart';

class ProductDetailBloc extends Bloc<ProductDetailEvent, ProductDetailState> {
  ProductDetailBloc(
      {required this.productBloc,
      this.variationBloc,
      String? name,
      String? description,
      bool isPublished = false,
      double? unitPrice})
      : super(ProductDetailState(
            name: name,
            description: description,
            unitPrice: unitPrice,
            isPublished: isPublished));

  final ProductBloc productBloc;
  final VariationBloc? variationBloc;

  @override
  Stream<ProductDetailState> mapEventToState(ProductDetailEvent event) async* {
    if (event is ProductNameChanged) {
      yield state.copyWith(
        name: event.name,
      );
    }

    if (event is ProductDescriptionChanged) {
      yield state.copyWith(
        description: event.description,
      );
    }

    if (event is ProductPriceChanged) {
      yield state.copyWith(
        unitPrice: event.unitPrice,
      );
    }

    if (event is ProductPublishChanged) {
      yield state.copyWith(
        isPublished: event.isPublished,
      );
    }

    // This event is only for new products
    if (event is ProductImageFileChanged) {
      yield state.copyWith(
        imageFile: event.imageFile,
        image: event.image,
      );
    }

    if (event is PerformSave) {
      if (productBloc.state.selectedProduct == null) {
        productBloc.add(CreateProduct(product: getNewProduct(), 
        image: state.imageFile));
      } else {
        productBloc.add(UpdateProduct(
            product: getUpdatedProduct(productBloc.state.selectedProduct!)));
      }
    }
  }

  Product getNewProduct() {
    return Product(
      name: state.name!,
      description: state.description,
      isPublished: state.isPublished,
      unitPrice: state.unitPrice,
      variations: removeTempIds(variationBloc?.state.draftVariationList 
        ?? <ProductVariation>[]),
    );
  }

  Product getUpdatedProduct(Product product) {
    return product.copyWith(
        name: state.name!,
        description: state.description,
        isPublished: state.isPublished,
        unitPrice: state.unitPrice);
  }

  List<ProductVariation> removeTempIds(List<ProductVariation> variations) {
    return variations
        .map((ProductVariation _var) => _var.copyWith(id: null))
        .toList();
  }
}
