import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:linnet_flutter_app/constants.dart' show ActionType;
import 'package:linnet_flutter_app/src/common/widgets/linnet_image_picker.dart';
import 'package:linnet_flutter_app/src/product/bloc/bloc.dart';
import 'package:linnet_flutter_app/src/product/bloc/product_bloc.dart';
import 'package:linnet_flutter_app/src/product_detail/bloc/bloc.dart';
import 'package:http/http.dart' show MultipartFile;
import 'package:linnet_flutter_app/src/product_variation/bloc/variation_bloc.dart';
import 'package:linnet_flutter_app/src/product_variation/models/product_variation.dart';
import 'package:linnet_flutter_app/src/user/blocs/user/user_bloc.dart';
import 'package:linnet_flutter_app/src/utils/utils.dart';

class ProductDetailForm extends StatefulWidget {
  const ProductDetailForm(
      {Key? key, required this.actionType, this.initialImage})
      : super(key: key);

  final Image? initialImage;
  final ActionType actionType;

  @override
  ProductDetailFormState createState() => ProductDetailFormState();
}

class ProductDetailFormState extends State<ProductDetailForm> {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _describtionController = TextEditingController();
  final GlobalKey<FormState> _productDetailForm = GlobalKey<FormState>();
  MoneyMaskedTextController _priceController = MoneyMaskedTextController(
          decimalSeparator: '.',
          thousandSeparator: ',');
  ActionType get _actionType => widget.actionType;
  bool isPublished = false;
  bool canPublish = true;
  int maxNameLength = 45;
  MultipartFile? selectedFile;
  late ProductDetailBloc _productDetailBloc;
  late ProductBloc _productBloc;

  @override
  void initState() {
    super.initState();

    _productDetailBloc = BlocProvider.of<ProductDetailBloc>(context);
    _productBloc = BlocProvider.of<ProductBloc>(context);

    _nameController.text = _productDetailBloc.state.name ?? '';
    _describtionController.text = _productDetailBloc.state.description ?? '';

    if (_productDetailBloc.state.unitPrice != null) {
      _priceController = MoneyMaskedTextController(
          initialValue: _productDetailBloc.state.unitPrice,
          decimalSeparator: '.',
          thousandSeparator: ',');
    }

    _setUpListeners();
  }

  void _setUpListeners() {
    _nameController.addListener(_onNameChanged);
    _describtionController.addListener(_onDescriptionChanged);
    _priceController.addListener(_onPriceChanged);
  }

  void _onNameChanged() {
    _productDetailBloc
        .add(ProductNameChanged(name: _nameController.text.trim()));
  }

  void _onDescriptionChanged() {
    _productDetailBloc.add(ProductDescriptionChanged(
        description: _describtionController.text.trim()));
  }

  void _onPriceChanged() {
    _productDetailBloc
        .add(ProductPriceChanged(unitPrice: _priceController.numberValue));
  }

  @override
  void dispose() {
    _nameController.dispose();
    _priceController.dispose();
    _describtionController.dispose();
    super.dispose();
  }

  void peformSave() {
    _productDetailBloc.add(const PerformSave());
  }

  bool isFormValid() {
    return _productDetailForm.currentState?.validate() ?? false;
  }

  bool isNameValid() {
    return _nameController.text.trim().isNotEmpty;
  }

  void _handlePublish(bool val) {
    _productDetailBloc.add(ProductPublishChanged(isPublished: val));
  }

  void _handlePickedImage(MultipartFile file, Image image) {
    selectedFile = file;
    if (_actionType == ActionType.EDIT) {
      _productBloc.add(UploadImage(
          product: _productBloc.state.selectedProduct!, image: file));
    } else {
      _productDetailBloc
          .add(ProductImageFileChanged(imageFile: file, image: image));
    }
  }

  void _handleDeleteImage() {
    if (_actionType == ActionType.EDIT) {
      final Map<String, dynamic> data = <String, dynamic>{'image': null};
      _productBloc.add(PartialUpdateProduct(
          product: _productBloc.state.selectedProduct!, data: data));
    } else {
      _productDetailBloc
          .add(ProductImageFileChanged(imageFile: null, image: null));
    }
  }

  bool _isPriceValid(List<ProductVariation> variationList) {
    bool isValid = false;
    if (variationList.isEmpty && _priceController.numberValue > 0) {
      isValid = true;
    } else if (variationList.isNotEmpty) {
      isValid = true;
    }
    return isValid;
  }

  @override
  Widget build(BuildContext context) {
     final bool hasWritePerm = context
        .watch<UserBloc>()
        .state
        .user
        !.hasWritePermission(_productBloc.tradeAccount);
    return BlocBuilder<ProductDetailBloc, ProductDetailState>(
        builder: (BuildContext context, ProductDetailState state) {
      final List<ProductVariation> variations = _actionType == ActionType.CREATE
          ? context.watch<VariationBloc>().state.draftVariationList
          : context.watch<ProductBloc>().state.selectedProduct!.variations;

      return Form(
          key: _productDetailForm,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TextFormField(
                enabled: hasWritePerm,
                keyboardType: TextInputType.text,
                controller: _nameController,
                maxLength: maxNameLength,
                autocorrect: false,
                validator: (_) => isNameValid() ? null : 'Invalid Name',
                decoration: const InputDecoration(
                  labelText: 'Name',
                ),
              ),
              const SizedBox(height: 16.0),
              TextFormField(
                enabled: hasWritePerm,
                maxLines: 2,
                autocorrect: false,
                keyboardType: TextInputType.text,
                controller: _describtionController,
                decoration: const InputDecoration(
                  labelText: 'Description',
                ),
              ),
              const SizedBox(height: 16.0),
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                Expanded(
                  flex: 6,
                  child: Padding(
                    padding: const EdgeInsets.only(right: 8),
                    child: Text(
                        'Leave this at 0.00 if you want to add variations',
                        style: Theme.of(context).textTheme.caption),
                  ),
                ),
                Expanded(
                  flex: 4,
                  child: TextFormField(
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    enabled: variations.isEmpty && hasWritePerm,
                    validator: (_) =>
                        _isPriceValid(variations) ? null : 'Invalid Unit Price',
                    textAlign: TextAlign.end,
                    decoration: InputDecoration(
                        prefix: Text(Utils.getCurrency(_productDetailBloc
                            .productBloc.tradeAccount.currency!))),
                    controller: _priceController,
                    keyboardType: const TextInputType.numberWithOptions(
                        signed: true, decimal: true),
                  ),
                ),
              ]),
              const SizedBox(height: 16.0),
              SwitchListTile(
                contentPadding: EdgeInsets.zero,
                title: const Text('Publish'),
                value: state.isPublished,
                onChanged: !hasWritePerm ? null : _handlePublish,
                // canPublish ? (bool val) => _handlePublish(val) : null,
              ),
              const Divider(height: 2),
              Padding(
                padding: const EdgeInsets.only(top: 16.0, bottom: 32.0),
                child:
                    Text('Image', style: Theme.of(context).textTheme.subtitle1),
              ),
              LinnetImagePicker(
                  initialImage: widget.initialImage,
                  containerHeight: 100,
                  enabled: hasWritePerm,
                  containerWidth: 100,
                  fieldName: 'image',
                  onPickedImage: !hasWritePerm ? null : _handlePickedImage,
                  shape: BoxShape.rectangle,
                  onDeleteImage: !hasWritePerm ? null : _handleDeleteImage),
            ],
          ));
    });
  }
}
