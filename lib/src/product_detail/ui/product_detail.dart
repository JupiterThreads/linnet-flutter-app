import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:linnet_flutter_app/constants.dart' show ActionType;
import 'package:linnet_flutter_app/src/product/bloc/bloc.dart';
import 'package:linnet_flutter_app/src/product/models/product.dart';
import 'package:linnet_flutter_app/src/product_detail/bloc/product_detail_bloc.dart';
import 'package:linnet_flutter_app/src/product_detail/ui/product_detail_form.dart';
import 'package:linnet_flutter_app/src/product_variation/bloc/bloc.dart';
import 'package:linnet_flutter_app/src/product_variation/models/product_variation.dart';
import 'package:linnet_flutter_app/src/product_variation/ui/trader/variation_detail.dart';
import 'package:linnet_flutter_app/src/product_variation/ui/trader/variation_list.dart';
import 'package:linnet_flutter_app/src/services/product_variation_api_service.dart';
import 'package:linnet_flutter_app/src/user/blocs/user/user_bloc.dart';

class ProductDetail extends StatefulWidget {
  const ProductDetail({
    required this.actionType,
    this.product,
  });

  final Product? product;
  final ActionType actionType;

  @override
  _ProductDetailState createState() => _ProductDetailState();
}

class _ProductDetailState extends State<ProductDetail>
    with SingleTickerProviderStateMixin {
  ActionType get _actionType => widget.actionType;
  Product? get _product => widget.product;
  late ProductBloc _productBloc;
  late ProductDetailBloc _productDetailBloc;
  late VariationBloc _variationBloc;
  late TabController _controller;
  bool showingVariations = false;
  final GlobalKey<ProductDetailFormState> _productDetailFormKey =
      GlobalKey<ProductDetailFormState>();

  bool get _newProduct => widget.product == null;

  @override
  void initState() {
    super.initState();
    _controller = TabController(length: 2, vsync: this);
    _controller.addListener(_tabListener);

    _productBloc = BlocProvider.of<ProductBloc>(context);
    _variationBloc = BlocProvider.of<VariationBloc>(context);

    if (_actionType == ActionType.EDIT) {
      _productBloc.add(SelectProduct(product: _product!));
      _createVariationApiService();
      _productDetailBloc = ProductDetailBloc(
          productBloc: _productBloc,
          isPublished: _product?.isPublished ?? false,
          name: _product?.name,
          description: _product?.description,
          unitPrice: _product?.unitPrice);
    } else {
      _productBloc.add(const UnselectProduct());
      _variationBloc.add(const Reset());
      _productDetailBloc = ProductDetailBloc(
          productBloc: _productBloc, variationBloc: _variationBloc);
    }
  }

  @override
  void dispose() {
    _controller.removeListener(_tabListener);
    _controller.dispose();
    super.dispose();
  }

  void _tabListener() {
    setState(() {
      showingVariations = _controller.index == 1;
    });
  }

  void _createVariationApiService() {
    _productBloc.productVariationApiService = ProductVariationApiService.create(
      _productBloc.tradeAccount.id!,
      _productBloc.category.id!,
      _product!.id!,
    );
  }

  void _save() {
    final bool isValid = _productDetailFormKey.currentState?.isFormValid() ?? false;
    if (isValid) {
      _productDetailFormKey.currentState?.peformSave();
      Navigator.pop(context);
    }
  }

  Widget _buildVariationListContainer() {
    if (_actionType == ActionType.CREATE) {
      return BlocBuilder<VariationBloc, VariationState>(
        builder: (BuildContext context, VariationState state) {
        return VariationList(
            isDraft: true, variationList: state.draftVariationList);
      });
    }
    return BlocProvider<ProductBloc>.value(
        value: BlocProvider.of<ProductBloc>(context),
        child: VariationList(
            isDraft: false,
            variationList: BlocProvider.of<ProductBloc>(context, listen: true)
                    .state
                    .selectedProduct
                    ?.variations ??
                <ProductVariation>[]));
  }

  Widget _buildTitle() {
    return _actionType == ActionType.CREATE
        ? const Text('New Product')
        : const Text('Product Detail');
  }

  Widget _buildVariationsTitle(List<ProductVariation> variations) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        const Text('Variations'),
        if (variations.isNotEmpty) const SizedBox(width: 8),
        if (variations.isNotEmpty)
          Badge(
            toAnimate: false,
            badgeContent: Padding(
              padding: const EdgeInsets.all(2.0),
              child: Text(variations.length.toString(),
                  style: const TextStyle(color: Colors.white)),
            ),
          ),
      ],
    );
  }

  Image? _getInitialImage() {
    Image? image;
    if (_actionType == ActionType.EDIT && _product?.image != null) {
      image = Image.network(_product!.image!);
    }

    if (_actionType == ActionType.CREATE &&
        _productDetailBloc.state.image != null) {
      image = _productDetailBloc.state.image;
    }
    return image;
  }

  Widget _buildProdutDetailForm() {
    return ProductDetailForm(
      key: _productDetailFormKey,
      actionType: _actionType,
      initialImage: _getInitialImage(),
    );
  }

  @override
  Widget build(BuildContext context) {
     final bool hasWritePerm = context
        .watch<UserBloc>()
        .state
        .user
        !.hasWritePermission(_productBloc.tradeAccount);
    final List<ProductVariation> variations = _actionType == ActionType.CREATE
        ? context.watch<VariationBloc>().state.draftVariationList
        : context.watch<ProductBloc>().state.selectedProduct?.variations ??
            <ProductVariation>[];
    return BlocProvider<ProductDetailBloc>(
      create: (BuildContext context) => _productDetailBloc,
      child: DefaultTabController(
          length: 2,
          child: Scaffold(
            appBar: AppBar(
              actions: <Widget>[
                if (!showingVariations && hasWritePerm)
                  IconButton(
                    tooltip: 'Save',
                    icon:
                        Icon(Icons.check, color: Theme.of(context).accentColor),
                    onPressed: _save,
                  ),
              ],
              bottom: TabBar(
                controller: _controller,
                tabs: <Widget>[
                  const Tab(child: Text('Detail')),
                  Tab(child: _buildVariationsTitle(variations)),
                ],
              ),
              title: _buildTitle(),
            ),
            body: TabBarView(controller: _controller, children: [
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: _buildProdutDetailForm(),
              ),
              _buildVariationListContainer(),
            ]),
            floatingActionButton: hasWritePerm && showingVariations
                ? FloatingActionButton(
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute<Widget>(
                          fullscreenDialog: true,
                          builder: (BuildContext context) {
                            return MultiBlocProvider(
                              providers: [
                                BlocProvider<ProductBloc>.value(
                                  value: _productBloc,
                                ),
                                BlocProvider<VariationBloc>.value(
                                    value: _variationBloc),
                                BlocProvider<ProductDetailBloc>.value(
                                    value: _productDetailBloc),
                              ],
                              child: VariationDetail(
                                  isDraft: _newProduct,
                                  actionType: ActionType.CREATE),
                            );
                          }));
                    },
                    child: const Icon(Icons.add))
                : null,
          )),
    );
  }
}
