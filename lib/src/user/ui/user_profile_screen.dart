import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:linnet_flutter_app/src/common/widgets/widgets.dart'
    show LinnetImagePicker;
import 'package:linnet_flutter_app/src/user/user.dart';
import 'package:http/http.dart' show MultipartFile;

class UserProfileScreen extends StatefulWidget {
  const UserProfileScreen({Key? key}) : super(key: key);

  @override
  _UserProfileScreenState createState() => _UserProfileScreenState();
}

class _UserProfileScreenState extends State<UserProfileScreen> {
  final TextEditingController _nameController = TextEditingController();
  final GlobalKey<FormState> _userProfileFormKey = GlobalKey<FormState>();
  late UserBloc _userBloc;
  int maxLength = 25;
  Image? _avatarImage;

  @override
  void initState() {
    super.initState();
    _userBloc = BlocProvider.of<UserBloc>(context);

    setState(() {
      _nameController.text = _userBloc.state.user!.name!;
      if (_userBloc.state.user!.profile?.avatar != null) {
        _avatarImage = Image.network(_userBloc.state.user!.profile!.avatar!);
      }
    });
  }

  bool isNameValid(String? text) {
    return text?.trim().isNotEmpty ?? false;
  }

  void performSave() {
    final bool formIsValid = 
      _userProfileFormKey.currentState?.validate() ?? false;
    if (!formIsValid) return;
    final Map<String, dynamic> data = <String, dynamic>{
      'name': _nameController.text,
    };
    _userBloc.add(PartialUpdateUser(data: data, showSuccessMessage: true));
  }

  void onDeleteImage() {
    final Map<String, dynamic> data = <String, dynamic>{
      'avatar': null,
    };
    _userBloc.add(PartialUpdateProfile(data: data));
  }

  void saveAvatar(MultipartFile file) {
    _userBloc.add(UploadAvatar(avatar: file));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Edit Profile'),
        actions: <Widget>[
          IconButton(
              onPressed: performSave,
              icon: Icon(Icons.check, color: Theme.of(context).accentColor),
              tooltip: 'Save'),
        ],
      ),
      body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Form(
            key: _userProfileFormKey,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 26.0),
                  child: Row(
                    children: <Widget>[
                  LinnetImagePicker(
                    shape: BoxShape.circle,
                    containerHeight: 80,
                    containerWidth: 80,
                    onDeleteImage: onDeleteImage,
                    initialImage: _avatarImage,
                    onPickedImage: (MultipartFile mFile, _) 
                      => saveAvatar(mFile),
                    fieldName: 'avatar',
                    placeholderText: 'Avatar',
                  ),
                  const SizedBox(width: 16.0),
                  const Expanded(
                    child: Text(
                      'Enter your name and an optional profile picture')),
                ]
              ),
            ),
              const SizedBox(height: 8),
              TextFormField(
                maxLength: maxLength,
                autocorrect: false,
                keyboardType: TextInputType.text,
                controller: _nameController,
                validator: (String? text) =>
                    isNameValid(text) ? null : 'Invalid Name',
                decoration: const InputDecoration(
                  labelText: 'Name',
                ),
              )
            ],
          ),
        )),
    );
  }
}
