import 'package:freezed_annotation/freezed_annotation.dart';

part 'user_profile.freezed.dart';
part 'user_profile.g.dart';

@freezed
abstract class UserProfile implements _$UserProfile {
  const factory UserProfile({
    required int id,
    @Default(null) String? avatar,
  }) = _UserProfile;
  const UserProfile._();

  factory UserProfile.fromJson(Map<String, dynamic> json) =>
      _$UserProfileFromJson(json);

  @override
  String toString() {
    return 'UserProfile(id: $id, avatar: $avatar)';
  }
}
