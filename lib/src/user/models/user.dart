import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:linnet_flutter_app/src/trader/trader.dart';
import 'package:linnet_flutter_app/src/member/models/member.dart';
import 'package:linnet_flutter_app/src/user/models/user_profile.dart';
import 'package:location/location.dart';
import 'package:location_permissions/location_permissions.dart' as perms;
import 'package:freezed_annotation/freezed_annotation.dart';

part 'user.freezed.dart';
part 'user.g.dart';

// Owners of traders are users but we only need their user id

@freezed
abstract class User implements _$User {
  const factory User(
      {@Default(null ) int? id,
      @Default(null) String? name,
      @Default(null) String? email,
      @JsonKey(name: 'is_active') @Default(true)
          bool? isActive,
      @JsonKey(name: 'is_staff') @Default(false)
          bool? isStaff,
      @JsonKey(name: 'owned_trade_accounts') @Default(null)
          List<TradeAccount>? ownedAccounts,
      @JsonKey(name: 'membership_accounts') @Default(null)
          List<Member>? membershipAccounts,
      UserProfile? profile}) = _User;

  const User._();
  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  @override
  String toString() {
    return 'User(id: $id, name: $name, email: $email, isActive: $isActive, isStaff: $isStaff';
  }

  static List<User> usersFromJson(List<dynamic> jsonList) => jsonList
      .map((dynamic json) => User.fromJson(json as Map<String, dynamic>))
      .toList();

  String? get avatar => profile?.avatar;

  bool hasTradeAccount() {
    if ((membershipAccounts ?? <Member>[]).isNotEmpty 
    || (ownedAccounts ?? <Member>[]).isNotEmpty) {
      return true;
    }
    return false;
  }

  List<TradeAccount> getTradeAccountsFromMembership() {
    return (membershipAccounts ?? <Member>[])
        .map((Member member) => member.tradeAccount!)
        .toList();
  }

  List<dynamic> getAllAccounts({bool sortAsc = false}) {
    final List<TradeAccount> allAccounts =
        List<TradeAccount>.from(ownedAccounts ?? <TradeAccount>[]);
    allAccounts.addAll(getTradeAccountsFromMembership());
    if (sortAsc) {
      allAccounts
          .sort((TradeAccount a, TradeAccount b) => a.name.compareTo(b.name));
    }
    return <dynamic>[this, ...allAccounts];
  }

  Member? getMembershipFromAccount(TradeAccount account) {
    return (membershipAccounts ?? <Member>[]).firstWhereOrNull(
      (Member member) => member.tradeAccount!.id == account.id);
  }

  dynamic getAccountById(int id) {
    final List<dynamic> accounts = getAllAccounts();
    return accounts.firstWhereOrNull((dynamic account) => account.id == id);
  }

  String? getPermissionFromAccount(TradeAccount account) {
    final Member? member = getMembershipFromAccount(account);
    return member?.permission;
  }

  bool _hasPermission(
      TradeAccount account, Member? member, String permission) {
    bool hasPerm = false;
    if (account.owner.id == id) {
      hasPerm = true;
    } else if (member != null) {
      if (member.permission == permission || member.permission == 'admin') {
        hasPerm = true;
      }
    }
    return hasPerm;
  }

  bool hasWritePermission(TradeAccount account) {
    final Member? member = getMembershipFromAccount(account);
    return _hasPermission(account, member, 'write');
  }

  bool hasAdminPermission(TradeAccount account) {
    final Member? member = getMembershipFromAccount(account);
    return _hasPermission(account, member, 'admin');
  }

  bool hasReadPermission(TradeAccount account) {
    final Member? member = getMembershipFromAccount(account);
    return _hasPermission(account, member, 'read');
  }

  String getAccountType(TradeAccount account) {
    if (id == account.owner.id) {
      return 'owner';
    }

    final String? permission = getPermissionFromAccount(account);
    return permission ?? '';
  }

  List<TradeAccount> getUpdatedOwnedAccounts(TradeAccount account) {
    return (ownedAccounts ?? <TradeAccount>[])
        .map((TradeAccount acc) => acc.id == account.id ? account : acc)
        .toList();
  }

  List<Member> getUpdatedMembershipAccounts(TradeAccount account) {
    return (membershipAccounts ?? <Member>[])
        .map((Member member) => member.tradeAccount!.id == account.id
            ? member.copyWith(tradeAccount: account)
            : member)
        .toList();
  }

  static Future<LatLng?> getUserLocation() async {
    LatLng? uLocation;
    perms.PermissionStatus permission =
        await perms.LocationPermissions().checkPermissionStatus();

    if (permission == perms.PermissionStatus.unknown) {
      permission = await perms.LocationPermissions().requestPermissions();
    }

    if (permission == perms.PermissionStatus.denied ||
        permission == perms.PermissionStatus.unknown) {
      return uLocation;
    }

    LocationData _locationData;
    bool serviceEnabled = false;
    final Location location = Location();
    serviceEnabled = await location.serviceEnabled();
    if (!serviceEnabled) {
      serviceEnabled = await location.requestService();
      if (!serviceEnabled) {
        return uLocation;
      }
    }

    try {
      _locationData =
          await location.getLocation().timeout(const Duration(seconds: 5));
        uLocation = LatLng(_locationData.latitude!, _locationData.longitude!);
    } catch (error) {
      print(error);
    }
    return uLocation;
  }
}
