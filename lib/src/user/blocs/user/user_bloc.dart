import 'package:chopper/chopper.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_settings_screens/flutter_settings_screens.dart';
import 'package:get_it/get_it.dart';
import 'package:linnet_flutter_app/src/member/models/member.dart';
import 'package:linnet_flutter_app/src/services/user_api_service.dart';
import 'package:linnet_flutter_app/src/snackbar/snackbar.dart';
import 'package:linnet_flutter_app/src/trader/trader.dart';
import 'package:linnet_flutter_app/src/user/user.dart';
import 'package:linnet_flutter_app/src/utils/utils.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:collection/collection.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  UserBloc({required this.userApiService, required this.snackbarCubit})
      : super(UserState());

  final UserApiService userApiService;
  final FlutterSecureStorage _storage = const FlutterSecureStorage();
  final SnackbarCubit snackbarCubit;

  @override
  Stream<UserState> mapEventToState(UserEvent event) async* {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    if (event is FetchCurrentUser) {
      final int prefAccountId =
          Settings.getValue<int>('selected_account_id', 0);

      try {
        final Response<User> response = await userApiService.getCurrentUser();
        final User user = response.body as User;
        dynamic selectedAccount = user;

        if (user.id != prefAccountId && prefAccountId != 0) {
          selectedAccount = user.getAccountById(prefAccountId) ?? user;
        }

        final bool traderMode = selectedAccount is TradeAccount;
        yield UserState(
            user: user,
            selectedAccount: selectedAccount,
            traderMode: traderMode);
        Settings.setValue<bool>('trader_mode', traderMode);
      } catch (err, stacktrace) {
        print(stacktrace);
        debugPrint(err.toString());
      }
    }

    if (event is AccountChanged) {
      yield state.copyWith(
        selectedAccount: event.account,
        traderMode: event.account is TradeAccount,
      );
      Settings.setValue<bool>('trader_mode', event.account is TradeAccount);
      Settings.setValue<int>('selected_account_id', event.account.id as int);
    }

    if (event is AddAccount) {
      final User updatedUser = state.user!.copyWith(
        ownedAccounts: List<TradeAccount>.from(state.user!.ownedAccounts ?? [])
          ..add(event.account),
      );
      yield state.copyWith(user: updatedUser);
    }

    if (event is RemoveMembershipAccount) {
      Member? removedMember;

      if (state.user!.membershipAccounts != null) {
        removedMember = state.user!.membershipAccounts!
            .firstWhereOrNull((Member member) => member.id == event.memberId);
      }

      if (removedMember?.tradeAccount!.id == state.selectedAccount.id) {
        add(AccountChanged(account: state.user));
      }

      final User updateUser = state.user!.copyWith(
        membershipAccounts:
            List<Member>.from(state.user!.membershipAccounts ?? <Member>[])
              ..removeWhere((Member member) => member.id == event.memberId),
      );
      yield state.copyWith(user: updateUser);
    }

    if (event is AddMembershipAccount) {
      final User updatedUser = state.user!.copyWith(
        membershipAccounts:
            List<Member>.from(state.user!.membershipAccounts ?? <Member>[])
              ..add(event.member),
      );
      yield state.copyWith(user: updatedUser);
    }

    if (event is UpdateMembershipAccount) {
      final User updatedUser = state.user!.copyWith(
        membershipAccounts: (state.user!.membershipAccounts ?? <Member>[])
            .map((Member member) =>
                member.id == event.member.id ? event.member : member)
            .toList(),
      );
      yield state.copyWith(user: updatedUser);
    }

    if (event is UpdateTradeAccountLists) {
      User updatedUser;
      if (event.account.owner.id == state.user!.id) {
        final List<TradeAccount> updatedAccounts =
            state.user!.getUpdatedOwnedAccounts(event.account);
        updatedUser = state.user!.copyWith(ownedAccounts: updatedAccounts);
      } else {
        final List<Member> updatedMembers =
            state.user!.getUpdatedMembershipAccounts(event.account);
        updatedUser = state.user!.copyWith(membershipAccounts: updatedMembers);
      }

      yield state.copyWith(
        user: updatedUser,
        selectedAccount: event.account.id == state.selectedAccount.id
            ? event.account
            : state.selectedAccount,
      );
    }

    if (event is PartialUpdateProfile) {
      userApiService
          .partialUpdateProfile(state.user!.profile!.id, event.data)
          .then((Response<UserProfile> response) {
        final UserProfile profile = response.body!;
        final User updatedUser = state.user!.copyWith(profile: profile);
        add(UpdateUser(user: updatedUser));
        if (event.showSuccessMessage) {
          snackbarCubit.showSuccess('Updated profile successfully');
        }
      }).catchError((error) {
        debugPrint(error.toString());
        snackbarCubit.showError('Something went wrong updating your profile');
      });
    }

    if (event is PartialUpdateUser) {
      userApiService
          .partialUpdateUser(state.user!.id!, event.data)
          .then((Response<User> response) {
        final User user = response.body!;
        add(UpdateUser(user: user));
        if (event.showSuccessMessage) {
          snackbarCubit.showSuccess('Updated profile successfully');
        }
      }).catchError((error) {
        debugPrint(error.toString());
        snackbarCubit.showError('Something went wrong updating your profile');
      });
    }

    if (event is UploadAvatar) {
      userApiService
          .uploadFile(
        state.user!.profile!.id,
        event.avatar,
      )
          .then((Response<UserProfile> response) {
        final UserProfile profile = response.body!;
        final User updatedUser = state.user!.copyWith(profile: profile);
        add(UpdateUser(user: updatedUser));
      }).catchError((error) {
        debugPrint(error.toString());
        // TODO notify bloc
      });
    }

    if (event is UpdateUser) {
      yield state.copyWith(
          user: event.user,
          selectedAccount: state.selectedAccount is User
              ? event.user
              : state.selectedAccount);
    }

    if (event is UserLoggedOut) {
      deleteToken();
      yield UserState();
    }

    if (event is UserReset) {
      yield UserState();
    }
  }

  Future<void> deleteToken() async {
    await _storage.delete(key: 'token');
    await _storage.delete(key: 'user_id');
  }
}
