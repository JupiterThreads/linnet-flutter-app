import 'package:flutter/foundation.dart';
import 'package:linnet_flutter_app/src/member/models/member.dart';
import 'package:linnet_flutter_app/src/trader/trader.dart';
import 'package:linnet_flutter_app/src/user/user.dart';
import 'package:http/http.dart' show MultipartFile;
import 'package:freezed_annotation/freezed_annotation.dart';

part 'user_event.freezed.dart';

@freezed
class UserEvent with _$UserEvent {
  const factory UserEvent.fetchCurrentUser() = FetchCurrentUser;

  const factory UserEvent.accountChanged({
    required dynamic account,
  }) = AccountChanged;

  const factory UserEvent.addAccount({
    required TradeAccount account,
  }) = AddAccount;

  const factory UserEvent.removeMembershipAccount({
    required String memberId,
  }) = RemoveMembershipAccount;

  const factory UserEvent.addMembershipAccount({
    required Member member,
  }) = AddMembershipAccount;

  const factory UserEvent.updateMembershipAccount({
    required Member member,
  }) = UpdateMembershipAccount;

  const factory UserEvent.updateAccountLists({
    required TradeAccount account,
  }) = UpdateTradeAccountLists;

  const factory UserEvent.partialUpdateUser({
    required Map<String, dynamic> data,
    @Default(false) bool showSuccessMessage,
  }) = PartialUpdateUser;

  const factory UserEvent.partialUpdateProfile({
    required Map<String, dynamic> data,
    @Default(false) bool showSuccessMessage,
  }) = PartialUpdateProfile;

  const factory UserEvent.uploadAvatar({
    required MultipartFile avatar,
  }) = UploadAvatar;

  const factory UserEvent.update({
    required User user,
  }) = UpdateUser;

  const factory UserEvent.logout() = UserLoggedOut;

  const factory UserEvent.reset() = UserReset;
}
