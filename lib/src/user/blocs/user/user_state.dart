import 'package:linnet_flutter_app/src/trader/models/trade_account.dart';
import 'package:linnet_flutter_app/src/user/user.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'user_state.freezed.dart';

@freezed
abstract class UserState implements _$UserState {
  factory UserState({
    @Default(null) User? user,
    @Default(null) dynamic selectedAccount,
    @Default(false) bool traderMode,
  }) = _UserState;

  const UserState._();

  bool isOwner() {
    if (selectedAccount is TradeAccount) {
      return selectedAccount?.owner?.id == user!.id;
    }
    return false;
  }

  List<dynamic> getRemaingAccounts() {
    List<dynamic> accounts = <dynamic>[];
    
    if (user != null) {
      accounts = user!
          .getAllAccounts(sortAsc: true)
          .where((dynamic account) => account?.id != selectedAccount?.id)
          .toList();
    }
    return accounts;
  }

  String displayAccountEmail() {
    if (traderMode) {
      return selectedAccount.name as String;
    }
    return user?.email ?? '';
  }

  bool showAccountManagement() {
    if (selectedAccount is TradeAccount && traderMode && user != null) {
      return user!.hasAdminPermission(selectedAccount as TradeAccount);
    }
    return false;
  }
}
