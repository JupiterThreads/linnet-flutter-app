import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class UserRegisterEvent extends Equatable {
  const UserRegisterEvent();

  @override
  List<Object> get props => <Object>[];
}

class EmailChanged extends UserRegisterEvent {
  const EmailChanged({required this.email});

  final String email;

  @override
  List<Object> get props => <Object>[email];

  @override
  String toString() => 'EmailChanged { email: $email }';
}

class PasswordChanged extends UserRegisterEvent {
  const PasswordChanged({required this.password});

  final String password;

  @override
  List<Object> get props => <Object>[password];

  @override
  String toString() => 'PasswordChanged { password: $password }';
}

class NameChanged extends UserRegisterEvent {
  const NameChanged({required this.name});

  final String name;

  @override
  List<Object> get props => <Object>[name];

  @override
  String toString() => 'NameChanged { name: $name }';
}

class FormSubmitted extends UserRegisterEvent {}

class FormReset extends UserRegisterEvent {}