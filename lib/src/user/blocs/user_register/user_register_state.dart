import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class UserRegisterState extends Equatable {

  const UserRegisterState({
    required this.email,
    required this.isEmailValid,
    required this.password,
    required this.isPasswordValid,
    required this.name,
    required this.isNameValid,
    required this.formSubmittedSuccessfully,
  });

  factory UserRegisterState.initial() {
    return const UserRegisterState(
      email: '',
      isEmailValid: false,
      password: '',
      isPasswordValid: false,
      name: '',
      isNameValid: false,
      formSubmittedSuccessfully: false,
    );
  }

  final String email;
  final bool isEmailValid;
  final String password;
  final bool isPasswordValid;
  final String name;
  final bool isNameValid;
  final bool formSubmittedSuccessfully;

  bool get isFormValid => isEmailValid && isPasswordValid && isNameValid;

  UserRegisterState copyWith({
    String? email,
    bool? isEmailValid,
    String? password,
    bool? isPasswordValid,
    String? name,
    bool? isNameValid,
    bool? formSubmittedSuccessfully,
  }) {
    return UserRegisterState(
      email: email ?? this.email,
      isEmailValid: isEmailValid ?? this.isEmailValid,
      password: password ?? this.password,
      isPasswordValid: isPasswordValid ?? this.isPasswordValid,
      name: name ?? this.name,
      isNameValid: isNameValid ?? this.isNameValid,
      formSubmittedSuccessfully:
        formSubmittedSuccessfully ?? this.formSubmittedSuccessfully,
    );
  }

  @override
  List<Object> get props => <Object>[
    email,
    isEmailValid,
    password,
    isPasswordValid,
    name,
    isNameValid,
    formSubmittedSuccessfully,
  ];

  @override
  String toString() {
    return '''UserRegisterState {
      email: $email,
      isEmailValid: $isEmailValid,
      password: $password,
      isPasswordValid: $isPasswordValid,
      name: $name,
      isNameValid: $isNameValid,
      formSubmittedSuccessfully: $formSubmittedSuccessfully
    }''';
  }

}