import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:linnet_flutter_app/src/authentication/authentication.dart';
import 'package:linnet_flutter_app/src/user/user.dart';
import 'package:linnet_flutter_app/src/utils/utils.dart';
import './bloc.dart';

class UserRegisterBloc extends Bloc<UserRegisterEvent, UserRegisterState> {
  UserRegisterBloc(
      {required this.authenticationBloc})
      : super(UserRegisterState.initial());

  final AuthenticationBloc authenticationBloc;
  
  @override
  Stream<UserRegisterState> mapEventToState(UserRegisterEvent event) async* {
    yield* mapUserFormChangedToState(event);
  }

  Stream<UserRegisterState> mapUserFormChangedToState(
      UserRegisterEvent event) async* {
    if (event is EmailChanged) {
      yield state.copyWith(
        email: event.email,
        isEmailValid: _isEmailValid(event.email),
      );
    }

    if (event is PasswordChanged) {
      yield state.copyWith(
        password: event.password,
        isPasswordValid: _isPasswordValid(event.password),
      );
    }

    if (event is NameChanged) {
      yield state.copyWith(
        name: event.name,
        isNameValid: _isNameValid(event.name),
      );
    }

    // if (event is FormSubmitted) {
    //   try {
    //     final User user = await userRepository.signUp(
    //       email: state.email,
    //       password: state.password,
    //       name: state.name,
    //     );

    //     yield state.copyWith(formSubmittedSuccessfully: true);
    //     authenticationBloc.add(Authenticate(email: ));
    //   } catch (error) {
    //     yield state.copyWith(formSubmittedSuccessfully: false);
    //   }
    // }

    if (event is FormReset) {
      yield UserRegisterState.initial();
    }
  }

  bool _isEmailValid(String email) {
    return Utils.emailRegExp.hasMatch(email);
  }

  bool _isPasswordValid(String password) {
    return Utils.passwordRegExp.hasMatch(password);
  }

  bool _isNameValid(String name) {
    return name.length > 3;
  }
}
