import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';

class CurrentLocationProvider extends ChangeNotifier {
  static CurrentLocationProvider of(BuildContext context,
          {bool listen = true}) =>
      Provider.of<CurrentLocationProvider>(context, listen: listen);

  LatLng? _location;

  LatLng? get location => _location;

  void setLocation(LatLng location) {
    if (_location != location) {
      _location = location;
      notifyListeners();
    }
  }
}
