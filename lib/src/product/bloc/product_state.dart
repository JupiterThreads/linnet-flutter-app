import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:linnet_flutter_app/src/common/models/api_state.dart';
import 'package:linnet_flutter_app/src/product/models/product.dart';

part 'product_state.freezed.dart';

@freezed
class ProductState with _$ProductState {
  factory ProductState({
    @Default(<Product>[]) List<Product> productList,
    @Default(<Product>[]) List<Product> searchedProducts,
    @Default(ApiState.loading()) ApiState productListState,
    @Default(null) ApiState? productNextListState,
    @Default(ApiState.done()) ApiState submitStatus,
    @Default(null) Map<String, dynamic>? nextQuery,
    @Default(null) Product? selectedProduct,
  }) = _ProductState;
}
