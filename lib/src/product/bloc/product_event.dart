import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:linnet_flutter_app/src/product/models/product.dart';
import 'package:http/http.dart' show MultipartFile;
import 'package:linnet_flutter_app/src/product_variation/models/product_variation.dart';

part 'product_event.freezed.dart';

@freezed
class ProductEvent with _$ProductEvent {
  const factory ProductEvent.fetchProducts() = FetchProductList;
  const factory ProductEvent.fetchNextProducts() = FetchNextProductList;
  const factory ProductEvent.createProduct({
    required Product product,
    MultipartFile? image,
  }) = CreateProduct;
  const factory ProductEvent.updateProduct({
    required Product product,
  }) = UpdateProduct;
  const factory ProductEvent.deleteProduct({
    required Product product,
  }) = DeleteProduct;
  const factory ProductEvent.partialUpdateProduct({
    required Product product,
    required Map<String, dynamic> data,
  }) = PartialUpdateProduct;
  const factory ProductEvent.addProduct({
    required Product product,
  }) = AddProduct;
  const factory ProductEvent.replaceProduct({
    required Product product,
  }) = ReplaceProduct;
  const factory ProductEvent.removeProduct({
    required int productId,
  }) = RemoveProduct;
  const factory ProductEvent.updateListOrder({
    required Product product,
    required int newIndex,
  }) = UpdateListOrder;
  const factory ProductEvent.uploadImage(
      {required Product product, required MultipartFile image}) = UploadImage;
  const factory ProductEvent.recordSearchedProducts(
      {required List<Product> products}) = RecordSearchedProducts;

  const factory ProductEvent.selectProduct({
    required Product product,
  }) = SelectProduct;

  const factory ProductEvent.unselectProduct() = UnselectProduct;

  // Variation events
  const factory ProductEvent.createVariation(
      {required ProductVariation productVariation}) = CreateVariation;

  const factory ProductEvent.updateVariation({
    required ProductVariation productVariation,
  }) = UpdateVariation;

  const factory ProductEvent.deleteVariation({
    required ProductVariation productVariation,
  }) = DeleteVariation;

  const factory ProductEvent.partialUpdateVariation({
    required ProductVariation productVariation,
    required Map<String, dynamic> data,
  }) = PartialUpdateVariation;

  const factory ProductEvent.addVariation({
    required ProductVariation productVariation,
  }) = AddVariation;

  const factory ProductEvent.replaceVariation({
    required ProductVariation productVariation,
  }) = ReplaceVariation;

  const factory ProductEvent.removeVariation({
    required int variationId,
  }) = RemoveVariation;

  const factory ProductEvent.updateVariationOrder({
    required ProductVariation variation,
    required int newIndex,
  }) = UpdateVariationOrder;
}
