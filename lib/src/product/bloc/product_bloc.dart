import 'package:bloc/bloc.dart';
import 'package:chopper/chopper.dart';
import 'package:flutter/foundation.dart' hide Category;
import 'package:linnet_flutter_app/constants.dart';
import 'package:linnet_flutter_app/src/category/models/category.dart';
import 'package:linnet_flutter_app/src/common/models/api_state.dart';
import 'package:linnet_flutter_app/src/common/models/paginate.dart';
import 'package:linnet_flutter_app/src/product/bloc/bloc.dart';
import 'package:linnet_flutter_app/src/product/models/product.dart';
import 'package:linnet_flutter_app/src/product_variation/bloc/bloc.dart';
import 'package:linnet_flutter_app/src/product_variation/models/product_variation.dart';
import 'package:linnet_flutter_app/src/services/product_api_service.dart';
import 'package:linnet_flutter_app/src/services/product_variation_api_service.dart';
import 'package:linnet_flutter_app/src/snackbar/cubit/cubit.dart';
import 'package:linnet_flutter_app/src/trader/models/trade_account.dart';
import 'package:linnet_flutter_app/src/utils/utils.dart';
import 'package:collection/collection.dart';

class ProductBloc extends Bloc<ProductEvent, ProductState> {
  ProductBloc({
    required this.snackbarCubit,
    required this.productApiService,
    required this.category,
    this.variationBloc,
    required this.tradeAccount,
  }) : super(ProductState());

  SnackbarCubit snackbarCubit;
  ProductApiService productApiService;
  TradeAccount tradeAccount;
  Category category;
  VariationBloc? variationBloc;
  ProductVariationApiService? productVariationApiService;

  ProductVariationApiService? get variationApiService {
    return productVariationApiService;
  }

  @override
  Stream<ProductState> mapEventToState(ProductEvent event) async* {
    if (event is FetchProductList) {
      yield state.copyWith(
        productListState: const ApiState.loading('Fetching products...'),
      );

      try {
        final Response<Paginate<Product>> response =
            await productApiService.getProducts();

        final Paginate<Product> paginate = response.body!;

        yield state.copyWith(
          productList: paginate.results,
          nextQuery:
              paginate.next != null ? Utils.getParams(paginate.next!) : null,
          productListState: const ApiState.done(),
        );
      } catch (error) {
        debugPrint(error.toString());
        yield state.copyWith(
          productListState:
              const ApiState.error('Something went wrong fetching products'),
        );
      }
    }

    if (event is FetchNextProductList) {
      yield state.copyWith(
        productNextListState: const ApiState.loading(),
      );

      try {
        final Response<Paginate<Product>> response = await productApiService
            .getProducts(query: state.nextQuery ?? const <String, dynamic>{});
        final Paginate<Product> paginate = response.body!;

        yield state.copyWith(
          productList: List<Product>.from(state.productList)
            ..addAll(paginate.results),
          nextQuery:
              paginate.next != null ? Utils.getParams(paginate.next!) : null,
          productNextListState: const ApiState.done(),
        );
      } catch (error) {
        debugPrint(error.toString());
        yield state.copyWith(
          productNextListState:
              const ApiState.error('Something went wrong fetching products'),
        );
      }
    }

    if (event is CreateProduct) {
      yield state.copyWith(
        submitStatus: const ApiState.loading('Creating product'),
      );
      try {
        final Response<Product> response =
            await productApiService.createProduct(event.product.toJson());
        final Product product = response.body!;

        if (event.image != null) {
          add(UploadImage(product: product, image: event.image!));
        } else {
          add(AddProduct(product: product));
        }
        yield state.copyWith(
          submitStatus: const ApiState.done(),
        );
        snackbarCubit
            .showSuccess('${product.name} has been created successfully');
      } catch (error) {
        debugPrint(error.toString());
        yield state.copyWith(
          submitStatus: const ApiState.error(),
        );
        snackbarCubit.showError('Something went wrong create a new product');
      }
    }

    if (event is UpdateProduct) {
      final Product product = event.product;
      final Map<String, dynamic> data = product.toJson();

      data.remove('image');
      data.remove('image_thumbnail');

      try {
        final Response<Product> response =
            await productApiService.updateProduct(product.id!, data);
        final Product updatedProduct = response.body!;
        add(ReplaceProduct(product: updatedProduct));
        snackbarCubit
            .showSuccess('${updatedProduct.name} updated successfully');
      } catch (error) {
        debugPrint(error.toString());
        snackbarCubit.showError(
            'Something went wrong updating product, ${product.name}.');
      }
    }

    if (event is PartialUpdateProduct) {
      try {
        final Response<Product> response = await productApiService
            .partialUpdate(event.product.id!, event.data);
        final Product product = response.body!;
        add(ReplaceProduct(product: product));
      } catch (error) {
        debugPrint(error.toString());
        snackbarCubit.showError(
            'Something went wrong updating product, ${event.product.name}');
      }
    }

    if (event is DeleteProduct) {
      final Product product = event.product;

      try {
        await productApiService.deleteProduct(product.id!);
        add(RemoveProduct(productId: product.id!));
      } catch (error) {
        debugPrint(error.toString());
        snackbarCubit
            .showError('Something went wrong deleting, ${product.name}');
      }
    }

    if (event is AddProduct) {
      final Product? foundProduct = state.productList.firstWhereOrNull(
          (Product product) => product.id == event.product.id);
      if (foundProduct != null) return;
      yield state.copyWith(
        productList: List<Product>.from(state.productList)..add(event.product),
      );
    }

    if (event is ReplaceProduct) {
      yield state.copyWith(
        productList: _getUpdatedList(state.productList, event.product),
        searchedProducts:
            _getUpdatedList(state.searchedProducts, event.product),
      );
    }

    if (event is RemoveProduct) {
      yield state.copyWith(
        productList: _removeProductfromList(state.productList, event.productId),
        searchedProducts:
            _removeProductfromList(state.searchedProducts, event.productId),
      );
    }

    if (event is UpdateListOrder) {
      final dynamic data = <String, dynamic>{
        'new_index': event.newIndex,
        'product_id': event.product.id,
      };

      try {
        await productApiService.updateProductDisplayOrder(data);
      } catch (error) {
        debugPrint(error.toString());
        snackbarCubit
            .showError('Something went wrong updating product list order.');
      }
    }

    if (event is RecordSearchedProducts) {
      yield state.copyWith(
        searchedProducts: event.products,
      );
    }

    if (event is UploadImage) {
      try {
        final Response<Product> response =
            await productApiService.uploadImage(event.product.id!, event.image);
        final Product product = response.body!;
        add(ReplaceProduct(product: product));
      } catch (error) {
        debugPrint(error.toString());
        snackbarCubit
            .showError('Something went wrong uploading product image.');
      }
    }

    if (event is SelectProduct) {
      yield state.copyWith(
        selectedProduct: event.product,
      );
    }

    if (event is UnselectProduct) {
      yield state.copyWith(
        selectedProduct: null,
      );
    }

    yield* _yieldVariationEvents(event);
  }

  Stream<ProductState> _yieldVariationEvents(ProductEvent event) async* {
    if (event is CreateVariation) {
      try {
        final Response<ProductVariation> response = await variationApiService!
            .createProductVariation(event.productVariation.toJson());

        final ProductVariation variation = response.body!;

        yield state.copyWith(
          submitStatus: const ApiState.done(),
        );

        add(AddVariation(productVariation: variation));
        snackbarCubit
            .showSuccess('${variation.name} has been created successfully');
      } catch (error) {
        snackbarCubit
            .showError('Something went wrong create a new product variation');
      }
    }

    if (event is AddVariation) {
      final ProductVariation? foundVariation = state.selectedProduct!.variations
          .firstWhereOrNull((ProductVariation variation) =>
              variation.id == event.productVariation.id);
      if (foundVariation != null) return;

      final Product product = state.selectedProduct!.copyWith(
          variations:
              List<ProductVariation>.from(state.selectedProduct!.variations)
                ..add(event.productVariation));
      updateSelectedProductAndProductList(product);
    }

    if (event is DeleteVariation) {
      try {
        await variationApiService!
            .deleteProductVariation(event.productVariation.id!);
        add(RemoveVariation(variationId: event.productVariation.id!));
      } catch (error) {
        debugPrint(error.toString());
        snackbarCubit.showError(
            'Something went wrong deleting, ${event.productVariation.name}');
      }
    }

    if (event is UpdateVariation) {
      final ProductVariation variation = event.productVariation;
      final Map<String, dynamic> data = variation.toJson();

      try {
        final Response<ProductVariation> response = await variationApiService!
            .updateProductVariation(variation.id!, data);
        final ProductVariation updatedVariation = response.body!;
        add(ReplaceVariation(productVariation: updatedVariation));
        snackbarCubit.showSuccess('${variation.name} updated successfully');
      } catch (error) {
        debugPrint(error.toString());
        snackbarCubit
            .showError('Something went wrong updating, ${variation.name}');
      }
    }

    if (event is ReplaceVariation) {
      final Product product = state.selectedProduct!.copyWith(
          variations: ProductVariation.getUpdatedVariations(
              state.selectedProduct!.variations, event.productVariation));
      updateSelectedProductAndProductList(product);
    }

    if (event is RemoveVariation) {
      final Product product = state.selectedProduct!.copyWith(
        variations: _removeVariationFromList(event.variationId),
      );
      updateSelectedProductAndProductList(product);
    }

    if (event is UpdateVariationOrder) {
      final dynamic data = <String, dynamic>{
        'new_index': event.newIndex,
        'variation_id': event.variation.id,
      };

      try {
        await variationApiService!.updateOrder(data);
        final Product product = state.selectedProduct!.copyWith(
          variations: _updateVariationList(event.newIndex, event.variation),
        );
        updateSelectedProductAndProductList(product);
      } catch (error) {
        debugPrint(error.toString());
        snackbarCubit
            .showError('Something went wrong updating variation list order.');
      }
    }
  }

  // Helper functions
  //
  List<ProductVariation> _updateVariationList(
      int newIndex, ProductVariation variation) {
    final List<ProductVariation> updatedList =
        _removeVariationFromList(variation.id!);
    updatedList.insert(newIndex, variation);
    return updatedList;
  }

  void updateSelectedProductAndProductList(Product product) {
    add(ReplaceProduct(product: product));
    add(SelectProduct(product: product));
  }

  List<ProductVariation> _removeVariationFromList(int variationId) {
    return List<ProductVariation>.from(state.selectedProduct!.variations)
      ..removeWhere(
          (ProductVariation variation) => variation.id == variationId);
  }

  List<Product> _getUpdatedList(List<Product> products, Product newProduct) {
    return List<Product>.from(products)
        .map((Product prod) => prod.id == newProduct.id ? newProduct : prod)
        .toList();
  }

  List<Product> _removeProductfromList(List<Product> products, int productId) {
    return List<Product>.from(products)
      ..removeWhere((Product product) => product.id == productId);
  }

  List<Product> filterProductItems(FilterType type) {
    if (type == FilterType.PUBLISHED) {
      return state.productList
          .where((Product item) => item.isPublished)
          .toList();
    }

    if (type == FilterType.DRAFT) {
      return state.productList
          .where((Product item) => !item.isPublished)
          .toList();
    }
    return state.productList;
  }

  Future<List<Product>> searchProducts(String query) async {
    final Map<String, String> queryMap = <String, String>{
      'name': query,
    };

    List<Product> results = <Product>[];
    try {
      final Response<Paginate<Product>> response =
          await productApiService.getProducts(query: queryMap);

      final Paginate<Product> paginate = response.body!;
      results = paginate.results;
    } catch (error) {
      debugPrint(error.toString());
    }
    return results;
  }
}
