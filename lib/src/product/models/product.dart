import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:linnet_flutter_app/src/product_option/models/product_option.dart';
import 'package:linnet_flutter_app/src/product_variation/models/product_variation.dart';
import 'package:linnet_flutter_app/src/utils/utils.dart';

part 'product.freezed.dart';
part 'product.g.dart';

@freezed
abstract class Product implements _$Product {
  const factory Product({
    @Default(null) int? id,
    required String name,
    @Default(null) String? description,
    @JsonKey(name: 'unit_price', fromJson: Utils.stringToDouble) 
      @Default(null) double? unitPrice,
    @JsonKey(name: 'unit_price_currency') 
      @Default(null) String? unitPriceCurrency,
    @JsonKey(name: 'is_published') @Default(false) bool isPublished,
    @Default(null) String? image,
    @JsonKey(name: 'image_thumbnail') @Default(null) String? imageThumbnail,
    @Default(<ProductOption>[]) List<ProductOption> options,
    @Default(<ProductVariation>[]) List<ProductVariation> variations,
  }) = _Product;
  const Product._();

  factory Product.fromJson(Map<String, dynamic> json) =>
      _$ProductFromJson(json);

  @override
  String toString() {
    return 'Product(id: $id, name: $name)';
  }

}
