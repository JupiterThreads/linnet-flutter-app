import 'package:flutter/material.dart';
import 'package:linnet_flutter_app/src/common/widgets/item_row_title.dart';
import 'package:linnet_flutter_app/src/common/widgets/trailing_image_container.dart';
import 'package:linnet_flutter_app/src/product/models/product.dart';

// try in list and then in search list by passing the handling of hte bottom sheet

class ProductRow extends StatelessWidget {
  const ProductRow({required this.product, 
    required this.handleShowingBottomSheet});

  final Product product;
  final Function(Product product) handleShowingBottomSheet;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      dense: true,
      title: ItemRowTitle(
        name: product.name,
      ),
      subtitle:
          product.description != null ? Text(product.description!) : null,
      trailing: product.imageThumbnail != null && product.image != null
          ? TrailingImageContainer(
              imageThumbnail: product.imageThumbnail!, image: product.image!)
          : null,
      onTap: () => handleShowingBottomSheet(product),
    );
  }
}
