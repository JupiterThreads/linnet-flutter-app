import 'package:flutter/material.dart';
import 'package:linnet_flutter_app/src/product_variation/models/product_variation.dart';
import 'package:linnet_flutter_app/src/utils/utils.dart';

class ProductVariationsSheetList extends StatelessWidget {
  const ProductVariationsSheetList(
      {required this.variations, required this.selectedCallback});

  final List<ProductVariation> variations;
  final Function(ProductVariation) selectedCallback;

  // look at old child_items_selected for selected variation
  Widget _buildTitle(ProductVariation variation) {
    return Text(
      variation.name,
      style: const TextStyle(fontWeight: FontWeight.bold),
    );
  }

  Widget _buildTrailing(ProductVariation variation) {
    return Text(Utils.getFormattedAmount(
      variation.unitPriceCurrency!, 
      variation.unitPrice.toString())
    );
  }

  Widget _buildItemRow(BuildContext context, ProductVariation variation) {
    return ListTile(
      onTap: () => selectedCallback(variation),
      contentPadding: EdgeInsets.zero,
      dense: true,
      title: _buildTitle(variation),
      trailing: _buildTrailing(variation),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: ListView.separated(
        shrinkWrap: true,
        itemCount: variations.length,
        separatorBuilder: (BuildContext context, int index) => const Divider(),
        itemBuilder: (BuildContext context, int index) => _buildItemRow(
          context,
          variations[index],
        ),
      ),
    );
  }
}
