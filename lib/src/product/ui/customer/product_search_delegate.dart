import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:linnet_flutter_app/src/common/widgets/loading_indicator.dart';
import 'package:linnet_flutter_app/src/product/bloc/bloc.dart';
import 'package:linnet_flutter_app/src/product/models/product.dart';
import 'package:linnet_flutter_app/src/product/ui/customer/product_row.dart';
import 'package:linnet_flutter_app/src/product/ui/customer/product_variations_sheet.dart';
import 'package:linnet_flutter_app/src/search/search.dart';

class ProductSearchDelegate extends SearchDelegate {
  ProductSearchDelegate({
    required this.searchCubit,
    required this.productBloc,
  }) : super(searchFieldLabel: 'Search name');

  final SearchCubit searchCubit;
  final ProductBloc productBloc;
  PersistentBottomSheetController<dynamic>? _bottomSheetController;


  @override
  ThemeData appBarTheme(BuildContext context) {
    final ThemeData theme = ThemeData();
    return theme.copyWith(
      primaryColor: Colors.white,
      primaryIconTheme: theme.primaryIconTheme.copyWith(color: Colors.grey),
      textTheme: theme.textTheme.copyWith(
        headline6: 
          const TextStyle(fontWeight: FontWeight.normal, fontSize: 16.0),
      ),
    );
  }

  @override
  List<Widget> buildActions(BuildContext context) {
    return <Widget>[
      IconButton(
        icon: const Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      )
    ];
  }
// TODO  look at this
  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: const Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
        // final DraftOrderBloc bloc = BlocProvider.of<DraftOrderBloc>(context);
        // bloc.add(ShowOrderTotal(show: false));

        // Future<dynamic>.delayed(const Duration(milliseconds: 100)).then((_) {
        //   if ((bloc.state?.draftOrderMap?.isNotEmpty ?? false) &&
        //       bloc.state?.tradeAccount?.id ==
        //           productBloc.tradeAccount.id) {
        //     bloc.add(ShowOrderTotal(show: true));
        //   }
        // });
        // return Future<bool>.value(true);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return _performSearch(context);
  }

  // TODO peform search here woudl be good when that cursor bug is fixed
  @override
  Widget buildSuggestions(BuildContext context) {
    return Container();
  }

  Widget _performSearch(BuildContext context) {
    query = query.trim();
    if (query.isEmpty) {
      return Container();
    }

    searchCubit.performSearch(query);
    return BlocBuilder<SearchCubit, SearchState>(
        bloc: searchCubit,
        builder: (BuildContext context, SearchState state) {
          return state.when(
              initial: () => Container(),
              loading: () => LoadingIndicator(),
              populated: (List<dynamic> items) => 
                _buildProductListView(items as List<Product>),
              empty: () => _buildFeedback('No items found'),
              error: () => _buildFeedback('Error Occurred'));
        });
  }

  Widget _buildFeedback(String message) {
    return Center(child: Text(message));
  }

   void _handleShowingBottomSheet(BuildContext context, Product product) {
    _bottomSheetController = showBottomSheet(
        backgroundColor: Colors.transparent,
        context: context,
        builder: (BuildContext builder) {
          return ProductVariationsSheet(product: product);
        });
  }

  Widget _buildProductListView(List<Product> foundItems) {
      return ListView.separated(
        separatorBuilder: (BuildContext context, int index) => Divider(),
        itemCount: foundItems.length,
        itemBuilder: (BuildContext context, int index) => ProductRow(
          product: foundItems[index],
          handleShowingBottomSheet: (product) => 
            _handleShowingBottomSheet(context, product),
        ),
      );
  }
}
