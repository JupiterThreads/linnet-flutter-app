import 'package:flutter/material.dart';
import 'package:linnet_flutter_app/src/product/models/product.dart';
import 'package:linnet_flutter_app/src/product/ui/customer/product_variations_sheet_list.dart';
import 'package:linnet_flutter_app/src/product_variation/models/product_variation.dart';

class ProductVariationsSheet extends StatefulWidget {
  const ProductVariationsSheet({required this.product});

  final Product product;

  @override
  _ProductVariationsSheetState createState() => _ProductVariationsSheetState();
}

class _ProductVariationsSheetState extends State<ProductVariationsSheet> {
  Product get _product => widget.product;
  

  void _handleVariationSelected(ProductVariation variation) {
    print('${variation.name} is selected');
  }

  Widget _buildControls() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        IconButton(
          disabledColor: Theme.of(context).primaryColor.withOpacity(0.5),
          icon: Icon(Icons.remove),
          onPressed: () {},
        ),
        Text('10', style: const TextStyle(fontSize: 30.0)),
        IconButton(
          icon: Icon(Icons.add),
          onPressed: () {},
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        constraints: BoxConstraints(
          minHeight: 250,
          maxHeight: MediaQuery.of(context).size.height - 200,
        ),
        padding: const EdgeInsets.all(16.0),
        decoration: const BoxDecoration(
          color: Color(0xffF5F5F5),
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(20.0), topLeft: Radius.circular(20.0)),
        ),
        child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Flexible(
                child:
                    Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                  Text(
                    _product.name,
                    style: const TextStyle(fontWeight: FontWeight.bold),
                  ),
                  if (_product.variations.isNotEmpty)
                      ProductVariationsSheetList(
                        selectedCallback: _handleVariationSelected,
                        variations: _product.variations,
                      )
                ]),
              ),
              const SizedBox(height: 8.0),
              Container(
                height: 32.0,
                child:  _buildControls(),
              ),
              const SizedBox(height: 8.0),
            ]));
  }
}
