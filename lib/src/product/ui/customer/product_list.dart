import 'package:flutter/material.dart';
import 'package:linnet_flutter_app/src/product/models/product.dart';
import 'package:linnet_flutter_app/src/common/widgets/widgets.dart'
    show ItemRowTitle, TrailingImageContainer;
import 'package:linnet_flutter_app/src/product/ui/customer/product_row.dart';
import 'package:linnet_flutter_app/src/product/ui/customer/product_variations_sheet.dart';

// TODO add draft order bloc provider
class ProductList extends StatefulWidget {
  const ProductList({required this.productList, Key? key}) : super(key: key);
  final List<Product> productList;

  @override
  _ProductListState createState() => _ProductListState();
}

class _ProductListState extends State<ProductList> {
  List<Product> get _productList => widget.productList;
  PersistentBottomSheetController<dynamic>? _bottomSheetController;

  @override
  Widget build(BuildContext context) {
    return SliverPadding(
      padding: const EdgeInsets.only(bottom: 20),
      sliver: SliverList(
          delegate: SliverChildBuilderDelegate(
        (BuildContext context, int index) {
          return _buildItemRow(context, _productList[index]);
        },
        childCount: _productList.length,
      )),
    );
  }

  Widget _buildItemRow(BuildContext context, Product product) {
    return Column(
      children: [
        ProductRow(
            product: product,
            handleShowingBottomSheet: _handleShowingBottomSheet),
        const Divider(indent: 16, endIndent: 16),
      ],
    );
  }

  void _handleShowingBottomSheet(Product product) {
    _bottomSheetController = showBottomSheet(
        backgroundColor: Colors.transparent,
        context: context,
        builder: (BuildContext builder) {
          return ProductVariationsSheet(product: product);
        });
  }
}
