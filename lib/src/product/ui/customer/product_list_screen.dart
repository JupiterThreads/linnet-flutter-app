import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:linnet_flutter_app/src/common/models/api_state.dart';
import 'package:linnet_flutter_app/src/common/widgets/loading.dart';
import 'package:linnet_flutter_app/src/common/widgets/loading_item_list.dart';
import 'package:linnet_flutter_app/src/common/widgets/sliver_app_bar_container.dart';
import 'package:linnet_flutter_app/src/product/bloc/bloc.dart';
import 'package:linnet_flutter_app/src/product/ui/customer/product_list.dart';
import 'package:linnet_flutter_app/src/product/ui/customer/product_search_delegate.dart';
import 'package:linnet_flutter_app/src/search/cubit/cubit.dart';

class ProductListScreen extends StatefulWidget {

  @override
  _ProductListScreenState createState() => _ProductListScreenState();
}

class _ProductListScreenState extends State<ProductListScreen> {
  late ProductBloc _productBloc;

  @override
  void initState() {
    super.initState();
    _productBloc = BlocProvider.of<ProductBloc>(context);
    _productBloc.add(const FetchProductList());
  }

  Future<void> showSearchPage(BuildContext context) async {
    SearchCubit searchCubit;

    searchCubit = SearchCubit(
      callback: (String query) => _productBloc.searchProducts(query));

    await showSearch(
        context: context,
        delegate: ProductSearchDelegate(
          searchCubit: searchCubit,
          productBloc: _productBloc,
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SliverAppBarContainer(
        titleText: _productBloc.category.name,
        actions: context.watch<ProductBloc>().state.productList.isNotEmpty 
          ? <Widget>[
            IconButton(
              icon: const Icon(Icons.search), 
              onPressed: () => showSearchPage(context),
            ),
          ] : <Widget>[],
        widgets:<Widget>[
          BlocBuilder<ProductBloc, ProductState>(
            builder: (BuildContext context, ProductState state) {
              return state.productListState.when(
                done: (_) => state.productList.isNotEmpty 
                  ? ProductList(productList: state.productList)
                  : const SliverFillRemaining(
                    child: Center(child: Text('No products'))),
                loading: (_) => 
                  const SliverFillRemaining(child: LoadingItemList()),
                error: (String? message) => SliverFillRemaining(
                  child: Center(child: Text(message ?? ''))),
              );
            }),
            if (context.watch<ProductBloc>()
              .state.productNextListState is ApiStateLoading)
            const SliverToBoxAdapter(child: Loading()),
          ]),
        );
  }
}


