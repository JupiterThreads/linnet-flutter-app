import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:linnet_flutter_app/src/category/category.dart';
import 'package:linnet_flutter_app/src/product/bloc/bloc.dart';
import 'package:linnet_flutter_app/src/product_variation/bloc/bloc.dart';
import 'package:linnet_flutter_app/src/services/product_api_service.dart';
import 'package:linnet_flutter_app/src/snackbar/snackbar.dart';
import 'package:linnet_flutter_app/src/trader/models/trade_account.dart';
import 'package:linnet_flutter_app/src/user/blocs/user/user_bloc.dart';
import 'package:linnet_flutter_app/src/product/ui/customer/customer.dart'
    as customer_ui_lib;
import 'package:linnet_flutter_app/src/product/ui/trader/trader.dart'
    as trader_ui_lib;
import 'package:provider/provider.dart';

class ProductHomeScreen extends StatefulWidget {
  const ProductHomeScreen({
    required this.tradeAccount,
    required this.category,
  });

  final Category category;
  final TradeAccount tradeAccount;

  @override
  _ProductHomeScreenState createState() => _ProductHomeScreenState();
}

class _ProductHomeScreenState extends State<ProductHomeScreen> {
  late ProductBloc _productBloc;
  late VariationBloc _variationBloc;
  TradeAccount get _tradeAccount => widget.tradeAccount;
  Category get _category => widget.category;
  late ProductApiService service;
  late UserBloc _userBloc;

  @override
  void initState() {
    super.initState();
    service = ProductApiService.create(_tradeAccount.id!, _category.id!);
    _userBloc = BlocProvider.of<UserBloc>(context);
    _variationBloc = VariationBloc();
    _productBloc = ProductBloc(
        variationBloc: _variationBloc,
        category: _category,
        tradeAccount: _tradeAccount,
        snackbarCubit: BlocProvider.of<SnackbarCubit>(context),
        productApiService: service);
  }

  @override
  void dispose() {
    service.client.dispose();
    _productBloc.close();
    _variationBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        BlocProvider<ProductBloc>(
          create: (BuildContext context) => _productBloc,
        ),
        BlocProvider<VariationBloc>(
            create: (BuildContext context) => _variationBloc),
      ],
      child: _buildListView(),
    );
  }

  Widget _buildListView() {
    if (_userBloc.state.traderMode) {
      return trader_ui_lib.ProductListScreen();
    }
    return customer_ui_lib.ProductListScreen();
  }
}
