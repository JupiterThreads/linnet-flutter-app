import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:linnet_flutter_app/src/common/widgets/loading_indicator.dart';
import 'package:linnet_flutter_app/src/product/bloc/bloc.dart';
import 'package:linnet_flutter_app/src/product/models/product.dart';
import 'package:linnet_flutter_app/src/product/ui/trader/product_row.dart';
import 'package:linnet_flutter_app/src/search/search.dart';

class ProductSearchDelegate extends SearchDelegate<dynamic> {
  ProductSearchDelegate(
      {required this.searchCubit, required this.productBloc,
      required this.editCallback, required this.deleteCallback})
      : super(searchFieldLabel: 'Search name');

  final SearchCubit searchCubit;
  final ProductBloc productBloc;
  final Function(Product, BuildContext) editCallback;
  final Function(Product) deleteCallback;

  @override
  ThemeData appBarTheme(BuildContext context) {
    final ThemeData theme = ThemeData();
    return theme.copyWith(
      primaryColor: Colors.white,
      primaryIconTheme: theme.primaryIconTheme.copyWith(color: Colors.grey),
      textTheme: theme.textTheme.copyWith(
        headline6: 
          const TextStyle(fontWeight: FontWeight.normal, fontSize: 16.0),
      ),
    );
  }

  @override
  List<Widget> buildActions(BuildContext context) {
    return <Widget>[
      IconButton(
        icon: const Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: const Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return _performSearch(context);
  }

  // TODO would be nice to perform search here as well, but bug with flutter
  // and cursor moving to start
  @override
  Widget buildSuggestions(BuildContext context) {
    return Container();
  }

  Widget _performSearch(BuildContext context) {
    query = query.trim();
    if (query.isEmpty) {
      return Container();
    }

    searchCubit.performSearch(query);
    return BlocBuilder<SearchCubit, SearchState>(
        bloc: searchCubit,
        builder: (BuildContext context, SearchState state) {
          return state.when(
              initial: () => Container(),
              loading: () => LoadingIndicator(),
              populated: (List<dynamic> items) {
                productBloc.add(
                    RecordSearchedProducts(products: items as List<Product>));
                return _buildProductListView();
              },
              empty: () => _buildFeedback('No items found'),
              error: () => _buildFeedback('Error occurred'));
        });
  }

  Widget _buildFeedback(String message) {
    return Center(child: Text(message));
  }

  Widget _buildProductListView() {
    return BlocBuilder<ProductBloc, ProductState>(
        bloc: productBloc,
        builder: (BuildContext context, ProductState state) {
          final SlidableController slidableController = SlidableController();
          return ListView.builder(
            itemCount: state.searchedProducts.length,
            itemBuilder: (BuildContext context, int index) => ProductRow(
              product: state.searchedProducts[index],
              deleteCallback: deleteCallback,
              editCallback: editCallback,
              slidableController: slidableController,
            ),
          );
        });
  }
}
