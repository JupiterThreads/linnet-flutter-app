import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:linnet_flutter_app/src/common/widgets/delete_item_dialog.dart';
import 'package:linnet_flutter_app/src/product/models/product.dart';

class ProductRow extends StatelessWidget {
  const ProductRow(
      {required this.product,
      required this.slidableController,
      required this.deleteCallback,
      required this.editCallback,
      this.hasWritePerm = false});

  final Product product;
  final SlidableController slidableController;
  final Function(Product, BuildContext) editCallback;
  final Function(Product) deleteCallback;
  final bool hasWritePerm;

  @override
  Widget build(BuildContext context) {
    return Slidable(
      key: Key(product.id.toString()),
      controller: slidableController,
      actionPane: const SlidableDrawerActionPane(),
      secondaryActions: <Widget>[
        IconSlideAction(
          caption: 'Edit',
          color: Theme.of(context).accentColor,
          icon: Icons.edit,
          onTap: () => editCallback(product, context),
        ),
        if (hasWritePerm)
        IconSlideAction(
          caption: 'Delete',
          color: Colors.red,
          icon: Icons.delete,
          onTap: () async {
            final dynamic result = await _showDialog(context, product);
            if (result == true) {
              deleteCallback(product);
            }
          },
        ),
      ],
      child: ListTile(
        dense: true,
        key: Key(product.id.toString()),
        title: Text(product.name,
            style: const TextStyle(fontWeight: FontWeight.bold)),
        subtitle: Text(product.description ?? ''),
        trailing: const Icon(Icons.drag_handle),
      ),
    );
  }

  Future<dynamic> _showDialog(BuildContext context, Product product) async {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return DeleteItemDialog(
          itemName: product.name,
          title: 'Delete Product Item',
        );
      },
    );
  }
}
