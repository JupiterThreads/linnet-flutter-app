import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:linnet_flutter_app/constants.dart';
import 'package:linnet_flutter_app/src/common/models/api_state.dart';
import 'package:linnet_flutter_app/src/common/widgets/filter_popup_menu.dart';
import 'package:linnet_flutter_app/src/common/widgets/loading.dart';
import 'package:linnet_flutter_app/src/product/bloc/bloc.dart';
import 'package:linnet_flutter_app/src/product/models/product.dart';
import 'package:linnet_flutter_app/src/product_detail/ui/product_detail.dart';
import 'package:linnet_flutter_app/src/product/ui/trader/product_row.dart';
import 'package:linnet_flutter_app/src/product/ui/trader/product_search_delegate.dart';
import 'package:linnet_flutter_app/src/product_variation/bloc/variation_bloc.dart';
import 'package:linnet_flutter_app/src/providers/socket_provider.dart';
import 'package:linnet_flutter_app/src/search/cubit/search_cubit.dart';
import 'package:linnet_flutter_app/src/user/blocs/user/user_bloc.dart';
import 'package:linnet_flutter_app/src/websockets/event_types/product_event_types.dart';
import 'package:provider/provider.dart';
import 'package:reorderables/reorderables.dart';

class ProductList extends StatefulWidget {
  const ProductList({required this.productList});
  final List<Product> productList;

  @override
  _ProductListState createState() => _ProductListState();
}

class _ProductListState extends State<ProductList> {
  late ProductBloc _productBloc;
  late VariationBloc _variationBloc;
  final SlidableController slidableController = SlidableController();
  late ScrollController _scrollController;
  FilterType filterType = FilterType.ALL;
  late SocketProvider socketProvider;
  List<Product> get _productList => _productBloc.filterProductItems(filterType);

  @override
  void initState() {
    super.initState();
    _productBloc = BlocProvider.of<ProductBloc>(context);
    _variationBloc = BlocProvider.of<VariationBloc>(context);
    socketProvider = Provider.of<SocketProvider>(context, listen: false);
    socketProvider.addListener(socketListener);
  }

  @override
  void dispose() {
    _scrollController.dispose();
    socketProvider.removeListener(socketListener);
    super.dispose();
  }

  void _scrollListener() {
    if (_scrollController.offset >=
        _scrollController.position.maxScrollExtent) {
      if (_productBloc.state.nextQuery != null &&
          _productBloc.state.productNextListState is! ApiStateLoading) {
        _productBloc.add(const FetchNextProductList());
      }
    }
  }

  void socketListener() {
    final dynamic data = socketProvider.data;
    final String type = data['type'] as String;
    final String categoryId = data['category_id'] as String;

    if (categoryId != _productBloc.category.id) return;

    switch (type) {
      case UPDATE_PRODUCT_DISPLAY_ORDER:
        _updateDispayOrderFromSocket(data['payload']);
        break;
      default:
        return;
    }
  }

  void _updateDispayOrderFromSocket(dynamic data) {
    final int newIndex = data['new_index'] as int;
    final int oldIndex = data['old_index'] as int;
    _onReorder(oldIndex, newIndex);
  }

  void _onReorder(int oldIndex, int newIndex, {bool saveAndBroadcast = false}) {
    setState(() {
      final Product row = _productList.removeAt(oldIndex);
      _productList.insert(newIndex, row);
      if (saveAndBroadcast) {
        _productBloc.add(UpdateListOrder(product: row, newIndex: newIndex));
      }
    });
  }

  Future<void> showSearchPage(BuildContext context) async {
    SearchCubit searchCubit;

    searchCubit = SearchCubit(
        callback: (String query) => _productBloc.searchProducts(query));

    await showSearch(
        context: context,
        delegate: ProductSearchDelegate(
          searchCubit: searchCubit,
          productBloc: _productBloc,
          editCallback: _editProduct,
          deleteCallback: _deleteProduct,
        ));
  }

  void _editProduct(Product product, BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute<Widget>(
        fullscreenDialog: true,
        builder: (BuildContext context) {
          return MultiBlocProvider(
            providers: [
              BlocProvider<ProductBloc>.value(value: _productBloc),
              BlocProvider<VariationBloc>.value(
                value: _variationBloc,
              )
            ],
            child: ProductDetail(actionType: ActionType.EDIT, product: product),
          );
        }));
  }

  void _deleteProduct(Product product) {
    _productBloc.add(DeleteProduct(product: product));
  }

  // @override
  // Widget loader() {
  //   return const Loading(message: 'Creating product item...');
  // }

  @override
  Widget build(BuildContext context) {
    _scrollController =
        PrimaryScrollController.of(context) ?? ScrollController();
    _scrollController.addListener(_scrollListener);

    final bool hasWritePerm = context
        .watch<UserBloc>()
        .state
        .user
        !.hasWritePermission(_productBloc.tradeAccount);
    return BlocListener<ProductBloc, ProductState>(
      bloc: _productBloc,
      listener: (BuildContext context, ProductState state) {
        // state.submitStatus.when(
        //     done: (_) => stopLoading(),
        //     loading: (_) => startLoading(),
        //     error: (_) => stopLoading());
      },
      child: CustomScrollView(
        controller: _scrollController,
        slivers: <Widget>[
          SliverAppBar(
            floating: true,
            title: Text(_productBloc.category.name),
            actions: <Widget>[
              if (context.watch<ProductBloc>().state.productList.isNotEmpty)
                FilterPopupMenu(
                    onSelected: (FilterType type) =>
                        setState(() => filterType = type)),
              if (context.watch<ProductBloc>().state.productList.isNotEmpty)
                IconButton(
                  tooltip: 'Search',
                  icon: const Icon(Icons.search),
                  onPressed: () => showSearchPage(context),
                )
            ],
          ),
          if (_productList.isNotEmpty)
            ReorderableSliverList(
              enabled: hasWritePerm,
              delegate: ReorderableSliverChildBuilderDelegate(
                  (BuildContext context, int index) {
                return ProductRow(
                  hasWritePerm: hasWritePerm,
                  product: _productList[index],
                  editCallback: _editProduct,
                  deleteCallback: _deleteProduct,
                  slidableController: slidableController,
                );
              }, childCount: _productList.length),
              onReorder: (int old, int newIndex) =>
                  _onReorder(old, newIndex, saveAndBroadcast: true),
            ),
          if (context.watch<ProductBloc>().state.productNextListState
              is ApiStateLoading)
            const SliverToBoxAdapter(child: Loading()),
          if (_productList.isEmpty)
            const SliverFillRemaining(
              child: Center(child: Text('No products')),
            )
        ],
      ),
    );
  }
}
