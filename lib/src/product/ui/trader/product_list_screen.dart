import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:linnet_flutter_app/constants.dart';
import 'package:linnet_flutter_app/src/common/widgets/sliver_app_bar_container.dart';
import 'package:linnet_flutter_app/src/common/widgets/widgets.dart';
import 'package:linnet_flutter_app/src/product/bloc/bloc.dart';
import 'package:linnet_flutter_app/src/product/models/product.dart';
import 'package:linnet_flutter_app/src/product_detail/ui/product_detail.dart';
import 'package:linnet_flutter_app/src/product/ui/trader/product_list.dart';
import 'package:linnet_flutter_app/src/product_variation/bloc/variation_bloc.dart';
import 'package:linnet_flutter_app/src/providers/socket_provider.dart';
import 'package:linnet_flutter_app/src/user/blocs/blocs.dart';
import 'package:linnet_flutter_app/src/websockets/event_types/product_event_types.dart';
import 'package:provider/provider.dart';

class ProductListScreen extends StatefulWidget {
  @override
  _ProductListScreenState createState() => _ProductListScreenState();
}

class _ProductListScreenState extends State<ProductListScreen> {
  late ProductBloc _productBloc;
  late VariationBloc _variationBloc;
  FilterType filterType = FilterType.ALL;
  late SocketProvider socketProvider;

  @override
  void initState() {
    super.initState();
    _productBloc = BlocProvider.of<ProductBloc>(context);
    _variationBloc = BlocProvider.of<VariationBloc>(context);
    _productBloc.add(const FetchProductList());
    socketProvider = Provider.of<SocketProvider>(context, listen: false);
    socketProvider.addListener(socketListener);
  }

  void socketListener() {
    final dynamic data = socketProvider.data;
    final String type = data['type'] as String;
    final String categoryId = data['category_id'] as String;

    if (categoryId != _productBloc.category.id) return;

    switch (type) {
      case ADD_PRODUCT:
        _productBloc.add(AddProduct(
            product:
                Product.fromJson(data['payload'] as Map<String, dynamic>)));
        break;
      case UPDATE_PRODUCT:
        _productBloc.add(ReplaceProduct(
            product:
                Product.fromJson(data['payload'] as Map<String, dynamic>)));
        break;
      case REMOVE_PRODUCT:
        _productBloc.add(RemoveProduct(productId: data['payload'] as int));
        break;
      default:
        return;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<ProductBloc, ProductState>(
          builder: (BuildContext context, ProductState state) {
        return state.productListState.when(
          done: (_) => ProductList(productList: state.productList),
          loading: (_) => SliverAppBarContainer(
              titleText: _productBloc.category.name,
              widgets: <Widget>[
                const SliverFillRemaining(
                    child: LoadingItemList(traderView: true)),
              ]),
          error: (String? message) => SliverAppBarContainer(
            titleText: _productBloc.category.name,
            widgets: <Widget>[
              SliverFillRemaining(child: Center(child: Text(message ?? '')))
            ],
          ),
        );
      }),
      floatingActionButton: context
              .watch<UserBloc>()
              .state
              .user
              !.hasWritePermission(_productBloc.tradeAccount)
          ? FloatingActionButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute<Widget>(
                    fullscreenDialog: true,
                    builder: (BuildContext context) {
                      return MultiBlocProvider(
                        providers: <BlocProvider>[
                          BlocProvider<ProductBloc>.value(
                            value: _productBloc,
                          ),
                          BlocProvider<VariationBloc>.value(
                            value: _variationBloc,
                          )
                        ],
                        child:
                            const ProductDetail(actionType: ActionType.CREATE),
                      );
                    }));
              },
              child: const Icon(Icons.add),
            )
          : null,
    );
  }
}
