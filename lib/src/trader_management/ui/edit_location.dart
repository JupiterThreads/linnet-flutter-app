import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:linnet_flutter_app/src/common/widgets/trader_location_picker.dart';
import 'package:linnet_flutter_app/src/place/place.dart';
import 'package:linnet_flutter_app/src/services/trader_api_service.dart';
import 'package:linnet_flutter_app/src/snackbar/snackbar.dart';
import 'package:linnet_flutter_app/src/trader/trader.dart';
import 'package:linnet_flutter_app/src/user/user.dart';
import 'package:linnet_flutter_app/src/utils/utils.dart';
import 'package:provider/provider.dart';

class EditLocation extends StatefulWidget {
  const EditLocation({Key? key}) : super(key: key);

  @override
  _EditLocationState createState() => _EditLocationState();
}

class _EditLocationState extends State<EditLocation> {
  late TradeAccount _tradeAccount;
  late UserBloc _userBloc;
  LatLng? _location;
  late TraderApiService _traderApiService;
  late SnackbarCubit _snackbarCubit;

  @override
  void initState() {
    super.initState();
    _userBloc = BlocProvider.of<UserBloc>(context);
    _tradeAccount = _userBloc.state.selectedAccount as TradeAccount;
    _traderApiService = Provider.of<TraderApiService>(context, listen: false);
    _snackbarCubit = BlocProvider.of<SnackbarCubit>(context);

    setState(() => _location = _tradeAccount.location!.latLng);
  }

  void _onLocationChanged(LocationResult locationResult) {
    setState(() => _location = locationResult.latLng);
  }

  void _performSave() {
    if (_location == null) return;
    final Map<String, dynamic> data = <String, dynamic>{
      'location': Utils.getLocationCoordinates(_location!)
    };
    _traderApiService
        .partialUpdate(_tradeAccount.id!, data)
        .then((Response<TradeAccount> response) {
      final TradeAccount account = response.body!;
      _userBloc.add(UpdateTradeAccountLists(account: account));
      _snackbarCubit.showSuccess('Location updated successfully.');
      Navigator.pop(context);
    }).catchError((error) {
      debugPrint(error.toString());
      _snackbarCubit.showError('Something went wrong during location update.');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Location'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.check, color: Theme.of(context).accentColor),
            onPressed: _performSave,
            tooltip: 'Save',
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: TraderLocationPicker(
          onLocationChanged: _onLocationChanged,
          tradeLocation: _location,
        ),
      ),
    );
  }
}
