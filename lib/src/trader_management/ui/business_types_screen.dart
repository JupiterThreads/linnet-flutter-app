// import 'package:chopper/chopper.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:linnet_flutter_app/src/common/widgets/widgets.dart'
//     show Loading, CardInfo;
// import 'package:linnet_flutter_app/src/services/trader_api_service.dart';
// import 'package:linnet_flutter_app/src/snackbar/cubit/snackbar_cubit.dart';
// import 'package:linnet_flutter_app/src/trader/trader.dart';
// import 'package:linnet_flutter_app/src/user/user.dart';
// import 'package:linnet_flutter_app/src/utils/utils.dart';
// import 'package:provider/provider.dart';

// class BusinessTypesScreen extends StatefulWidget {
//   @override
//   _BusinessTypesScreenState createState() => _BusinessTypesScreenState();
// }

// class _BusinessTypesScreenState extends State<BusinessTypesScreen> {
//   List<dynamic> optionList = <dynamic>[];
//   List<dynamic> _selectedBussinesTypes = <dynamic>[];
//   List<BusinessType> _allBusinessTypes = <BusinessType>[];
//   late UserBloc _userBloc;
//   late TradeAccount _account;
//   late TraderApiService _traderApiService;
//   late SnackbarCubit _snackbarCubit;

//   @override
//   void initState() {
//     super.initState();
//     _userBloc = BlocProvider.of<UserBloc>(context);
//     _account = _userBloc.state.selectedAccount as TradeAccount;
//     _traderApiService = Provider.of<TraderApiService>(context, listen: false);
//     _snackbarCubit = BlocProvider.of<SnackbarCubit>(context);

//     _selectedBussinesTypes =
//         BusinessType.getBusinessTypeNames(_account.businessTypes);
//     _fetchBusinessTypes();
//   }

//   List<dynamic> _getDisplayList(List<String> names) {
//     return names
//         .map((String name) => {
//               'display': name,
//               'value': name,
//             })
//         .toList();
//   }

//   void _onTypesChanged(List<dynamic> values) {
//     final List<String> types =
//         values.map((dynamic el) => el.toString()).toList();
//     setState(() => _selectedBussinesTypes = types);
//   }

//   List<dynamic> _getBusinessTypesDisplay(List<BusinessType> bTypes) {
//     final List<String> names = BusinessType.getBusinessTypeNames(bTypes);
//     return _getDisplayList(names);
//   }

//   Future<void> _fetchBusinessTypes() async {
//     try {
//       final Response<List<BusinessType>> response =
//           await _traderApiService.getBusinessTypes();
//       List<BusinessType> bTypes =
//           Utils.getResponseBody(response) as List<BusinessType>;

//       setState(() {
//         _allBusinessTypes = bTypes;
//         optionList = _getBusinessTypesDisplay(bTypes);
//       });
//     } catch (error) {
//       debugPrint(error.toString());
//       _snackbarCubit.showError('Something went wrong. Please try again.');
//     }
//   }

//   List<BusinessType> _getSelectedBusinessTypes() {
//     return _allBusinessTypes
//         .where(
//             (BusinessType type) => _selectedBussinesTypes.contains(type.name))
//         .toList();
//   }

//   void _performUpdate() {
//     final List<BusinessType> selectedTypes = _getSelectedBusinessTypes();
//     final Map<String, dynamic> data = <String, dynamic>{
//       'business_types_id':
//           selectedTypes.map((BusinessType type) => type.id).toList(),
//     };

//     _traderApiService
//         .partialUpdate(_account.id, data)
//         .then((Response<TradeAccount> response) {
//       final TradeAccount account =
//           Utils.getResponseBody(response) as TradeAccount;
//       _userBloc.add(UpdateTradeAccountLists(account: account));
//       _snackbarCubit.showSuccess('Business types updated successfully.');
//     }).catchError((error) {
//       debugPrint(error.toString());
//       _snackbarCubit.showError('Somethign went wrong. Please try again.');
//     });
//     Navigator.pop(context);
//   }

//   String? _validateOptions(List values) {
//     if (values == null) return null;
//     if (values.length > 3) {
//       return 'Please select no more than 3 business types';
//     }
//     if (values.isEmpty) {
//       return 'Please select at least one business type';
//     }
//     return null;
//   }

//   Widget _buildMultiSelectForm() {
//     if (optionList.isEmpty) {
//       return Container(
//         child: Loading(message: 'Loading business types...'),
//       );
//     }
//     return Container();
//     // return SingleChildScrollView(
//     //   child: Form(
//     //     child: optionList.isEmpty
//     //         ? const Loading(message: 'Loading business types...')
//     //         : MultiSelectFormField(
//     //             initialValue: _selectedBussinesTypes,
//     //             maxSelections: 3,
//     //             validator: (dynamic values) => _validateOptions(values as List),
//     //             title: const Text('Select up to 3 types'),
//     //             errorText: 'Please select up to 3 options',
//     //             dataSource: optionList,
//     //             textField: 'display',
//     //             valueField: 'value',
//     //             required: true,
//     //             onSaved: (dynamic values) =>
//     //                 _onTypesChanged(values as List<dynamic>),
//     //           ),
//     //   ),
//     // );
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('Business Types'),
//         actions: <Widget>[
//           IconButton(
//             icon: Icon(Icons.check, color: Theme.of(context).accentColor),
//             onPressed: _performUpdate,
//             tooltip: 'Save',
//           ),
//         ],
//       ),
//       body: Padding(
//         padding: const EdgeInsets.all(8.0),
//         child: Form(
//             child: Column(
//           children: <Widget>[
//             const CardInfo(
//                 '''Select up to 3 place types that best describes your business.'''),
//             Expanded(
//               child: Align(
//                 alignment: Alignment.center,
//                 child: Container(
//                   height: 200,
//                   child: _buildMultiSelectForm(),
//                 ),
//               ),
//             ),
//           ],
//         )),
//       ),
//     );
//   }
// }
