import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:linnet_flutter_app/src/services/trader_api_service.dart';
import 'package:linnet_flutter_app/src/snackbar/snackbar.dart';
import 'package:linnet_flutter_app/src/trader/trader.dart';
import 'package:linnet_flutter_app/src/user/user.dart';
import 'package:linnet_flutter_app/src/utils/utils.dart';
import 'package:provider/provider.dart';

class OrderNotesScreen extends StatefulWidget {
  @override
  _OrderNotesScreenState createState() => _OrderNotesScreenState();
}

class _OrderNotesScreenState extends State<OrderNotesScreen> {
  bool _isRequired = false;
  final TextEditingController _controller = TextEditingController();
  late UserBloc _userBloc;
  late TradeAccount _account;
  late TraderApiService _traderApiService;
  late SnackbarCubit _snackbarCubit;

  @override
  void initState() {
    super.initState();
    _controller.addListener(_onLabelChanged);
    _userBloc = BlocProvider.of<UserBloc>(context);
    _account = _userBloc.state.selectedAccount as TradeAccount;
    _traderApiService = Provider.of<TraderApiService>(context, listen: false);
    _snackbarCubit = BlocProvider.of<SnackbarCubit>(context);

    _setNotesLabel();
    _setNotesRequired();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void _setNotesLabel() {
    if (_account.orderNotesLabel?.isNotEmpty ?? false ) {
      _controller.text = _account.orderNotesLabel!;
    }
  }

  void _setNotesRequired() {
    setState(() => _isRequired = _account.orderNotesRequired);
  }

  void _handleRequiredChange(bool val) {
    if (val && _controller.text.trim().isNotEmpty) {
      setState(() => _isRequired = true);
    } else {
      setState(() => _isRequired = false);
    }
  }

  void _onLabelChanged() {
    if (_controller.text.trim().isEmpty) {
      setState(() => _isRequired = false);
    }
  }

  void _performSave() {
    final Map<String, dynamic> data = <String, dynamic>{
      'order_notes_label': _controller.text.trim(),
      'order_notes_required': _isRequired,
    };
    _traderApiService
        .partialUpdate(_account.id!, data)
        .then((Response<TradeAccount> response) {
      final TradeAccount account = response.body!;
      _userBloc.add(UpdateTradeAccountLists(account: account));
      _snackbarCubit.showSuccess('Order notes updated successfully.');

      Navigator.pop(context);
    }).catchError((error) {
      debugPrint(error.toString());
      _snackbarCubit.showError('Something went wrong updating notes.');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Order Notes'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.check, color: Theme.of(context).accentColor),
            onPressed: _performSave,
            tooltip: 'Save',
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TextFormField(
                  keyboardType: TextInputType.text,
                  controller: _controller,
                  maxLines: 2,
                  decoration: const InputDecoration(
                      hintText: 'Order Label', labelText: 'Order Label'),
                ),
                const SizedBox(height: 16.0),
                SwitchListTile(
                  contentPadding: EdgeInsets.zero,
                  value: _isRequired,
                  onChanged: _handleRequiredChange,
                  title: const Text('Required'),
                  subtitle: const Text(
                      '''Enable this if you require your customers to provide a response before placing an order'''),
                ),
              ]),
        ),
      ),
    );
  }
}
