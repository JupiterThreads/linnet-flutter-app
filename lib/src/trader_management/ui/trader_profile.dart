import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' show MultipartFile;
import 'package:linnet_flutter_app/src/common/widgets/linnet_image_picker.dart';
import 'package:linnet_flutter_app/src/common/widgets/widgets.dart';
import 'package:linnet_flutter_app/src/services/trader_api_service.dart';
import 'package:linnet_flutter_app/src/snackbar/snackbar.dart';
import 'package:linnet_flutter_app/src/trader/models/trade_account.dart';
import 'package:linnet_flutter_app/src/user/blocs/user/bloc.dart';
import 'package:linnet_flutter_app/src/utils/utils.dart';
import 'package:provider/provider.dart';

// TODO why is avatar not clearing
// the size of the background image

class TraderProfile extends StatefulWidget {
  const TraderProfile({Key? key, required this.tradeAccount}) : super(key: key);

  final TradeAccount tradeAccount;

  @override
  _TraderProfileState createState() => _TraderProfileState();
}

class _TraderProfileState extends State<TraderProfile> {
  TradeAccount get _tradeAccount => widget.tradeAccount;
  final TextEditingController _nameController = TextEditingController();
  final GlobalKey<FormState> _traderProfileFormKey = GlobalKey<FormState>();
  late UserBloc _userBloc;
  late TraderApiService _traderApiService;
  late SnackbarCubit _snackbarCubit;
  int maxLength = 160;
  Image? _avatarImage;
  Image? _coverImage;

  @override
  void initState() {
    super.initState();
    _userBloc = BlocProvider.of<UserBloc>(context);
    _snackbarCubit = BlocProvider.of<SnackbarCubit>(context);
    _traderApiService = Provider.of<TraderApiService>(context, listen: false);
    _setInitialImages();
  }

  void _setInitialImages() {
    setState(() {
      _nameController.text = _tradeAccount.name;
      if (_tradeAccount.avatar != null) {
        _avatarImage = Image.network(_tradeAccount.avatar!);
      }
      if (_tradeAccount.coverImage != null) {
        _coverImage = Image.network(_tradeAccount.coverImage!);
      }
    });
  }

  bool isNameValid(String? text) {
    return text?.trim().isNotEmpty ?? false;
  }

  void _updateTradeAccount(Map<String, dynamic> data,
      {String? successMessage, String? errorMessage}) {
    _traderApiService
        .partialUpdate(_tradeAccount.id!, data)
        .then((Response<TradeAccount> response) {
      final TradeAccount account = response.body!;
      _userBloc.add(UpdateTradeAccountLists(account: account));
      if (successMessage != null) {
        _snackbarCubit.showSuccess(successMessage);
      }
    }).catchError((error) {
      debugPrint(error.toString());
      if (errorMessage != null) {
        _snackbarCubit.showError(errorMessage);
      }
    });
  }

  void _onDeleteImage(bool isAvatar) {
    final String imageLabel = isAvatar ? 'Avatar' : 'Cover image';

    setState(() {
      if (isAvatar) {
        _avatarImage = null;
      } else {
        _coverImage = null;
      }
    });

    final String fieldName = isAvatar ? 'avatar' : 'cover_image';
    final Map<String, dynamic> data = <String, dynamic>{
      fieldName: null,
    };
    _updateTradeAccount(data,
        errorMessage:
            'Something went wrong removing ${imageLabel.toLowerCase()}');
  }

  Future<void> _saveImage(MultipartFile file, bool isAvatar) async {
    final String imageLabel = isAvatar ? 'Avatar' : 'Cover image';

    try {
      Response<TradeAccount> response;
      if (isAvatar) {
        response =
            await _traderApiService.uploadAvatar(_tradeAccount.id!, file);
      } else {
        response = await _traderApiService.uploadCoverImage(
          _tradeAccount.id!, file);
      }

      final TradeAccount tradeAccount = response.body!;

      _userBloc.add(UpdateTradeAccountLists(account: tradeAccount));
    } catch (error) {
      debugPrint(error.toString());
      _snackbarCubit
          .showError('Something went wrong saving ${imageLabel.toLowerCase()}');
    }
  }

  void _saveForm() {
    final bool formIsValid = _traderProfileFormKey
      .currentState?.validate() ?? false;
    if (!formIsValid) return;
    final Map<String, dynamic> data = <String, dynamic>{
      'name': _nameController.text,
    };
    _updateTradeAccount(data,
        successMessage: 'Trader profile updated successfully',
        errorMessage: 'Something went wrong updating the trader profile');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Trader Profile'),
          actions: <Widget>[
            IconButton(
                onPressed: _saveForm,
                icon: Icon(Icons.check, color: Theme.of(context).accentColor),
                tooltip: 'Save'),
          ],
        ),
        body: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const CardInfo(
                    '''Enter your trading name and add an optional profile picture and cover image'''),
                const SizedBox(height: 8.0),
                Form(
                  key: _traderProfileFormKey,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  child: TextFormField(
                        maxLength: maxLength,
                        autocorrect: false,
                        keyboardType: TextInputType.text,
                        controller: _nameController,
                        validator: (String? text) =>
                            isNameValid(text) ? null : 'Invalid Trader Name',
                        decoration: const InputDecoration(
                          labelText: 'Trader Name',
                        ),
                      ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0, bottom: 32),
                  child: Text('Profile picture', 
                    style: Theme.of(context).textTheme.subtitle1),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 16),
                  child: 
                    LinnetImagePicker(
                      shape: BoxShape.circle,
                      containerHeight: 80,
                      containerWidth: 80,
                      onDeleteImage: () => _onDeleteImage(true),
                      initialImage: _avatarImage,
                      onPickedImage: (MultipartFile mFile, _) =>
                          _saveImage(mFile, true),
                      fieldName: 'avatar',
                    )
                ),
            const Divider(height: 2),
            Padding(
              padding: const EdgeInsets.only(top: 16, bottom: 16),
              child: Text('Cover image', 
                style: Theme.of(context).textTheme.subtitle1),
            ),
            LinnetImagePicker(
              shape: BoxShape.rectangle,
              containerHeight: 200,
              containerWidth: MediaQuery.of(context).size.width,
              initialImage: _coverImage,
              onDeleteImage: () => _onDeleteImage(false),
              onPickedImage: (MultipartFile mFile, _) 
                => _saveImage(mFile, false),
              fieldName: 'cover_image',
              placeholderText: 'Cover Image',
            )
          ]
        )
      )
    );
  }
}
