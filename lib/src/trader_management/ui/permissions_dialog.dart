import 'package:flutter/material.dart';
import 'package:linnet_flutter_app/src/member/models/member.dart';

class PermissionsDialog extends StatefulWidget {
  const PermissionsDialog(
      {Key? key, this.defaultPermission, 
        required this.titleText, 
        required this.buttonText})
      : super(key: key);
  final String? defaultPermission;
  final String titleText;
  final String buttonText;

  @override
  _PermissionsDialogState createState() => _PermissionsDialogState();
}

class _PermissionsDialogState extends State<PermissionsDialog> {
  String selectedPermission = 'read';

  @override
  void initState() {
    super.initState();
    if (widget.defaultPermission != null) {
      setState(() { 
        selectedPermission = widget.defaultPermission!;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: const EdgeInsets.all(8.0),
      title: Text(widget.titleText),
      content: Column(mainAxisSize: MainAxisSize.min, 
      children: <Widget>[
        ...Member.Permissions.map((String perm) => RadioListTile<String>(
                  title: Text(perm.toUpperCase()),
                  subtitle: Text(Member.getPermissionDescription(perm)),
                  value: perm,
                  groupValue: selectedPermission,
                  onChanged: (String? value) =>
                      setState(() => selectedPermission = value!),
                ))
      ]),
      actions: <Widget>[
        TextButton(
          onPressed: () => Navigator.pop(context, null),
          child: Text('CANCEL',
              style: TextStyle(color: Theme.of(context).primaryColor)),
        ),
        TextButton(
          onPressed: () => Navigator.pop(context, selectedPermission),
          child: Text(widget.buttonText),
        ),
      ],
    );
  }
}
