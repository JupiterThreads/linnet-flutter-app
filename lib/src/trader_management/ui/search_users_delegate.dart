import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:linnet_flutter_app/src/common/widgets/loading_indicator.dart';
import 'package:linnet_flutter_app/src/search/search.dart';
import 'package:linnet_flutter_app/src/user/user.dart';

class SearchUsersDelegate extends SearchDelegate<dynamic> {
  SearchUsersDelegate({
    required this.searchCubit,
    String hintText = 'Search...',
  }) : super(
          searchFieldLabel: hintText,
          searchFieldDecorationTheme: InputDecorationTheme(border: InputBorder.none),
          keyboardType: TextInputType.text,
          textInputAction: TextInputAction.search,
        );

  final SearchCubit searchCubit;

  @override
  ThemeData appBarTheme(BuildContext context) {
    final ThemeData theme = ThemeData();
    return theme.copyWith(
      primaryColor: Colors.white,
      primaryIconTheme: theme.primaryIconTheme.copyWith(color: Colors.grey),
      textTheme: theme.textTheme.copyWith(
        headline6: 
          const TextStyle(fontWeight: FontWeight.normal, fontSize: 16.0),
      ),
    );
  }

  @override
  List<Widget> buildActions(BuildContext context) {
    return <Widget>[
      IconButton(
        icon: const Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: const Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    query = query.trim();
    if (query.isEmpty) {
      return Container();
    }

    searchCubit.performSearch(query);
    return BlocBuilder<SearchCubit, SearchState>(
        bloc: searchCubit,
        builder: (BuildContext context, SearchState state) {
          return state.when(
              initial: () => Container(),
              loading: () => LoadingIndicator(),
              populated: (List<dynamic> items) => ListView.builder(
                    itemCount: items.length,
                    itemBuilder: (BuildContext context, int index) =>
                        _buildItemRow(context, items[index] as User),
                  ),
              empty: () => _buildFeedback('No user found'),
              error: () => _buildFeedback('Error occurred'),
              );
        });
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return Container();
  }

  Widget _buildItemRow(BuildContext context, User user) {
    return ListTile(
        title: Text(user.name!),
        subtitle: Text(user.email!),
        trailing: const Icon(Icons.person_add),
        onTap: () {
          close(context, user);
        });
  }

  Widget _buildFeedback(String message) {
    return Center(
      child: Text(message),
    );
  }
}
