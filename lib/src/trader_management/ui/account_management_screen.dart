import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:linnet_flutter_app/routing_constants.dart';
import 'package:linnet_flutter_app/src/services/trader_api_service.dart';
import 'package:linnet_flutter_app/src/snackbar/snackbar.dart';
import 'package:linnet_flutter_app/src/trader/models/business_type.dart';
import 'package:linnet_flutter_app/src/trader/models/trade_account.dart';
import 'package:linnet_flutter_app/src/trader/trader.dart';
import 'package:linnet_flutter_app/src/user/blocs/blocs.dart';
import 'package:linnet_flutter_app/src/utils/utils.dart';
import 'package:provider/provider.dart';

class AccountManagementScreen extends StatefulWidget {
  @override
  _AccountManagementScreenState createState() =>
      _AccountManagementScreenState();
}

class _AccountManagementScreenState extends State<AccountManagementScreen> {
  bool isVisible = false;
  late UserBloc _userBloc;
  late TradeAccount _account;
  late TraderApiService _traderApiService;
  late SnackbarCubit _snackbarCubit;

  @override
  void initState() {
    super.initState();
    _userBloc = BlocProvider.of<UserBloc>(context);
    _account = _userBloc.state.selectedAccount as TradeAccount;
    _traderApiService = Provider.of<TraderApiService>(context, listen: false);
    _snackbarCubit = BlocProvider.of<SnackbarCubit>(context);

    setState(() {
      isVisible = _account.isVisible;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Account Management'),
          leading: IconButton(
              icon: const Icon(Icons.close),
              onPressed: () => Navigator.pop(context)),
        ),
        body: BlocBuilder<UserBloc, UserState>(
            builder: (BuildContext context, UserState state) {
          return ListView(children: <Widget>[
            ListTile(
              title: const Text('Manage Members'),
              onTap: () => Navigator.pushNamed(context, ManageMembersRoute),
              trailing: const Icon(Icons.keyboard_arrow_right),
            ),
            ListTile(
              title: const Text('Trader Profile'),
              onTap: () => Navigator.pushNamed(
                context, TraderProfileRoute, arguments: state.selectedAccount),
              trailing: const Icon(Icons.keyboard_arrow_right),
            ),
            ListTile(
              title: const Text('Bank Details'),
              onTap: () => print('bank details'),
              trailing: const Icon(Icons.keyboard_arrow_right),
            ),
            ListTile(
              title: const Text('Business Types'),
              onTap: () => Navigator.pushNamed(context, BusinessTypesRoute),
              trailing: const Icon(Icons.keyboard_arrow_right),
            ),
            SwitchListTile(
              value: isVisible,
              title: const Text('Enable Visibility'),
              onChanged: _handleVisibleChange,
            ),
            ListTile(
              title: const Text('Location'),
              onTap: () =>
                  Navigator.pushNamed(context, TraderLocationRoute),
              trailing: const Icon(Icons.keyboard_arrow_right),
            ),
            ListTile(
              title: const Text('Order Notes'),
              subtitle: const Text('Handy for table service'),
              onTap: () => Navigator.pushNamed(context, OrderNotesRoute),
              trailing: const Icon(Icons.keyboard_arrow_right),
            ),
          ]);
        }));
  }

  void _handleVisibleChange(bool val) {
    setState(() => isVisible = val);
    final Map<String, dynamic> data = <String, dynamic>{
      'is_visible': isVisible
    };

    final String placeholder = isVisible ? 'visible' : 'invisible';
    final String message = 'Account set to $placeholder.';

    _traderApiService
        .partialUpdate(_account.id!, data)
        .then((Response<TradeAccount> response) {
      final TradeAccount account = response.body!;
      _userBloc.add(UpdateTradeAccountLists(account: account));
      _snackbarCubit.showSuccess(message);
    }).catchError((dynamic error) {
      debugPrint(error.toString());
      _snackbarCubit
          .showError('Something went wrong updating account visibility');
    });
  }
}
