import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:linnet_flutter_app/src/product/models/product.dart';

part 'product_option.freezed.dart';
part 'product_option.g.dart';

@freezed
abstract class ProductOption implements _$ProductOption {

  const factory ProductOption({
    @Default(null) String? id,
    required String title,
    @Default(null) String? description,
    @JsonKey(name: 'selection_type') required int selectionType,
    @JsonKey(name: 'selection_number') required int selectionNumber,
    @JsonKey(name: 'is_published') @Default(false)
    bool isPublished,
    @Default(null) List<Product>? products,
  }) = _ProductOption;
  const ProductOption._();

  factory ProductOption.fromJson(Map<String, dynamic> json) => _$ProductOptionFromJson(json);

}
