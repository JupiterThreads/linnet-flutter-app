import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:linnet_flutter_app/src/user/user.dart';

class SignUpForm extends StatefulWidget {

  const SignUpForm({required  this.changeAuthMode});
  final VoidCallback changeAuthMode;

  @override
  _SignUpFormState createState() => _SignUpFormState();
}

class _SignUpFormState extends State<SignUpForm> {

  FocusNode emailNode = FocusNode();
  FocusNode passwordNode = FocusNode();
  FocusNode nameNode = FocusNode();
  bool visibilityOn = false;

  Map<String, bool> _mapValidateField = <String, bool>{};

  late UserRegisterBloc _userRegisterBloc;
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _nameController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _userRegisterBloc = BlocProvider.of<UserRegisterBloc>(context);
    _nameController.addListener(_onNameChanged);
    _emailController.addListener(_onEmailChanged);
    _passwordController.addListener(_onPasswordChanged);

    _mapValidateField['name'] = false;
    _mapValidateField['email'] = false;
    _mapValidateField['password'] = false;

    nameNode.addListener(() => _onFocusChange(nameNode, 'name'));
    emailNode.addListener(() => _onFocusChange(emailNode, 'email'));
    passwordNode.addListener(() => _onFocusChange(passwordNode, 'password'));
  }

  @override
  Widget build(BuildContext context) {
   return BlocBuilder<UserRegisterBloc, UserRegisterState>(
      builder: (BuildContext context, UserRegisterState state) {

        if (state is FormSubmitted) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }

        return Form(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget> [
                TextFormField(
                  focusNode: nameNode,
                  controller: _nameController,
                  validator: (_) => state.isNameValid ? null : 'Invalid Name',
                  decoration: const InputDecoration(
                    labelText: 'Name',
                  ),
                ),
                TextFormField(
                  controller: _emailController,
                  focusNode: emailNode,
                  keyboardType: TextInputType.emailAddress,
                  validator: (_) => state.isEmailValid ? null : 'Invalid Email',
                  decoration: InputDecoration(
                    suffixIcon: Icon(Icons.email),
                    labelText: 'Email',
                  ),
                ),
                TextFormField(
                  focusNode: passwordNode,
                  controller: _passwordController,
                  obscureText: !visibilityOn,
                  validator: (_) => state.isPasswordValid ? null 
                    : 'Invalid Password',
                  decoration: InputDecoration(
                    suffixIcon: IconButton(
                      onPressed: () => setState(() 
                        => visibilityOn = !visibilityOn),
                      icon: Icon(visibilityOn ? Icons.visibility 
                        : Icons.visibility_off),
                    ),
                    helperMaxLines: 2,
                    helperText: '''Password must contain at least 8 characters and cannot be entirely numeric''',
                    labelText: 'Password',
                    ),
                  ),
                  const SizedBox(height: 20.0),
                  ElevatedButton(
                    onPressed: state.isFormValid ? () => 
                      _userRegisterBloc.add(FormSubmitted()) : null,
                    child: const Text('SIGN UP'),
                  ),
                  TextButton(
                    onPressed: widget.changeAuthMode,
                    child: const Text('LOGIN'),
                  ),
                ]
            ),
          );
        }
      );
    }


  void _onNameChanged() {
    _userRegisterBloc.add(NameChanged(name: _nameController.text.trim()));
  }

  void _onEmailChanged() {
    _userRegisterBloc.add(EmailChanged(email: _emailController.text.trim()));
  }

  void _onPasswordChanged() {
    _userRegisterBloc.add(PasswordChanged(
      password: _passwordController.text.trim()));
  }

  void _updateValidateMap(String key) {
    setState(() => _mapValidateField = 
      _mapValidateField.map((String k, bool v) => 
        MapEntry<String, bool>(k, k == key)));
  }

  void _onFocusChange(FocusNode focusNode, String key) {
    if (focusNode.hasFocus) {
      _updateValidateMap(key);
    }
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    _nameController.dispose();
    super.dispose();
  }

}