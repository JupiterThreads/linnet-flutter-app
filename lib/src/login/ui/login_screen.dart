import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:linnet_flutter_app/src/authentication/bloc/authentication_bloc.dart';
import 'package:linnet_flutter_app/src/login/login.dart';
import 'package:linnet_flutter_app/src/login/ui/login_form.dart';
import 'package:linnet_flutter_app/src/services/auth_api_service.dart';
import 'package:linnet_flutter_app/src/user/blocs/user/bloc.dart';
import 'package:provider/provider.dart';


class LoginScreen extends StatefulWidget {
  const LoginScreen();

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  late AuthApiService authApiService;

  @override
  void initState() {
    authApiService = Provider.of<AuthApiService>(context, listen: false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Log in'),
        ),
        body: BlocProvider<LoginBloc>(
              create: (BuildContext context) => LoginBloc(
                userBloc: BlocProvider.of<UserBloc>(context),
                authApiService: authApiService,
                  authenticationBloc:
                      BlocProvider.of<AuthenticationBloc>(context),
            ),
          child: const Padding(
            padding: EdgeInsets.all(36.0),
            child: LoginForm()
          ),
        ));
  }
}
