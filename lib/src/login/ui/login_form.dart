import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:linnet_flutter_app/routing_constants.dart';
import 'package:linnet_flutter_app/src/authentication/authentication.dart';
import 'package:linnet_flutter_app/src/login/login.dart';
import 'package:linnet_flutter_app/src/snackbar/snackbar.dart';
import 'package:progress_state_button/iconed_button.dart';
import 'package:progress_state_button/progress_button.dart';
import 'package:validatorless/validatorless.dart';

class LoginForm extends StatefulWidget {
  const LoginForm();

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  late LoginBloc _loginBloc;
  bool visibilityOn = false;
  late SnackbarCubit _snackbarCubit;

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final GlobalKey<FormState> _loginForm = GlobalKey<FormState>();
  ButtonState buttonState = ButtonState.idle;

  @override
  void dispose() {
    _passwordController.dispose();
    _emailController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _loginBloc = BlocProvider.of<LoginBloc>(context);
    _snackbarCubit = BlocProvider.of<SnackbarCubit>(context);

    _emailController.addListener(_onEmailChanged);
    _passwordController.addListener(_onPasswordChanged);
  }

  void _onLogin(BuildContext context) {
    Navigator.popUntil(context, ModalRoute.withName(HomeRoute));
  }

  void _onError(String message) {
    setState(() => buttonState = ButtonState.fail);
    _snackbarCubit.showError(message, title: 'Login Error');
  }

  void _submitForm() {
    final bool isValid = _loginForm.currentState?.validate() ?? false;

    if (isValid) {
      setState(() => buttonState = ButtonState.loading);
      _loginBloc.add(const LoginSubmitted());
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginBloc, LoginState>(
        listener: (BuildContext context, LoginState state) {
      state.loginSubmitStatus?.maybeWhen(
          loading: (_) => setState(() => buttonState = ButtonState.loading),
          done: (_) => _onLogin(context),
          error: (String? message) => _onError(message!),
          orElse: () => {});
    }, child: BlocBuilder<LoginBloc, LoginState>(
            builder: (BuildContext context, LoginState state) {
      return Form(
        key: _loginForm,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TextFormField(
              autovalidateMode: AutovalidateMode.onUserInteraction,
              autocorrect: false,
              controller: _emailController,
              validator: Validatorless.multiple([
                Validatorless.email('Invalid Email'),
                Validatorless.required('This is a required field'),
              ]),
              decoration: const InputDecoration(
                suffixIcon: Icon(Icons.email),
                labelText: 'Email',
              ),
            ),
            TextFormField(
              autovalidateMode: AutovalidateMode.onUserInteraction,
              autocorrect: false,
              controller: _passwordController,
              validator: Validatorless.multiple([
                Validatorless.min(
                    5, 'Password must contain at least 5 characters'),
                Validatorless.required('This is a required field'),
              ]),
              decoration: InputDecoration(
                errorMaxLines: 2,
                labelText: 'Password',
                suffixIcon: IconButton(
                  onPressed: () => setState(() => visibilityOn = !visibilityOn),
                  icon: Icon(
                      visibilityOn ? Icons.visibility : Icons.visibility_off),
                ),
              ),
              obscureText: !visibilityOn,
            ),
            const SizedBox(height: 20.0),
            Container(
              width: 200,
              child: buildProgressButton(),
              ),
          ],
        ),
      );
    }));
  }

  void _onPasswordChanged() {
    _loginBloc.add(PasswordChanged(password: _passwordController.text.trim()));
  }

  void _onEmailChanged() {
    _loginBloc.add(EmailChanged(email: _emailController.text.trim()));
  }

  void onPressed() {
    switch (buttonState) {
      case ButtonState.idle:
        _submitForm();
        break;
      case ButtonState.loading:
        break;
      case ButtonState.success:
        buttonState = ButtonState.idle;
        break;
      case ButtonState.fail:
        buttonState = ButtonState.idle;
        break;
    }
    setState(() {
      buttonState = buttonState;
    });
  }

  Widget buildProgressButton() {
    return ProgressButton.icon(iconedButtons: {
      ButtonState.idle: IconedButton(
          text: 'LOGIN',
          icon: Icon(Icons.send, color: Colors.white),
          color: Colors.deepPurple.shade500),
      ButtonState.loading:
          IconedButton(text: 'Loading', color: Colors.deepPurple.shade700),
      ButtonState.fail: IconedButton(
          text: 'Failed',
          icon: const Icon(Icons.cancel, color: Colors.white),
          color: Colors.red.shade300),
      ButtonState.success: IconedButton(
          text: 'Success',
          icon: Icon(
            Icons.check_circle,
            color: Colors.white,
          ),
          color: Colors.green.shade400)
    }, onPressed: onPressed, state: buttonState);
  }


}
