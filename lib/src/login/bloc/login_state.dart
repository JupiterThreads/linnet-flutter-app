import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:linnet_flutter_app/src/common/models/api_state.dart';

part 'login_state.freezed.dart';

@freezed
class LoginState with _$LoginState {
  const factory LoginState({
    @Default('') String email,
    @Default('') String password,
    @Default(null) ApiState? loginSubmitStatus,
  }) = _LoginState;
}
