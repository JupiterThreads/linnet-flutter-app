import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:linnet_flutter_app/src/authentication/authentication.dart';
import 'package:linnet_flutter_app/src/common/models/api_state.dart';
import 'package:linnet_flutter_app/src/services/auth_api_service.dart';
import 'package:linnet_flutter_app/src/user/blocs/user/bloc.dart';
import 'package:linnet_flutter_app/src/utils/utils.dart';
import 'bloc.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc({
    required this.authenticationBloc,
    required this.authApiService,
    required this.userBloc,
  }) : super(const LoginState());

  final AuthenticationBloc authenticationBloc;
  final AuthApiService authApiService;
  final UserBloc userBloc;
  static String clientId = dotenv.env['CLIENT_ID']!;
  static String clientSecret = dotenv.env['CLIENT_SECRET']!;

  @override
  Stream<LoginState> mapEventToState(
    LoginEvent event,
  ) async* {
    if (event is EmailChanged) {
      yield state.copyWith(email: event.email);
    }

    if (event is PasswordChanged) {
      yield state.copyWith(
        password: event.password,
      );
    }

    if (event is LoginSubmitted) {
      yield state.copyWith(loginSubmitStatus: const ApiState.loading());

      try {
        final Response<dynamic> response = await authApiService.getAccessToken(
            state.email, state.password, clientId, clientSecret);

        final Map<String, dynamic> data = response.body as Map<String, dynamic>;

        yield state.copyWith(
          loginSubmitStatus: const ApiState.done(),
        );

        authenticationBloc.add(LoggedIn(tokenData: data));
        userBloc.add(const FetchCurrentUser());
      } catch (err) {
        debugPrint(err.toString());
        yield state.copyWith(
          loginSubmitStatus: ApiState.error(err.toString()),
        );
        yield state.copyWith(
          loginSubmitStatus: null,
        );
      }
    }

    if (event is LoginReset) {
      yield const LoginState();
    }
  }
}
