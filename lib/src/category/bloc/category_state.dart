import 'package:linnet_flutter_app/src/category/models/category.dart';
import 'package:linnet_flutter_app/src/common/models/api_state.dart';
import 'package:meta/meta.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'category_state.freezed.dart';

@freezed
class CategoryState with _$CategoryState {
  const factory CategoryState({
    @Default(<Category> []) List<Category> categories,
    @Default(ApiState.loading()) ApiState categoryListState,
    @Default(null) ApiState? categoryNextListState,
    @Default(ApiState.done()) ApiState submitStatus,
    @Default(null) Map<String, dynamic>? nextQuery,
  }) = _CategoryState;
}
