import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:linnet_flutter_app/src/category/category.dart';
import 'package:linnet_flutter_app/src/trader/trader.dart';
import 'package:http/http.dart' show MultipartFile;

part 'category_event.freezed.dart';

@freezed
class CategoryEvent with _$CategoryEvent {
  const factory CategoryEvent.fetchCategories({
    required TradeAccount tradeAccount,
  }) = FetchCategoryList;
  const factory CategoryEvent.fetchNextCategories() = FetchNextCategoryList;
  const factory CategoryEvent.createCategory({
    required Category category,
    MultipartFile? image,
  }) = CreateCategory;
  const factory CategoryEvent.upateListOrder({
    required Category category,
    required int newIndex,
  }) = UpdateListOrder;
  const factory CategoryEvent.uploadImage(
      {required Category category,
      required MultipartFile image}) = UploadImage;
  const factory CategoryEvent.deleteCategory({
    required Category category,
  }) = DeleteCategory;
  const factory CategoryEvent.updateCategory({required Category category}) =
      UpdateCategory;
  const factory CategoryEvent.partialUpdateCategory({
    required Category category,
    required Map<String, dynamic> data,
  }) = PartialUpdateCategory;
  const factory CategoryEvent.addCategory({
    required Category category,
  }) = AddCategory;
  const factory CategoryEvent.replaceCategory({
    required Category category,
  }) = ReplaceCategory;
  const factory CategoryEvent.removeCategory({
    required int categoryId,
  }) = RemoveCategory;
}
