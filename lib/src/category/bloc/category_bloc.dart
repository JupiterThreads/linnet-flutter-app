import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:linnet_flutter_app/constants.dart';
import 'package:linnet_flutter_app/src/category/category.dart';
import 'package:linnet_flutter_app/src/common/models/api_state.dart';
import 'package:linnet_flutter_app/src/common/models/paginate.dart';
import 'package:linnet_flutter_app/src/services/category_api_service.dart';
import 'package:linnet_flutter_app/src/snackbar/cubit/snackbar_cubit.dart';
import 'package:linnet_flutter_app/src/trader/models/trade_account.dart';
import 'package:linnet_flutter_app/src/utils/utils.dart';
import 'package:linnet_flutter_app/src/category/bloc/category_event.dart';
import 'package:linnet_flutter_app/src/category/bloc/bloc.dart';
import 'package:collection/collection.dart';

class CategoryBloc extends Bloc<CategoryEvent, CategoryState> {
  CategoryBloc({
    required this.categoryApiService,
    required this.snackbarCubit,
    required this.tradeAccount,
  }) : super(const CategoryState());

  CategoryApiService categoryApiService;
  SnackbarCubit snackbarCubit;
  TradeAccount tradeAccount;

  @override
  Stream<CategoryState> mapEventToState(CategoryEvent event) async* {
    if (event is FetchCategoryList) {
      yield state.copyWith(
        categoryListState: const ApiState.loading('Fetching categories...'),
      );
      try {
        final Response<Paginate<Category>> response =
            await categoryApiService.getCategories();
        final Paginate<Category> paginate = response.body!;

        yield state.copyWith(
          categories: paginate.results,
          nextQuery:
              paginate.next != null ? Utils.getParams(paginate.next!) : null,
          categoryListState: const ApiState.done(),
        );
      } catch (error) {
        debugPrint(error.toString());
        yield state.copyWith(
          categoryListState:
              const ApiState.error('Something went wrong fetching categories'),
        );
      }
    }

    if (event is FetchNextCategoryList) {
      yield state.copyWith(
        categoryNextListState: const ApiState.loading(),
      );

      try {
        final Response<Paginate<Category>> response =
            await categoryApiService.getCategories(query: state.nextQuery!);
        final Paginate<Category> paginate = response.body!;

        yield state.copyWith(
          categories: List<Category>.from(state.categories)
            ..addAll(paginate.results),
          nextQuery:
              paginate.next != null ? Utils.getParams(paginate.next!) : null,
          categoryNextListState: const ApiState.done(),
        );
      } catch (error) {
        debugPrint(error.toString());
        yield state.copyWith(
          categoryNextListState:
              const ApiState.error('Something went wrong fetching categories'),
        );
      }
    }

    if (event is CreateCategory) {
      final Category newCategory = event.category;

      yield state.copyWith(
        submitStatus: const ApiState.loading(),
      );

      try {
        Response<Category> response;
        if (event.image != null) {
          response = await categoryApiService.createCategoryWithImage(
            newCategory.name,
            event.image!,
            isPublished: newCategory.isPublished,
            description: newCategory.description,
          );
        } else {
          response = await categoryApiService
            .createCategory(newCategory.toJson());
        }

        final Category category = response.body!;

        yield state.copyWith(
          submitStatus: const ApiState.done(),
        );
        add(AddCategory(category: category));
        snackbarCubit
            .showSuccess('${category.name} has been created successfully');
      } catch (error) {
        debugPrint(error.toString());
        yield state.copyWith(
          submitStatus: const ApiState.error(),
        );
        snackbarCubit.showError('Something went wrong creating a new category');
      }
    }

    if (event is AddCategory) {
      final Category? foundCategory = state.categories.firstWhereOrNull(
          (Category category) => category.id == event.category.id);

      if (foundCategory != null) return;

      yield state.copyWith(
        categories: List<Category>.from(state.categories)..add(event.category),
      );
    }

    if (event is RemoveCategory) {
      yield state.copyWith(
        categories: List<Category>.from(state.categories)
          ..removeWhere((Category cat) => cat.id == event.categoryId),
      );
    }

    if (event is ReplaceCategory) {
      yield state.copyWith(
        categories:
            Category.getUpdatedCategories(state.categories, event.category),
      );
    }

    if (event is UpdateListOrder) {
      final dynamic data = <String, dynamic>{
        'new_index': event.newIndex,
        'category_id': event.category.id,
      };

      try {
        await categoryApiService.updateCategoryDisplayOrder(data);
      } catch (error) {
        debugPrint(error.toString());
        snackbarCubit
            .showError('Something went wrong updating category list order');
      }
    }

    if (event is DeleteCategory) {
      final Category category = event.category;

      try {
        await categoryApiService.deleteCategory(category.id!);
        add(RemoveCategory(categoryId: category.id!));
        snackbarCubit.showSuccess('${category.name} deleted successfully');
      } catch (error) {
        debugPrint(error.toString());
        snackbarCubit.showError(
            'Something went wrong deleting category, ${category.name}');
      }
    }

    if (event is UpdateCategory) {
      final Category updateCategory = event.category;
      final Map<String, dynamic> data = updateCategory.toJson();

      /* the ignore decorator ignores fields completely, so we have to comprise
      by removing fields we dont want to send */
      data.remove('image');
      data.remove('image_thumbnail');

      try {
        final Response<Category> response =
            await categoryApiService.updateCategory(updateCategory.id!, data);
        final Category category = response.body!;

        add(ReplaceCategory(category: category));
        snackbarCubit
            .showSuccess('${updateCategory.name} updated successfully');
      } catch (error) {
        debugPrint(error.toString());
        snackbarCubit.showError(
            'Something went wrong updating category, ${updateCategory.name}');
      }
    }

    if (event is PartialUpdateCategory) {
      try {
        final Response<Category> response = await categoryApiService
            .partialUpdate(event.category.id!, event.data);
        final Category category = response.body!;
        add(ReplaceCategory(category: category));
      } catch (error) {
        debugPrint(error.toString());
        snackbarCubit.showError(
            'Something went wrong updating category, ${event.category.name}');
      }
    }

    if (event is UploadImage) {
      try {
        final Response<Category> response = await categoryApiService
            .uploadImage(event.category.id!, event.image);
        final Category category = response.body!;
        add(ReplaceCategory(category: category));
      } catch (error) {
        debugPrint(error.toString());
        snackbarCubit
            .showError('Something went wrong uploading category image');
      }
    }
  }

  List<Category> getCategoriesFromSearch(List<Category> searchedItems) {
    return searchedItems.where((Category cat) {
      return state.categories.any((Category _cat) => _cat.id == cat.id);
    }).toList();
  }

  List<Category> filterCategories(FilterType type) {
    if (type == FilterType.PUBLISHED) {
      return state.categories
          .where((Category item) => item.isPublished)
          .toList();
    }

    if (type == FilterType.DRAFT) {
      return state.categories
          .where((Category item) => !item.isPublished)
          .toList();
    }
    return state.categories;
  }

  List<Category> filterCategoriesFromSearchQuery(String query) {
    final RegExp regExp = RegExp(query, caseSensitive: false);
    return query.isNotEmpty
        ? state.categories
            .where((Category category) =>
                regExp.matchAsPrefix(category.name) != null)
            .toList()
        : state.categories;
  }

  Future<List<Category>> searchCategories(String query) async {
    final Map<String, dynamic> queryMap = {
      'name': query,
    };
    List<Category> results = <Category>[];
    try {
      final Response<Paginate<Category>> response =
          await categoryApiService.getCategories(query: queryMap);
      final Paginate<Category> paginate = response.body!;
      results = paginate.results;
    } catch (error) {
      debugPrint(error.toString());
    }
    return results;
  }
}
