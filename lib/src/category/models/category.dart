import 'package:json_annotation/json_annotation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'category.g.dart';
part 'category.freezed.dart';

@freezed
abstract class Category implements _$Category {
  const factory Category({
    @Default(null) int? id,
    required String name,
    @Default(null) String? description,
    @Default(null) String? image,
    @JsonKey(name: 'image_thumbnail') @Default(null) String? imageThumbnail,
    @JsonKey(name: 'display_order') @Default(null) int? displayOrder,
    @JsonKey(name: 'is_published') @Default(false) bool isPublished,
  }) = _Category;

  const Category._();

  @override
  String toString() {
    return 'Category(id: $id, name: $name)';
  }

  factory Category.fromJson(Map<String, dynamic> json) =>
      _$CategoryFromJson(json);

  static Category convertFromJson(Map<String, dynamic> json) => 
    _$CategoryFromJson(json);

  static List<Category> categoriesFromJson(List<dynamic> jsonList) => jsonList
      .map((dynamic json) => Category.fromJson(json as Map<String, dynamic>))
      .toList();

  static List<Category> getUpdatedCategories(
      List<Category> categories, Category updatedCategory) {
    return List<Category>.from(categories)
        .map((Category cat) =>
            cat.id == updatedCategory.id ? updatedCategory : cat)
        .toList();
  }
}
