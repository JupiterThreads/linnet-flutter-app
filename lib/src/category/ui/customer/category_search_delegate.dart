import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:linnet_flutter_app/src/category/category.dart';
import 'package:linnet_flutter_app/src/common/widgets/loading_indicator.dart';
import 'package:linnet_flutter_app/src/search/search.dart';

class CategorySearchDelegate extends SearchDelegate<dynamic> {
  CategorySearchDelegate(
      {required this.searchCubit, required this.categoryBloc});
  final SearchCubit searchCubit;
  final CategoryBloc categoryBloc;

  @override
  List<Widget> buildActions(BuildContext context) {
    return <Widget>[
      IconButton(
        icon: const Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      ),
    ];
  }

  @override
  ThemeData appBarTheme(BuildContext context) {
    final ThemeData theme = ThemeData();
    return theme.copyWith(
      primaryColor: Colors.white,
      primaryIconTheme: theme.primaryIconTheme.copyWith(color: Colors.grey),
      textTheme: theme.textTheme.copyWith(
        headline6: 
        const TextStyle(fontWeight: FontWeight.normal, fontSize: 16.0),
      ),
    );
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: const Icon(Icons.arrow_back),
      onPressed: () => close(context, null),
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    query = query.trim();
    if (query.isEmpty) {
      return Container();
    }

    searchCubit.performSearch(query);
    return BlocBuilder<SearchCubit, SearchState>(
        bloc: searchCubit,
        builder: (BuildContext context, SearchState state) {
          return state.when(
              initial: () => Container(),
              loading: () => LoadingIndicator(),
              populated: (List<dynamic> items) => CategoryListSearchView(
                  categoryList: items as List<Category>,
                  onTap: (Category category) => close(context, category)),
              empty: () => _buildFeedback('No categories found'),
              error: () => _buildFeedback('Error occurred'),
              );
        });
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return CategoryListSearchView(
      categoryList: categoryBloc.filterCategoriesFromSearchQuery(query),
      onTap: (Category category) => close(context, category),
    );
  }

  Widget _buildFeedback(String message) {
    return Center(
      child: Text(message),
    );
  }

}

class CategoryListSearchView extends StatelessWidget {
  const CategoryListSearchView(
      {Key? key, required this.categoryList, required this.onTap})
      : super(key: key);

  final List<Category> categoryList;
  final Function onTap;

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      separatorBuilder: (BuildContext context, int index) => const Divider(),
      itemCount: categoryList.length,
      itemBuilder: (BuildContext context, int index) =>
          _buildItemRow(context, categoryList[index]),
    );
  }

  Widget _buildItemRow(BuildContext context, Category category) {
    return ListTile(
      dense: true,
      title: Text(category.name, style: 
        const TextStyle(fontWeight: FontWeight.bold)),
      subtitle:
          category.description != null ? Text(category.description!) : null,
      onTap: () => onTap(category),
    );
  }
}
