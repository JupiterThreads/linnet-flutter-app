import 'dart:math' as math;
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:linnet_flutter_app/src/category/category.dart';
import 'package:linnet_flutter_app/src/category/ui/customer/category_list.dart';
import 'package:linnet_flutter_app/src/common/models/models.dart'
    show ApiStateLoading;
import 'package:linnet_flutter_app/src/common/widgets/loading_item_list.dart';
import 'package:linnet_flutter_app/src/common/widgets/sliver_app_bar_container.dart';
import 'package:linnet_flutter_app/src/common/widgets/widgets.dart'
    show Loading;
import 'package:linnet_flutter_app/src/trader/models/trade_account.dart';

class CategoryListScreen extends StatefulWidget {
  const CategoryListScreen({
    Key? key,
    required this.tradeAccount,
    required this.searchCallback,
  }) : super(key: key);

  final TradeAccount tradeAccount;
  final Function searchCallback;
  @override
  _CategoryListScreenState createState() => _CategoryListScreenState();
}

class _CategoryListScreenState extends State<CategoryListScreen> {
  TradeAccount get _account => widget.tradeAccount;
  late CategoryBloc _bloc;
  final ScrollController _scrollController = ScrollController();

  @override
  void dispose() {
    _scrollController.removeListener(_scrollListener);
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _bloc = BlocProvider.of<CategoryBloc>(context);
    _bloc.add(FetchCategoryList(tradeAccount: widget.tradeAccount));
    _scrollController.addListener(_scrollListener);
  }

  void _scrollListener() {
    if (_scrollController.offset >=
        _scrollController.position.maxScrollExtent) {
      if (_bloc.state.nextQuery != null &&
          _bloc.state.categoryNextListState is! ApiStateLoading) {
        _bloc.add(const FetchNextCategoryList());
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SliverAppBarContainer(
          scrollController: _scrollController,
          coverImage: _account.coverImage,
          titleText: _account.name,
          actions: context.watch<CategoryBloc>().state.categories.isNotEmpty
              ? <Widget>[
                  IconButton(
                      icon: const Icon(Icons.search),
                      onPressed: () => widget.searchCallback(context)),
                ]
              : <Widget>[],
          widgets: <Widget>[
            BlocBuilder<CategoryBloc, CategoryState>(
                builder: (BuildContext context, CategoryState state) {
              return state.categoryListState.when(
                done: (_) => state.categories.isNotEmpty ? 
                CategoryList(
                    tradeAccount: _account, categoryList: state.categories)
                : const SliverFillRemaining(
                    child: Center(child:  Text('No categories'))),
                loading: (_) => 
                  const SliverFillRemaining(child: LoadingItemList()),
                error: (String? message) =>
                    SliverFillRemaining(child: 
                    Center(child: message !=null ? Text(message) : null)),
              );
            }),
            if (context.watch<CategoryBloc>().state.categoryNextListState
                is ApiStateLoading)
              const SliverToBoxAdapter(
                child: Loading(),
              )
          ]),
    );
  }
}
