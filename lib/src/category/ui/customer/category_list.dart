import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:linnet_flutter_app/routing_constants.dart';
import 'package:linnet_flutter_app/src/category/category.dart';
import 'package:linnet_flutter_app/src/common/widgets/widgets.dart'
    show ItemRowTitle, ItemRowSubtitle, TrailingImageContainer;
import 'package:linnet_flutter_app/src/trader/trader.dart';

class CategoryList extends StatefulWidget {
  const CategoryList({
    Key? key,
    required this.tradeAccount,
    required this.categoryList,
  }) : super(key: key);

  final TradeAccount tradeAccount;
  final List<Category> categoryList;

  @override
  _CategoryListViewState createState() => _CategoryListViewState();
}

class _CategoryListViewState extends State<CategoryList> {
  TradeAccount get _tradeAccount => widget.tradeAccount;
  List<Category> get _categories => widget.categoryList;
  Flushbar<dynamic>? _flushbar;

  @override
  void initState() {
    super.initState();

    // WidgetsBinding.instance.addPostFrameCallback((_) {
    //   if ((draftOrderBloc.state?.draftOrderMap?.isNotEmpty ?? false) &&
    //       draftOrderBloc.state?.tradeAccount?.id == _tradeAccount.id) {
    //     _performShowFlushbar();
    //   }
    // });
  }

  @override
  void dispose() {
    _flushbar?.dismiss();
    super.dispose();
  }

  // void _performShowFlushbar() {
  //   Future<dynamic>.delayed(const Duration(milliseconds: 100)).then((_) {
  //     if (_flushbar == null) {
  //       _showFlushbar(context);
  //     } else {
  //       _flushbar.show(context);
  //     }
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    return _buildListView(context);
    // return BlocListener<DraftOrderBloc, DraftOrderState>(
    //   listener: (BuildContext context, DraftOrderState state) {
    //     // if (state.showOrderTotal && !state.isReviewing) {
    //     //   _performShowFlushbar();
    //     // } else {
    //     //   _flushbar?.dismiss();
    //     // }
    //   },
    //   child: _buildListView(context),
    // );
  }

  Widget _buildListView(BuildContext context) {
    return SliverPadding(
      padding: const EdgeInsets.only(bottom: 20),
      sliver: SliverList(
          delegate: SliverChildBuilderDelegate(
        (BuildContext context, int index) {
          return _buildCategoryRow(context, _categories[index]);
        },
        childCount: _categories.length,
      )),
    );
  }

  Widget _buildCategoryRow(BuildContext context, Category category) {
    final dynamic args = <String, dynamic>{
      'account': _tradeAccount,
      'category': category
    };

    // final DraftOrderBloc draftOrderBloc =
    //     BlocProvider.of<DraftOrderBloc>(context);
    const int qty = 10;
    return Column(
      children: <Widget>[
        ListTile(
          dense: true,
          title: ItemRowTitle(
            name: category.name,
            isSelected: qty > 0,
            qty: qty,
            nameStyle: const TextStyle(fontWeight: FontWeight.bold),
          ),
          subtitle: category.description != null
              ? ItemRowSubtitle(subtitleText: category.description)
              : null,
          trailing: category.imageThumbnail != null
              ? TrailingImageContainer(
                  imageThumbnail: category.imageThumbnail!,
                  image: category.image!)
              : null,
          onTap: () {
            // if (draftOrderBloc.state.showOrderTotal &&
            //     _tradeAccount.id == draftOrderBloc.state.tradeAccount.id) {
            //   _flushbar?.dismiss();
            //   _performShowFlushbar();
            // }
            Navigator.pushNamed(context, ProductListRoute, arguments: args);
          },
        ),
        const Divider(indent: 16, endIndent: 16),
      ],
    );
  }

  // void _showFlushbar(BuildContext context) {
  //   _flushbar = Flushbar<dynamic>(
  //     isDismissible: false,
  //     onTap: (Flushbar<dynamic> fb) =>
  //         Navigator.pushNamed(context, DraftOrderRoute),
  //     icon: Basket(),
  //     animationDuration: const Duration(milliseconds: 100),
  //     flushbarStyle: FlushbarStyle.GROUNDED,
  //     titleText: BottomBarTitle(),
  //     messageText: const Padding(
  //       padding: EdgeInsets.only(left: 8.0),
  //       child: Text('Click to checkout',
  //           style: TextStyle(color: Colors.white, fontSize: 16.0)),
  //     ),
  //     backgroundColor: const Color(0xffff5f52),
  //   )..show(context);
  // }
}
