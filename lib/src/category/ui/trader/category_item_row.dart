import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:linnet_flutter_app/constants.dart';
import 'package:linnet_flutter_app/routing_constants.dart';
import 'package:linnet_flutter_app/src/category/category.dart';
import 'package:linnet_flutter_app/src/category/ui/trader/trader.dart';
import 'package:linnet_flutter_app/src/common/widgets/widgets.dart'
    show SlideUpRoute, DeleteItemDialog;

class CategoryItemRow extends StatelessWidget {
  const CategoryItemRow({
    required this.category,
    required this.bloc,
    required this.slidableController,
    this.enabled = false,
  });

  final Category category;
  final CategoryBloc bloc;
  final SlidableController slidableController;
  final bool enabled;

  @override
  Widget build(BuildContext context) {
    return Slidable(
      enabled: enabled,
      key: Key(category.id.toString()),
      controller: slidableController,
      actionPane: const SlidableDrawerActionPane(),
      secondaryActions: <Widget>[
        IconSlideAction(
            caption: 'Edit',
            color: Theme.of(context).accentColor,
            icon: Icons.edit,
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute<Widget>(
                  fullscreenDialog: true,
                  builder: (BuildContext context) {
                    return CategoryDetail(
                        bloc: bloc,
                        category: category,
                        actionType: ActionType.EDIT);
                  }));
            }),
        IconSlideAction(
          caption: 'Delete',
          color: Colors.red,
          icon: Icons.delete,
          onTap: () async {
            final dynamic result =
                await _showDialog(context, category: category);
            if (result == true) {
              bloc.add(DeleteCategory(category: category));
            }
          },
        ),
      ],
      child: ListTile(
        dense: true,
        key: Key(category.id.toString()),
        title: Text(category.name,
            style: const TextStyle(fontWeight: FontWeight.bold)),
        subtitle:
            category.description != null ? Text(category.description!) : null,
        onTap: () {
          final dynamic args = <String, dynamic>{
            'account': bloc.tradeAccount,
            'category': category
          };
          Navigator.pushNamed(context, ProductListRoute, arguments: args);
        },
        trailing: const Icon(Icons.drag_handle),
      ),
    );
  }

  Future<dynamic> _showDialog(BuildContext context, 
    {required Category category}) async {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return DeleteItemDialog(
          itemName: category.name,
          title: 'Delete Category',
        );
      },
    );
  }
}
