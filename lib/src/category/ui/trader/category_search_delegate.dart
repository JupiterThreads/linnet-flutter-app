import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:linnet_flutter_app/src/category/category.dart';
import 'package:linnet_flutter_app/src/category/ui/trader/category_item_row.dart';
import 'package:linnet_flutter_app/src/common/widgets/loading_indicator.dart';
import 'package:linnet_flutter_app/src/search/search.dart';

class CategorySearchDelegate extends SearchDelegate {
  CategorySearchDelegate(
      {required this.searchCubit, required this.categoryBloc})
      : super(searchFieldLabel: 'Search name');

  final SearchCubit searchCubit;
  final CategoryBloc categoryBloc;

  @override
  ThemeData appBarTheme(BuildContext context) {
    final ThemeData theme = ThemeData();
    return theme.copyWith(
      primaryColor: Colors.white,
      primaryIconTheme: theme.primaryIconTheme.copyWith(color: Colors.grey),
      textTheme: theme.textTheme.copyWith(
        headline6: 
          const TextStyle(fontWeight: FontWeight.normal, fontSize: 16.0),
      ),
    );
  }

  @override
  List<Widget> buildActions(BuildContext context) {
    return <Widget>[
      IconButton(
        icon: const Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: const Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    query = query.trim();
    if (query.isEmpty) {
      return Container();
    }

    searchCubit.performSearch(query);
    return BlocBuilder<SearchCubit, SearchState>(
        bloc: searchCubit,
        builder: (BuildContext context, SearchState state) {
          return state.when(
              initial: () => Container(),
              loading: () => LoadingIndicator(),
              populated: (List<dynamic> items) => CategoryListSearchView(
                  categoryList: items as List<Category>, bloc: categoryBloc),
              empty: () => _buildFeedback('No categories found'),
              error: () => _buildFeedback('Error occurred'),
              );
        });
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final List<Category> categories =
        categoryBloc.filterCategoriesFromSearchQuery(query);
    return CategoryListSearchView(categoryList: categories, bloc: categoryBloc);
  }

  Widget _buildFeedback(String message) {
    return Center(
      child: Text(message),
    );
  }
}

class CategoryListSearchView extends StatelessWidget {
  const CategoryListSearchView({
    Key? key, 
    required this.categoryList, 
    required this.bloc})
      : super(key: key);

  final List<Category> categoryList;
  final CategoryBloc bloc;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CategoryBloc, CategoryState>(
        bloc: bloc,
        builder: (BuildContext context, CategoryState catState) {
          final List<Category> categories =
              bloc.getCategoriesFromSearch(categoryList);
          final SlidableController slidableController = SlidableController();
          return ListView.builder(
              itemCount: categories.length,
              itemBuilder: (BuildContext context, int index) => CategoryItemRow(
                  bloc: bloc,
                  category: categories[index],
                  slidableController: slidableController));
        });
  }
}
