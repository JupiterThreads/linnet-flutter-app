import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:linnet_flutter_app/constants.dart';
import 'package:linnet_flutter_app/src/category/category.dart';
import 'package:linnet_flutter_app/src/category/ui/trader/category_list.dart';
import 'package:linnet_flutter_app/src/category/ui/trader/trader.dart';
import 'package:linnet_flutter_app/src/common/widgets/loading_item_list.dart';
import 'package:linnet_flutter_app/src/common/widgets/sliver_app_bar_container.dart';
import 'package:linnet_flutter_app/src/common/widgets/widgets.dart'
    show NavigationDrawer;
import 'package:linnet_flutter_app/src/providers/socket_provider.dart';
import 'package:linnet_flutter_app/src/trader/models/trade_account.dart';
import 'package:linnet_flutter_app/src/trader/trader.dart';
import 'package:linnet_flutter_app/src/user/user.dart';
import 'package:linnet_flutter_app/src/websockets/event_types/category_event_types.dart';
import 'package:provider/provider.dart';

class CategoryListScreen extends StatefulWidget {
  const CategoryListScreen(
      {required this.account, required this.searchCallback});
  final TradeAccount account;
  final Function searchCallback;

  @override
  _CategoryListScreenState createState() => _CategoryListScreenState();
}

class _CategoryListScreenState extends State<CategoryListScreen> {
  late CategoryBloc _categoryBloc;
  TradeAccount get _account => widget.account;
  FilterType filterType = FilterType.ALL;
  late SocketProvider socketProvider;
  final SlidableController slidableController = SlidableController();

  @override
  void initState() {
    super.initState();
    _categoryBloc = BlocProvider.of<CategoryBloc>(context);
    _categoryBloc.add(FetchCategoryList(tradeAccount: _account));
    socketProvider = Provider.of<SocketProvider>(context, listen: false);
    socketProvider.addListener(socketListener);
  }

  @override
  void didUpdateWidget(CategoryListScreen oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.account != widget.account) {
      _categoryBloc.add(FetchCategoryList(tradeAccount: _account));
    }
  }

  void socketListener() {
    final dynamic data = socketProvider.data;
    final String type = data['type'] as String;

    switch (type) {
      case ADD_CATEGORY:
        _categoryBloc.add(AddCategory(
            category:
                Category.fromJson(data['payload'] as Map<String, dynamic>)));
        break;
      case UPDATE_CATEGORY:
        _categoryBloc.add(ReplaceCategory(
            category:
                Category.fromJson(data['payload'] as Map<String, dynamic>)));
        break;
      case REMOVE_CATEGORY:
        _categoryBloc
            .add(RemoveCategory(categoryId: data['payload'] as int));
        break;
      default:
        return;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const Drawer(
        child: NavigationDrawer(),
      ),
      body: BlocBuilder<CategoryBloc, CategoryState>(
          builder: (BuildContext context, CategoryState state) {
        return state.categoryListState.when(
            done: (_) => CategoryList(categoryList: state.categories),
            loading: (_) => SliverAppBarContainer(
                    titleText: 'Categories',
                    widgets: const <Widget>[
                      SliverFillRemaining(
                        child: LoadingItemList(traderView: true))
                    ]),
            error: (String? message) => SliverAppBarContainer(
                    titleText: 'Categories',
                    widgets: <Widget>[
                      SliverFillRemaining(child: Center(
                        child: message != null ? Text(message) : null))
                    ]));
      }),
      floatingActionButton:
          context.watch<UserBloc>().state.user!.hasWritePermission(_account)
              ? FloatingActionButton(
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute<Widget>(
                        fullscreenDialog: true,
                        builder: (BuildContext context) {
                          return CategoryDetail(
                            bloc: _categoryBloc,
                            actionType: ActionType.CREATE,
                          );
                        }));
                  },
                  child: const Icon(Icons.add),
                )
              : null,
    );
  }
}
