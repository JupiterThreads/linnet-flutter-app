import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:linnet_flutter_app/constants.dart';
import 'package:linnet_flutter_app/src/category/bloc/bloc.dart';
import 'package:linnet_flutter_app/src/category/bloc/category_bloc.dart';
import 'package:linnet_flutter_app/src/category/models/category.dart';
import 'package:linnet_flutter_app/src/category/ui/trader/category_item_row.dart';
import 'package:linnet_flutter_app/src/common/models/api_state.dart';
import 'package:linnet_flutter_app/src/common/widgets/widgets.dart'
    show FilterPopupMenu, Loading;
import 'package:linnet_flutter_app/src/providers/socket_provider.dart';
import 'package:linnet_flutter_app/src/user/blocs/user/user_bloc.dart';
import 'package:linnet_flutter_app/src/websockets/event_types/category_event_types.dart';
import 'package:provider/provider.dart';
import 'package:reorderables/reorderables.dart';

class CategoryList extends StatefulWidget {
  const CategoryList({required this.categoryList});

  final List<Category> categoryList;

  @override
  _CategoryListState createState() => _CategoryListState();
}

class _CategoryListState extends State<CategoryList> {
  late ScrollController _scrollController;
  late CategoryBloc _categoryBloc;
  final SlidableController slidableController = SlidableController();
  FilterType filterType = FilterType.ALL;
  late SocketProvider socketProvider;
  List<Category> get _categoryList =>
      _categoryBloc.filterCategories(filterType);

  @override
  void initState() {
    super.initState();
    _categoryBloc = BlocProvider.of<CategoryBloc>(context);
    socketProvider = Provider.of<SocketProvider>(context, listen: false);
    socketProvider.addListener(socketListener);
  }

  void _scrollListener() {
    if (_scrollController.offset >=
        _scrollController.position.maxScrollExtent) {
      if (_categoryBloc.state.nextQuery != null &&
          _categoryBloc.state.categoryNextListState is! ApiStateLoading) {
        _categoryBloc.add(const FetchNextCategoryList());
      }
    }
  }

  void socketListener() {
    final dynamic data = socketProvider.data;
    final String type = data['type'] as String;

    switch (type) {
      case UPDATE_CATEGORY_DISPLAY_ORDER:
        _updateDispayOrderFromSocket(data['payload']);
        break;
      default:
        return;
    }
  }

  void _updateDispayOrderFromSocket(dynamic data) {
    final int newIndex = data['new_index'] as int;
    final int oldIndex = data['old_index'] as int;
    _onReorder(oldIndex, newIndex);
  }

  void _onReorder(int oldIndex, int newIndex, {bool saveAndBroadcast = false}) {
    setState(() {
      final Category row = _categoryList.removeAt(oldIndex);
      _categoryList.insert(newIndex, row);
      if (saveAndBroadcast) {
        _categoryBloc.add(UpdateListOrder(category: row, newIndex: newIndex));
      }
    });
  }

  // @override
  // Widget loader() {
  //   return const Loading(message: 'Creating category...');
  // }

  // TODO search
  @override
  Widget build(BuildContext context) {
    _scrollController =
        PrimaryScrollController.of(context) ?? ScrollController();
    _scrollController.addListener(_scrollListener);
    return BlocListener<CategoryBloc, CategoryState>(
      bloc: _categoryBloc,
      listener: (BuildContext context, CategoryState state) {
        //  state.submitStatus.when(
        //     done: (_) => stopLoading(),
        //     loading: (_) => startLoading(),
        //     error: (_) => stopLoading());
      },
      child: CustomScrollView(controller: _scrollController, slivers: <Widget>[
        SliverAppBar(
          floating: true,
          title: const Text('Categories'),
          actions: <Widget>[
            if (context.watch<CategoryBloc>().state.categories.isNotEmpty)
              FilterPopupMenu(
                  onSelected: (FilterType type) =>
                      setState(() => filterType = type)),
            if (context.watch<CategoryBloc>().state.categories.isNotEmpty)
              IconButton(
                tooltip: 'Search',
                icon: const Icon(Icons.search),
                onPressed: () {
                  print('we can do our search here');
                  // widget.searchCallback(context);
                },
              ),
          ],
        ),
        if (_categoryList.isNotEmpty)
          ReorderableSliverList(
            enabled: context
                    .watch<UserBloc>()
                    .state
                    .user
                    !.hasWritePermission(_categoryBloc.tradeAccount) &&
                filterType == FilterType.ALL,
            delegate: ReorderableSliverChildBuilderDelegate(
                (BuildContext context, int index) => CategoryItemRow(
                    category: _categoryList[index],
                    bloc: _categoryBloc,
                    enabled: context
                        .watch<UserBloc>()
                        .state
                        .user
                        !.hasWritePermission(_categoryBloc.tradeAccount),
                    slidableController: slidableController),
                childCount: _categoryList.length),
            onReorder: (int old, int newIndex) =>
                _onReorder(old, newIndex, saveAndBroadcast: true),
          ),
        if (context.watch<CategoryBloc>().state.categoryNextListState
            is ApiStateLoading)
          const SliverToBoxAdapter(child: Loading()),
        if (_categoryList.isEmpty)
          const SliverFillRemaining(
            child: Center(child: Text('No categories')),
          )
      ]),
    );
  }
}
