
import 'package:flutter/material.dart';
import 'package:linnet_flutter_app/constants.dart';

class CategoryDialog extends StatefulWidget {

  const CategoryDialog({
    required this.actionType,
    this.name,
  });

  final String? name;
  final ActionType actionType;

  @override
  _CategoryDialogState createState() => _CategoryDialogState();
}

class _CategoryDialogState extends State<CategoryDialog> {
  final TextEditingController _nameController = TextEditingController();
  ActionType get _actionType => widget.actionType;
  String? get _name => widget.name;
  bool _validCategoryName = false;

  @override
  void initState() {
    super.initState();

    if (_actionType == ActionType.EDIT) {
      _nameController.text = _name!;
    }
    _nameController.addListener(() {
      _updateValidName(_nameController.text);
    });
  }

  void _updateValidName(String newName) {
    bool isValid = false;
    if (_actionType == ActionType.EDIT) {
      if (newName != _name && newName.isNotEmpty) {
        isValid = true;
      } 
    } else if (newName.isNotEmpty) {
      isValid = true;
    }
    setState(() => _validCategoryName = isValid);
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: _actionType == ActionType.CREATE
        ? const Text('Add Category')
        : const Text('Edit Category'),
      content:
        TextField(
          autofocus: true,
          controller: _nameController,
          decoration: const InputDecoration(
            hintText: 'Category Name',
          ),
        ),
      actions: <Widget>[
        TextButton(
          onPressed: () => Navigator.pop(context), 
          child: const Text('CANCEL'),
        ),
        TextButton(
          onPressed: _validCategoryName ? () =>
            Navigator.pop(context, _nameController.text) : null, 
          child: _actionType == ActionType.CREATE 
            ? const Text('ADD') 
            : const Text('SAVE'),
        ),
      ],
    );
  }
}
