import 'package:flutter/material.dart';
import 'package:linnet_flutter_app/constants.dart';
import 'package:linnet_flutter_app/src/category/category.dart';
import 'package:linnet_flutter_app/src/common/widgets/widgets.dart';
import 'package:http/http.dart' show MultipartFile;

class CategoryDetail extends StatefulWidget {
  const CategoryDetail({
    required this.actionType,
    required this.bloc,
    this.category,
  });

  final ActionType actionType;
  final Category? category;
  final CategoryBloc bloc;

  @override
  _CategoryDetailState createState() => _CategoryDetailState();
}

class _CategoryDetailState extends State<CategoryDetail> {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _description = TextEditingController();
  final GlobalKey<FormState> _categoryDetailForm = GlobalKey<FormState>();

  ActionType get _actionType => widget.actionType;
  Category? get _category => widget.category;
  CategoryBloc get _bloc => widget.bloc;
  bool isPublished = false;
  Image? _image;
  MultipartFile? selectedFile;

  @override
  void initState() {
    super.initState();

    setState(() {
      if (_actionType == ActionType.EDIT) {
        isPublished = _category!.isPublished;
        _nameController.text = _category!.name;
        if (_category?.description !=null) {
          _description.text = _category!.description!;
        }
      }
      if (_category?.image != null) {
        _image = Image.network(_category!.image!);
      }
    });
  }

  void _handlePickedImage(MultipartFile file, _) {
    selectedFile = file;
    if (_actionType == ActionType.EDIT) {
      _bloc.add(UploadImage(category: _category!, image: file));
    }
  }

  void _handleDeleteImage() {
    if (_actionType == ActionType.EDIT) {
      final Map<String, dynamic> data = <String, dynamic>{'image': null};
      _bloc.add(PartialUpdateCategory(category: _category!, data: data));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: _actionType == ActionType.CREATE
            ? const Text('Add Category')
            : const Text('Edit Category'),
        actions: <Widget>[
          IconButton(
              onPressed: _performSave,
              icon: Icon(Icons.check, color: Theme.of(context).accentColor),
              tooltip: 'Save'),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _categoryDetailForm,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              TextFormField(
                keyboardType: TextInputType.text,
                controller: _nameController,
                validator: (_) => isNameValid() ? null : 'Invalid Name',
                decoration: const InputDecoration(
                  labelText: 'Name',
                ),
              ),
              const SizedBox(height: 16.0),
              TextFormField(
                keyboardType: TextInputType.text,
                controller: _description,
                decoration: const InputDecoration(
                  labelText: 'Description',
                ),
              ),
              const SizedBox(height: 16.0),
              SwitchListTile(
                contentPadding: EdgeInsets.zero,
                title: const Text('Publish'),
                value: isPublished,
                onChanged: _handlePublish,
              ),
              const Divider(height: 2),
              Padding(
                padding: const EdgeInsets.only(top: 16.0, bottom: 32.0),
                child:
                    Text('Image', style: Theme.of(context).textTheme.subtitle1),
              ),
              LinnetImagePicker(
                  initialImage: _image,
                  containerHeight: 100,
                  containerWidth: 100,
                  fieldName: 'image',
                  onPickedImage: _handlePickedImage,
                  shape: BoxShape.rectangle,
                  onDeleteImage: _handleDeleteImage)
            ],
          ),
        ),
      ),
    );
  }

  bool isNameValid() {
    return _nameController.text.trim().isNotEmpty;
  }

  void _performSave() {
    final bool isFormValid = _categoryDetailForm
      .currentState?.validate() ?? false;

    if (!isFormValid) return;

    final String? description =
        _description.text.trim().isNotEmpty ? _description.text.trim() : null;

    if (_actionType == ActionType.CREATE) {
      final Category category = Category(
          name: _nameController.text.trim(),
          isPublished: isPublished,
          description: description);

      _bloc.add(CreateCategory(
        category: category,
        image: selectedFile,
      ));
    } else {
      // this needs to change to use a category object
      final Category updatedCategory = _category!.copyWith(
        name: _nameController.text.trim(),
        isPublished: isPublished,
        description: description,
      );

      _bloc.add(UpdateCategory(category: updatedCategory));
    }
    Navigator.pop(context);
  }

  void _handlePublish(bool val) {
    setState(() => isPublished = val);
  }
}
