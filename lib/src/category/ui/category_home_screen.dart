import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:linnet_flutter_app/routing_constants.dart';
import 'package:linnet_flutter_app/src/category/category.dart';
import 'package:linnet_flutter_app/src/category/ui/customer/customer.dart'
    as buyer_ui_lib;
import 'package:linnet_flutter_app/src/category/ui/trader/trader.dart'
    as trader_ui_lib;
import 'package:linnet_flutter_app/src/search/search.dart';
import 'package:linnet_flutter_app/src/snackbar/cubit/snackbar_cubit.dart';
import 'package:linnet_flutter_app/src/trader/trader.dart';
import 'package:linnet_flutter_app/src/user/user.dart';
import 'package:linnet_flutter_app/src/services/category_api_service.dart';

class CategoryHomeScreen extends StatefulWidget {
  const CategoryHomeScreen({Key? key, required this.tradeAccount})
      : super(key: key);

  final TradeAccount tradeAccount;

  @override
  _CategoryListHomeScreenState createState() => _CategoryListHomeScreenState();
}

class _CategoryListHomeScreenState extends State<CategoryHomeScreen> {
  late CategoryBloc _categoryBloc;
  late UserBloc _userBloc;
  TradeAccount get _tradeAccount => widget.tradeAccount;
  late CategoryApiService service;

  @override
  void initState() {
    super.initState();
    service = CategoryApiService.create(_tradeAccount.id!);
    _userBloc = BlocProvider.of<UserBloc>(context);
    _categoryBloc = CategoryBloc(
        tradeAccount: _tradeAccount,
        snackbarCubit: BlocProvider.of<SnackbarCubit>(context),
        categoryApiService: service);
  }

  @override
  void dispose() {
    service.client.dispose();
    _categoryBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CategoryBloc>(
      create: (BuildContext context) => _categoryBloc,
      child: _buildListScreen(),
    );
  }

  Widget _buildListScreen() {
    if (_userBloc.state.traderMode) {
      return trader_ui_lib.CategoryListScreen(
          account: _tradeAccount, searchCallback: _showSearchPage);
    }
    return buyer_ui_lib.CategoryListScreen(
        tradeAccount: _tradeAccount, searchCallback: _showSearchPage);
  }

  Future<void> _showSearchPage(BuildContext context) async {
    final SearchCubit searchCubit = SearchCubit(
        callback: (String query) => _categoryBloc.searchCategories(query));

    if (_userBloc.state.traderMode) {
      await showSearch(
          context: context,
          delegate: trader_ui_lib.CategorySearchDelegate(
              searchCubit: searchCubit, categoryBloc: _categoryBloc));
    } else {
      final dynamic result = await showSearch(
        context: context,
        delegate: buyer_ui_lib.CategorySearchDelegate(
            searchCubit: searchCubit, categoryBloc: _categoryBloc),
      );

      if (result is Category) {
        final dynamic args = <String, dynamic>{
          'account': _tradeAccount,
          'category': result
        };
        Navigator.pushNamed(context, ProductListRoute, arguments: args);
      }
    }
  }
}
