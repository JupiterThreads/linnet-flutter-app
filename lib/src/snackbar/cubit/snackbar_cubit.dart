import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:linnet_flutter_app/src/snackbar/cubit/snackbar_state.dart';
import 'package:linnet_flutter_app/src/utils/utils.dart';

class SnackbarCubit extends Cubit<SnackbarState> {
  SnackbarCubit() : super(const SnackbarState.initial());

  void reset() {
    emit(const SnackbarState.initial());
  }

  void showSuccess(String message, {String? title}) {
    final String _message = Utils.removeAllWhiteSpace(message);
    emit(SnackbarState.success(title: title, message: _message));
  }

  void showError(String message, {String? title}) {
    final String _message = Utils.removeAllWhiteSpace(message);
    emit(SnackbarState.error(title: title, message: _message,));
  }
}
