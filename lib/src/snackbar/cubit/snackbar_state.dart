import 'package:freezed_annotation/freezed_annotation.dart';

part 'snackbar_state.freezed.dart';

@freezed
class SnackbarState with _$SnackbarState {
  const factory SnackbarState.initial() = SnackbarInitial;

  const factory SnackbarState.success({
    @Default(null) String? title,
    required String message,
  }) = SnackbarSuccess;

  const factory SnackbarState.error({
    @Default(null) String? title,
    required String message,
  }) = SnackbarError;
}
