import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:linnet_flutter_app/src/place/place.dart';
import 'package:linnet_flutter_app/src/trader/models/terms_and_conditions.dart';

part 'onboard_event.freezed.dart';

@freezed
class OnboardEvent with _$OnboardEvent {
  const factory OnboardEvent.nameChanged({required String name}) = NameChanged;

  const factory OnboardEvent.currencyChanged({required String currency}) =
      CurrencyChanged;

  const factory OnboardEvent.businessTypesChanged({
    required List<String> businessTypeNames,
  }) = BusinessTypeChanged;

  const factory OnboardEvent.locationChanged({
    required LocationResult? locationResult,
  }) = LocationChanged;

  const factory OnboardEvent.placeChanged({
    required Place? place,
  }) = PlaceChanged;

  const factory OnboardEvent.acceptedTermsChanged({
    required bool isAccepted,
  }) = AcceptedTermsChanged;

  const factory OnboardEvent.createTradeAccount({
    required TermsAndConditions termsAndConditions,
  }) = CreateTradeAccount;

  const factory OnboardEvent.reset() = OnboardReset;
}
