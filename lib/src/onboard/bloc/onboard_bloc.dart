import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:chopper/chopper.dart';
import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:linnet_flutter_app/src/common/models/api_state.dart';
import 'package:linnet_flutter_app/src/onboard/onboard.dart';
import 'package:linnet_flutter_app/src/services/trader_api_service.dart';
import 'package:linnet_flutter_app/src/trader/models/business_type.dart';
import 'package:linnet_flutter_app/src/trader/models/trade_account.dart';
import 'package:linnet_flutter_app/src/trader/models/trader_location.dart';
import 'package:linnet_flutter_app/src/user/user.dart' as user_lib;
import 'package:linnet_flutter_app/src/utils/utils.dart';

class OnboardBloc extends Bloc<OnboardEvent, OnboardState> {
  OnboardBloc({
    required this.userBloc,
    required this.traderApiService,
  }) : super(const OnboardState());

  final TraderApiService traderApiService;
  final user_lib.UserBloc userBloc;

  @override
  Stream<OnboardState> mapEventToState(OnboardEvent event) async* {
    if (event is NameChanged) {
      yield state.copyWith(name: event.name);
    }

    if (event is CurrencyChanged) {
      yield state.copyWith(
        currency: event.currency,
      );
    }

    if (event is LocationChanged) {
      yield state.copyWith(
        locationResult: event.locationResult,
      );
    }

    if (event is BusinessTypeChanged) {
      yield state.copyWith(
        businessTypeNames: event.businessTypeNames,
      );
    }

    if (event is PlaceChanged) {
      yield state.copyWith(
        place: event.place,
      );
    }

    if (event is AcceptedTermsChanged) {
      yield state.copyWith(
        termsAccepted: event.isAccepted,
      );
    }

    if (event is CreateTradeAccount) {
      yield state.copyWith(
        submitStatus: const ApiState.loading('Creating trade account...'),
        termsAndConditions: event.termsAndConditions,
      );

      try {
        final dynamic data = await prepareDraftTradeAccountToJson();
        final Response<TradeAccount> response =
            await traderApiService.createAccount(data);
        final TradeAccount tradeAccount = response.body!;

        userBloc.add(user_lib.AddAccount(account: tradeAccount));

        yield state.copyWith(
          submitStatus: const ApiState.done(),
        );
      } catch (error) {
        debugPrint(error.toString());
        yield state.copyWith(
          submitStatus: const ApiState.error(),
        );
      }
    }

    if (event is OnboardReset) {
      yield const OnboardState();
    }
  }

  Future<dynamic> prepareDraftTradeAccountToJson() async {
    TraderLocation? location;

    if (state.locationResult?.latLng != null) {
      // location = TraderLocation(state.locationResult!.latLng.latitude,
      //     state.locationResult!.latLng.longitude);
    }

    final TradeAccount account = TradeAccount(
      name: state.name!,
      owner: userBloc.state.user!,
      businessTypes: state.businessTypeNames
          .map((String name) => BusinessType(name))
          .toList(),
      termsAndConditions: state.termsAndConditions,
      location: location,
      placeId: state.place?.id,
    );

    return account.toJson();
  }
}
