import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:linnet_flutter_app/src/common/models/api_state.dart';
import 'package:linnet_flutter_app/src/place/models/location_result.dart';
import 'package:linnet_flutter_app/src/place/place.dart';
import 'package:linnet_flutter_app/src/trader/models/terms_and_conditions.dart';

part 'onboard_state.freezed.dart';

@freezed
class OnboardState with _$OnboardState {

  const factory OnboardState({
    @Default(null) String? name,
    @Default(null) String? currency,
    @Default(null) LocationResult? locationResult,
    @Default(<String>[]) List<String> businessTypeNames,
    @Default(null) Place? place,
    @Default(false) bool termsAccepted,
    @Default(null) ApiState? submitStatus,
    @Default(null) TermsAndConditions? termsAndConditions,
  }) = _OnboardState;
}

