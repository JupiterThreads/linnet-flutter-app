import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:linnet_flutter_app/src/common/widgets/widgets.dart' show CardInfo;
import 'package:linnet_flutter_app/src/onboard/onboard.dart';
import 'package:linnet_flutter_app/src/place/place.dart';
import 'package:linnet_flutter_app/src/services/google_maps_services.dart';
import 'package:linnet_flutter_app/src/services/place_api_service.dart';
import 'package:linnet_flutter_app/src/snackbar/cubit/snackbar_cubit.dart';
import 'package:linnet_flutter_app/src/utils/utils.dart';
import 'package:provider/provider.dart';

class PlaceCandidates extends StatefulWidget {
  const PlaceCandidates({Key? key}) : super(key: key);

  @override
  _PlaceCandidatesState createState() => _PlaceCandidatesState();
}

class _PlaceCandidatesState extends State<PlaceCandidates> {
  List<Place> placeCandidates = <Place>[];
  late OnboardBloc _onboardTraderBloc;

  @override
  void initState() {
    super.initState();
    _onboardTraderBloc = BlocProvider.of<OnboardBloc>(context);
    _init();
  }

  void _init() {
    final SnackbarCubit snackbarCubit = BlocProvider.of<SnackbarCubit>(context);
    final GoogleMapsServices googleMapsServices =
        Provider.of<GoogleMapsServices>(context, listen: false);
    final PlaceApiService placeApiService =
        googleMapsServices.client.getService<PlaceApiService>();

    final String locationStr = Utils.locationQueryStr(
        _onboardTraderBloc.state.locationResult?.latLng);
    final String locationBias = 'circle:2000@$locationStr';

    placeApiService
        .placeSearch(_onboardTraderBloc.state.name!,
            fields: Utils.getPlaceSearchFields(), locationBias: locationBias)
        .then((Response<List<Place>> response) {
      final List<Place> places = response.body!;
      if (mounted) {
        setState(() => placeCandidates = places);
      }
    }).catchError((dynamic error) {
      debugPrint(error.toString());
      snackbarCubit.showError('Unable to find places.');
    });
  }

  Future<Object?> _showHelpDialog() async {
    return showGeneralDialog<Object?>(
        barrierColor: Colors.black.withOpacity(0.5),
        pageBuilder: (BuildContext context, Animation<double> animation,
                Animation<double> secondaryAnimation) =>
            Container(),
        context: context,
        barrierDismissible: true,
        barrierLabel: '',
        transitionDuration: const Duration(milliseconds: 200),
        transitionBuilder: (BuildContext context, Animation<double> animation,
            Animation<double> secondaryAnimation, Widget child) {
          return Transform.scale(
              scale: animation.value,
              child: Opacity(
                opacity: animation.value,
                child: AlertDialog(
                  content: SingleChildScrollView(
                    child: ListBody(children: const <Widget>[
                      Text(
                          '''If your business is registered with google maps and is not listed, try changing your business name and/or your business location in the previous steps.\n'''),
                      Text(
                          '''If your business still does not appear, you will be able to assign a google place to your business later.\n'''),
                    ]),
                  ),
                  actions: <Widget>[
                    ElevatedButton(
                      onPressed: () => Navigator.of(context).pop(),
                      child: const Text('OK'),
                    ),
                  ],
                ),
              ));
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
      padding: const EdgeInsets.only(top: 16.0, right: 16.0, left: 16.0),
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            const PageTitle(title: 'Google Place'),
            const SizedBox(height: 20),
            const CardInfo(
                '''To improve user experience, assign a google place to your business from the list of places below by clicking on it.\n\nIf your business is not listed, you can continue by clicking the next arrow at the bottom.'''),
            const SizedBox(height: 20),
            ElevatedButton(
                onPressed: _showHelpDialog,
                child: const Text("Where's my business?")),
            const SizedBox(height: 20),
            Expanded(child: PlaceListView(places: placeCandidates)),
          ]),
    ));
  }
}
