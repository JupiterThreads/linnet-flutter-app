import 'package:flutter/material.dart';

class Subtitle extends StatelessWidget {
  const Subtitle({required this.title});
  final String title;
  @override
  Widget build(BuildContext context) {
    return Align(
        alignment: Alignment.centerLeft,
        child: Text(title, style: Theme.of(context).textTheme.subtitle2));
  }
}
