import 'package:flutter/material.dart';
import 'package:linnet_flutter_app/src/place/place.dart';

class PlaceCard extends StatelessWidget {
  const PlaceCard(
    this.place, {
    Key? key,
    this.isSelected = false,
    required this.onTap,
  }) : super(key: key);
  final Place? place;
  final bool isSelected;
  final Function onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadius.circular(5),
      hoverColor: Theme.of(context).accentColor,
      highlightColor: Theme.of(context).accentColor,
      onTap: () => onTap(place),
      onDoubleTap: () => onTap(place),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5),
          side: _buildBorderSide(context),
        ),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                ListTile(
                  leading: Image.network(place?.icon ?? '',
                      width: 35.0, height: 40.0, fit: BoxFit.fill),
                  title: Text(place?.name ?? ''),
                  subtitle: Text(place?.address ?? ''),
                ),
                const SizedBox(height: 8),
                Text('Status', style: Theme.of(context).textTheme.bodyText1),
                Text(place?.businessStatusDisplay ?? '',
                    style: Theme.of(context).textTheme.caption),
                const SizedBox(height: 8),
                Text('Place types',
                    style: Theme.of(context).textTheme.bodyText1),
                const SizedBox(height: 4),
                Text(place?.getPlaceTypesDisplay() ?? '',
                    style: Theme.of(context).textTheme.caption),
              ]),
        ),
      ),
    );
  }

  BorderSide _buildBorderSide(BuildContext context) {
    if (isSelected) {
      return BorderSide(color: Theme.of(context).accentColor, width: 4);
    }
    return const BorderSide(color: Colors.white70);
  }
}
