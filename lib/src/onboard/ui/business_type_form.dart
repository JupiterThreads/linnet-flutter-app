// import 'dart:async';

// import 'package:chopper/chopper.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:linnet_flutter_app/src/common/widgets/widgets.dart' show CardInfo, Loading;
// import 'package:linnet_flutter_app/src/onboard/bloc/onboard_bloc.dart';
// import 'package:linnet_flutter_app/src/onboard/onboard.dart';
// import 'package:linnet_flutter_app/src/services/trader_api_service.dart';
// import 'package:linnet_flutter_app/src/trader/trader.dart';
// import 'package:linnet_flutter_app/src/utils/utils.dart';
// import 'package:multiselect_formfield/multiselect_formfield.dart';
// import 'package:provider/provider.dart';

// class BusinessTypeForm extends StatefulWidget {
//   const BusinessTypeForm({Key? key}) : super(key: key);

//   @override
//   BusinessTypeFormState createState() => BusinessTypeFormState();
// }

// class BusinessTypeFormState extends State<BusinessTypeForm> {
//   OnboardBloc _onboardBloc;
//   List _selectedBussinesTypes = <dynamic>[];
//   final GlobalKey<FormState> _formTypeKey = GlobalKey<FormState>();
//   List<dynamic> optionList = <dynamic>[];

//   @override
//   void initState() {
//     super.initState();
//     _onboardBloc = BlocProvider.of<OnboardBloc>(context);

//     if (_onboardBloc.state.businessTypeNames != null) {
//       _selectedBussinesTypes = _onboardBloc.state.businessTypeNames;
//     }
//     _getMultiSelectData();
//   }

//   List<dynamic> _getDisplayList(List<String> names) {
//     return names
//         .map((String name) => {
//               'display': name,
//               'value': name,
//             })
//         .toList();
//   }

//   Future<void> _getMultiSelectData() async {
//     final List<String> names = await _getBusinessTypeNames();
//     setState(() {
//       optionList = _getDisplayList(names);
//       _selectedBussinesTypes =
//           _selectedBussinesTypes.where((type) => names.contains(type)).toList();
//     });
//   }

//   Future<List<String>> _fetchStoredBusinessNames() async {
//     final TraderApiService traderApiService =
//         Provider.of<TraderApiService>(context, listen: false);
//     List<BusinessType> bTypes = <BusinessType>[];
//     try {
//       final Response<List<BusinessType>> response =
//           await traderApiService.getBusinessTypes();
//       bTypes = Utils.getResponseBody(response) as List<BusinessType>;
//     } catch (error) {
//       debugPrint(error.toString());
//     }
//     return BusinessType.getBusinessTypeNames(bTypes);
//   }

//   Future<List<String>> _getBusinessTypeNames() async {
//     List<String> names = <String>[];

//     if (_onboardBloc.state?.place?.types != null) {
//       names = _onboardBloc.state.place.getPlaceTypeDisplayList().toList();
//     }
//     List<String> storedNames = <String>[];
    
//     try {
//       storedNames = await _fetchStoredBusinessNames();
//     } catch (error) {
//       debugPrint(error.toString());
//     }

//     List<String> combinedNames = List<String>.from(names)..addAll(storedNames);
//     combinedNames = <String>[
//       ...<String>{...combinedNames}
//     ];
//     combinedNames = Utils.sortAlphabetically(combinedNames);
//     return combinedNames;
//   }

//   void _onTypesChanged(List<dynamic> values) {
//     final List<String> names =
//         values.map((dynamic el) => el.toString()).toList();
//     _onboardBloc.add(BusinessTypeChanged(businessTypeNames: names));
//     setState(() => _selectedBussinesTypes = names);
//   }

//   String _validateOptions(List values) {
//     if (values == null) return null;
//     if (values.length > 3) {
//       return 'Please select no more than 3 business types';
//     }
//     if (values.isEmpty) {
//       return 'Please select at least one business type';
//     }
//     return null;
//   }

//   bool isFormValid() {
//     if (_formTypeKey.currentState.validate()) {
//       return true;
//     }
//     return false;
//   }

//   // TODO needs a loading spinner
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         body: Padding(
//             padding: const EdgeInsets.all(16.0),
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: <Widget>[
//                 const PageTitle(title: 'Business Type', isRequired: true),
//                 const SizedBox(height: 20),
//                 const CardInfo(
//                     '''Select up to 3 place types that best describes your business.'''),
//                 Expanded(
//                   child: Align(
//                     alignment: Alignment.center,
//                     child: Container(
//                       height: 200,
//                       child: SingleChildScrollView(
//                         child: Form(
//                           key: _formTypeKey,
//                           child: optionList.isEmpty
//                               ? const Loading(
//                                   message: 'Loading business types...')
//                               : MultiSelectFormField(
//                                   // initialValue: _selectedBussinesTypes,
//                                   maxSelections: 3,
//                                   validator: (dynamic values) =>
//                                       _validateOptions(values as List),
//                                   title: const Text('Select up to 3 types'),
//                                   errorText: 'Please select up to 3 options',
//                                   dataSource: optionList,
//                                   textField: 'display',
//                                   valueField: 'value',
//                                   required: true,
//                                   onSaved: (dynamic values) =>
//                                       _onTypesChanged(values as List<dynamic>),
//                                 ),
//                         ),
//                       ),
//                     ),
//                   ),
//                 ),
//               ],
//             )));
//   }
// }
