import 'package:flutter/material.dart';

class ControlButtons extends StatelessWidget {

  const ControlButtons({
    required this.onBackPressed, 
    required this.onNextPressed});
  final VoidCallback? onBackPressed;
  final VoidCallback? onNextPressed;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        IconButton(
          tooltip: 'Previous',
          iconSize: 46,
          color: Theme.of(context).primaryColor,
          onPressed: onBackPressed,
          icon: const Icon(Icons.chevron_left),
        ),
        IconButton(
          tooltip: 'Next',
          iconSize: 46,
          color: Theme.of(context).primaryColor,
          onPressed: onNextPressed,
          icon:  const Icon(Icons.chevron_right),
        ),
      ],
    );
  }
}