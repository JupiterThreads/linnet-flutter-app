import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:linnet_flutter_app/routing_constants.dart';
import 'package:linnet_flutter_app/src/common/widgets/widgets.dart'
    show CardInfo, LocationPreview;
import 'package:linnet_flutter_app/src/onboard/bloc/onboard_event.dart';
import 'package:linnet_flutter_app/src/onboard/onboard.dart';
import 'package:linnet_flutter_app/src/place/place.dart';

class LocationForm extends StatefulWidget {
  LocationForm({Key? key}) : super(key: key);

  @override
  LocationFormState createState() => LocationFormState();
}

class LocationFormState extends State<LocationForm> {
  late LatLng? _tradeLocation;
  late OnboardBloc _onboardBloc;
  final TextEditingController _controller = TextEditingController();

  Future<void> _openLocationMap() async {
    final dynamic result = await Navigator.pushNamed(context, LocationMapRoute,
        arguments: <String, dynamic>{
          'location': _tradeLocation,
        });

    if (result is LocationResult) {
      _onboardBloc.add(LocationChanged(locationResult: result));
      setState(() => _tradeLocation = result.latLng);
      if (result.address != null) {
        _controller.text = result.address!;
      }
    }
  }

  void clearLocation() {
    _onboardBloc.add(const LocationChanged(locationResult: null));
    setState(() => _tradeLocation = null);
  }

  @override
  void initState() {
    super.initState();
    _onboardBloc = BlocProvider.of<OnboardBloc>(context);
    _tradeLocation = _onboardBloc.state.locationResult?.latLng;

    setState(() {
      _controller.text = _tradeLocation != null
          ? _onboardBloc.state.locationResult!.address!
          : '';
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              const PageTitle(title: 'Location'),
              const SizedBox(height: 20),
              const CardInfo(
                  '''Add a location to allow your customers to see how far away they are from your business.'''),
              Expanded(
                child: Align(
                  child: _buildLocationPreview(),
                ),
              ),
            ]),
      ),
    );
  }

  Widget _buildLocationPreview() {
    if (_tradeLocation == null) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          const Text('No location set'),
          ElevatedButton(
            onPressed: _openLocationMap,
            child: const Text('Add location'),
          )
        ],
      );
    }
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text('Tap anywhere on the map to change location',
            style: Theme.of(context).textTheme.caption),
        const SizedBox(height: 20),
        Flexible(
            child: SizedBox(
                width: MediaQuery.of(context).size.width,
                height: 200,
                child: LocationPreview(
                    markSelectedPosition: true,
                    selectedPosition: _tradeLocation,
                    onTap: _openLocationMap))),
        const SizedBox(height: 20),
        TextField(
          controller: _controller,
          minLines: 1,
          maxLines: 3,
          readOnly: true,
          decoration: const InputDecoration(
            labelText: 'Address',
          ),
        ),
        const SizedBox(height: 20),
        if (_tradeLocation != null)
          ElevatedButton(onPressed: clearLocation, child: const Text('Clear location'))
      ],
    );
  }
}
