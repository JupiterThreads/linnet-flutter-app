import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:linnet_flutter_app/src/common/widgets/loading.dart';
import 'package:linnet_flutter_app/src/onboard/onboard.dart';
import 'package:linnet_flutter_app/src/place/place.dart';

class PlaceListView extends StatefulWidget {
  const PlaceListView({Key? key, this.places}) : super(key: key);
  final List<Place>? places;

  @override
  _PlaceListViewState createState() => _PlaceListViewState();
}

class _PlaceListViewState extends State<PlaceListView> {
  List<Place>? get _places => widget.places;
  Place? selectedPlace;
  late OnboardBloc _bloc;

  @override
  void initState() {
    super.initState();
    _bloc = BlocProvider.of<OnboardBloc>(context);
    if (_bloc.state.place != null) {
      setState(() => selectedPlace = _bloc.state.place);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: Builder(builder: (BuildContext context) {
      if (_places == null) {
        return const Loading(message: 'Searching nearby places...');
      }
      if (_places?.isEmpty ?? false) {
        return const Center(child: Text('No nearby places found'));
      }
      return _buildPlaceCandidates(context);
    }));
  }

  Widget _buildPlaceCandidates(BuildContext context) {
    return ListView.builder(
      padding: EdgeInsets.zero,
      itemCount: _places!.length,
      itemBuilder: (BuildContext context, int index) {
        return _buildPlaceItem(_places![index]);
      },
    );
  }

  bool _isPlaceSelected(Place place) {
    return place.id == selectedPlace?.id;
  }

  void _handlePlaceSelected(Place place) {
    final Place? newPlace = _isPlaceSelected(place) ? null : place;
    setState(() => selectedPlace = newPlace);
    _bloc.add(PlaceChanged(place: newPlace));
  }

  Widget _buildPlaceItem(Place place) {
    return PlaceCard(place,
        onTap: _handlePlaceSelected, isSelected: _isPlaceSelected(place));
  }
}
