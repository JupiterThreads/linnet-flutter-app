import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:linnet_flutter_app/src/onboard/onboard.dart';

class NameForm extends StatefulWidget {
  const NameForm({Key? key}) : super(key: key);

  @override
  NameFormState createState() => NameFormState();
}

class NameFormState extends State<NameForm> {
  final TextEditingController _nameController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _textFieldKey = GlobalKey<FormState>();
  late OnboardBloc _onboardBloc;

  String? _validateName(String? name) {
    if (name?.isEmpty ?? false) {
      return 'Name cannot be empty';
    }
    return null;
  }

  bool isFormValid() {
    if (_formKey.currentState?.validate() ?? false) {
      FocusManager.instance.primaryFocus?.unfocus();
      return true;
    }
    return false;
  }

  void _nameChanged() {
    _onboardBloc.add(NameChanged(name: _nameController.text.trim()));
  }

  @override
  void initState() {
    super.initState();
    _onboardBloc = BlocProvider.of<OnboardBloc>(context);
    _nameController.addListener(_nameChanged);

    if (_onboardBloc.state.name !=null) {
      _nameController.text = _onboardBloc.state.name!;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          const PageTitle(title: 'Business Name', isRequired: true),
          Expanded(
            child: Align(
              alignment: Alignment.center,
              child: Form(
                key: _formKey,
                autovalidate: true,
                child: TextFormField(
                  autofocus: true,
                  validator: _validateName,
                  decoration: InputDecoration(
                    labelText: 'Business Name',
                  ),
                  controller: _nameController,
                ),
              ),
            ),
          ),
        ],
      ),
    ));
  }

  @override
  void dispose() {
    _nameController.removeListener(_nameChanged);
    _nameController.dispose();
    super.dispose();
  }
}
