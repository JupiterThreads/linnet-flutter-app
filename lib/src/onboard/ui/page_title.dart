import 'package:flutter/material.dart';

class PageTitle extends StatelessWidget {

  const PageTitle({required this.title, this.isRequired = false});

  final String title;
  final bool isRequired;

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        children: <InlineSpan>[
          TextSpan(text: title,
            style: Theme.of(context).textTheme.subtitle2),
          const WidgetSpan(
            child: SizedBox(width: 8)
          ),
          TextSpan(
            text: isRequired ? '(Required)' : '(Optional)',
            style: Theme.of(context).textTheme.caption,
          ),
        ],
      ),
    );
  }
}