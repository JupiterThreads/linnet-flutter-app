import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:linnet_flutter_app/src/common/widgets/widgets.dart'
    show LinnetFadeIn;
import 'package:linnet_flutter_app/src/onboard/onboard.dart';
import 'package:linnet_flutter_app/src/services/trader_api_service.dart';
import 'package:linnet_flutter_app/src/user/user.dart';
import 'package:provider/provider.dart';

class OnboardScreen extends StatefulWidget {
  @override
  _OnboardScreenState createState() => _OnboardScreenState();
}

class _OnboardScreenState extends State<OnboardScreen> {
  final PageController _controller = PageController();
  double currentPageValue = 0.0;
  List<Widget> _forms = <Widget>[];
  late OnboardBloc bloc;
  bool isPageFormValid = false;
  int _pageIndex = 0;

  final GlobalKey<NameFormState> _nameFormKey = GlobalKey<NameFormState>();
  // final GlobalKey<BusinessTypeFormState> _businessTypeFormKey =
  //     GlobalKey<BusinessTypeFormState>();
  late Map<double, dynamic> formMapKeys;

  @override
  void initState() {
    super.initState();
    _controller.addListener(_controllerListener);

    final UserBloc uBloc = BlocProvider.of<UserBloc>(context);
    final TraderApiService traderApiService =
        Provider.of<TraderApiService>(context, listen: false);
    bloc = OnboardBloc(userBloc: uBloc, traderApiService: traderApiService);

    formMapKeys = <double, GlobalKey>{
      0: _nameFormKey,
    };
      // 3: _businessTypeFormKey,

    _setForms();
  }

  @override
  void dispose() {
    bloc.close();
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _setForms();
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.close, color: Colors.black),
            onPressed: () => Navigator.of(context).pop()),
        elevation: 0,
        backgroundColor: Colors.white10,
        title: Container(
          margin: EdgeInsets.zero,
          width: 54,
          height: 30,
          child: DecoratedBox(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: [const Color(0xff845EC2), const Color(0xff2C73D2)],
                    begin: FractionalOffset.centerRight,
                    end: FractionalOffset.centerLeft,
                    stops: [0.0, 1.0],
                    ),
                borderRadius: BorderRadius.circular(5),
                color: Colors.amber,
                shape: BoxShape.rectangle),
            child: Center(
              child: Text('${_pageIndex + 1}/${_forms.length}'),
            ),
          ),
        ),
        centerTitle: true,
      ),
      body: SafeArea(
        child: BlocProvider<OnboardBloc>(
          create: (BuildContext context) => bloc,
          child: Column(
            children: <Widget>[
              Expanded(
                child: PageView.builder(
                  pageSnapping: false,
                  controller: _controller,
                  physics: const NeverScrollableScrollPhysics(),
                  itemBuilder: (BuildContext context, int index) {
                    return _buildTransformedPage(index);
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 16.0),
                child: ControlButtons(
                    onNextPressed: _pageIndex < 5 ? _nextStep : null,
                    onBackPressed: _pageIndex > 0 ? _prevStep : null),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildTransformedPage(int position) {
    if (position == currentPageValue.floor()) {
      return _buildTransformWidget(position);
    } else if (position == currentPageValue.floor() + 1) {
      return _buildTransformWidget(position);
    } else {
      return _forms[position];
    }
  }

  Widget _buildTransformWidget(int position) {
    return LinnetFadeIn(child: _forms[position]);
  }

  void _controllerListener() {
    setState(() {
      currentPageValue = _controller.page!;
    });
  }

  void _nextStep() {
    if (!isPageValid()) return;
    _controller.nextPage(
      duration: const Duration(milliseconds: 300),
      curve: Curves.ease,
    );

    setState(() => _pageIndex = _pageIndex + 1);
  }

  bool isPageValid() {
    if (!formMapKeys.containsKey(_pageIndex)) return true;
    final dynamic key = formMapKeys[_pageIndex];
    bool isValid = false;
    try {
      isValid = key.currentState.isFormValid() as bool;
    } catch (error) {
      debugPrint(error.toString());
    }
    return isValid;
  }

  void _prevStep() {
    _controller.previousPage(
        duration: const Duration(milliseconds: 300), curve: Curves.ease);
    setState(() => _pageIndex = _pageIndex - 1);
  }

  bool onWillPop() {
    if (_controller.page?.round() == _controller.initialPage) return true;

    _controller.previousPage(
      duration: const Duration(milliseconds: 500),
      curve: Curves.ease,
    );
    return false;
  }

  // TODO add bank details and maybe avatar later
  void _setForms() {
    _forms = [
      WillPopScope(
        onWillPop: () => Future<bool>.sync(onWillPop),
        child: NameForm(key: _nameFormKey),
      ),
      WillPopScope(
        onWillPop: () => Future<bool>.sync(onWillPop),
        child: LocationForm(key: Key('location_form')),
      ),
      WillPopScope(
        onWillPop: () => Future<bool>.sync(onWillPop),
        child: const PlaceCandidates(key: Key('place_candiates')),
      ),
      // WillPopScope(
      //   onWillPop: () => Future<bool>.sync(onWillPop),
      //   child: BusinessTypeForm(key: _businessTypeFormKey),
      //),
      WillPopScope(
        onWillPop: () => Future<bool>.sync(onWillPop),
        child: const TermsAndConditionsScreen(key: Key('terms_conditions')),
      ),
    ];
  }
}
