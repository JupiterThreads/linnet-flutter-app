import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:linnet_flutter_app/src/common/widgets/widgets.dart'
    show SlideUpRoute, Loading, CardInfo, TermsAndConditionsView;
import 'package:linnet_flutter_app/src/onboard/onboard.dart';
import 'package:linnet_flutter_app/src/trader/models/models.dart' as models;

class TermsAndConditionsScreen extends StatefulWidget {
  const TermsAndConditionsScreen({Key? key}) : super(key: key);

  @override
  TermsAndConditionsScreenState createState() =>
      TermsAndConditionsScreenState();
}

class TermsAndConditionsScreenState extends State<TermsAndConditionsScreen> {
  late OnboardBloc _onboardTraderBloc;

  @override
  void initState() {
    super.initState();
    _onboardTraderBloc = BlocProvider.of<OnboardBloc>(context);
  }

  void _handleAcceptTerms(models.TermsAndConditions terms) {
    _onboardTraderBloc.add(CreateTradeAccount(termsAndConditions: terms));
  }

  void _showSuccessScreen() {
    Navigator.of(context).push(SlideUpRoute(widget: SuccessScreen()));
  }

  // @override
  // Widget loader() {
  //   return const Loading(message: 'Creating trade account...');
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
    body: BlocListener<OnboardBloc, OnboardState>(
        listener: (BuildContext context, OnboardState state) {
          // state.submitStatus.when(
          //     done: (_) {
          //       stopLoading();
          //       _showSuccessScreen();
          //     },
          //     loading: (_) => startLoading(),
          //     error: (_) => stopLoading());
        },
        child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                const PageTitle(title: 'Terms & Conditions', isRequired: true),
                const SizedBox(height: 20),
                const CardInfo(
                    '''Please read the following carefully. If you agree with our terms and conditions, click the accept button at the bottom.'''),
                const SizedBox(height: 20),
                Expanded(
                    child: TermsAndConditionsView(
                        onAcceptedTerms: _handleAcceptTerms)),
              ],
            )),
      ),
    );
  }
}
