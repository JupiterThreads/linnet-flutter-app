import 'package:flutter/material.dart';
import 'package:linnet_flutter_app/routing_constants.dart';

class SuccessScreen extends StatefulWidget {
  @override
  _SuccessScreenState createState() => _SuccessScreenState();
}

class _SuccessScreenState extends State<SuccessScreen> {
  @override
  void initState() {
    super.initState();
    Future<void>.delayed(const Duration(seconds: 3),
        () => Navigator.popUntil(context, ModalRoute.withName(HomeRoute)));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        color: const Color(0xff39DB80),
        width: MediaQuery.of(context).size.width,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                const Icon(
                  Icons.check_circle_outline,
                  size: 100,
                  color: Colors.white,
                ),
                const SizedBox(height: 8),
                Text('Trade account created',
                    textAlign: TextAlign.center,
                    style: Theme.of(context)
                        .textTheme
                        .headline5
                        ?.merge(const TextStyle(color: Colors.white))),
              ]),
        ));
  }
}
