// import 'package:flutter/material.dart';
// import 'package:flutter_form_builder/flutter_form_builder.dart';
// import 'package:linnet_flutter_app/src/onboard/onboard.dart';

// class BankDetails extends StatefulWidget {
//   @override
//   _BankDetailsState createState() => _BankDetailsState();
// }

// class _BankDetailsState extends State<BankDetails> {
//   final GlobalKey<FormBuilderState> _bankFormKey =
//       GlobalKey<FormBuilderState>();
//   final GlobalKey<FormFieldState> _specifyTextFieldKey =
//       GlobalKey<FormFieldState>();

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         body: Padding(
//             padding: EdgeInsets.all(16.0),
//             child: Column(children: <Widget>[
//               const Subtitle(title: 'Bank Details'),
//               const SizedBox(height: 16.0),
//               Expanded(
//                 child: FormBuilder(
//                   key: _bankFormKey,
//                   child: Column(
//                     children: <Widget>[
//                       FormBuilderTextField(
//                         autocorrect: false,
//                         decoration: const InputDecoration(labelText: "Bank name"),
//                         validators: [FormBuilderValidators.minLength(2)],
//                         attribute: 'name',
//                       ),
//                       const SizedBox(height: 16.0),
//                     ],
//                   ),
//                 )),
//             ])));
//   }
// }
