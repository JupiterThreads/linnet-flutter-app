import 'package:freezed_annotation/freezed_annotation.dart';

part 'register_event.freezed.dart';

@freezed
class RegisterEvent with _$RegisterEvent {
  const factory RegisterEvent.emailChanged({required String email}) =
      EmailChanged;
  const factory RegisterEvent.passwordChanged({required String password}) =
      PasswordChanged;
  const factory RegisterEvent.nameChanged({required String name}) =
      NameChanged;
  const factory RegisterEvent.registerSubmitted() = RegisterSubmitted;
  const factory RegisterEvent.reset() = RegisterReset;
  const factory RegisterEvent.error({required String message}) = RegisterError;
}
