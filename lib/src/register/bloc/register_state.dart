import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:linnet_flutter_app/src/common/models/api_state.dart';

part 'register_state.freezed.dart';

@freezed
class RegisterState with _$RegisterState {
  const factory RegisterState({
    @Default('') String email,
    @Default('') String password,
    @Default('') String name,
    @Default(null) ApiState? registerSubmitStatus,
  }) = _RegisterState;
}
