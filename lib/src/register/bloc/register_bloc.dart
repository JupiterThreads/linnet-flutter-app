import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';
import 'package:linnet_flutter_app/src/common/models/api_state.dart';
import 'package:linnet_flutter_app/src/register/bloc/bloc.dart';
import 'package:linnet_flutter_app/src/services/user_api_service.dart';
import 'package:linnet_flutter_app/src/user/models/user.dart';
import 'package:linnet_flutter_app/src/utils/utils.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  RegisterBloc({
    required this.userApiService,
  }) : super(const RegisterState());

  UserApiService userApiService;

  @override
  Stream<RegisterState> mapEventToState(RegisterEvent event) async* {
    if (event is EmailChanged) {
      yield state.copyWith(email: event.email);
    }

    if (event is PasswordChanged) {
      yield state.copyWith(
        password: event.password,
      );
    }

    if (event is NameChanged) {
      yield state.copyWith(
        name: event.name,
      );
    }

    if (event is RegisterSubmitted) {
      yield state.copyWith(registerSubmitStatus: const ApiState.loading());

      final Map<String, dynamic> data = <String, dynamic>{
        'name': state.name,
        'password': state.password,
        'email': state.email
      };

      try {
        await userApiService.register(data);

        yield state.copyWith(
          registerSubmitStatus: 
          const ApiState.done('User account created successfully!'),
        );

      } catch (err) {
        yield state.copyWith(
          registerSubmitStatus: ApiState.error(err.toString()),
        );
        yield state.copyWith(
          registerSubmitStatus: null,
        );
      }
    }
  }
}
