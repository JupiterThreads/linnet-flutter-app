import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:linnet_flutter_app/src/register/register.dart';
import 'package:linnet_flutter_app/src/register/ui/register_form.dart';
import 'package:linnet_flutter_app/src/services/user_api_service.dart';
import 'package:provider/provider.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  late UserApiService userApiService;

  @override
  void initState() {
    userApiService = Provider.of<UserApiService>(context, listen: false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Register'),
        ),
        body: BlocProvider<RegisterBloc>(
          create: (BuildContext context) => RegisterBloc(
              userApiService: userApiService),
          child: const Padding(
              padding: EdgeInsets.all(36.0), child: RegisterForm()),
        ));
  }
}
