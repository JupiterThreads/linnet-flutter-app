import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:linnet_flutter_app/routing_constants.dart';
import 'package:linnet_flutter_app/src/register/bloc/bloc.dart';
import 'package:linnet_flutter_app/src/snackbar/snackbar.dart';
import 'package:progress_state_button/iconed_button.dart';
import 'package:progress_state_button/progress_button.dart';
import 'package:validatorless/validatorless.dart';

class RegisterForm extends StatefulWidget {
  const RegisterForm({Key? key}) : super(key: key);

  @override
  _RegisterFormState createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  late RegisterBloc _registerBloc;
  late SnackbarCubit _snackbarCubit;
  bool passwordVisibilityOn = false;
  bool password2VisibilityOn = false;
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _password2Controller = TextEditingController();
  final GlobalKey<FormState> _registerForm = GlobalKey<FormState>();
  ButtonState buttonState = ButtonState.idle;

  @override
  void dispose() {
    _passwordController.dispose();
    _emailController.dispose();
    _nameController.dispose();
    _password2Controller.dispose();
    super.dispose();
  }

  @override
  void initState() {
    _registerBloc = BlocProvider.of<RegisterBloc>(context);
    _snackbarCubit = BlocProvider.of<SnackbarCubit>(context);

    _emailController.text = _registerBloc.state.email;
    _nameController.text = _registerBloc.state.name;

    _emailController.addListener(_onEmailChanged);
    _passwordController.addListener(_onPasswordChanged);
    _nameController.addListener(_onNameChanged);
    super.initState();
  }

  void _onPasswordChanged() {
    _registerBloc
        .add(PasswordChanged(password: _passwordController.text.trim()));
  }

  void _onEmailChanged() {
    _registerBloc.add(EmailChanged(email: _emailController.text.trim()));
  }

  void _onNameChanged() {
    _registerBloc.add(NameChanged(name: _nameController.text.trim()));
  }

  Future<void> _submitForm() async {
    final bool isValid = _registerForm.currentState?.validate() ?? false;
    if (isValid) {
      _registerBloc.add(const RegisterSubmitted());
    }
  }

  Future<void> _onRegisterSuccess(BuildContext context) async {
    setState(() => buttonState = ButtonState.success);
    await Future<dynamic>.delayed(const Duration(seconds: 2));
    Navigator.popAndPushNamed(context, LoginRoute);
  }

  void _onRegisterError(BuildContext context, String message) {
    _snackbarCubit.showError(message, title: 'Registeration Error');
    setState(() => buttonState = ButtonState.fail);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<RegisterBloc, RegisterState>(
      listener: (BuildContext context, RegisterState state) {
        state.registerSubmitStatus?.maybeWhen(
            loading: (_) => setState(() => buttonState = ButtonState.loading),
            done: (String? message) => _onRegisterSuccess(context),
            error: (String? message) => _onRegisterError(context, message!),
            orElse: () => {});
      },
      child: Form(
        key: _registerForm,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TextFormField(
              autovalidateMode: AutovalidateMode.onUserInteraction,
              autocorrect: false,
              controller: _nameController,
              validator: Validatorless.multiple([
                Validatorless.required('This is a required field'),
              ]),
              decoration: const InputDecoration(
                labelText: 'Name',
              ),
            ),
            const SizedBox(height: 16.0),
            TextFormField(
              autovalidateMode: AutovalidateMode.onUserInteraction,
              autocorrect: false,
              controller: _emailController,
              validator: Validatorless.multiple([
                Validatorless.email('Invalid Email'),
                Validatorless.required('This is a required field'),
              ]),
              decoration: const InputDecoration(
                suffixIcon: Icon(Icons.email),
                labelText: 'Email',
              ),
            ),
            const SizedBox(height: 16.0),
            TextFormField(
              autovalidateMode: AutovalidateMode.onUserInteraction,
              autocorrect: false,
              controller: _passwordController,
              validator: Validatorless.multiple([
                Validatorless.min(
                    5, 'Password must contain at least 5 characters'),
                Validatorless.required('This is a required field'),
              ]),
              decoration: InputDecoration(
                errorMaxLines: 2,
                labelText: 'Password',
                suffixIcon: IconButton(
                  onPressed: () => setState(
                      () => passwordVisibilityOn = !passwordVisibilityOn),
                  icon: Icon(passwordVisibilityOn
                      ? Icons.visibility
                      : Icons.visibility_off),
                ),
              ),
              obscureText: !passwordVisibilityOn,
            ),
            const SizedBox(height: 16.0),
            TextFormField(
              autovalidateMode: AutovalidateMode.onUserInteraction,
              autocorrect: false,
              controller: _password2Controller,
              validator: Validatorless.multiple([
                Validatorless.compare(
                    _passwordController, 'Password does not match'),
                Validatorless.required('This is a required field'),
              ]),
              decoration: InputDecoration(
                errorMaxLines: 2,
                labelText: 'Confirm Password',
                suffixIcon: IconButton(
                  onPressed: () => setState(
                      () => password2VisibilityOn = !password2VisibilityOn),
                  icon: Icon(password2VisibilityOn
                      ? Icons.visibility
                      : Icons.visibility_off),
                ),
              ),
              obscureText: !password2VisibilityOn,
            ),
            const SizedBox(height: 20.0),
            Container(
              width: 200,
              child: buildProgressButton(),
            ),
          ],
        ),
      ),
    );
  }

  void onPressed() {
    switch (buttonState) {
      case ButtonState.idle:
        _submitForm();
        break;
      case ButtonState.loading:
        break;
      case ButtonState.success:
        buttonState = ButtonState.idle;
        break;
      case ButtonState.fail:
        buttonState = ButtonState.idle;
        break;
    }
    setState(() {
      buttonState = buttonState;
    });
  }

  Widget buildProgressButton() {
    return ProgressButton.icon(iconedButtons: {
      ButtonState.idle: IconedButton(
          text: 'REGISTER',
          icon: Icon(Icons.send, color: Colors.white),
          color: Colors.deepPurple.shade500),
      ButtonState.loading:
          IconedButton(text: 'Loading', color: Colors.deepPurple.shade700),
      ButtonState.fail: IconedButton(
          text: 'Failed',
          icon: Icon(Icons.cancel, color: Colors.white),
          color: Colors.red.shade300),
      ButtonState.success: IconedButton(
          text: 'Success',
          icon: Icon(
            Icons.check_circle,
            color: Colors.white,
          ),
          color: Colors.green.shade400)
    }, onPressed: onPressed, state: buttonState);
  }
}
