import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SocketProvider extends ChangeNotifier {
  static SocketProvider of(BuildContext context, {bool listen = true}) =>
      Provider.of<SocketProvider>(context, listen: listen);

  dynamic _data;

  dynamic get data => _data;

  void setData(dynamic data) {
    if (_data != data) {
      _data = data;
      notifyListeners();
    }
  }
}
