import 'package:flutter/material.dart';

class Paginate<T> {
  Paginate({
    required this.count,
    this.next,
    this.previous,
    required this.results,
  });

  int count;
  String? next;
  String? previous;
  List<T> results;

  factory Paginate.fromJson(Map<String, dynamic> json, Function fromJsonModel) {
    final dynamic results = json['results'].cast<Map<String, dynamic>>();
    final List<T> accounts = <T>[];

    // NO bloody idea why I couldn't map through them, but this works
    for (var result in results) {
      final account = fromJsonModel(result as Map<String, dynamic>);
      accounts.add(account as T);
    }

    return Paginate<T>(
        count: json['count'] as int,
        next: json['next'] as String?,
        previous: json['previous'] as String?,
        results: accounts);
  }
}
