import 'package:freezed_annotation/freezed_annotation.dart';

part 'api_state.freezed.dart';

@freezed
class ApiState with _$ApiState {
  const factory ApiState.done([@Default(null) String? message]) = ApiStateDone;
  const factory ApiState.loading([@Default(null) String? message]) = ApiStateLoading;
  const factory ApiState.error([@Default(null) String? message]) = ApiStateError;
}
