import 'package:flutter/material.dart';

class DropdownFilter extends StatefulWidget {
  const DropdownFilter(
      {Key? key, 
      required this.options, 
      required this.onSelectCallback, 
      required this.displayHintVal})
      : super(key: key);

  final List<String> options;
  final Function onSelectCallback;
  final String displayHintVal;

  @override
  _DropdownFilterState createState() => _DropdownFilterState();
}

class _DropdownFilterState extends State<DropdownFilter> {
  late String? _hintVal;

  @override
  void initState() {
    super.initState();
    _hintVal = widget.displayHintVal;
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 120,
      child: DropdownButtonHideUnderline(
        child: ButtonTheme(
          padding: EdgeInsets.zero,
          alignedDropdown: true,
          child: DropdownButton<String>(
              isDense: true,
              isExpanded: true,
              items: widget.options.map((String val) {
                return DropdownMenuItem<String>(
                  value: val,
                  child: Text(val, style: Theme.of(context).textTheme.subtitle2),
                );
              }).toList(),
              onChanged: _handleOnChange,
              iconEnabledColor: Colors.white,
              hint: Text(
                _hintVal ?? '',
                style: const TextStyle(color: Colors.white, fontSize: 10.0),
              )),
        ),
      ),
    );
  }

  void _handleOnChange(String? val) {
    widget.onSelectCallback(val);
    setState(() => _hintVal = val);
  }
}
