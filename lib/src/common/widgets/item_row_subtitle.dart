import 'package:flutter/material.dart';

class ItemRowSubtitle extends StatelessWidget {
  const ItemRowSubtitle({Key? key, this.subtitleText, this.children})
      : super(key: key);

  final List<Widget>? children;
  final String? subtitleText;

  Text _buildSubtitleTextWidget() {
    return Text(subtitleText!, maxLines: 2, overflow: TextOverflow.ellipsis);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        if (subtitleText != null) _buildSubtitleTextWidget(),
        if (children != null) ...children!
      ],
    );
  }
}
