import 'package:flutter/material.dart';

class LinnetDialog extends StatelessWidget {

  const LinnetDialog({required this.options, required this.title});
  final List<dynamic> options;
  final String title;

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      title: Text(title),
      children: <Widget>[
        ...options.map((dynamic option) => 
          SimpleDialogOption(
            onPressed: () => Navigator.pop(context, option['value']),
            child: Text(option['name'] as String),
          )
        ).toList(),
      ]
    );
  }
}