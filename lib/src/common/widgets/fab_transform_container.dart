import 'package:animations/animations.dart';
import 'package:flutter/material.dart';

const double _fabDimension = 56.0;

class FabTransformContainer extends StatelessWidget {
  FabTransformContainer({required this.containerWidget});

  Widget containerWidget;
  final ContainerTransitionType _transitionType = ContainerTransitionType.fade;

  @override
  Widget build(BuildContext context) {
    return OpenContainer(
        transitionType: _transitionType,
        closedShape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(_fabDimension / 2),
          ),
        ),
        closedElevation: 6.0,
        openBuilder: (BuildContext context, VoidCallback _) {
          return containerWidget;
        },
        closedBuilder: (BuildContext context, VoidCallback openContainer) {
          return SizedBox(
            height: _fabDimension,
            width: _fabDimension,
            child: Center(
              child: Icon(
                Icons.add,
                color: Theme.of(context).accentColor,
              ),
            ),
          );
        });
  }
}
