import 'package:flutter/material.dart';
import 'package:linnet_flutter_app/src/common/widgets/item_selected.dart';

class ItemRowTitle extends StatelessWidget {
  const ItemRowTitle(
      {Key? key,
      required this.name,
      this.isSelected = false,
      this.qty,
      this.nameStyle = const TextStyle(fontSize: 18)})
      : super(key: key);

  final String name;
  final bool isSelected;
  final int? qty;
  final TextStyle nameStyle;

  Widget _buildTitle() {
    if (isSelected) {
      return ItemSelected(qty: qty!, name: name, nameStyle: nameStyle);
    }
    return Text(name, style: nameStyle);
  }

  @override
  Widget build(BuildContext context) {
    return _buildTitle();
  }
}
