import 'package:flutter/material.dart';

class SlideUpRoute extends PageRouteBuilder {
  SlideUpRoute({ required this.widget })
      : super( 
        pageBuilder: (BuildContext context, Animation<double> animation,
        Animation<double> secondaryAnimation) {
      return widget;
    },
    transitionsBuilder: (
      BuildContext context, 
      Animation<double> animation, 
      Animation<double> secondaryAnimation, 
      Widget child) {
        final Animatable<Offset> tween = Tween<Offset>(
          begin: const Offset(0.0, 1.0), end: Offset.zero)
          .chain(CurveTween(curve: Curves.ease));
       return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      }
  );
  final Widget widget;
}
