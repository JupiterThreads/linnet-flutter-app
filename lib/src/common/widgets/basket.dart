import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Basket extends StatelessWidget {
  Basket({
    this.basketColor,
    this.badgeColor,
    this.badgeContentColor,
    this.badgePosition,
  });

  Color? basketColor;
  Color? badgeColor;
  Color? badgeContentColor;
  BadgePosition? badgePosition;

  @override
  Widget build(BuildContext context) {
    // TOOD CHANGE THIS
    const int itemCount = 7;

    return IconButton(
      color: basketColor ?? Colors.white,
      icon: Badge(
        badgeColor: badgeColor ?? Colors.white,
        animationType: BadgeAnimationType.scale,
        showBadge: false,
        badgeContent: Text(
          '${itemCount.toString()}',
          style: TextStyle(color: badgeContentColor ?? Colors.red),
        ),
        position: badgePosition ?? BadgePosition.topEnd(),
        child: Icon(Icons.shopping_basket),
      ),
      onPressed: () {},
    );
    // return BlocBuilder<DraftOrderBloc, DraftOrderState>(
    //   builder: (BuildContext context, DraftOrderState state) {
    //     int itemCount = 0;
    //     if (state?.draftOrderMap != null) {
    //       itemCount = DraftOrderItem.getItemsCount(state.draftOrderMap);
    //     }

    //   },
    // );
  }
}
