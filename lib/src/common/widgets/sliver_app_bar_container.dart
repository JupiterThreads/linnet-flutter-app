import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:linnet_flutter_app/src/common/widgets/loading_flexspacebar.dart';

class SliverAppBarContainer extends StatelessWidget {
  SliverAppBarContainer(
      {required this.titleText,
      required this.widgets,
      this.coverImage,
      this.actions,
      this.scrollController});
  final String titleText;
  final List<Widget> widgets;
  final String? coverImage;
  final List<Widget>? actions;
  ScrollController? scrollController;

  bool get _hasCoverImage => coverImage != null;

  CachedNetworkImage _buildCoverImage() {
    return CachedNetworkImage(
      imageUrl: coverImage!,
      imageBuilder: (BuildContext context, ImageProvider imageProvider) {
        return FittedBox(fit: BoxFit.fill, child: Image(image: imageProvider));
      },
      placeholder: (BuildContext context, String url) =>
          LoadingFlexibleSpaceBar(),
      errorWidget: (BuildContext context, String url, dynamic error) =>
          const Icon(Icons.store, size: 60, color: Colors.white),
    );
  }

  Widget _buildFlexibleSpacebar(BuildContext context) {
    return FlexibleSpaceBar(
      title: Text(
        titleText,
        textAlign: TextAlign.center,
        style: Theme.of(context).appBarTheme.textTheme?.headline6,
      ),
      background: _buildCoverImage(),
    );
  }

  @override
  Widget build(BuildContext context) {
      scrollController ??=
          PrimaryScrollController.of(context) ?? ScrollController();
    return CustomScrollView(
      controller: scrollController,
      slivers: <Widget>[
        SliverAppBar(
          title: _hasCoverImage ? null : Text(titleText),
          expandedHeight:
              _hasCoverImage ? MediaQuery.of(context).size.height / 4 : null,
          flexibleSpace:
              _hasCoverImage ? _buildFlexibleSpacebar(context) : null,
          actions: actions,
        ),
        ...widgets,
      ],
    );
  }
}
