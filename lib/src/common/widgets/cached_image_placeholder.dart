import 'package:flutter/material.dart';

class CachedImagePlaceholder extends StatelessWidget {
  const CachedImagePlaceholder({Key? key, this.width = 45, this.height = 45})
      : super(key: key);

  final double width;
  final double height;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        width: width,
        height: height,
        child: const CircularProgressIndicator(),
      ),
    );
  }
}
