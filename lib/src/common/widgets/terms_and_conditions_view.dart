import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:linnet_flutter_app/src/common/widgets/widgets.dart' show Loading;
import 'package:linnet_flutter_app/src/services/trader_api_service.dart';
import 'package:linnet_flutter_app/src/snackbar/cubit/snackbar_cubit.dart';
import 'package:linnet_flutter_app/src/trader/models/terms_and_conditions.dart';
import 'package:linnet_flutter_app/src/utils/utils.dart';
import 'package:provider/provider.dart';

class TermsAndConditionsView extends StatefulWidget {
  const TermsAndConditionsView({required this.onAcceptedTerms});
  final Function onAcceptedTerms;

  @override
  _TermsAndConditionsViewState createState() => _TermsAndConditionsViewState();
}

class _TermsAndConditionsViewState extends State<TermsAndConditionsView> {
  bool _isChecked = false;
  TermsAndConditions? termsConditions;

  @override
  void initState() {
    super.initState();
    _getTermsConditions();
  }

  Future<void> _getTermsConditions() async {
    SnackbarCubit snackbarCubit = BlocProvider.of<SnackbarCubit>(context);
    final TraderApiService traderApiService =
        Provider.of<TraderApiService>(context, listen: false);

    try {
      final Response<TermsAndConditions> response =
          await traderApiService.getTermsAndConditions();
      final TermsAndConditions terms = response.body!;
      setState(() => termsConditions = terms);
    } catch (error) {
      debugPrint(error.toString());
      snackbarCubit
          .showError('Something went wrong retrieving terms and conditions.');
    }
  }

  Widget _bodyContent() {
    if (termsConditions != null) {
      return Column(
        children: <Widget>[
          Html(data: termsConditions?.content ?? ''),
          const SizedBox(height: 20),
          ListTileTheme(
            contentPadding: EdgeInsets.zero,
            child: CheckboxListTile(
              dense: true,
              controlAffinity: ListTileControlAffinity.leading,
              value: _isChecked,
              title: const Text(
                  '''Check to confirm that you have read and understood our terms and conditions.'''),
              onChanged: (bool? isAccepted) =>
                  setState(() => _isChecked = isAccepted!),
            ),
          ),
          const SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              TextButton(
                  onPressed: () => Navigator.pop(context),
                  child: const Text('Decline')),
              const SizedBox(width: 8),
              ElevatedButton(
                  onPressed: !_isChecked
                      ? null
                      : () => widget.onAcceptedTerms(termsConditions),
                  child: const Text('Accept')),
            ],
          )
        ],
      );
    }
    return const Center(
        child: Loading(message: 'Loading terms and conditions...'));
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: _bodyContent(),
    );
  }
}
