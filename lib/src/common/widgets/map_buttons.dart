import 'package:flutter/material.dart';

class MapButtons extends StatelessWidget {
  const MapButtons({Key? key, required this.moveToUserLocation, required this.toggleMapType})
      : super(key: key);

  final VoidCallback moveToUserLocation;
  final VoidCallback toggleMapType;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        FloatingActionButton(
          heroTag: 'myLocation',
          materialTapTargetSize: MaterialTapTargetSize.padded,
          mini: true,
          backgroundColor: Theme.of(context).accentColor,
          onPressed: moveToUserLocation,
          child: Icon(Icons.my_location),
        ),
        const SizedBox(height: 8),
        FloatingActionButton(
          heroTag: 'layers',
          materialTapTargetSize: MaterialTapTargetSize.padded,
          mini: true,
          backgroundColor: Theme.of(context).accentColor,
          onPressed: toggleMapType,
          child: Icon(Icons.map),
        ),
      ],
    );
  }
}
