import 'package:flutter/material.dart';

class CircularAvatar extends StatelessWidget {
  const CircularAvatar({
    Key? key,
    required this.imageProvider,
    this.height = 60,
    this.width = 60,
  }) : super(key: key);

  final ImageProvider imageProvider;
  final double width;
  final double height;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(image: imageProvider, fit: BoxFit.scaleDown)),
    );
  }
}
