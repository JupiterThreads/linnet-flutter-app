import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:linnet_flutter_app/constants.dart';

class UserAccountBadge extends StatelessWidget {

  const UserAccountBadge({
    required this.accountType,
  });

  final String accountType;

  @override
  Widget build(BuildContext context) {
    return Badge(
      badgeColor: AccountBadgeColor[accountType]!,
      badgeContent: Text(accountType.toUpperCase(), 
        style: TextStyle(color: Colors.white, fontSize: 10)),
      shape: BadgeShape.square,
      borderRadius: BorderRadius.circular(20),
      toAnimate: false,
    );
  }
}