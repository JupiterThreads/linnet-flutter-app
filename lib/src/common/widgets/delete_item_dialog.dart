
import 'package:flutter/material.dart';

class DeleteItemDialog extends StatelessWidget {

  const DeleteItemDialog({
    required this.itemName,
    this.title,
  });

  final String itemName;
  final String? title;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(title ?? 'Delete Item'),
      content: RichText(
        text: TextSpan(
          style: const TextStyle(
            fontSize: 14.0,
            color: Colors.black,
          ),
          children: <TextSpan>[
            const TextSpan(text: 'Are you sure you want to delete '),
            TextSpan(text: '$itemName?',
              style: const TextStyle(fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
      actions: <Widget>[
        TextButton(
          onPressed: () => Navigator.pop(context), 
          child: const Text('CANCEL'),
        ),
        TextButton(
          onPressed: () => Navigator.pop(context, true),
          child: const Text('DELETE'),
        ),
      ],
    );
  }
}