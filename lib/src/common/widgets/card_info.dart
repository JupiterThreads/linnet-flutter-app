import 'package:flutter/material.dart';

class CardInfo extends StatelessWidget {

  const CardInfo(this.textContent);
  final String textContent;

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.zero,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                const Icon(Icons.info_outline),
                const SizedBox(width: 8),
                Expanded(
                  child: Text(textContent,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
