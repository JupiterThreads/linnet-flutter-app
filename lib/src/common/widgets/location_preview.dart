import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:linnet_flutter_app/src/common/widgets/map_buttons.dart';
import 'package:linnet_flutter_app/src/trader/trader.dart';
import 'package:linnet_flutter_app/src/user/user.dart';
import 'package:provider/provider.dart';

class LocationPreview extends StatefulWidget {
  const LocationPreview(
      {this.selectedPosition,
      this.markerPositions,
      this.markSelectedPosition = false,
      this.onTap,
      this.enableScroll = false,
      this.zoom = 18,
      this.enableUserLocation = false,
      this.getMarkerTrader});
  final LatLng? selectedPosition;
  final List<LatLng>? markerPositions;
  final bool enableScroll;
  final Function? onTap;
  final bool markSelectedPosition;
  final double zoom;
  final bool enableUserLocation;
  final Function? getMarkerTrader;

  @override
  _LocationPreviewState createState() => _LocationPreviewState();
}

class _LocationPreviewState extends State<LocationPreview> {
  LatLng? get _selectedPosition => widget.selectedPosition;
  bool get _enableScroll => widget.enableScroll;
  List<LatLng>? get _markerPositions => widget.markerPositions;
  final Set<Marker> _markers = <Marker>{};
  final Completer<GoogleMapController> _controller =
      Completer<GoogleMapController>();
  MapType _currentMapType = MapType.normal;

  @override
  void didUpdateWidget(LocationPreview oldWidget) {
    super.didUpdateWidget(oldWidget);

    if (oldWidget.selectedPosition != widget.selectedPosition &&
        widget.selectedPosition != null) {
      _updateMap(widget.selectedPosition!);
      _setMarkers();
    }
  }

  Future<void> _onMapCreated(GoogleMapController controller) async {
    _controller.complete(controller);
    if (_selectedPosition != null) {
      _updateMap(_selectedPosition!);
    }
  }

  Future<void> _updateMap(LatLng position) async {
    final GoogleMapController cont = await _controller.future;
    final CameraPosition newPosition = CameraPosition(
      target: position,
      zoom: widget.zoom,
    );
    cont.animateCamera(CameraUpdate.newCameraPosition(newPosition));
  }

  void _toggleMapType() {
    setState(() {
      _currentMapType = _currentMapType == MapType.normal
          ? MapType.satellite
          : MapType.normal;
    });
  }

  void _onCameraMove(CameraPosition position) {}

  String _getInfoTitle(TradeAccount trader) {
    if (trader.distanceDisplay.isNotEmpty) {
      return '${trader.name} (${trader.distanceDisplay})';
    }
    return trader.name;
  }

  InfoWindow _getInfoWindow(TradeAccount? trader, {required bool showInfo}) {
    if (!showInfo || trader == null) {
      return InfoWindow.noText;
    }

    return InfoWindow(
      title: _getInfoTitle(trader),
      snippet: trader.getBusinessTypesDisplay(),
    );
  }

  TradeAccount? _getTraderFromMarker(int? index) {
    if (widget.getMarkerTrader != null && index != null) {
      return widget.getMarkerTrader!(index) as TradeAccount;
    }
    return null;
  }

  void _setMarker(LatLng position, {bool showInfo = false, int? index}) {
    final TradeAccount? trader = _getTraderFromMarker(index);
    _markers.add(Marker(
      infoWindow: _getInfoWindow(trader, showInfo: showInfo),
      markerId: MarkerId(position.toString()),
      position: position,
    ));
  }

  void _setMarkers() {
    setState(() {
      _markers.clear();
      if (_markerPositions != null) {
        for (int i = 0; i < _markerPositions!.length; i++) {
          final LatLng position = _markerPositions![i];
          _setMarker(position, showInfo: true, index: i);
        }
      }
      if (widget.markSelectedPosition && widget.selectedPosition != null) {
        _setMarker(widget.selectedPosition!);
      }
    });
  }

  @override
  void initState() {
    super.initState();
    _setMarkers();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Builder(builder: (BuildContext context) {
        if (widget.selectedPosition == null) {
          return const Center(child: CircularProgressIndicator());
        }
        return _buildMap();
      }),
    );
  }

  Widget _buildMap() {
    return Stack(
      children: <Widget>[
        GoogleMap(
            onTap: widget.onTap != null
                ? (LatLng latLng) => widget.onTap!()
                : null,
            initialCameraPosition: CameraPosition(
              target: widget.selectedPosition!,
              zoom: widget.zoom,
            ),
            markers: _markers,
            onMapCreated: (GoogleMapController controller) =>
                _onMapCreated(controller),
            mapType: _currentMapType,
            scrollGesturesEnabled: _enableScroll,
            myLocationButtonEnabled: false,
            myLocationEnabled: widget.enableUserLocation,
            onCameraMove: _onCameraMove,
            gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>{
              Factory<OneSequenceGestureRecognizer>(
                () => EagerGestureRecognizer(),
              ),
            }),
        if (widget.enableUserLocation)
          Align(
            alignment: Alignment.topRight,
            child: Container(
              margin: const EdgeInsets.only(top: 70, right: 8),
              child: MapButtons(
                moveToUserLocation: () => _updateMap(
                    context.read<CurrentLocationProvider>().location!),
                toggleMapType: _toggleMapType,
              ),
            ),
          )
      ],
    );
  }
}
