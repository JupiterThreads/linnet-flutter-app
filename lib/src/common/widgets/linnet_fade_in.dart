import 'package:flutter/material.dart';

class LinnetFadeIn extends StatefulWidget {
  const LinnetFadeIn({Key? key, required this.child}) : super(key: key);
  final Widget child;

  @override
  _LinnetFadeInState createState() => _LinnetFadeInState();
}

class _LinnetFadeInState extends State<LinnetFadeIn>
    with TickerProviderStateMixin {
  late AnimationController _animationController;
  late Animation<double> _animation;

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 600));
    _animation = Tween<double>(begin: 0.0, end: 1.0).animate(
        CurvedAnimation(curve: Curves.easeIn, parent: _animationController));
    _animationController.forward();
  }

  @override
  Widget build(BuildContext context) {
    _animationController.forward();
    return FadeTransition(
      opacity: _animation,
      child: widget.child,
    );
  }
}
