import 'dart:async';

import 'package:flutter/material.dart';

class IconScaleAnimation extends StatefulWidget {
  const IconScaleAnimation({required this.icon});
  final Icon icon;

  @override
  _IconScaleAnimationState createState() => _IconScaleAnimationState();
}

class _IconScaleAnimationState extends State<IconScaleAnimation>
    with TickerProviderStateMixin {
  late AnimationController _animationController;
  late Animation<double> _animation;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 1000), value: 0.2);
    _animation = CurvedAnimation(
        parent: _animationController, curve: Curves.bounceInOut);
    _animationController.forward();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ScaleTransition(
      scale: _animation,
      child: widget.icon,
    );
  }
}
