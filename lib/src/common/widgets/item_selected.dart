import 'package:badges/badges.dart';
import 'package:flutter/material.dart';

class ItemSelected extends StatelessWidget {

  const ItemSelected({
    required this.qty, 
    required this.name,
    this.nameStyle,
  });
  
  final int qty;
  final String name;
  final TextStyle? nameStyle;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        if (qty > 0)
        Badge(
          toAnimate: false,
          badgeColor: Colors.green,
          badgeContent: Text(qty.toString(),
                          style:
                              TextStyle(color:Colors.white),
                          ),
        ),
        if (qty > 0)
        const SizedBox(width: 12),
        Text(name, style: nameStyle),
      ],        
    );
  }
}