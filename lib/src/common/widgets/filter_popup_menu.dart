import 'package:flutter/material.dart';
import 'package:linnet_flutter_app/constants.dart';

class FilterPopupMenu extends StatelessWidget {
  FilterPopupMenu({required this.onSelected});
  ValueChanged<FilterType> onSelected;

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton<FilterType>(
        icon: Icon(Icons.filter_list),
        onSelected: (FilterType type) => onSelected(type),
        itemBuilder: (BuildContext context) {
          return filterOptionsMap.map((Map<String, dynamic> option) {
            return PopupMenuItem<FilterType>(
              value: option['value'] as FilterType,
              child: Text(option['name'] as String),
            );
          }).toList();
        });
  }
}
