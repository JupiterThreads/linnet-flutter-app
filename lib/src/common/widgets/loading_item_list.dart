import 'package:flutter/material.dart';
import 'package:linnet_flutter_app/src/common/widgets/list_item_shimmer.dart';

class LoadingItemList extends StatelessWidget {
  const LoadingItemList({this.traderView = false});
  final bool traderView;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Column(
        children: <Widget>[
          Expanded(
            child: ListView.separated(
              padding: const EdgeInsets.all(16.0),
              itemBuilder: (_, __) => Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: ListItemShimmer(traderView: traderView),
              ),
              itemCount: 20,
              separatorBuilder: (_, __) =>
                  const Divider(indent: 16, endIndent: 16),
            ),
          ),
        ],
      ),
    );
  }
}
