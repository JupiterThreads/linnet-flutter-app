import 'package:flutter/material.dart';

class DummySearchInput extends StatelessWidget {
  const DummySearchInput({this.onTap});
  final VoidCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: Theme.of(context).brightness == Brightness.dark
              ? Colors.black54
              : Colors.grey[50],
        ),
        child: TextField(
          style: const TextStyle(fontSize: 12.0),
          enabled: false,
          decoration: InputDecoration(
            isDense: true,
            prefixIconConstraints: const BoxConstraints(
              minHeight: 32,
              minWidth: 32,
            ),
            prefixIcon: Icon(Icons.search, color: Colors.black),
            hintText: 'Search traders',
            border: InputBorder.none,
          ),
        ),
      ),
    );
  }
}
