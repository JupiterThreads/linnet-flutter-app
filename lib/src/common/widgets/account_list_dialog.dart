import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:linnet_flutter_app/src/common/widgets/circle_account_avatar.dart';
import 'package:linnet_flutter_app/src/common/widgets/user_account_badge.dart';
import 'package:linnet_flutter_app/src/trader/trader.dart';
import 'package:linnet_flutter_app/src/user/user.dart';

class AccountListDialog extends StatelessWidget {
  const AccountListDialog({
    required this.accountList,
  });

  final List<dynamic> accountList;

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      contentPadding: EdgeInsets.zero,
      title: const Text('Select Account'),
      children: <Widget>[
        Container(
          padding: const EdgeInsets.fromLTRB(8, 24.0, 16, 0.0),
          height: 300,
          width: 200,
          child: ListView.separated(
            separatorBuilder: (BuildContext context, int index) =>
                const Divider(),
            itemCount: accountList.length,
            itemBuilder: (BuildContext context, int index) =>
                _buildAccountRow(context, accountList[index]),
          ),
        )
      ],
    );
  }

  Widget _buildAccountRow(BuildContext context, dynamic account) {
    return ListTile(
      leading: CircleAccountAvatar(account: account),
      contentPadding: EdgeInsets.zero,
      dense: true,
      title: _buildTitle(account),
      onTap: () => Navigator.pop(context, account),
      subtitle: _buildSubtitle(context, account),
      trailing: _buildTrailing(account),
    );
  }

  Widget? _buildTrailing(dynamic account) {
    if (account is TradeAccount) {
      return account.isVisible
          ? const Icon(
              Icons.visibility,
              color: Colors.lightGreen,
            )
          : const Icon(Icons.visibility_off);
    }
    return null;
  }

  Widget _buildTitle(dynamic account) {
    String content;
    if (account is TradeAccount) {
      content = account.name;
    } else {
      content = account.name as String;
    }
    return Text(
      content,
      style: const TextStyle(
        fontSize: 16,
      ),
    );
  }

  Widget _buildBadge(String accountType) {
    return Padding(
      padding: const EdgeInsets.only(right: 8.0),
      child: UserAccountBadge(accountType: accountType),
    );
  }

  Widget _buildSubtitle(BuildContext context, dynamic account) {
    final UserBloc userBloc = BlocProvider.of<UserBloc>(context);
    if (account is TradeAccount) {
      final String accountType = userBloc.state.user!.getAccountType(account);
      return Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[_buildBadge(accountType)],
        ),
      );
    }
    return Text(account.email as String);
  }
}
