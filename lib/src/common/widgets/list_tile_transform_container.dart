import 'package:animations/animations.dart';
import 'package:flutter/material.dart';

class ListTileTransformContainer extends StatelessWidget {
  const ListTileTransformContainer(
      {this.title, this.subtitle, this.trailing, required this.detailWidget});

  final Widget? title;
  final Widget? subtitle;
  final Widget? trailing;
  final Widget detailWidget;

  @override
  Widget build(BuildContext context) {
    return OpenContainer<bool>(
      openBuilder: (BuildContext _, VoidCallback openContainer) {
        return detailWidget;
      },
      tappable: false,
      closedColor: Colors.transparent,
      closedElevation: 0.0,
      closedBuilder: (BuildContext _, VoidCallback openContainer) {
        return ListTile(
          title: title,
          subtitle: subtitle,
          trailing: trailing,
          onTap: openContainer,
        );
      },
    );
  }
}
