// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:linnet_flutter_app/src/draft_order/bloc/bloc.dart';

// class BottomBarTitle extends StatelessWidget {

//   @override
//   Widget build(BuildContext context) {
//     final DraftOrderBloc bloc = BlocProvider.of<DraftOrderBloc>(context);
//     return BlocBuilder<DraftOrderBloc, DraftOrderState>(
//       builder: (BuildContext context, DraftOrderState state) {
//           return Padding(
//             padding: const EdgeInsets.only(left: 8.0),
//             child: Text(bloc.state.draftOrder.totalDisplay(), 
//                   style: TextStyle(color: Colors.white, 
//                         fontSize: 24.0, fontWeight: FontWeight.bold)
//             ),
//           );
//       }
//     );
//   }
// }