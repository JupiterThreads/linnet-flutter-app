import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:linnet_flutter_app/routing_constants.dart';
import 'package:linnet_flutter_app/src/authentication/authentication.dart';
import 'package:linnet_flutter_app/src/common/widgets/widgets.dart'
    show AccountListDialog, CircleAccountAvatar, SlideUpRoute;
import 'package:linnet_flutter_app/src/onboard/onboard.dart';
import 'package:linnet_flutter_app/src/trader_management/ui/account_management_screen.dart';
import 'package:linnet_flutter_app/src/user/user.dart';

class NavigationDrawer extends StatelessWidget {
  const NavigationDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthenticationBloc, AuthenticationState>(
        builder: (BuildContext context, AuthenticationState state) {
      return state.when(
        unauthenticated: () => UnauthenticatedNavDrawer(navigateTo: navigateTo),
        authenticated: () => AuthenticatedNavDrawer(navigateTo: navigateTo),
      );
    });
  }

  void navigateTo(BuildContext context, String routeName) {
    Navigator.popAndPushNamed(context, routeName);
  }
}

class AuthenticatedNavDrawer extends StatelessWidget {
  const AuthenticatedNavDrawer({Key? key, required this.navigateTo});

  final Function(BuildContext context, String routeName) navigateTo;

  GestureDetector _buildOtherAccount(BuildContext context, dynamic account) {
    return GestureDetector(
      onTap: () {
        _handleAccountChange(context, account);
      },
      child: Semantics(
        label: 'Switch to Account ${account.name}',
        child: CircleAccountAvatar(account: account),
      ),
    );
  }

  List<Widget> _getNextThreeAccountPictures(
      BuildContext context, List<dynamic> allAccounts) {
    final List<GestureDetector> otherAccounts = <GestureDetector>[];
    for (final dynamic account in allAccounts.take(3)) {
      otherAccounts.add(_buildOtherAccount(context, account));
    }
    return otherAccounts;
  }

  void _onLogout(BuildContext context) {
    BlocProvider.of<AuthenticationBloc>(context).add(const LoggedOut());
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserBloc, UserState>(
        builder: (BuildContext context, UserState state) {
      final List<dynamic> remainingAccounts = state.getRemaingAccounts();
      return Drawer(
          child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: Text(
              state.user!.name!,
              style: GoogleFonts.montserrat(
                  textStyle: Theme.of(context).textTheme.bodyText1,
                  color: Colors.white,
                  letterSpacing: .5),
            ),
            accountEmail: Text(
              state.displayAccountEmail(),
              style: GoogleFonts.montserrat(
                  textStyle: Theme.of(context).textTheme.bodyText2,
                  color: Colors.white,
                  letterSpacing: .5),
            ),
            onDetailsPressed: () async {
              if (remainingAccounts.isNotEmpty) {
                final dynamic result =
                    await _accountDialog(context, remainingAccounts);
                if (result != null) {
                  _handleAccountChange(context, result);
                }
              }
            },
            currentAccountPicture:
                CircleAccountAvatar(account: state.selectedAccount),
            otherAccountsPictures:
                _getNextThreeAccountPictures(context, remainingAccounts),
          ),
          if (!state.traderMode)
            ListTile(
                title: const Text('Create Trade Account'),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.of(context)
                      .push(SlideUpRoute(widget: OnboardScreen()));
                }),
          if (state.showAccountManagement())
            ListTile(
                title: const Text('Account Management'),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.of(context)
                      .push(SlideUpRoute(widget: AccountManagementScreen()));
                }),
          ListTile(
            leading: const Icon(Icons.settings),
            title: const Text('Settings'),
            onTap: () => navigateTo(context, SettingsRoute),
          ),
          ListTile(
            leading: const Icon(Icons.power_settings_new),
            title: const Text('Log out'),
            onTap: () => _onLogout(context),
          ),
        ],
      ));
    });
  }

  Future<dynamic> _accountDialog(
      BuildContext context, List<dynamic> accounts) async {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AccountListDialog(accountList: accounts);
        });
  }

  Future<void> _handleAccountChange(
      BuildContext context, dynamic account) async {
    final UserBloc userBloc = BlocProvider.of<UserBloc>(context);
    userBloc.add(AccountChanged(account: account));
    Navigator.popUntil(context, ModalRoute.withName(HomeRoute));
  }
}

class UnauthenticatedNavDrawer extends StatelessWidget {
  const UnauthenticatedNavDrawer({Key? key, required this.navigateTo})
      : super(key: key);
  final Function(BuildContext context, String routeName) navigateTo;

  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: ListView(
      padding: EdgeInsets.zero,
      children: <Widget>[
        const DrawerHeader(
          decoration: BoxDecoration(
            color: Colors.white,
          ),
          child: Text('Linnet header'),
        ),
        ListTile(
          leading: const Icon(Icons.settings),
          title: const Text('Settings'),
          onTap: () => navigateTo(context, SettingsRoute),
        ),
        ListTile(
          leading: const Icon(Icons.power_settings_new),
          title: const Text('Log in'),
          onTap: () => navigateTo(context, LoginRoute),
        ),
        ListTile(
          leading: const Icon(Icons.how_to_reg),
          title: const Text('Register'),
          onTap: () => navigateTo(context, RegisterRoute),
        ),
      ],
    ));
  }
}
