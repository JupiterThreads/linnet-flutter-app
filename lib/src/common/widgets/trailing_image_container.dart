import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:linnet_flutter_app/src/common/widgets/cached_image_placeholder.dart';

// NOt using full image for now

class TrailingImageContainer extends StatelessWidget {
  const TrailingImageContainer({
    Key? key,
    required this.image,
    required this.imageThumbnail,
    this.placeholderWidth = 25,
    this.placeholderHeight = 25,
  }) : super(key: key);

  final String image;
  final String imageThumbnail;
  final double width = 80;
  final double height = 80;
  final double placeholderWidth;
  final double placeholderHeight;

  Widget _buildImage(BoxFit boxFit) {
    return CachedNetworkImage(
        imageUrl: imageThumbnail,
        imageBuilder: (BuildContext context, ImageProvider imageProvider) {
          return FittedBox(
              key: Key(image), fit: boxFit, child: Image(image: imageProvider));
        },
        placeholder: (BuildContext context, String url) =>
            CachedImagePlaceholder(
                width: placeholderWidth, height: placeholderHeight),
        errorWidget: (BuildContext context, String url, dynamic error) =>
            const Icon(Icons.error));
  }

  Widget _buildFullImage(BuildContext context) {
    return FittedBox(
      fit: BoxFit.scaleDown,
      clipBehavior: Clip.hardEdge,
      child: Image.network(image),
    );
  }

// TOOD not using this atm, but don't want to get rid of it
  Future<dynamic> _showFullImage(BuildContext context) async {
    return showGeneralDialog(
      barrierColor: Colors.black54,
      pageBuilder: (BuildContext context, Animation<double> anim1,
              Animation<double> anim2) =>
          Container(),
      context: context,
      barrierLabel: '',
      barrierDismissible: true,
      transitionDuration: const Duration(milliseconds: 200),
      transitionBuilder: (BuildContext context, Animation<double> animation,
          Animation<double> secondaryAnimaion, Widget child) {
        return Transform.scale(
            scale: animation.value,
            child: Opacity(
                opacity: animation.value, child: _buildFullImage(context)));
      },
    );
  }

  // @override
  // Widget build(BuildContext context) {
  //   return InkWell(
  //     onTap: () async => _showFullImage(context),
  //     onDoubleTap: () async => _showFullImage(context),
  //     child: SizedBox(
  //         width: width, height: height, child: _buildImage(BoxFit.scaleDown)),
  //   );
  // }
  @override
  Widget build(BuildContext context) {
    return SizedBox(width: width, height: height, child: _buildImage(BoxFit.contain));
  }
}
