import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:linnet_flutter_app/routing_constants.dart';
import 'package:linnet_flutter_app/src/common/widgets/widgets.dart' show LocationPreview;
import 'package:linnet_flutter_app/src/place/place.dart';

class TraderLocationPicker extends StatefulWidget {
  const TraderLocationPicker(
      {Key? key, this.tradeLocation, required this.onLocationChanged})
      : super(key: key);

  final LatLng? tradeLocation;
  final Function onLocationChanged;

  @override
  _TraderLocationPickerState createState() => _TraderLocationPickerState();
}

class _TraderLocationPickerState extends State<TraderLocationPicker> {
  LatLng? _tradeLocation;
  final TextEditingController _controller = TextEditingController();

  Future<void> _openLocationMap() async {
    final dynamic result = await Navigator.pushNamed(
        context, LocationMapRoute,
        arguments: <String, dynamic>{
          'location': _tradeLocation,
        });

    if (result is LocationResult) {
      widget.onLocationChanged(result);
      setState(() {
        _tradeLocation = result.latLng;
        if (result.address !=null) {
          _controller.text = result.address!;
        }
      });
    }
  }

  void _clearLocation() {
    widget.onLocationChanged(null);
    setState(() {
      _tradeLocation = null;
      _controller.text = '';
    });
  }

  @override
  void initState() {
    super.initState();
    _tradeLocation = widget.tradeLocation;
  }

  @override
  Widget build(BuildContext context) {
    return _buildLocationPreview();
  }

  Widget _buildLocationPreview() {
    if (_tradeLocation == null) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          const Text('No location set'),
          ElevatedButton(
            onPressed: _openLocationMap,
            child: const Text('Add location'),
          )
        ],
      );
    }
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Flexible(
            child: SizedBox(
                width: MediaQuery.of(context).size.width,
                height: 200,
                child: LocationPreview(
                    markSelectedPosition: true,
                    selectedPosition: _tradeLocation,
                    onTap: _openLocationMap))),
        const SizedBox(height: 20),
        if (_controller.text.isNotEmpty)
          TextField(
            controller: _controller,
            minLines: 1,
            maxLines: 3,
            readOnly: true,
            decoration: const InputDecoration(
              labelText: 'Address',
            ),
          ),
        const SizedBox(height: 20),
        if (_tradeLocation != null)
          ElevatedButton(
              onPressed: _openLocationMap,
              child: const Text('Change location'))
      ],
    );
  }
}
