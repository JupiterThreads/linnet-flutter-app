import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class LoadingFlexibleSpaceBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FlexibleSpaceBar(
            background: SizedBox(
                child: Shimmer.fromColors(
                    baseColor: Colors.grey[200]!,
                    highlightColor: Colors.grey[100]!,
                    child: Container(color: Colors.white))),         
      
    );
  }
}