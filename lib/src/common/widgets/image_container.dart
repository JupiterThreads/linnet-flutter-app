import 'package:flutter/material.dart';

class ImageContainer extends StatelessWidget {
  const ImageContainer({
    Key? key,
    this.onTap,
    this.imageWidget,
    this.onDelete,
    this.fabTopPosition = -20,
    this.fabRightPosition = -20,
    this.customBorder,
    this.width = 150,
    this.height = 150,
    this.placeholderText,
    required this.shape,
  }) : super(key: key);

  final VoidCallback? onTap;
  final Widget? imageWidget;
  final VoidCallback? onDelete;
  final String? placeholderText;
  final double fabTopPosition;
  final double fabRightPosition;
  final ShapeBorder? customBorder;
  final double width;
  final double height;
  final BoxShape shape;

  Widget _buildPlaceholder() {
    return Column(mainAxisAlignment: MainAxisAlignment.center, children: [
      const Icon(Icons.add, size: 48, color: Colors.white),
    ]);
  }

  Widget _buildContainer() {
    return Stack(
      overflow: Overflow.visible,
      fit: StackFit.expand,
      children: <Widget>[
        imageWidget!,
        Positioned(
          top: fabTopPosition,
          right: fabRightPosition,
          child: FloatingActionButton(
              heroTag: 'delete_$placeholderText',
              tooltip: 'Delete',
              mini: true,
              onPressed: onDelete,
              child: const Icon(Icons.close)),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Material(
        child: Ink(
      width: width,
      height: height,
      decoration: BoxDecoration(
        shape: shape,
        color: Colors.grey[350],
        border: Border.all(
          color: Colors.white60,
          width: 2.0,
        ),
      ),
      child: InkWell(
        customBorder: customBorder,
        onTap: onTap,
        child: imageWidget != null ? _buildContainer() : _buildPlaceholder(),
      ),
    ));
  }
}
