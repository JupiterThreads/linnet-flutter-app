import 'package:flutter/material.dart';

class CircleAccountAvatar extends StatelessWidget {
  const CircleAccountAvatar({
    Key? key,
    this.account,
    this.imageProvider,
  }) : super(key: key);

  final dynamic account;
  final ImageProvider? imageProvider;

  Widget _buildAvatar() {
    if (account?.avatar != null && imageProvider == null) {
      return CircleAvatar(
          backgroundColor: Colors.transparent,
          backgroundImage: NetworkImage(account.avatar as String));
    } else if (imageProvider != null) {
      return CircleAvatar(
        backgroundColor: Colors.transparent,
        backgroundImage: imageProvider,
      );
    }
    if (account != null) {
      return CircleAvatar(
        backgroundColor: Colors.cyan[300],
        child: Text(account.name[0].toString().toUpperCase()),
      );
    }
    return const CircleAvatar(backgroundColor: Colors.transparent);
  }

  @override
  Widget build(BuildContext context) {
    return _buildAvatar();
  }
}
