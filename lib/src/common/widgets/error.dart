import 'package:flutter/material.dart';

class Error extends StatelessWidget {
    const Error({Key? key, required this.errorMessage, required this.onRetryPressed})
        : super(key: key);

    final String errorMessage;
    final Function onRetryPressed;

    @override
    Widget build(BuildContext context) {
      return Padding(
        padding: const EdgeInsets.all(16.0),
        child: Center(
            child: Column(children: <Widget>[
                Text(
                    errorMessage,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.red,
                        fontSize: 18, 
                  ),
                ),
                const SizedBox(height: 8),
                ElevatedButton(
                    onPressed: () => onRetryPressed(),
                    child: Text('Retry', style: TextStyle(color: Colors.white)),
              )
            ]),
        ),
      ) ;
    }
}
