import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' show MultipartFile;
import 'package:linnet_flutter_app/src/common/widgets/widgets.dart'
    show CircleAccountAvatar, ImageContainer;
import 'package:linnet_flutter_app/src/snackbar/snackbar.dart';
import 'package:linnet_flutter_app/src/utils/utils.dart';

class LinnetImagePicker extends StatefulWidget {
  const LinnetImagePicker(
      {Key? key,
      this.initialImage,
      this.placeholderText,
      required this.containerHeight,
      required this.containerWidth,
      this.enabled = true,
      required this.fieldName,
      this.onPickedImage,
      required this.shape,
      this.onDeleteImage})
      : super(key: key);

  final Image? initialImage;
  final String fieldName;
  final Function(MultipartFile, Image)? onPickedImage;
  final VoidCallback? onDeleteImage;
  final String? placeholderText;
  final BoxShape shape;
  final double containerWidth;
  final double containerHeight;
  final bool enabled;

  @override
  _LinnetImagePickerState createState() => _LinnetImagePickerState();
}

class _LinnetImagePickerState extends State<LinnetImagePicker> {
  Image? _displayImage;
  String get _fieldName => widget.fieldName;
  int maxUploadFileSizeMb = int.parse(dotenv.env['MAX_UPLOAD_FILE_SIZE_MB']!);
  late SnackbarCubit _snackbarCubit;

  @override
  void initState() {
    super.initState();
    _snackbarCubit = BlocProvider.of<SnackbarCubit>(context);

    if (widget.initialImage != null) {
      setState(() {
        _displayImage = widget.initialImage;
      });
    }
  }

  Future<void> _selectFile() async {
    FilePickerResult? result;
    try {
      result = await FilePicker.platform
          .pickFiles(type: FileType.image, withData: true);
    } on PlatformException catch (error) {
      debugPrint('Unsupported operation ${error.toString()}');
    } catch (error) {
      debugPrint(error.toString());
    }

    if (result != null && result.files.isNotEmpty) {
      final PlatformFile file = result.files.single;
      final Image image = Image.file(File(file.path!));
      final double sizeInMb = Utils.bytesToMBytes(file.size);
      if (sizeInMb > maxUploadFileSizeMb) {
        final String displaySize = sizeInMb.toStringAsFixed(1);
        _snackbarCubit.showError(
            '''This file size ($displaySize MB) is too large. File size limit is $maxUploadFileSizeMb MB.''',
            title: 'File upload failed');
        return;
      }

      final MultipartFile mFile =
          MultipartFile.fromBytes(_fieldName, file.bytes!, filename: file.name);

      if (widget.onPickedImage != null) {
        widget.onPickedImage!(mFile, image);
      }

      setState(() {
        _displayImage = image;
      });
    }
  }

  void _removeImage() {
    setState(() {
      _displayImage = null;
    });

    if (widget.onDeleteImage != null) {
      widget.onDeleteImage!();
    }
  }

  Widget _buildContainer() {
    if (widget.shape == BoxShape.circle) {
      return ImageContainer(
        shape: widget.shape,
        placeholderText: widget.placeholderText,
        onTap: !widget.enabled ? null : _selectFile,
        width: widget.containerWidth,
        height: widget.containerHeight,
        onDelete: !widget.enabled ? null : _removeImage,
        customBorder: const CircleBorder(),
        imageWidget: _displayImage != null
            ? CircleAccountAvatar(imageProvider: _displayImage?.image)
            : null,
      );
    }

    if (widget.shape == BoxShape.rectangle) {
      return ImageContainer(
        shape: widget.shape,
        placeholderText: widget.placeholderText,
        width: widget.containerWidth,
        height: widget.containerHeight,
        fabTopPosition: -25,
        fabRightPosition: -8,
        onTap: !widget.enabled ? null : _selectFile,
        onDelete: !widget.enabled ? null : _removeImage,
        imageWidget: _displayImage != null
            ? FittedBox(
                fit: BoxFit.fill,
                child: ConstrainedBox(
                    constraints: BoxConstraints(minWidth: 1, minHeight: 1),
                    child: _displayImage),
              )
            : null,
      );
    }
    return Container();
  }

  @override
  Widget build(BuildContext context) {
    return _buildContainer();
  }
}
