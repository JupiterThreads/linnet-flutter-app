class AppException implements Exception {
  const AppException([this._message, this._prefix]);

  final dynamic _message;
  final String? _prefix;

  @override
  String toString() {
    return '$_prefix$_message';
  }
}

class FetchDataException extends AppException {
  FetchDataException([String? message])
      : super(message, 'Error During Communication: ');
}

class BadRequestException extends AppException {
  BadRequestException([dynamic message]) : super(message, 'Invalid Request: ');
}

class UnauthorisedException extends AppException {
  UnauthorisedException([dynamic message]) : super(message, 'Unauthorised: ');
}

class InvalidInputException extends AppException {
  InvalidInputException([dynamic message]) : super(message, 'Invalid Input: ');
}

class TooManyRequestsException extends AppException {
  TooManyRequestsException([dynamic message])
      : super(message, 'Too many requests: ');
}

class NotFoundException extends AppException {
  NotFoundException([dynamic message])
      : super(message, 'Not found exception: ');
}

class ForbiddenException extends AppException {
  ForbiddenException([dynamic message]) : super(message, 'Forbidden');
}
