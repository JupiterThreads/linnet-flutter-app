class ApiSubmitting<T> {

  ApiSubmitting.submtting(this.message) : status = Status.SUBMITTING;
  ApiSubmitting.completed(this.data) : status = Status.COMPLETED;
  ApiSubmitting.error(this.message) : status = Status.ERROR;

	Status status;
	T? data;
	String? message;

  
@override
  String toString() {
    return 'Status : $status \n Message : $message \n Data : $data';
  }
}

enum Status {
	SUBMITTING,
	COMPLETED,
	ERROR
}
