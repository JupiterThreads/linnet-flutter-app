import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:linnet_flutter_app/src/common/widgets/map_buttons.dart';
import 'package:linnet_flutter_app/src/place/place.dart';
import 'package:linnet_flutter_app/src/place/ui/location_details.dart';

class MapPicker extends StatefulWidget {
  const MapPicker(
      {Key? key, this.initialPosition, required this.currentLocation})
      : super(key: key);
  final LatLng? initialPosition;
  final LatLng currentLocation;

  @override
  MapPickerState createState() => MapPickerState();
}

class MapPickerState extends State<MapPicker> {
  LatLng? _lastMapPosition;
  late LatLng _currentLocation;
  LatLng? _initialPosition;
  final Set<Marker> _markers = <Marker>{};
  MapType _currentMapType = MapType.normal;
  final Completer<GoogleMapController> mapController =
      Completer<GoogleMapController>();
  final double initialZoom = 16;

  @override
  void initState() {
    super.initState();
    final LatLng initPos = widget.initialPosition ?? widget.currentLocation;
    _setMarker(initPos);
    _setPositions();
  }

  void _setPositions() {
    setState(() {
      _currentLocation = widget.currentLocation;
      _initialPosition = widget.initialPosition;
    });
  }

  @override
  void didUpdateWidget(MapPicker oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.currentLocation != widget.currentLocation) {
      setState(() {
        _currentLocation = widget.currentLocation;
      });
    }

    if (oldWidget.initialPosition != widget.initialPosition) {
      setState(() {
        _initialPosition = widget.initialPosition;
      });
    }
  }

  void _toggleMapType() {
    setState(() {
      _currentMapType = _currentMapType == MapType.normal
          ? MapType.satellite
          : MapType.normal;
    });
  }

  void _onMapCreated(GoogleMapController controller) {
    mapController.complete(controller);
    _lastMapPosition = widget.initialPosition;
      LocationProvider.of(context, listen: false)
        .setLastIdleLocation(_lastMapPosition);
  }

  Future<void> _updateCamera(LatLng target) async {
    final GoogleMapController cont = await mapController.future;
    final CameraPosition newPosition = CameraPosition(
      target: target,
      zoom: initialZoom,
    );
    cont.animateCamera(CameraUpdate.newCameraPosition(newPosition));
  }

  void _setMarker(LatLng position) {
    setState(() {
      _markers.clear();
      _markers.add(Marker(
        markerId: MarkerId(position.toString()),
        position: position,
      ));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Builder(builder: (BuildContext context) {
        if (widget.initialPosition == null) {
          return const Center(child: CircularProgressIndicator());
        }
        return _buildMap();
      }),
    );
  }

  Widget _buildMap() {
    return Center(
      child: Stack(children: <Widget>[
        GoogleMap(
            markers: _markers,
            mapType: _currentMapType,
            initialCameraPosition: CameraPosition(
              target: _initialPosition ?? _currentLocation,
              zoom: initialZoom,
            ),
            compassEnabled: false,
            myLocationButtonEnabled: false,
            myLocationEnabled: true,
            onMapCreated: _onMapCreated,
            onCameraIdle: () => LocationProvider.of(context, listen: false)
                .setLastIdleLocation(_lastMapPosition),
            onCameraMove: (CameraPosition position) {
              _lastMapPosition = position.target;
              _setMarker(position.target);
            }),
        Align(
          alignment: Alignment.topRight,
          child: Container(
            margin: const EdgeInsets.only(top: 120, right: 16.0),
            child: MapButtons(
                moveToUserLocation: () => _updateCamera(_currentLocation),
                toggleMapType: _toggleMapType),
          ),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: LocationDetails(),
          ),
        )
      ]),
    );
  }
}
