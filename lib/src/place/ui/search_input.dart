import 'dart:async';

import 'package:flutter/material.dart';

class SearchInput extends StatefulWidget {
  const SearchInput(this.onSearchInput, 
  {this.hintText = 'Search place', 
    required this.onFocusChange});
  final ValueChanged<String> onSearchInput;
  final ValueChanged<bool> onFocusChange;
  final String hintText;

  @override
  _SearchInputState createState() => _SearchInputState();
}

class _SearchInputState extends State<SearchInput> {
  TextEditingController editController = TextEditingController();
  final FocusNode _focusNode = FocusNode();
  Timer? debouncer;
  bool hasSearchEntry = false;

  @override
  void initState() {
    super.initState();
    editController.addListener(onSearchInputChange);
    _focusNode.addListener(_onFocusChange);
  }

  @override
  void dispose() {
    editController.removeListener(onSearchInputChange);
    _focusNode.removeListener(_onFocusChange);
    _focusNode.dispose();
    editController.dispose();
    super.dispose();
  }

  void _onFocusChange() {
    widget.onFocusChange(_focusNode.hasFocus);
  }

  void onSearchInputChange() {
    if (editController.text.isEmpty) {
      debouncer?.cancel();
      widget.onSearchInput(editController.text);
      return;
    }

    if (debouncer?.isActive ?? false) {
      debouncer?.cancel();
    }

    debouncer = Timer(const Duration(milliseconds: 500),
        () => widget.onSearchInput(editController.text));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Theme.of(context).brightness == Brightness.dark
            ? Colors.black54
            : Colors.white,
      ),
      padding: const EdgeInsets.symmetric(horizontal: 8),
      child: Row(
        children: <Widget>[
          const Icon(Icons.search, color: Colors.black),
          const SizedBox(width: 8),
          Expanded(
            child: TextField(
                controller: editController,
                focusNode: _focusNode,
                decoration: InputDecoration(
                  hintText: widget.hintText,
                  border: InputBorder.none,
                ),
                onChanged: (String value) =>
                    setState(() => hasSearchEntry = value.isNotEmpty)),
          ),
          const SizedBox(width: 8),
          if (hasSearchEntry)
            GestureDetector(
              onTap: () {
                editController.clear();
                setState(() => hasSearchEntry = false);
              },
              child: const Icon(Icons.clear, color: Colors.black),
            )
        ],
      ),
    );
  }
}
