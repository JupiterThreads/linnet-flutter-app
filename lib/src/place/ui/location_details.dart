import 'dart:async';

import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:linnet_flutter_app/src/place/place.dart';
import 'package:linnet_flutter_app/src/services/geocode_api_service.dart';
import 'package:linnet_flutter_app/src/services/google_maps_services.dart';
import 'package:linnet_flutter_app/src/utils/utils.dart';
import 'package:provider/provider.dart';

class LocationDetails extends StatelessWidget {
  String? _address;

  Future<String?> getAddress(BuildContext context, LatLng? location) async {
    if (location == null) return null;

    final GoogleMapsServices googleMapsServices =
        Provider.of<GoogleMapsServices>(context, listen: false);
    final GeocodeApiService geocodeApiService =
        googleMapsServices.client.getService<GeocodeApiService>();

    String? address;
    try {
      final String locString = Utils.locationQueryStr(location);
      final Response<dynamic> response =
          await geocodeApiService.getAddress(locString);
      final dynamic result = response.body;
      address = result['results'][0]['formatted_address'] as String;
    } catch (error) {
      debugPrint(error.toString());
    }
    return address;
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
      child: Consumer<LocationProvider>(builder:
          (BuildContext context, LocationProvider locationProvider, _) {
        return Padding(
          padding: const EdgeInsets.all(16.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Flexible(
                flex: 20,
                child: FutureBuilder<String?>(
                    future:
                        getAddress(context, locationProvider.lastIdleLocation),
                    builder: (BuildContext context,
                        AsyncSnapshot<String?> snapshot) {
                      switch (snapshot.connectionState) {
                        case ConnectionState.none:
                        case ConnectionState.active:
                        case ConnectionState.waiting:
                          return LoadingAddress();
                        case ConnectionState.done:
                          if (snapshot.hasError) {
                            return const Center(
                                child: Text('Please check your connection'));
                          }
                          if (snapshot.hasData && snapshot.data != null) {
                            _address = snapshot.data;
                            return Text(snapshot.data!);
                          }
                      }
                      return LoadingAddress();
                    }),
              ),
              const Spacer(),
              if (_address != null)
                FloatingActionButton(
                  tooltip: 'Confirm location',
                  onPressed: () => Navigator.of(context).pop(LocationResult(
                    latLng: locationProvider.lastIdleLocation!,
                    address: _address!,
                  )),
                  child: const Icon(Icons.check),
                ),
            ],
          ),
        );
      }),
    );
  }
}

class LoadingAddress extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        const CircularProgressIndicator(),
      ],
    );
  }
}
