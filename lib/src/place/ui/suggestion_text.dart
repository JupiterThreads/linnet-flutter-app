import 'package:flutter/material.dart';
import 'package:linnet_flutter_app/src/place/place.dart';

class SuggestionText extends StatelessWidget {
  const SuggestionText(this.autoCompleteItem, this.onTap);

  final VoidCallback onTap;
  final AutoCompleteItem autoCompleteItem;

  @override
  Widget build(BuildContext context) {
    return Material(
      child: InkWell(
        onTap: onTap,
        child: Container(
            margin: const EdgeInsets.all(5),
            padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 16),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Text(autoCompleteItem.description ?? '') 
                )
              ],
            )),
      ),
    );
  }
}
