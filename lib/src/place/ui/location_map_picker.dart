import 'dart:async';

import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:linnet_flutter_app/src/place/place.dart';
import 'package:linnet_flutter_app/src/place/ui/map_picker.dart';
import 'package:linnet_flutter_app/src/place/ui/suggestion_text.dart';
import 'package:linnet_flutter_app/src/services/google_maps_services.dart';
import 'package:linnet_flutter_app/src/services/place_api_service.dart';
import 'package:linnet_flutter_app/src/user/user.dart';
import 'package:linnet_flutter_app/src/utils/utils.dart';
import 'package:linnet_flutter_app/src/utils/uuid.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

class LocationMapPicker extends StatefulWidget {
  const LocationMapPicker({this.selectedPosition});
  final LatLng? selectedPosition;

  @override
  _LocationMapPickerState createState() => _LocationMapPickerState();
}

class _LocationMapPickerState extends State<LocationMapPicker> {
  LatLng? get _selectedPosition => widget.selectedPosition;
  GlobalKey appBarKey = GlobalKey();
  GlobalKey<MapPickerState> mapKey = GlobalKey<MapPickerState>();

  OverlayEntry? overlayEntry;
  String sessionToken = Uuid().generateV4();
  bool hasSearchTerm = false;
  bool searchInputHasFocus = false;
  LocationResult? _locationResult;
  late PlaceApiService _placeApiService;

  @override
  void initState() {
    super.initState();
    _setLocationResult();
    _setPlaceApiService();
  }

  @override
  void dispose() {
    clearOverlay();
    super.dispose();
  }

  void clearOverlay() {
    overlayEntry?.remove();
    overlayEntry = null;
  }

  void _setPlaceApiService() {
    final GoogleMapsServices googleMapsServices =
        Provider.of<GoogleMapsServices>(context, listen: false);
    _placeApiService = googleMapsServices.client.getService<PlaceApiService>();
  }

  /// We set the location result early so that search suggestions
  /// are limited to our location
  void _setLocationResult() {
    final LatLng? loc =
        _selectedPosition ?? context.read<CurrentLocationProvider>().location;
    setState(() {
      if (loc != null) {
        _locationResult = LocationResult(latLng: loc);
      }
    });
  }

  Map<String, String> getSearchQueryParms(LatLng location) {
    return <String, String>{
      'location': '${location.latitude},${location.longitude}',
      'radius': '10000',
    };
  }

  Future<List<AutoCompleteItem>> getPlaceSuggestions(String place,
      {Map<String, String>? query}) async {
    List<AutoCompleteItem> items = <AutoCompleteItem>[];
    try {
      final Response<List<AutoCompleteItem>> response = await _placeApiService
          .getSuggestedPlaces(sessionToken, place, query: query ?? const {});
      items = response.body!;
    } catch (error) {
      debugPrint(error.toString());
    }
    return items;
  }

  void _searchPlace(String place) {
    if (context == null) return;
    clearOverlay();

    //TODO Has search term
    setState(() => hasSearchTerm = place.isNotEmpty);

    if (place.isEmpty || !searchInputHasFocus) return;

    final RenderBox? renderBox = context.findRenderObject() as RenderBox?;
    final Size? size = renderBox?.size;

    final RenderBox? appBarBox =
        appBarKey.currentContext?.findRenderObject() as RenderBox?;

    if (appBarBox != null && size != null) {
      overlayEntry = OverlayEntry(
        builder: (BuildContext context) => Positioned(
          top: appBarBox.size.height,
          width: size.width,
          child: Material(
            elevation: 1,
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 24),
              child: Row(
                children: <Widget>[
                  SizedBox(
                    height: 24,
                    width: 24,
                    child: const CircularProgressIndicator(strokeWidth: 3),
                  ),
                  const SizedBox(width: 24),
                  const Expanded(
                    child: Text(
                      'Finding place...',
                      style: TextStyle(fontSize: 16),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      );

      Overlay.of(context)!.insert(overlayEntry!);
    }
    autoCompleteSearch(place);
  }

  Future<void> autoCompleteSearch(String place) async {
    final String cleanedPlace = place.replaceAll(' ', '+');

    Map<String, String>? query;

    if (_locationResult != null) {
      query = getSearchQueryParms(_locationResult!.latLng);
    }

    final List<AutoCompleteItem> suggestions =
        await getPlaceSuggestions(cleanedPlace, query: query);

    if (suggestions.isEmpty) {
      const AutoCompleteItem autoCompleteItem =
          AutoCompleteItem(description: 'No results found');
      suggestions.add(autoCompleteItem);
    }
    displayAutoCompleteSuggestions(suggestions);
  }

  void displayAutoCompleteSuggestions(List<AutoCompleteItem> suggestions) {
    final RenderBox? renderBox = context.findRenderObject() as RenderBox;
    final Size? size = renderBox?.size;

    final RenderBox? appBarBox =
        appBarKey.currentContext?.findRenderObject() as RenderBox?;
    clearOverlay();

    if (!searchInputHasFocus) return;

    if (appBarBox != null && size != null) {
      overlayEntry = OverlayEntry(
        builder: (BuildContext context) => Positioned(
          width: size.width,
          top: appBarBox.size.height,
          child: Material(
            elevation: 1,
            child: Column(
              children: suggestions
                  .map((AutoCompleteItem suggestion) =>
                      SuggestionText(suggestion, () {
                        decodeAndSelectPlace(suggestion);
                      }))
                  .toList(),
            ),
          ),
        ),
      );

      Overlay.of(context)!.insert(overlayEntry!);
    }
  }

  void decodeAndSelectPlace(AutoCompleteItem? suggestion) {
    clearOverlay();
    if (suggestion?.placeId != null) {
      _placeApiService
          .getLocation(suggestion!.placeId!)
          .then((Response<dynamic> response) {
        final dynamic body = response.body;
        final Map<String, dynamic> location =
            body['result']['geometry']['location'] as Map<String, dynamic>;
        final LatLng loc =
            LatLng(location['lat'] as double, location['lng'] as double);
        moveToLocation(loc, suggestion);
      }).catchError((error) {
        debugPrint(error.toString());
      });
    }
  }

  void _handleSearchInputFocusChange(bool isFocused) {
    setState(() => searchInputHasFocus = isFocused);
  }

  void moveToLocation(LatLng target, AutoCompleteItem suggestion) {
    mapKey.currentState?.mapController.future
        .then((GoogleMapController controller) {
      controller.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(
          target: target,
          zoom: mapKey.currentState?.initialZoom ?? 0.0,
        ),
      ));
    });
    _locationResult = LocationResult(latLng: target);
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: <SingleChildWidget>[
          ChangeNotifierProvider<LocationProvider>(
              create: (_) => LocationProvider()),
        ],
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            extendBodyBehindAppBar: true,
            appBar: AppBar(
                leading: IconButton(
                    icon: Icon(Icons.arrow_back_ios, color: Colors.black),
                    onPressed: () => Navigator.pop(context)),
                key: appBarKey,
                elevation: 0,
                backgroundColor: Colors.transparent,
                title: SearchInput((String input) => _searchPlace(input),
                    onFocusChange: _handleSearchInputFocusChange)),
            body: MapPicker(
              key: mapKey,
              currentLocation:
                  context.watch<CurrentLocationProvider>().location!,
              initialPosition: widget.selectedPosition ??
                  context.watch<CurrentLocationProvider>().location,
            )));
  }
}
