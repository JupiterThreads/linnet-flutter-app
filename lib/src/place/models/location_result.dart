import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'location_result.freezed.dart';

@freezed
class LocationResult with _$LocationResult {
  const factory LocationResult({String? address, required LatLng latLng}) =
      _LocationResult;
}
