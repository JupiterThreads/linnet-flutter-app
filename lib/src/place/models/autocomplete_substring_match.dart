import 'package:freezed_annotation/freezed_annotation.dart';

part 'autocomplete_substring_match.g.dart';
part 'autocomplete_substring_match.freezed.dart';

@freezed
abstract class AutoCompleteSubstringMatch
    implements _$AutoCompleteSubstringMatch {
  const factory AutoCompleteSubstringMatch(
    int offset,
    int length,
  ) = _AutoCompleteSubstringMatch;

  const AutoCompleteSubstringMatch._();

  factory AutoCompleteSubstringMatch.fromJson(Map<String, dynamic> json) =>
      _$AutoCompleteSubstringMatchFromJson(json);

  static List<AutoCompleteSubstringMatch> subStringMatchesFromJson(
          List<dynamic> jsonList) =>
      jsonList
          .map((dynamic json) =>
              AutoCompleteSubstringMatch.fromJson(json as Map<String, dynamic>))
          .toList();
}
