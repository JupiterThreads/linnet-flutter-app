import 'package:linnet_flutter_app/src/place/place.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'autocomplete_item.g.dart';
part 'autocomplete_item.freezed.dart';

@freezed
abstract class AutoCompleteItem implements _$AutoCompleteItem {
  const factory AutoCompleteItem(
      {String? id,
      @JsonKey(name: 'place_id') String? placeId,
      @Default(null) String? description,
      @Default(null) 
      List<AutoCompleteSubstringMatch>? matchedSubstrings}) = _AutoCompleteItem;

  const AutoCompleteItem._();

  factory AutoCompleteItem.fromJson(Map<String, dynamic> json) =>
      _$AutoCompleteItemFromJson(json);

  static List<AutoCompleteItem> autoCompleteItemsFromJson(
          List<dynamic> jsonList) =>
      jsonList
          .map((dynamic json) =>
              AutoCompleteItem.fromJson(json as Map<String, dynamic>))
          .toList();
}
