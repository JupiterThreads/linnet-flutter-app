import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'nearby_place.freezed.dart';
part 'nearby_place.g.dart';

@freezed
abstract class NearbyPlace implements _$NearbyPlace {
  const factory NearbyPlace({
    required String name,
    @Default(null) String? icon,
    @JsonKey(ignore: true)
    @Default(null) LatLng? latLng,
  }) = _NearbyPlace;
  const NearbyPlace._();

  factory NearbyPlace.fromJson(Map<String, dynamic> json) =>
      _$NearbyPlaceFromJson(json);

  static List<NearbyPlace> nearbyPlacesFromJson(List<dynamic> jsonList) =>
      jsonList.map((dynamic json) {
        final NearbyPlace _nearbyPlace =
            NearbyPlace.fromJson(json as Map<String, dynamic>);
        final double lat = json['geometry']['location']['lat'] as double;
        final double long = json['geometry']['location']['lng'] as double;
        final NearbyPlace nearbyPlace =
            _nearbyPlace.copyWith(latLng: LatLng(lat, long));
        return nearbyPlace;
      }).toList();
}
