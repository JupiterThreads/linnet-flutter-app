import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:linnet_flutter_app/src/utils/utils.dart';

part 'place.g.dart';
part 'place.freezed.dart';

@freezed
abstract class Place implements _$Place {
  const factory Place(
    @JsonKey(name: 'place_id') String id,
    String name,
    @JsonKey(name: 'business_status') String? businessStatus,
    @JsonKey(name: 'formatted_address') String address,
    List<String> types,
    String icon,
  ) = _Place;
  const Place._();

  factory Place.fromJson(Map<String, dynamic> json) => _$PlaceFromJson(json);

  static List<Place> placeFromJson(List<dynamic> jsonList) => jsonList
      .map((dynamic json) => Place.fromJson(json as Map<String, dynamic>))
      .toList();

  List<String> getPlaceTypeDisplayList() {
    List<String> titleStrings = Utils.removeUnderscores(types);
    titleStrings = Utils.toTitleStrings(titleStrings);
    titleStrings.sort((String a, String b) => b.compareTo(a));
    return titleStrings;
  }

  String getPlaceTypesDisplay() {
    final List<String> titleStrings = getPlaceTypeDisplayList();
    return Utils.flattenStringList(titleStrings, includeSpaces: true);
  }

  String get businessStatusDisplay {
    return businessStatus?.replaceAll(RegExp('_'), ' ') ?? '';
  }
}
