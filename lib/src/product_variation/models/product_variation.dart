import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:linnet_flutter_app/src/utils/utils.dart';

part 'product_variation.freezed.dart';
part 'product_variation.g.dart';

@freezed
abstract class ProductVariation implements _$ProductVariation {
  const factory ProductVariation({
    @Default(null) int? id,
    required String name,
    @Default(null) String? description,
    @JsonKey(name: 'display_order') int? displayOrder,
    @JsonKey(name: 'unit_price', fromJson: Utils.defStringtoDouble)
        required double unitPrice,
    @JsonKey(name: 'unit_price_currency')
    @Default(null)
        String? unitPriceCurrency,
    @JsonKey(name: 'is_published') @Default(false) bool isPublished,
  }) = _ProductVariation;
  const ProductVariation._();

  factory ProductVariation.fromJson(Map<String, dynamic> json) =>
      _$ProductVariationFromJson(json);

  static List<ProductVariation> getUpdatedVariations(
      List<ProductVariation> variations, ProductVariation newVariation) {
    return List<ProductVariation>.from(variations)
        .map((ProductVariation variation) =>
            variation.id == newVariation.id ? newVariation : variation)
        .toList();
  }

  @override
  String toString() {
    return 'ProductVariation(id: $id, name: $name)';
  }
}
