import 'dart:math';
import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:linnet_flutter_app/constants.dart';
import 'package:linnet_flutter_app/src/product/bloc/bloc.dart';
import 'package:linnet_flutter_app/src/product_detail/bloc/bloc.dart';
import 'package:linnet_flutter_app/src/product_variation/bloc/bloc.dart';
import 'package:linnet_flutter_app/src/product_variation/bloc/variation_bloc.dart';
import 'package:linnet_flutter_app/src/product_variation/models/product_variation.dart';
import 'package:linnet_flutter_app/src/trader/models/trade_account.dart';
import 'package:linnet_flutter_app/src/user/blocs/user/user_bloc.dart';
import 'package:linnet_flutter_app/src/utils/utils.dart';


class VariationDetail extends StatefulWidget {
  const VariationDetail(
      {Key? key,
      required this.actionType,
      required this.isDraft,
      this.variation})
      : super(key: key);

  final ActionType actionType;
  final ProductVariation? variation;
  final bool isDraft;

  @override
  _VariationDetailState createState() => _VariationDetailState();
}

class _VariationDetailState extends State<VariationDetail> {
  ProductVariation? get _variation => widget.variation;
  ActionType get _actionType => widget.actionType;
  bool get _isDraft => widget.isDraft;
  final GlobalKey<FormState> _variationForm = GlobalKey<FormState>();
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _describtionController = TextEditingController();
  MoneyMaskedTextController _priceController =
      MoneyMaskedTextController(decimalSeparator: '.', thousandSeparator: ',');
  late TradeAccount _tradeAccount;
  int maxNameLength = 45;
  bool isPublished = false;
  late ProductBloc _productBloc;
  late VariationBloc _variationBloc;
  late ProductDetailBloc _productDetailBloc;

  @override
  void dispose() {
    _nameController.dispose();
    _priceController.dispose();
    _describtionController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _productBloc = BlocProvider.of<ProductBloc>(context);
    _variationBloc = BlocProvider.of<VariationBloc>(context);
    _tradeAccount = BlocProvider.of<UserBloc>(context).state.selectedAccount
        as TradeAccount;
    _productDetailBloc = BlocProvider.of<ProductDetailBloc>(context);

    if (_actionType == ActionType.EDIT) {
      _nameController.text = _variation!.name;
      if (_variation!.description != null) {
        _describtionController.text = _variation!.description!;
      }
      _priceController = MoneyMaskedTextController(
          initialValue: _variation!.unitPrice,
          decimalSeparator: '.',
          thousandSeparator: ',');
      setState(() {
        isPublished = _variation!.isPublished;
      });
    }
  }

  Widget _buildTitle() {
    return _actionType == ActionType.CREATE
        ? const Text('Add Variation')
        : const Text('Edit Variation');
  }

  void _performSave() {
    final bool isValid = _variationForm.currentState?.validate() ?? false;

    if (!isValid) return;

    if (_actionType == ActionType.CREATE) {
      _createVariation();
      resetProductDetailPrice();
    } else {
      _updateVariation();
    }
    Navigator.of(context).pop();
  }

  // TODO: bug product item pric doesn't get updated if they close the screen
  void resetProductDetailPrice() {
    final double productPrice =
        BlocProvider.of<ProductDetailBloc>(context, listen: false)
                .state
                .unitPrice ??
            0;
    if (productPrice > 0) {
      _productDetailBloc.add(const ProductPriceChanged(unitPrice: 0));
    }
  }

  void _createVariation() {
    ProductVariation variation = ProductVariation(
        name: _nameController.text.trim(),
        description: _describtionController.text.trim(),
        unitPrice: _priceController.numberValue,
        isPublished: isPublished);
    if (_isDraft) {
      final Random random = Random();
      variation = variation.copyWith(
        id: random.nextInt(1000),
      );
      _variationBloc.add(AddDraftVariation(productVariation: variation));
    } else {
      _productBloc.add(CreateVariation(productVariation: variation));
    }
  }

  void _updateVariation() {
    final ProductVariation variation = _variation!.copyWith(
        name: _nameController.text.trim(),
        description: _describtionController.text.trim(),
        unitPrice: _priceController.numberValue,
        isPublished: isPublished);

    if (_isDraft) {
      _variationBloc.add(ReplaceDraftVariation(productVariation: variation));
    } else {
      _productBloc.add(UpdateVariation(productVariation: variation));
    }
  }

  bool isNameValid() {
    return _nameController.text.trim().isNotEmpty;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: _buildTitle(), actions: [
        IconButton(
          tooltip: 'Save',
          icon: Icon(Icons.check, color: Theme.of(context).accentColor),
          onPressed: _performSave,
        ),
      ]),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
            key: _variationForm,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TextFormField(
                  keyboardType: TextInputType.text,
                  controller: _nameController,
                  maxLength: maxNameLength,
                  autocorrect: false,
                  validator: (_) => isNameValid() ? null : 'Invalid Name',
                  decoration: const InputDecoration(
                    labelText: 'Name',
                  ),
                ),
                const SizedBox(height: 16.0),
                TextFormField(
                  maxLines: 2,
                  autocorrect: false,
                  keyboardType: TextInputType.text,
                  controller: _describtionController,
                  decoration: const InputDecoration(
                    labelText: 'Description',
                  ),
                ),
                const SizedBox(height: 16.0),
                Align(
                  alignment: Alignment.centerRight,
                  child: SizedBox(
                    width: MediaQuery.of(context).size.width * 0.3,
                    child: TextFormField(
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      textAlign: TextAlign.end,
                      validator: (_) => _priceController.numberValue > 0
                          ? null
                          : 'Invalid Unit Price',
                      decoration: InputDecoration(
                          prefix:
                              Text(Utils.getCurrency(_tradeAccount.currency!))),
                      controller: _priceController,
                      keyboardType: const TextInputType.numberWithOptions(
                          signed: true, decimal: true),
                    ),
                  ),
                ),
                const SizedBox(height: 16.0),
                SwitchListTile(
                  contentPadding: EdgeInsets.zero,
                  title: const Text('Publish'),
                  value: isPublished,
                  onChanged: (bool val) => setState(() => isPublished = val),
                ),
              ],
            )),
      ),
    );
  }
}
