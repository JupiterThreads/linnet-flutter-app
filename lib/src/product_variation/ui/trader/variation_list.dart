import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:linnet_flutter_app/constants.dart';
import 'package:linnet_flutter_app/src/common/widgets/delete_item_dialog.dart';
import 'package:linnet_flutter_app/src/product/bloc/bloc.dart';
import 'package:linnet_flutter_app/src/product/bloc/product_bloc.dart';
import 'package:linnet_flutter_app/src/product/models/product.dart';
import 'package:linnet_flutter_app/src/product_detail/bloc/bloc.dart';
import 'package:linnet_flutter_app/src/product_variation/bloc/bloc.dart';
import 'package:linnet_flutter_app/src/product_variation/models/product_variation.dart';
import 'package:linnet_flutter_app/src/product_variation/ui/trader/variation_detail.dart';
import 'package:linnet_flutter_app/src/product_variation/ui/trader/variation_row.dart';
import 'package:linnet_flutter_app/src/providers/socket_provider.dart';
import 'package:linnet_flutter_app/src/user/blocs/user/user_bloc.dart';
import 'package:linnet_flutter_app/src/websockets/event_types/variation_event_types.dart';
import 'package:provider/provider.dart';

class VariationList extends StatefulWidget {
  const VariationList(
      {Key? key, required this.variationList, required this.isDraft})
      : super(key: key);
  final List<ProductVariation> variationList;
  final bool isDraft;

  @override
  _VariationListState createState() => _VariationListState();
}

class _VariationListState extends State<VariationList> {
  final SlidableController slidableController = SlidableController();
  bool get _isDraft => widget.isDraft;
  late ProductBloc _productBloc;
  late VariationBloc _variationBloc;
  late  ProductDetailBloc _productDetailBloc;
  List<ProductVariation> get _variationList => widget.variationList;
  SocketProvider? socketProvider;

  @override
  void dispose() {
    socketProvider?.removeListener(socketListener);
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _productBloc = BlocProvider.of<ProductBloc>(context);
    _variationBloc = BlocProvider.of<VariationBloc>(context);
    _productDetailBloc = BlocProvider.of<ProductDetailBloc>(context);

    if (!_isDraft) {
      socketProvider = Provider.of<SocketProvider>(context, listen: false);
      socketProvider?.addListener(socketListener);
    }
  }

  void socketListener() {
    final dynamic data = socketProvider!.data;
    final String type = data['type'] as String;
    final String productId = data['product_id'] as String;

    final Product? product = _productBloc.state.selectedProduct;

    if (product?.id != productId) return;

    switch (type) {
      case ADD_VARIATION:
        _productBloc.add(AddVariation(
            productVariation: ProductVariation.fromJson(
                data['payload'] as Map<String, dynamic>)));
        break;
      case UPDATE_VARIATION:
        _productBloc.add(ReplaceVariation(
            productVariation: ProductVariation.fromJson(
                data['payload'] as Map<String, dynamic>)));
        break;
      case REMOVE_VARIATION:
        _productBloc
            .add(RemoveVariation(variationId: data['payload'] as int));
        break;
      case UPDATE_VARIATION_DISPLAY_ORDER:
        _updateDispayOrderFromSocket(data['payload']);
        break;
      default:
        return;
    }
  }

  void _updateDispayOrderFromSocket(dynamic data) {
    final int newIndex = data['new_index'] as int;
    final int oldIndex = data['old_index'] as int;
    _onReorder(oldIndex, newIndex);
  }

  void _onReorder(int oldIndex, int newIndex, {bool saveAndBroadcast = false}) {
    if (newIndex == _variationList.length) {
      // ignore: parameter_assignments
      newIndex -= 1;
    }

    if (_isDraft) {
      final ProductVariation variation = _variationList.elementAt(oldIndex);
      _variationBloc.add(ReorderDraftVariations(
          variation: variation, newIndex: newIndex, oldIndex: oldIndex));
      return;
    }

    setState(() {
      final ProductVariation variation = _variationList.removeAt(oldIndex);
      _variationList.insert(newIndex, variation);
      if (saveAndBroadcast) {
        _productBloc.add(
            UpdateVariationOrder(variation: variation, newIndex: newIndex));
      }
    });
  }

  Future<void> _deleteVariation(ProductVariation variation) async {
    final dynamic result = await _showDialog(context, variation.name);
    if (result == true) {
      if (_isDraft) {
        _variationBloc.add(RemoveDraftVariation(variationId: variation.id!));
      } else {
        _productBloc.add(DeleteVariation(productVariation: variation));
      }
    }
  }

  void _editVariation(ProductVariation variation, BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute<Widget>(
        fullscreenDialog: true,
        builder: (BuildContext context) {
          return MultiBlocProvider(
            providers: [
              BlocProvider<ProductBloc>.value(
                value: _productBloc,
              ),
              BlocProvider<VariationBloc>.value(
                value: _variationBloc,
              ),
              BlocProvider<ProductDetailBloc>.value(value: _productDetailBloc),
            ],
            child: VariationDetail(
              isDraft: _isDraft,
              actionType: ActionType.EDIT,
              variation: variation,
            ),
          );
        }));
  }

  VariationRow _buildVariationRow(ProductVariation variation, bool enabled) {
    return VariationRow(
        key: Key(variation.id.toString()),
        enabled: enabled,
        variation: variation,
        editCallback: _editVariation,
        deleteVariation: _deleteVariation,
        slidableController: slidableController);
  }

  Widget _buildList(bool hasWritePerm) {
    if (hasWritePerm) {
      return ReorderableListView(
        onReorder: (int old, int newIndex) =>
            _onReorder(old, newIndex, saveAndBroadcast: true),
        children: <Widget>[
          for (final ProductVariation variation in _variationList)
            _buildVariationRow(variation, hasWritePerm)
        ],
      );
    }
    return ListView.builder(
      itemCount: _variationList.length,
      itemBuilder: (BuildContext context, int index) =>
          _buildVariationRow(_variationList[index], hasWritePerm),
    );
  }

  @override
  Widget build(BuildContext context) {
    final bool hasWritePerm = context
        .watch<UserBloc>()
        .state
        .user
        !.hasWritePermission(_productBloc.tradeAccount);
    if (_variationList.isEmpty) {
      return const Center(child: Text('No variations'));
    }
    return _buildList(hasWritePerm);
  }

  Future<dynamic> _showDialog(BuildContext context, String name) async {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return DeleteItemDialog(
          itemName: name,
          title: 'Delete Variation',
        );
      },
    );
  }
}
