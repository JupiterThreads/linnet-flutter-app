import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:linnet_flutter_app/src/product_variation/models/product_variation.dart';

class VariationRow extends StatelessWidget {
  const VariationRow({
    Key? key,
    required this.variation,
    required this.slidableController,
    required this.deleteVariation,
    required this.editCallback,
    this.enabled = false,
  }) : super(key: key);

  final ProductVariation variation;
  final SlidableController slidableController;
  final Function(ProductVariation) deleteVariation;
  final Function(ProductVariation, BuildContext) editCallback;
  final bool enabled;

  @override
  Widget build(BuildContext context) {
    return Slidable(
      key: Key(variation.id.toString()),
      controller: slidableController,
      actionPane: const SlidableScrollActionPane(),
      secondaryActions: <Widget>[
        if (enabled)
        IconSlideAction(
          caption: 'Edit',
          color: Theme.of(context).accentColor,
          icon: Icons.edit,
          onTap: () => editCallback(variation, context),
        ),
        if (enabled)
        IconSlideAction(
          caption: 'Delete',
          color: Colors.red,
          icon: Icons.delete,
          onTap: () => deleteVariation(variation),
        )
      ],
      child: ListTile(
        dense: true,
        key: Key(variation.id.toString()),
        title: Text(variation.name,
            style: const TextStyle(fontWeight: FontWeight.bold)),
        subtitle: Text(variation.description ?? ''),
        trailing: const Icon(Icons.drag_handle),
      ),
    );
  }
}
