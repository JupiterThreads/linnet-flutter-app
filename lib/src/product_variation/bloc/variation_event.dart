import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:linnet_flutter_app/src/product_variation/models/product_variation.dart';

part 'variation_event.freezed.dart';

@freezed
class VariationEvent with _$VariationEvent {
  const factory VariationEvent.addVariation({
    required ProductVariation productVariation,
  }) = AddDraftVariation;

  const factory VariationEvent.replaceVariation({
    required ProductVariation productVariation,
  }) = ReplaceDraftVariation;

  const factory VariationEvent.removeVariation({
    required int variationId,
  }) = RemoveDraftVariation;

  const factory VariationEvent.reorderVariations({
    required ProductVariation variation,
    required int newIndex,
    required int oldIndex,
  }) = ReorderDraftVariations;

  const factory VariationEvent.reset() = Reset;
}
