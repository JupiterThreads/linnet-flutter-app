import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:linnet_flutter_app/src/product_variation/models/product_variation.dart';

part 'variation_state.freezed.dart';

@freezed
class VariationState with _$VariationState {
  factory VariationState({
    @Default(<ProductVariation>[]) List<ProductVariation> draftVariationList,
  }) = _VariationState;
}
