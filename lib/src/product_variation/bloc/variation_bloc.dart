import 'package:bloc/bloc.dart';
import 'package:linnet_flutter_app/src/product_variation/bloc/bloc.dart';
import 'package:linnet_flutter_app/src/product_variation/models/product_variation.dart';

// draft variations to be used on product creation

class VariationBloc extends Bloc<VariationEvent, VariationState> {
  VariationBloc() : super(VariationState());

  @override
  Stream<VariationState> mapEventToState(VariationEvent event) async* {
    if (event is AddDraftVariation) {
      final ProductVariation variation = event.productVariation.copyWith(
        displayOrder: state.draftVariationList.length,
      );
      yield state.copyWith(
        draftVariationList:
            List<ProductVariation>.from(state.draftVariationList)
              ..add(variation),
      );
    }

    if (event is RemoveDraftVariation) {
      yield state.copyWith(
        draftVariationList: List<ProductVariation>.from(
            state.draftVariationList)
          ..removeWhere((ProductVariation _var) => 
            _var.id == event.variationId),
      );
    }

    if (event is ReplaceDraftVariation) {
      yield state.copyWith(
        draftVariationList: ProductVariation.getUpdatedVariations(
            state.draftVariationList, event.productVariation),
      );
    }

    if (event is ReorderDraftVariations) {
      final ProductVariation variation =
          state.draftVariationList.removeAt(event.oldIndex);
      state.draftVariationList.insert(event.newIndex, variation);

      yield state.copyWith(
        draftVariationList: 
          state.draftVariationList.asMap().entries.map((entry) {
            final int idx = entry.key;
            final ProductVariation _var = entry.value;
            return _var.copyWith(displayOrder: idx);
          }).toList(),
      );
    }
    if (event is Reset) {
      yield VariationState();
    }
  }

}
