import 'package:freezed_annotation/freezed_annotation.dart';

part 'search_state.freezed.dart';

@freezed
class SearchState with _$SearchState {
  const factory SearchState.initial() = SearchStateInitial;
  const factory SearchState.loading() = SearchStateLoading;
  const factory SearchState.populated({required List<dynamic> results}) =
      SearchStatePopulated;
  const factory SearchState.empty() = SearchStateEmpty;
  const factory SearchState.error() = SearchStateError;
}
