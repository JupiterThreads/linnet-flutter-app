import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:linnet_flutter_app/src/search/cubit/search_state.dart';

class SearchCubit extends Cubit<SearchState> {
  SearchCubit({required this.callback}) : super(const SearchStateInitial());

  final Function callback;

  Future<void> performSearch(String query) async {
    emit(const SearchStateLoading());

    try {
      final List<dynamic> results =
          await callback(query.trim()) as List<dynamic>;
      if (results.isNotEmpty) {
        emit(SearchStatePopulated(results: results));
      } else {
        emit(const SearchStateEmpty());
      }
    } catch (error) {
      emit(const SearchStateError());
    }
  }
}
