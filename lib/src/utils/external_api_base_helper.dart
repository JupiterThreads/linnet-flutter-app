
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:linnet_flutter_app/src/api/api_exception.dart';
import 'package:linnet_flutter_app/src/utils/utils.dart';

class ExternalApiBaseHelper {

  Future<Map<String, String>> _getHeaders() async {
      final Map<String, String> headers = 
        <String, String>{
        'Content-type': 'application/json', 
        'Accept': 'application/json'
        };
      return headers;
  }

	Future<dynamic> get(String url) async {
		dynamic responseJson; 
    final Map<String, String> requestHeaders = await _getHeaders();

		try {
			final http.Response response = await http.get(Uri.parse(url), headers: requestHeaders);
			responseJson = response.body;
		} on SocketException {
			throw FetchDataException('No internet Exception');
		} 
		return responseJson;
	}
}