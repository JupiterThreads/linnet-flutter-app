import 'dart:convert';
import 'dart:math';

import 'package:flutter/services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_settings_screens/flutter_settings_screens.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:linnet_flutter_app/src/utils/uuid.dart';

class Utils {
  static final FlutterSecureStorage storage = FlutterSecureStorage();

  static String getDateTime(String datetimeStr) {
    final DateTime dateTime = DateTime.parse(datetimeStr).toLocal();
    final DateFormat formatter = DateFormat('dd-MM-yyy h:mm a');
    final String formatted = formatter.format(dateTime);
    return formatted;
  }

  static String getFormattedAmount(String currencyName, String amount) {
    return NumberFormat.simpleCurrency(name: currencyName)
        .format(double.parse(amount));
  }

  static String getCurrency(String currencyCode) {
    return NumberFormat().simpleCurrencySymbol(currencyCode);
  }

  static String getFormattedAmountFromDouble(
      String currencyName, double amount) {
    final double roundedAmount = getRoundedAmount(amount);
    return NumberFormat.simpleCurrency(name: currencyName)
        .format(roundedAmount);
  }

  static double defStringtoDouble(String price) {
    return Utils.stringToDouble(price)!;
  }

  static double? stringToDouble(String? price) =>
      price != null ? double.parse(price) : null;

  static double getRoundedAmount(double val, {int places = 2}) {
    final double mod = pow(10.0, places) as double;
    return (val * mod).round().toDouble() / mod;
  }

  static RegExp emailRegExp = RegExp(
    r'^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$',
  );

  static RegExp passwordRegExp = RegExp(
    r'^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$',
  );

  static List<String> getIdList(List<dynamic> objList) {
    return objList.map((dynamic obj) => obj?.id as String).toList();
  }

  static String removeAllWhiteSpace(String target) {
    final String result = target.split(' ').fold('', (String acc, String val) {
      final String _val = val.trim();
      if (_val.isNotEmpty) {
        return '$acc $_val';
      }
      return acc;
    });
    return result.trim();
  }

  static Future<Map<String, dynamic>> getHeaders() async {
    String? token;
    final Map<String, dynamic> headers = <String, dynamic>{};
    const FlutterSecureStorage storage = FlutterSecureStorage();

    try {
      token = await storage.read(key: 'token');
    } on PlatformException catch (e) {
      print(e);
    } catch (ex) {
      print(ex);
    }

    if (token != null) {
      headers['Authorization'] = 'Token $token';
    }
    return headers;
  }

  static Future<bool?> isUserInTraderMode() async {
    return Settings.getValue<bool>('trader_mode', false);
  }

  static Future<String?> getUserId() async {
    const FlutterSecureStorage storage = FlutterSecureStorage();
    return storage.read(key: 'user_id');
  }

  static String flattenStringList(List<String> list,
      {bool includeSpaces = false}) {
    return list.reduce((String value, String acc) {
      if (includeSpaces) {
        return '$acc, $value';
      }
      return '$acc,$value';
    });
  }

  static List<String> toTitleStrings(List<String> list) {
    return list
        .map((String title) =>
            '''${title.substring(0, 1).toUpperCase()}${title.substring(1, title.length)}''')
        .toList();
  }

  static List<String> removeUnderscores(List<String> _list) {
    return _list
        .map((String name) => name.replaceAll(RegExp('_'), ' '))
        .toList();
  }

  static List<String> sortAlphabetically(List<String> list) {
    list.sort(
        (String a, String b) => a.toLowerCase().compareTo(b.toLowerCase()));
    return list;
  }

  static dynamic getLocationCoordinates(LatLng latLng) {
    return <String, dynamic>{
      'longitude': latLng.longitude,
      'latitude': latLng.latitude,
    };
  }

  static dynamic getNextUrl(String fullUrl, String startStr) {
    final RegExp regex = RegExp('$startStr.*');
    final RegExpMatch? match = regex.firstMatch(fullUrl);
    return match?.group(0);
  }

  static String locationQueryStr(LatLng? location) =>
      location != null ? '${location.latitude},${location.longitude}' : '';

  static Map<String, dynamic> getParams(String url) {
    return Uri.dataFromString(url).queryParameters;
  }

  static double kbToMbytes(int kBytes) {
    return kBytes / 1000;
  }

  static double bytesToMBytes(int bytes) {
    return bytes / 1000000;
  }

  static String getPlaceSearchFields({List<String>? fields}) {
    final List<String> defaultFields = <String>[
      'photos',
      'business_status',
      'icon',
      'formatted_address',
      'name',
      'photos',
      'place_id',
      'types'
    ];

    final List<String> searchFields = fields ?? defaultFields;
    return flattenStringList(searchFields);
  }

  static String generateId() {
    final Uuid uuid = Uuid();
    return uuid.generateV4();
  }

  static Future<void> persistTokens(Map<String, dynamic> response) async {
    if (response.containsKey('access_token')) {
      final String accessToken = response['access_token'] as String;
      await storage.write(key: 'access_token', value: accessToken);
    }

    if (response.containsKey('refresh_token')) {
      final String refreshToken = response['refresh_token'] as String;
      await storage.write(key: 'refresh_token', value: refreshToken);
    }

    if (response.containsKey('expires_in')) {
      final int expireSeconds = response['expires_in'] as int;
      final DateTime now = DateTime.now();
      final DateTime expireDate = now.add(Duration(seconds: expireSeconds));
      await storage.write(key: 'expire_date', value: expireDate.toString());
    }
  }

  static Future<void> deleteTokens() async {
    await storage.delete(key: 'access_token');
    await storage.delete(key: 'refresh_token');
    await storage.delete(key: 'expire_date');
  }
}
