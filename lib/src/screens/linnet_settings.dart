import 'package:flutter/material.dart';
import 'package:flutter_settings_screens/flutter_settings_screens.dart';
import 'package:linnet_flutter_app/src/user/blocs/user/user_bloc.dart';
import 'package:linnet_flutter_app/src/user/models/user.dart';
import 'package:linnet_flutter_app/src/user/ui/user_profile_screen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LinnetSettings extends StatefulWidget {
  @override
  _LinnetSettingsState createState() => _LinnetSettingsState();
}

class _LinnetSettingsState extends State<LinnetSettings> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: SettingsScreen(children: [
        SettingsGroup(
          title: 'General',
          children: <Widget>[
            if (context.watch<UserBloc>().state.user != null) 
            SimpleSettingsTile(
              title: 'Profile',
              subtitle: '',
              child: UserProfileScreen(),
            )
          ],
        ),
        SimpleSettingsTile(
            title: 'Distance Settings',
            subtitle: '',
            child: SettingsScreen(
              title: 'Distance Settings',
              children: <Widget>[
              RadioSettingsTile<String>(
                  title: 'Distance Display',
                  settingKey: 'dm_units',
                  values: <String, String>{
                      'imperial': 'Imperial (miles)',
                      'metric': 'Meric (Kilometers)', 
                  },
                  selected: 'imperial',
              ),
              RadioSettingsTile(title: 'Travel Mode', 
                settingKey: 'dm_travel_mode', 
                selected: 'driving', 
                values: <String, String>{
                  'driving': 'Driving',
                  'walking': 'Walking',
                  'bicycling': 'Bicyling',
                  'transit': 'Transit',
                },
              ),
            
            ]
            )
            ),
          SwitchSettingsTile(title: 'Enable touch login', 
            settingKey: 'enable_touch'),
      ]),
    );
  }
}