import 'dart:async';

import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:linnet_flutter_app/src/providers/socket_provider.dart';
import 'package:linnet_flutter_app/src/screens/buyer_home_screen.dart';
import 'package:linnet_flutter_app/src/screens/trader_home_screen.dart';
import 'package:linnet_flutter_app/src/services/trader_api_service.dart';
import 'package:linnet_flutter_app/src/trader/trader.dart';
import 'package:linnet_flutter_app/src/user/providers/current_location_provider.dart';
import 'package:linnet_flutter_app/src/user/user.dart';
import 'package:linnet_flutter_app/src/utils/utils.dart';
import 'package:linnet_flutter_app/src/websockets/websocket_listeners.dart';
import 'package:linnet_flutter_app/src/websockets/websocket_manager.dart';
import 'package:location/location.dart';
import 'package:location_permissions/location_permissions.dart' as perms;
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  WebSocketManager socket = WebSocketManager();
  final Location location = Location();
  StreamSubscription<LocationData>? _locationSubscription;

  @override
  void initState() {
    super.initState();
    // WidgetsBinding.instance.addObserver(this);
    _setUpWebSocket();
    _setUpLocation();
    _listenLocation();
    //_getPermissions();
  }

  @override
  void dispose() {
    // WidgetsBinding.instance.removeObserver(this);
    _locationSubscription?.cancel();
    super.dispose();
  }

  // @override
  // void didChangeAppLifecycleState(AppLifecycleState state) {
  //   // I'm sure I'm going to need this, but not sure when
  //   print('state of app... $state');
  //   setState(() => appState = state);
  // }


  Future<void> _setUpLocation() async {
    perms.PermissionStatus permission =
        await perms.LocationPermissions().checkPermissionStatus();

    if (permission == perms.PermissionStatus.unknown) {
      permission = await perms.LocationPermissions().requestPermissions();
    }

    if (permission == perms.PermissionStatus.denied ||
        permission == perms.PermissionStatus.unknown) {
      return;
    }

    bool serviceEnabled = false;
    serviceEnabled = await location.serviceEnabled();
    if (!serviceEnabled) {
      serviceEnabled = await location.requestService();
      if (!serviceEnabled) {
        return;
      }
    }

    location.changeSettings(
      interval: 5000,
      distanceFilter: 100,
    );

    try {
      final LocationData locationData = await location
          .getLocation()
          .timeout(const Duration(seconds: 5));
      final LatLng latLng =
          LatLng(locationData.latitude!, locationData.longitude!);
      CurrentLocationProvider.of(context, listen: false).setLocation(latLng);
    } catch (error, stacktrace) {
      print('erroring in getting location data');
      print(error);
      print(stacktrace);
      debugPrint(error.toString());
    }
  }

  Future<void> _listenLocation() async {
    _locationSubscription =
        location.onLocationChanged.listen((LocationData locationData) async {
      final LatLng latLng =
          LatLng(locationData.latitude!, locationData.longitude!);
      CurrentLocationProvider.of(context, listen: false).setLocation(latLng);
    }, onError: (dynamic error) {
      _locationSubscription?.cancel();
      debugPrint(error.toString());
    }, cancelOnError: true);
  }

  void _setUpWebSocket() {
    socket.initSocket();
    final WebSocketListeners socketListeners = WebSocketListeners(
      socketProvider: Provider.of<SocketProvider>(context, listen: false),
      userBloc: BlocProvider.of<UserBloc>(context),
    );

    socket.addListener((dynamic response) =>
        socketListeners.membershipAccountListener(response));

    socket.addListener(
        (dynamic response) => socketListeners.broadcastListener(response));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<UserBloc, UserState>(
        buildWhen: (UserState prevState, UserState state) {
          return state.selectedAccount?.id != prevState.selectedAccount?.id;
        },
        builder: (BuildContext context, UserState state) {
          if (state.traderMode) {
            return TraderHomeScreen(
                tradeAccount: state.selectedAccount as TradeAccount);
          }
          return BuyerHomeScreen();
        },
      ),
    );
  }
}
