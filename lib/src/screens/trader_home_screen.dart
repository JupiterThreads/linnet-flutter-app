import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:linnet_flutter_app/src/category/category.dart';
import 'package:linnet_flutter_app/src/common/widgets/navigation_drawer.dart';
import 'package:linnet_flutter_app/src/trader/models/trade_account.dart';

class TraderHomeScreen extends StatefulWidget {
  const TraderHomeScreen({required this.tradeAccount});
  final TradeAccount tradeAccount;

  @override
  _TraderHomeScreenState createState() => _TraderHomeScreenState();
}

class _TraderHomeScreenState extends State<TraderHomeScreen> {
  final PageStorageBucket bucket = PageStorageBucket();
  TradeAccount get _tradeAccount => widget.tradeAccount;
  int _selectedIndex = 0;

  List<Widget> getPages() {
    return <Widget>[
      Container(key: 
      const PageStorageKey<String>('orders'), 
        child: const Center(child: Text('order list screen - under construction'))),
      CategoryHomeScreen(
        tradeAccount: _tradeAccount,
        key: const PageStorageKey<String>('categories'),
      ),
    ];
  }

  Widget _bottomNavigationBar(int selectedIndex) => BottomNavigationBar(
        onTap: (int index) => setState(() => _selectedIndex = index),
        currentIndex: selectedIndex,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(Icons.store), label: 'Orders'),
          BottomNavigationBarItem(
              icon: Icon(Icons.list), label: 'Categories'),
        ],
      );

  @override
  Widget build(BuildContext context) {
    final List<Widget> pages = getPages();
    return Scaffold(
      drawer: const Drawer(
        child: NavigationDrawer(),
      ),
      bottomNavigationBar: _bottomNavigationBar(_selectedIndex),
      body: PageStorage(
        bucket: bucket,
        child: pages[_selectedIndex],
      ),
    );
  }
}
