import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:linnet_flutter_app/src/authentication/bloc/bloc.dart';
import 'package:linnet_flutter_app/src/common/widgets/navigation_drawer.dart';
import 'package:linnet_flutter_app/src/trader/trader.dart';
import 'package:linnet_flutter_app/src/user/user.dart';

class BuyerHomeScreen extends StatefulWidget {
  @override
  _BuyerHomeScreenState createState() => _BuyerHomeScreenState();
}

class _BuyerHomeScreenState extends State<BuyerHomeScreen> {
  final PageStorageBucket bucket = PageStorageBucket();
  int _selectedIndex = 0;

  List<Widget> pages = <Widget>[
    const TraderListScreen(
      key: PageStorageKey<String>('traders'),
    ),
    Container(
      key: const PageStorageKey<String>('orders'), // these should not be showing if unauthenticated
    ),
  ];

  Widget _bottomNavigationBar(int selectedIndex) => BottomNavigationBar(
        onTap: (int index) => setState(() => _selectedIndex = index),
        currentIndex: selectedIndex,
        items: <BottomNavigationBarItem>[
          const BottomNavigationBarItem(
              icon: Icon(Icons.store), label: 'Local'),
          const BottomNavigationBarItem(
              icon: Icon(Icons.list), label:'Orders'),
        ],
      );

  @override
  Widget build(BuildContext context) {
    final bool isAuth = 
      context.watch<AuthenticationBloc>().state is Authenticated;
    return Scaffold(
      body: isAuth ? PageStorage(
        bucket: bucket,
        child: pages[_selectedIndex],
      ) : const TraderListScreen(),
      drawer: const Drawer(
        child: NavigationDrawer(),
      ),
      bottomNavigationBar: isAuth ? _bottomNavigationBar(_selectedIndex) : null,
    );
  }
}
