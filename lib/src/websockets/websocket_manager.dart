import 'package:flutter/foundation.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:linnet_flutter_app/src/utils/utils.dart';
import 'package:web_socket_channel/io.dart';

WebSocketManager socket = WebSocketManager();

class WebSocketManager {
  factory WebSocketManager() {
    return _socket;
  }

  WebSocketManager._internal();

  static final WebSocketManager _socket = WebSocketManager._internal();

  IOWebSocketChannel? _channel;
  bool _isOn = false;
  ObserverList<Function> _listeners = ObserverList<Function>();

  Future<void> initSocket() async {
    reset();
    final Map<String, dynamic> headers = await Utils.getHeaders();

    print('Connecting to websocket');
    _channel = IOWebSocketChannel.connect(dotenv.env['WS_BASE_URL']!,
        headers: headers);
    _channel!
      .stream.listen(_onDataReceived, onError: _onError, onDone: _onDone);
  }

  void _onDone() {
    _isOn = false;
  }

  void _onError(dynamic error) {
    print('Websocket error...${error.message}');
    print('Trying to reconnect in 3 seconds...');
    Future<void>.delayed(
        const Duration(milliseconds: 3000), () => initSocket());
  }

  void reset() {
    if (_channel != null) {
        _channel!.sink.close();
        _isOn = false;
        _listeners = ObserverList<Function>();
    }
  }

  void send(String message) {
    if (_channel != null && _isOn) {
        _channel!.sink.add(message);
    }
  }

  void addListener(Function callback) {
    _listeners.add(callback);
  }

  void removeListener(Function callback) {
    _listeners.remove(callback);
  }

  void _onDataReceived(dynamic message) {
    _isOn = true;

    for (final Function callback in _listeners) {
      callback(message);
    }
  }
}
