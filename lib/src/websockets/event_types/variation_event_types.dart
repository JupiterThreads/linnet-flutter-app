const String ADD_VARIATION = 'ADD_VARIATION';
const String UPDATE_VARIATION = 'UPDATE_VARIATION';
const String REMOVE_VARIATION = 'REMOVE_VARIATION';
const String UPDATE_VARIATION_DISPLAY_ORDER = 'UPDATE_VARIATION_DISPLAY_ORDER';