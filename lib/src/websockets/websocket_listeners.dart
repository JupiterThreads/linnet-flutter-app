import 'dart:convert';
import 'package:linnet_flutter_app/src/member/models/member.dart';
import 'package:linnet_flutter_app/src/providers/socket_provider.dart';
import 'package:linnet_flutter_app/src/user/user.dart';
import 'package:linnet_flutter_app/src/websockets/event_types/membership_event_types.dart';


class WebSocketListeners {
  WebSocketListeners({
    required this.userBloc,
    required this.socketProvider,
  });

  final UserBloc userBloc;
  final SocketProvider socketProvider;

  void membershipAccountListener(dynamic response) {
    final dynamic data = json.decode(response as String);
    final String type = data['type'] as String;
    final String userId = data['user_id'] as String;
    final bool isBroadcast = data['is_broadcast'] as bool;

    if (!(userId == userBloc.state.user!.id && !isBroadcast)) return;

    switch (type) {
      case ADD_MEMBERSHIP_ACCOUNT:
        userBloc.add(AddMembershipAccount(
            member: Member.fromJson(data['payload'] as Map<String, dynamic>)));
        break;
      case UPDATE_MEMBERSHIP_ACCOUNT:
        userBloc.add(UpdateMembershipAccount(
            member: Member.fromJson(data['payload'] as Map<String, dynamic>)));
        break;
      case REMOVE_MEMBERSHIP_ACCOUNT:
        userBloc
            .add(RemoveMembershipAccount(memberId: data['payload'] as String));
        break;
      default:
        return;
    }
  }

  void broadcastListener(dynamic response) {
    final dynamic data = json.decode(response as String);
    final String traderId = data['trader_id'] as String;
    final String actionUserId = data['action_user_id'] as String;

    if (actionUserId == userBloc.state.user!.id) return;

    if (userBloc.state.selectedAccount.id == traderId) {
      socketProvider.setData(data);
    }
  }
}
