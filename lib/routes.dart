import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:linnet_flutter_app/routing_constants.dart';
import 'package:linnet_flutter_app/src/category/category.dart';
import 'package:linnet_flutter_app/src/login/ui/ui.dart';
import 'package:linnet_flutter_app/src/member/ui/members_screen.dart';
import 'package:linnet_flutter_app/src/onboard/onboard.dart';
import 'package:linnet_flutter_app/src/place/place.dart';
import 'package:linnet_flutter_app/src/product/ui/product_home_screen.dart';
import 'package:linnet_flutter_app/src/register/ui/register_screen.dart';
import 'package:linnet_flutter_app/src/screens/home_screen.dart';
import 'package:linnet_flutter_app/src/screens/linnet_settings.dart';
import 'package:linnet_flutter_app/src/trader_management/trader_management.dart';

import 'package:linnet_flutter_app/src/trader/trader.dart';
import 'package:linnet_flutter_app/src/trader_management/ui/trader_profile.dart';
import 'package:linnet_flutter_app/src/user/ui/user_profile_screen.dart';

class LinnetRouter {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case HomeRoute:
        return MaterialPageRoute<dynamic>(
            builder: (BuildContext context) => const HomeScreen(),
            settings: const RouteSettings(name: '/'));
      case LoginRoute:
        return MaterialPageRoute<dynamic>(
          builder: (BuildContext context) => const LoginScreen(),
        );
      case RegisterRoute:
        return MaterialPageRoute<dynamic>(
            builder: (BuildContext context) => const RegisterScreen());
      case SettingsRoute:
        return MaterialPageRoute<dynamic>(
            builder: (BuildContext context) => LinnetSettings());
      case CreateTraderRoute:
        return MaterialPageRoute<dynamic>(
            builder: (BuildContext context) => OnboardScreen());
      case CategoryListRoute:
        final TradeAccount account = settings.arguments as TradeAccount;
        return MaterialPageRoute<dynamic>(
            builder: (BuildContext context) =>
                CategoryHomeScreen(tradeAccount: account));

      case TraderProfileRoute:
        final TradeAccount account = settings.arguments as TradeAccount;
        return MaterialPageRoute<dynamic>(
            builder: (BuildContext context) =>
                TraderProfile(tradeAccount: account));
      case ManageMembersRoute:
        return MaterialPageRoute<dynamic>(
            builder: (BuildContext context) => MembersScreen());
      // case BusinessTypesRoute:
      //   return MaterialPageRoute<dynamic>(
      //       builder: (BuildContext context) => BusinessTypesScreen());
      case TraderLocationRoute:
        return MaterialPageRoute<dynamic>(
            builder: (BuildContext context) => const EditLocation());
      case OrderNotesRoute:
        return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
          return OrderNotesScreen();
        });

      case ProductListRoute:
        final dynamic args = settings.arguments;
        final TradeAccount account = args['account'] as TradeAccount;
        final Category category = args['category'] as Category;
        return MaterialPageRoute<dynamic>(
            builder: (BuildContext context) =>
                ProductHomeScreen(tradeAccount: account, category: category));
      case UserProfileRoute:
        return MaterialPageRoute<dynamic>(
            builder: (BuildContext context) => const UserProfileScreen());
      case LocationMapRoute:
        final dynamic args = settings.arguments;
        final LatLng position = args['location'] as LatLng;
        return MaterialPageRoute<dynamic>(
            builder: (BuildContext context) =>
                LocationMapPicker(selectedPosition: position));
      default:
        return MaterialPageRoute<dynamic>(
            builder: (_) => Scaffold(
                body: Center(
                    child: Text('No route defined for ${settings.name}'))));
    }
  }
}
