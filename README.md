# linnet_flutter_app

Linnet Flutter app

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

### Model files
Model files all have built files that accompany them that get auto-generated - g.dart and freezed.dart files. As these files aren't pushed to the repo, they will need to be generated with the following command:

```flutter packages pub run build_runner build ```

If you are working on a model, you can run the following command that will automatically rebuild the built files with the new model schema when the file has changed.

```flutter packages pub run build_runner watch ```

You can also remove any conflicts from the build graph with the following command:

```flutter packages pub run build_runner build --delete-conflicting-outputs ```


### Clears network cache
PaintingBinding.instance.imageCache.clear();

### Env file

You will need to create a .env file in the project route. Copy the .env.example file and remove the .example.
